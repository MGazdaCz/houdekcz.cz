<?php

namespace App\AdminModule\CmsModule\Forms;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CoreModule\Model\Forms\ImageUpload;
use App\AdminModule\CoreModule\Model\Forms\ManyToMany;
use Nette;


class ArticleFormFactory extends Nette\Object
{
    protected $_articleRepo;
    protected $_userRepo;
    protected $_photogalleryRepo;
    protected $_photogalleryItemRepo;
    protected $_articleLabelRepo;
    protected $_articleColorRepo;
    protected $_podcastRepo;
    protected $_bannerRepo;

    protected $_articleRelatedRepo;
    protected $_articleRelated2Repo;
    protected $_articleLabelsRepo;

    protected $_mnArticleRelated;
    protected $_mnArticleRelated2;
    protected $_mnArticleLabel;

    /** @var Nette\DI\Container $context */
	private $_context;

    /**
     * @var Nette\Application\UI\Presenter
     */
    private $_presenter;

	public function __construct(Nette\DI\Container $context)
	{
		$this->_context = $context;

        // konfigurace repositaru
        $this->_articleRepo          = $this->_context->getService('cmsArticle');
        $this->_userRepo             = $this->_context->getService('coreUser');
        $this->_photogalleryRepo     = $this->_context->getService('cmsPhotogallery');
        $this->_photogalleryItemRepo = $this->_context->getService('cmsPhotogalleryItem');
        $this->_articleLabelRepo     = $this->_context->getService('cmsArticleLabel');
        $this->_articleColorRepo     = $this->_context->getService('cmsArticleColor');
        $this->_podcastRepo          = $this->_context->getService('cmsPodcast');
        $this->_bannerRepo           = $this->_context->getService('cmsBanner');

        $this->_articleRelatedRepo  = $this->_context->getService('cmsArticleRelated');
        $this->_articleRelated2Repo = $this->_context->getService('cmsArticleRelated2');
        $this->_articleLabelsRepo   = $this->_context->getService('cmsArticleLabels');

        // sestaveni MkuN vazby na clanky - souvisejici
        $this->_mnArticleRelated = new ManyToMany($this->_articleRelatedRepo, $this->_articleRepo);
        $this->_mnArticleRelated->setMColumn('cms_article_id');
        $this->_mnArticleRelated->setNColumn('related_id');

        // sestaveni MkuN vazby na clanky - souvisejici 2 (pro souvisejici v obsahu clanku)
        $this->_mnArticleRelated2 = new ManyToMany($this->_articleRelated2Repo, $this->_articleRepo);
        $this->_mnArticleRelated2->setMColumn('cms_article_id');
        $this->_mnArticleRelated2->setNColumn('related_id');

        // sestaveni MkuN vazby na stitky
        $this->_mnArticleLabel = new ManyToMany($this->_articleLabelsRepo, $this->_articleRepo, $this->_articleLabelRepo);
        $this->_mnArticleLabel->setMColumn('cms_article_id');
        $this->_mnArticleLabel->setNColumn('cms_article_label_id');
	}

    public function setPresenter(Nette\Application\UI\Presenter &$presenter){
        $this->_presenter = &$presenter;
    }

    /**
     * @return \App\AdminModule\CmsModule\Model\Form\PageBase
     */
    public function create()
    {
        // zalozeni formulare
        $form = new PageBase();
        //$form->setLanguages($languages);
        //$form->templateEnable();
        $form->initFormItems();
        // ---

        // customizace base formulare
        unset($form['name']);
        unset($form['heading']);
        unset($form['showHeading']);
        $form['view']->caption = 'publikovat';

        $form
            ->addTextArea('name', 'Název', 80, 3)
            ->setRequired('Vyplňte název článku.');


        // rozsireni formulare
        $form->addCheckbox('showOnHomepage', ' zobrazit na homepage');
        $form->addCheckbox('mainArticle', ' hlavní článek na homepage');
        $form->addUpload('mainImage', 'Hlavní obrázek');

        $form
            ->addText('datePublication', 'Datum publikace')
            ->setAttribute('class', 'date-picker')
            ->setRequired('Vyberte datum publikace.');
        $form
            ->addText('timePublication', 'Datum publikace')
            ->setAttribute('placeholder', 'HH:MM')
            ->addRule(Nette\Application\UI\Form::PATTERN, 'Zadejte čas ve správném formátu HH:MM (např. 08:20).', '([0-9]){2}:([0-9]){2}');

        $form->addSelect('cms_photogallery_id', 'Fotogalerie', $this->getGaleries());
        $form->addUpload('galleryImages', 'Obrázky pro novou galerii', true);

        $form->addCheckbox('showLastPodcast', ' zobrazit poslední podcast');
        $form->addSelect('cms_podcast_id', 'Související podcast', $this->getPodcasts());

        $form->addCheckbox('showNewsletter', ' zobrazit newsletter');

        $form->addSelect('author_id', 'Autor', $this->getAuthors());

        $form->addCheckbox('showBannerBig', ' zobrazit velký banner');
        $form->addSelect('bannerBig_id', 'Banner', $this->getBanners());

        $form->addCheckbox('showBannerSmall', ' zobrazit malý banner');
        $form->addSelect('bannerSmall_id', 'Banner', $this->getBanners());

        $form->addSelect('bannerContent_id', 'Banner v obsahu', $this->getBanners());

        $form->addMultiSelect('related', 'Související články', $this->getArticles(), 20);
        $form->addMultiSelect('related2', 'Mohlo by se vám líbit', $this->getArticles(), 20);
        $form->addCheckboxList('labels', 'Štítky', $this->getLabels());
        $form
            ->addRadioList('cms_article_color_id', 'Barvy', $this->getColors())
            ->setRequired('Vyberte alespoň jednu barvu efektu.');

        return $form;
    }

    public function getGaleries(){
        return $this->_photogalleryRepo->fetchPairs('id', 'name', true);
    }

    public function getPodcasts(){
        return $this->_podcastRepo->fetchPairsWithWhere('id', 'name', array('view' => 1), true);
    }

    public function getAuthors(){
        $authors1 = $this->_userRepo->fetchPairsWithWhereAndOrder('id', 'name surname', array('id' => 38), 'name,surname');
        $authors2 = $this->_userRepo->fetchPairsWithWhereAndOrder('id', 'name surname', array('id NOT IN' => array(38)), 'name,surname');

        return $authors1 + $authors2;
    }

    public function getArticles(){
        return $this->_articleRepo->fetchPairsWithWhereAndOrder('id', 'name', array(), 'name');
    }

    public function getLabels(){
        return $this->_articleLabelRepo->fetchPairsWithWhere('id', 'name', array('view' => 1));
    }

    public function getMnRelated() {
        return $this->_mnArticleRelated;
    }

    public function getMnRelated2() {
        return $this->_mnArticleRelated2;
    }

    public function getMnArticleLabel() {
        return $this->_mnArticleLabel;
    }

    public function getColors(){
        return $this->_articleColorRepo->fetchPairsWithWhereAndOrder('id', 'name', array('view' => 1), '_order');
    }

    public function getBanners() {
        return $this->_bannerRepo->fetchPairsWithWhereAndOrder('id', 'name', array('cms_banner_type_id' => 1), 'name', true);
    }

    /**
     * Ulozeni dat do DB.
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     */

    public function formSucceeded(\Nette\Application\UI\Form $form, $values){
        $id = $values['id'];

        // osetreni dat
        $this->_prepareValuesForSave($values, true);

        // moznost ovlivnit data pred ulozenim
        $this->_beforeSave($id, $values);

        if (empty($id)) {
            $result = $this->_articleRepo->insert($values);

            if (isset($result->id)) {
                $id = $result->id;

                // moznost ovlivnit data po ulozeni
                $this->_afterSave($id, $values);

                $this->_presenter->flashMessage('Záznam byl úspěšně založen.');

                $this->_presenter->redirect('edit', array('id' => $id));
                return;
            }

            $this->redirect('add');
            return;
        } else {
            $result = $this->_articleRepo->update($values, array(
                'id' => $id
            ));

            // moznost ovlivnit data po ulozeni
            $this->_afterSave($id, $values);

            $this->_presenter->flashMessage('Záznam byl úspěšně uložen.');
        }

        $this->_presenter->redirect('edit', array('id' => $id));

        /*
        echo '<pre>'.print_r($this->getParameters(), true).'</pre>';
        echo '<pre>'.print_r($result, true).'</pre>';
        echo '<pre>'.print_r($values, true).'</pre>';
        die;
        */
    }

    /**
     * Metoda predpripravi data pro ulozeni.
     *
     * Prida napr. sloupce s informacemi o tom kdo a kdy zaznam vklada atd.
     *
     * @param array $values
     */
    protected function _prepareValuesForSave(&$values, $removeId = false, $appendInsertData = true){
        $id = $values['id'];

        if ($removeId) {
            unset($values['id']);
        }

        // udaje o vlozeni vkladam pouze pri vkladani noveho zaznamu
        if ($appendInsertData && empty($id)) {
            $values['_insertUser'] = $this->_presenter->getUser()->getId();
            $values['_insertDate'] = date('Y-m-d H:i:s');
        }
    }

    protected function _beforeSave($id, &$values) {
        // kvuli odstranenym tlacitkum musim plnit systemove sloupce rucne
        $values['heading']     = $values['name'];
        $values['showHeading'] = 1;

        // "validace hodnot" pro ulozeni do DB
        if (empty($values['bannerSmall_id'])) {
            $values['bannerSmall_id'] = null;
        }

        if (empty($values['bannerBig_id'])) {
            $values['bannerBig_id'] = null;
        }

        if (empty($values['cms_photogallery_id'])) {
            $values['cms_photogallery_id'] = null;
        }

        if (empty($values['cms_podcast_id'])) {
            $values['cms_podcast_id'] = null;
        }

        if (empty($values['heading'])) {
            $values['heading'] = $values['name'];
        }

        if (empty($values['title'])) {
            $values['title'] = $values['name'];
        }

        if (empty($values['url'])) {
            $values['url'] = '/'.Nette\Utils\Strings::webalize($values['heading']);
        }

        // nahrazovani souvisejicich a banneru
        $this->_insertRelated2Text($values);
        $this->_insertBanner2Text($values);

        // osetreni data a casu publikace
        $values['datePublication'] = Nette\Utils\DateTime::from($values['datePublication'].' '.$values['timePublication'])->format('Y-m-d H:i:s');
        unset($values['timePublication']);

        // prevzeti related clanku do "mezipameti"
        $this->_mnArticleRelated->setRelations($values['related']);
        unset($values['related']);

        // prevzeti related 2 clanku do "mezipameti"
        $this->_mnArticleRelated2->setRelations($values['related2']);
        unset($values['related2']);

        // prevzeti stitku do "mezipameti"
        $this->_mnArticleLabel->setRelations($values['labels']);
        unset($values['labels']);

        // osetreni uploadu obrazku
        if ($values['mainImage']->isOk()) {
            $image  = $values['mainImage']->toImage();
            $image2 = clone $image;
            $image3 = clone $image;
            $image4 = clone $image;

            $fileNameArray = explode('.', $values['mainImage']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                //$values['mainImage']->move(DATA_DIR . "/articles/$filename");

                // nahled
                if ($image) {
                    $image->resize(120,90);
                    $image->save(DATA_DIR . "/articles/preview/$filename");
                }

                // obrazek pro karticu
                if ($image2) {
                    $image2->resize(390,0);
                    $image2->save(DATA_DIR . "/articles/box/$filename");
                }

                // obrazek pro karticu
                if ($image3) {
                    $image3->resize(750,0);
                    $image3->save(DATA_DIR . "/articles/box2/$filename");
                }

                // obrazek pro karticu
                if ($image4) {
                    $image4->resize(1200,0);
                    $image4->save(DATA_DIR . "/articles/$filename");
                }
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['mainImage'] = $filename;
        } else {
            unset($values['mainImage']);
        }

        // ulozeni souboru do fotogalerie
        $this->_saveFilesToFotogallery($id, $values);
    }

    protected function _afterSave($id, $values){
        // ulozeni related vazeb do DB
        $this->_mnArticleRelated->saveRelations($id);
        $this->_mnArticleRelated2->saveRelations($id);
        $this->_mnArticleLabel->saveRelations($id);
    }

    protected function _saveFilesToFotogallery($id, &$values){
        if (isset($values['galleryImages']) && !empty($values['galleryImages'])) {
            // zalozeni nove fotogalerie
            $photogalleryName  = $values['name'];
            $photogalleryAlias = Nette\Utils\Strings::webalize($values['name']);
            $photogalleryAlias = Nette\Utils\Strings::substring($photogalleryAlias, 0, 60);

            $photogallery = $this->_photogalleryRepo->insert(array(
                'name'  => $photogalleryName,
                'alias' => $photogalleryAlias,
                'view'  => 1
            ));

            if (is_object($photogallery) && $photogallery->id > 0) {
                // overeni existence adresare galerie
                $dirPath = WWW_DIR.'/data/photogallery/'.$photogalleryAlias;
                if (!empty($photogalleryAlias) && !is_dir($dirPath))
                {
                    if (!mkdir($dirPath))
                    {
                        $this->flashMessage('Chyba při vytváření adresáře "'.$photogalleryAlias.'" (path: '.$dirPath.').');
                        return;
                    } // end if
                } // end if

                // pokud je zalozena, uploaduju soubory
                foreach ($values['galleryImages'] as $fileUpload) {
                    $imageUpload = new ImageUpload($fileUpload);

                    $state = false;
                    try {
                        $imageUpload->generatePreviews(array(
                            'dir' => $dirPath,
                            'previews' => $this->_context->parameters['admin']['cms']['photogallery']['previews']
                        ));

                        $state = true;
                    } catch (\Exception $e) {
                        $this->_presenter->flashMessage($e->getMessage());
                    } // end try/catch

                    if ($state)
                    {
                        $this->_photogalleryItemRepo->insert(array(
                            'file' => $imageUpload->getSanitizedName(),
                            'cms_photogallery_id' => $photogallery->id
                        ));
                    } // end if

                    unset($imageUpload);
                }

                // predvybrani nove zalozene galerie
                $values['cms_photogallery_id'] = $photogallery->id;
            } else {
                $this->_presenter->flashMessage('Nepovedlo se založit novou fotogalerii. Zkuste to prosím později.');
            }
        }

        unset($values['galleryImages']);
    }

    /**
     * Metoda vlozi misto klicoveho retezce {souvisejici-clanky} souvisejici clanky.
     *
     * @param $values
     */
    protected function _insertRelated2Text(&$values){
        // pokud v textu nenajdu klicove slovo, nic nemenim
        if (strpos($values['text'], '{souvisejici-clanky}') === false || !isset($values['related2']) || empty($values['related2'])) {
            return;
        }

        // nacteni souvisejicich
        $relatedSelection = $this->_articleRepo->findBy(array('id IN' => $values['related2']));

        if ($relatedSelection->count()) {
            // ziskani base path
            $basePath = $this->_presenter->template->basePath;

            // generovani sablony souvisejicich
            $o = '<ul>';
            foreach ($relatedSelection as $article) {
                $o .= '<li><a href="'.$basePath.'/clanek'.$article->url.'" title="'.$article->heading.'">'.$article->heading.'</a></li>';
            }
            $o .= '</ul>';

            $values['text'] = str_replace('{souvisejici-clanky}', $o, $values['text']);
        }
    }

    /**
     * Metoda vlozi misto klicoveho retezce {banner} vybrany banner.
     *
     * @param $values
     */
    protected function _insertBanner2Text(&$values){
        // pokud v textu nenajdu klicove slovo, nic nemenim
        if (strpos($values['text'], '{banner}') === false || !isset($values['bannerContent_id']) || empty($values['bannerContent_id'])) {
            return;
        }

        $banner = $this->_bannerRepo->findByPk($values['bannerContent_id']);

        if (!is_null($banner) && !empty($banner->image)) {
            // ziskani base path
            $basePath = $this->_presenter->template->basePath;

            // generovani obsahu
            $o = '';

            if (!empty($banner->url)) {
                $o .= '<a href="'.$banner->url.'" title="'.$banner->name.'" target="_blank">';
            }

            $o .= '<img src="'.$basePath.'/data/banner/'.$banner->image.'" alt="'.$banner->name.'" />';

            if (!empty($banner->url)) {
                $o .= '</a>';
            }

            // nahrazeni klicoveho slova
            $values['text'] = str_replace('{banner}', $o, $values['text']);
        }
    }

}
