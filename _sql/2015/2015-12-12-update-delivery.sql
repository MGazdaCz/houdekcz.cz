-- Adminer 4.0.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+01:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `eshop_delivery`;
CREATE TABLE `eshop_delivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_delivery` (`id`, `name`, `description`, `price`, `active`, `_insertUser`, `_insertDate`) VALUES
(1,	'Balík do ruky',	'',	90.00,	'0',	1,	'2015-05-25 13:41:35'),
(2,	'Balík na poštu',	'',	100.00,	'0',	1,	'2015-05-25 13:41:58'),
(3,	'Geis - ČR',	'',	100.00,	'0',	1,	'2015-05-25 13:42:18'),
(4,	'Osobní odběr',	'osobní odběr na provozovně v RK - zdarma (mimo provozní dobu odběr možný po tel.domluvě)',	0.00,	'1',	1,	'2015-05-25 13:42:34'),
(5,	'Balík do ruky - SK',	'',	200.00,	'0',	NULL,	'0000-00-00 00:00:00'),
(6,	'Geis - SK',	'',	200.00,	'0',	NULL,	'0000-00-00 00:00:00'),
(7,	'PPL',	'',	90.00,	'1',	1,	'2015-11-03 16:33:13'),
(8,	'Rozvoz v okresech RK, UO, NA, HK, RK',	'termín doručení je přizpůsoben rozvozového plánu firmy po tel.domluvě',	0.00,	'1',	1,	'2015-11-19 14:55:39');

DROP TABLE IF EXISTS `eshop_delivery_payment`;
CREATE TABLE `eshop_delivery_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eshop_delivery_id` int(10) unsigned DEFAULT NULL,
  `eshop_payment_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `eshop_delivery_id` (`eshop_delivery_id`),
  KEY `eshop_payment_id` (`eshop_payment_id`),
  CONSTRAINT `eshop_delivery_payment_ibfk_3` FOREIGN KEY (`eshop_delivery_id`) REFERENCES `eshop_delivery` (`id`) ON DELETE CASCADE,
  CONSTRAINT `eshop_delivery_payment_ibfk_4` FOREIGN KEY (`eshop_payment_id`) REFERENCES `eshop_payment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_delivery_payment` (`id`, `eshop_delivery_id`, `eshop_payment_id`, `name`, `price`) VALUES
(12,	1,	2,	'Převodem',	0.00),
(13,	1,	NULL,	'Dobírka Česká Pošta',	50.00),
(14,	2,	NULL,	'Dobírka Česká Pošta',	50.00),
(15,	2,	2,	'Převodem',	0.00),
(16,	3,	NULL,	'Dobírka Geis',	50.00),
(17,	3,	2,	'Převodem',	0.00),
(18,	4,	1,	'Hotově',	0.00),
(19,	5,	NULL,	'Převodem',	0.00),
(20,	6,	NULL,	'Převodem',	0.00),
(21,	7,	2,	'Převodem',	0.00),
(22,	7,	NULL,	'Hotově',	50.00),
(23,	8,	NULL,	'Hotově',	0.00),
(24,	8,	NULL,	'Převodem',	0.00);

DROP TABLE IF EXISTS `eshop_payment`;
CREATE TABLE `eshop_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_payment` (`id`, `name`, `price`, `active`, `_insertUser`, `_insertDate`) VALUES
(1,	'Hotově',	0.00,	'1',	1,	'2015-05-25 13:53:10'),
(2,	'Převodem',	0.00,	'1',	1,	'2015-05-25 13:53:24');

-- 2015-12-12 11:22:51
