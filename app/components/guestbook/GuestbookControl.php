<?php
namespace Control\Guestbook;

use Control,
    \Nette\Application\UI\Form;
use Model\Form\FormPara;

class GuestbookControl extends Control\AbstractControl
{

    public function render()
    {
        $this->template->rows = $this->_context->guestbookRepository->findBy(array('display' => 1))->order("date DESC");

        $this->template->setFile(__DIR__.'/list.latte');
        $this->template->render();
    }

    protected function createComponentForm()
    {
        $form = new FormPara();

        $form->addHidden('backlink', $this->_url);
        $form->addText('name', 'Jméno a příjmení')
             ->setRequired('Vyplňte prosím jméno a příjmení.')
             ->addRule(Form::MIN_LENGTH, 'Zadejte prosím jméno a příjmení (minimální délka 5 znaků).', 5);
        $form->addText('email', 'E-mail');
             //->addRule(Form::EMAIL, 'Email musí být ve správném tvaru např. jan.novak@gmail.com');
             //->setRequired('Zadejte prosím email ve správném tvaru.');
        $form->addTextArea('text', 'Text')
             ->setRequired('Vyplňte prosím text.');
        $form->addSubmit('submit', 'Odeslat příspěvek');

        $form->onSuccess[] = callback($this, 'processForm');

        return $form;
    }

    public function processForm(Form $form){
        $values = (array)$form->values;
        unset($values['backlink']);

        $this->_context->guestbookRepository->insertRecord($values);
        $this->flashMessage('Příspěvek byl úspěšně přidán.');
        //$this->redirect(302, '/'.$form->values->backlink);
        //$this->redirect('this');

        header('Location: '.$form->values->backlink);
        return;
    }

} // end class