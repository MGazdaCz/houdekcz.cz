<?php

namespace App\EshopModule\Model;
use Nette\Database\Table\ActiveRow;

/**
 * Trida pro poskytnuti detailnich informaci o produktu
 */
class ProductDetail extends Product
{
    protected $_heading;
    protected $_showHeading;
    protected $_annotation;
    protected $_text;
    protected $_title;
    protected $_description;
    protected $_keywords;
    protected $_template;

    // obrazky productu
    protected $_image;
    protected $_imageOriginalUrl;


    /**
     * Metoda zalozi produkt z predaneho active row.
     *
     * @param ActiveRow $row
     * @return \App\Model\EshopProduct
     */
    public static function getFromActiveRow(ActiveRow $row){
        $product = new self;
        $product->initFromActiveRow($row);

        return $product;
    }

    /**
     * Metoda inicializuje vlastnosti z active row.
     *
     * @param ActiveRow $row
     */
    public function initFromActiveRow(ActiveRow $row){
        // inicializace zakladnich dat
        parent::initFromActiveRow($row);

        $this->_heading = $row->heading;
        $this->_showHeading = $row->showHeading;
        $this->_annotation = $row->annotation;
        $this->_text = $row->text;
        $this->_title = $row->title;
        $this->_description = $row->description;
        $this->_keywords = $row->keywords;
        $this->_template = $row->template;

        $this->_image = null;
        if (!empty($row->image) && file_exists(DATA_DIR.'/eshop/product/zoom2/'.$row->image)) {
            $this->_image            = $row->image;
            $this->_imageOriginalUrl = DATA_WWW.'/eshop/product/zoom2/'.$row->image;
        }
    }

    /**
     * Metoda vraci true, pokud obrazek existuje. Jinak vraci false.
     *
     * @return bool
     */
    public function hasImage(){
        if (!is_null($this->_image)) {
            return true;
        }
        return false;
    }

}
