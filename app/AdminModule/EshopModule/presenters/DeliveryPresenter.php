<?php

namespace App\AdminModule\EshopModule\Presenters;

use \App\AdminModule\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Delivery presenter.
 */
class DeliveryPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Delivery
     */
    protected $_repository;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Payment
     */
    protected $_repositoryPayment;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\DeliveryPayment
     */
    protected $_repositoryDelPayment;

    protected function startup(){
        parent::startup();

        $this->_repository           = $this->context->eshopDelivery;
        $this->_repositoryPayment    = $this->context->eshopPayment;
        $this->_repositoryDelPayment = $this->context->eshopDeliveryPayment;

        $this->template->bodyClass = 'module-delivery';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Delivery/form.latte';
    }

    public function renderDefault()
    {
        $this->template->h1    = 'Způsoby dopravy';
        $this->template->items = $this->_repository->findAll()->order('name');
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Nový způsob dopravy';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $item = $this->_repository->findByPk($id);

        $this->template->id          = $id;
        $this->template->h1          = 'Editace dopravy: '.$item->name;
        $this->template->delPayments = $item->related('eshop_delivery_payment.eshop_delivery_id');
    }

    protected function createComponentForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form->addText('name', 'Název', 50);
        $form->addText('price', 'Cena', 5);
        $form->addCheckbox('active', 'aktivní');
        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);

        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_repository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {

                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        } else {
            $form->setDefaults(array(
                'price' => '0.00'
            ));
        }

        return $form;
    }

    public function formSucceeded(Nette\Application\UI\Form $form, $values){
        $id = $values['id'];

        // metoda predpripravi data pro ulozeni zaznamu
        $this->_prepareValuesForSave($values, true);

        if (empty($id)) {
            $result = $this->_repository->insert($values);

            if (isset($result->id)) {
                $id = $result->id;

                $this->flashMessage('Záznam byl úspěšně založen.');

                $this->redirect('edit', array('id' => $id));
                return;
            }

            $this->redirect('add');
            return;
        } else {
            $result = $this->_repository->update($values, array(
                'id' => $id
            ));

            $this->flashMessage('Záznam byl úspěšně uložen.');
        }

        $this->redirect('edit', array('id' => $id));

        /*
        echo '<pre>'.print_r($this->getParameters(), true).'</pre>';
        echo '<pre>'.print_r($result, true).'</pre>';
        echo '<pre>'.print_r($values, true).'</pre>';
        die;
        */
    }

    public function createComponentAddSubitem(){
        // nacteni polozek pro vyber platebni metody
        $paymentItems = $this->_repositoryPayment->fetchPairsWithWhere('id', 'name price', array('active' => 1), true);

        $deliveryId = $this->getParameter('deliveryId');

        // sestaveni formulare
        $form = new Nette\Application\UI\Form();
        //$form->setAction($this->link('savesubitem!'));
        $form->addHidden('id');
        $form->addHidden('eshop_delivery_id', $deliveryId);
        $form
            ->addSelect('eshop_payment_id', null, $paymentItems);
        $form
            ->addText('name')
            ->setAttribute('placeholder', 'Název plat. metody');
        $form
            ->addText('price', null, 5)
            ->setAttribute('placeholder', 'Cena');
        $form
            ->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceededSubitem');

        return $form;
    }

    /**
     * Akce po submitnuti formulare pridavajici platebni metodu.
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     */
    public function formSucceededSubitem(Nette\Application\UI\Form $form, $values){
        $id = $values['id'];

        // metoda predpripravi data pro ulozeni zaznamu
        $this->_prepareValuesForSave($values, true, false);

        // osetreni moznosti zadat custom platbu bez vazby na modul platby
        if (empty($values['eshop_payment_id'])) {
            $values['eshop_payment_id'] = null;
        }

        if (empty($id)) {
            $result = $this->_repositoryDelPayment->insert($values);

            if (isset($result->id)) {
                $id = $result->id;

                $this->flashMessage('Záznam byl úspěšně založen.');
            }
        } else {
            $result = $this->_repositoryDelPayment->update($values, array(
                'id' => $id
            ));

            $this->flashMessage('Záznam byl úspěšně uložen.');
        }

        // aktualizace listu
        $item = $this->_repository->findByPk($values['eshop_delivery_id']);
        $this->template->delPayments = $item->related('eshop_delivery_payment.eshop_delivery_id');

        $this->redrawControl('deliveryPaymentItems');
        return;
    }

    // pridani platby
    public function handleadd($deliveryId) {
        $this->template->addSubitem = true;

        $this->redrawControl('addDeliveryPaymentItems');
        return;
    }

    // odstraneni platby
    public function handledelete($itemId) {
        $item = $this->_repositoryDelPayment->findByPk($itemId);

        if (!is_null($item)) {
            $eshopDeliveryId = $item->eshop_delivery_id;

            $this->_repositoryDelPayment->delete(array('id' => $itemId));

            $this->template->delPayments = $this->_repositoryDelPayment->findBy(array(
                'eshop_delivery_id' => $eshopDeliveryId
            ));

            $this->redrawControl('deliveryPaymentItems');
        }

        return;
    }

}
