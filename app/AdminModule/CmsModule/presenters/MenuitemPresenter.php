<?php

namespace App\AdminModule\CmsModule\Presenters;
use App\AdminModule\CoreModule\Component\FilterControl;
use App\AdminModule\CoreModule\Model\Forms\FormEditorDetail;


/**
 * Homepage presenter.
 */
class MenuitemPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Menu
     */
    protected $_repositoryMenu;
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Page
     */
    private $_pageRepository;

    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsMenuItem');
        $this->_repositoryMenu = $this->context->getService('cmsMenu');
        $this->_pageRepository = $this->context->getService('cmsPage');

        $this->_listH1 = 'Modul hlavní menu';
        $this->_addH1  = 'Přidat novou položku menu';
        $this->_editH1 = 'Upravit položku menu';

        $this->_orderDefault = '_order';

        $this->template->bodyClass = 'module-menuitem';
        $this->template->menuId    = null;

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Menuitem/form.latte';
    }

    public function renderAdd(){
        parent::renderAdd();

        $this->template->menuId = $this->getParameter('cms_menu');
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $menu = $this->_mainRepository->findByPk($id);
        $this->template->menuId = $menu->cms_menu_id;
    }

    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        $menuArray = $this->_repositoryMenu->fetchPairs('id', 'name', true);
        $pages     = $this->_pageRepository->fetchPairs('id', 'name', true);
        $languages = $this->_languageRepository->fetchPairs('id', 'name');

        //$form->addHidden('id');
        $form->addHidden('id');
        $form->addSelect('cms_menu_id', 'Menu', $menuArray);
        $form->addText('name', 'Název', 50);
        $form->addText('title', 'Titulek', 50);
        $form->addText('aClass', 'Class', 50);
        $form->addCheckbox('view', 'Shváleno');
        $form->addText('url', 'Url', 100);
        $form->addSelect('cms_page_id', 'Stránka', $pages);
        $form->addSelect('core_language_id', 'Jazyková mutace', $languages);

        $form->addCheckbox('dropdownEnable', 'zobrazit jako rozbalovací menu');
        $form
            ->addTextArea('dropdownContent', 'Obsah rozbalovacího menu')
            ->setAttribute('class', 'ckeditor');

        $form->addSubmit('submit', 'Ulož');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            $menuId = $this->getParameter('cms_menu');
            if (!is_null($menuId)) {
                $form->setDefaults(array('cms_menu_id' => $menuId));
            }

            if ($this->_itemId > 0) {
                $item = $this->_mainRepository->findByPk($this->_itemId);
                $form->setDefaults($item);
            }
        } // end if

        $form->onSuccess[] = $this->formSucceeded;

        return $form;
    }

    protected function _beforeSave($id, &$values){
        if (empty($values['cms_page_id'])) {
            $values['cms_page_id'] = null;
        }
    }

}
