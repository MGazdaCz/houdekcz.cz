<?php

namespace App\AdminModule\CoreModule\Presenters;

use App\AdminModule\CoreModule\Model\Forms\Controls\ImageInput;
use App\AdminModule\CoreModule\Model\Forms\FormEditorDetail;
use \Nette\Application\UI\Form;
use \Nette\Image;
use Nette\Security\Passwords;
use Nette\Utils\Strings;

/**
 * Homepage presenter.
 */
class UserPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    private $_userRole;
    private $_usermoduleRepository;
    private $_userwidgetRepository;

    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('coreUser');
        $this->_userRole       = $this->context->getService('coreUserRole');
        //$this->_usermoduleRepository = $this->context->coreUsermoduleRepository;
        //$this->_userwidgetRepository = $this->context->coreUserwidgetRepository;

        $this->_listH1 = 'Uživatelé systému';

        $this->_orderDefault = 'surname, name';
    }

    /**
    * Metoda umoznuje rozsirit SQL podminku pred nactenim dat.
    * 
    * @author MiG
    * @param \Nette\Database\Table\Selection $where
    * @return void
    */
    protected function _beforeRenderDefault(&$querySelection) {
        // pokud mam opravneni ktera umoznuji nastaveni systemu
        if ($this->user->isInRole('administrator')) {
            // admin nema omezeni
        } elseif ($this->user->isInRole('supervisor')) {
            $querySelection->where('core_user_role_id NOT IN ?', array('administrator'));
        } else {
            $querySelection->where('id = ?',$this->user->getId());
        }
        
    } // end function
    
    protected function createComponentDefaultForm($name)
    {
        $roles = array(0 => ' ');
        $roles = array_merge($roles, $this->_userRole->findAll()->fetchPairs('id', 'name'));
        
        $modulesItems = array();
        if (isset($this->context->container->parameters['modules']))
        {
            foreach ($this->context->container->parameters['modules'] as $id => $item) {
                $modulesItems[$id] = $item['name'];
            }
        } // end if
        
        $form = new FormEditorDetail($this, $name);

        // musim nastavit manualne, protoze nevim, jak to nastavit automaticky pri pridani custom fieldu "ImageInput"
        $form->elementPrototype->enctype = 'multipart/form-data';
        
        $form->addGroup('Základní nastavení');
        $form->addHidden('id');
        $form->addText('name', 'Jméno', 50);
        $form->addText('surname', 'Příjmení', 50);
        $form->addText('alias', 'Alias', 50);
        $form->addText('username', 'Uživatelské jméno', 50);
        //$form->addUpload('photo', 'Fotografie');
        $form['photo'] = new ImageInput('photo', 'Fotografie');
        $form->addText('password', 'Heslo', 50);
        $form->addCheckbox('active', 'aktivní')->setValue(1);
        $form->addSelect('core_user_role_id', 'Role', $roles);
        $form->addTextArea('annotation', 'Anotace', 80, 5);
        $form->addSubmit('submit', 'Ulož');

        /*
        // pokud mam opravneni ktera umoznuji nastaveni systemu
        if ($this->user->isInRole('supervisor') || $this->user->isInRole('administrator'))
        {
            $form->addGroup('Oprávnění');
            $form->addCheckboxList('modules', 'Aktivní moduly', $modulesItems);
            $form->addSubmit('submit2', 'Ulož');
            
            $form->addGroup('Widgety na homepage');
            $form->addCheckboxList('widgets', 'Widgety', $this->context->container->parameters['widgets']);
            $form->addSubmit('submit3', 'Ulož');
        }
        else
        {
            $form['username']->setDisabled(true);
            $form['active']->setDisabled(true);
            $form['CoreUsergroup_id']->setDisabled(true);
        } // end if
        */
        
        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $user            = $this->_mainRepository->findByPk($this->_itemId)->toArray();
            unset($user['password']);
            //$user['modules'] = $this->_usermoduleRepository->fetchAllForUser($this->_itemId, true);
            //$user['widgets'] = $this->_userwidgetRepository->fetchAllForUser($this->_itemId, true);

            $form['photo']
                //->setDeleteUrl('/admin/photogalleryitem/deleteimage/'.$item->id)
                ->setPathDir(WWW_DIR.'/data/user/pw1')
                ->setPathWww(WWW_WWW.'/data/user/pw1');
            
            $form->setDefaults($user);
        } // end if

        $form->onSuccess[] = $this->formSucceeded;

        return $form;
    }

    protected function _beforeSave($id, &$values){
        if (empty($values['alias'])) {
            $values['alias'] = Strings::webalize($values['name'].' '.$values['surname']);
        }

        if (isset($values['password']) && !empty($values['password'])) {
            $values['password'] = Passwords::hash($values['password']);
        } else {
            unset($values['password']);
        }

        // upload obrazku
        $file = $values['photo'];
        unset($values['photo']);

        if ($file->isOk()) {
            $values['photo'] = $file->getSanitizedName();
            try {
                $file->generatePreviews(array(
                    'dir' => WWW_DIR.'/data/user',
                    'previews' => $this->context->parameters['user']['previews']
                ));
            }
            catch (\Exception $e)
            {
                $this->flashMessage($e->getMessage(), 'error');
                //return $this->redirect('edit', array('id' => $id));
                unset($values['photo']);
            } // end try/catch
        } // end if
    }

    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();
        
        // hodnoty formulare preberu v poli
        $formValues   = $form->getValues(true);
        $modulesArray = array();
        $widgetsArray = array();

        if (isset($formValues['password']) && !empty($formValues['password'])) {
            $formValues['password'] = Passwords::hash($formValues['password']);
        } else {
            unset($formValues['password']);
        }

        if (isset($formValues['modules']))
        {
            $modulesArray = $formValues['modules'];
            unset($formValues['modules']);
        } // end if
        
        if (isset($formValues['widgets']))
        {
            $widgetsArray = $formValues['widgets'];
            unset($formValues['widgets']);
        } // end if
        
        /**
        * Zpracovani obrazku.
        * 
        * @var \Nette\Http\FileUpload
        */
        $photoFile = $form->values['photo'];
        if ($photoFile->isOk())
        {
            if ($photoFile->isImage())
            {
                // vygenerovani nahledu
                $image = $photoFile->toImage();
                $image->resize(36,36);
                $image->save(WWW_DIR.'/data/user/x36/'.$photoFile->sanitizedName);
                
                // ulozeni originalu
                $photoFile->move(WWW_DIR.'/data/user/original/'.$photoFile->sanitizedName);
                
                // ulozim pouze nazev obrazku
                $formValues['photo'] = $photoFile->sanitizedName;
            }
            else
            {
                $this->flashMessage('Vkládaný soubor není obrázkem. Soubor byl zahozen.', 'error');
                unset($formValues['photo']);
            } // end if
        }
        else
        {
            unset($formValues['photo']);
        } // end if
        // ---
        
        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $userId = $requestParams['id'];
            $this->_mainRepository->update($formValues, array('id' => $requestParams['id']));
        }
        else
        {
            $userId = $this->_mainRepository->insert($form->values);
        } // end if

        // ulozeni naklikanych modulu
        if ($userId > 0)
        {
            $this->_usermoduleRepository->delete(array('user_id' => $userId));
            $this->_usermoduleRepository->saveModules($userId, $modulesArray);
        } // end if
        
        // ulozeni naklikanych widgetu na homepage
        if ($userId > 0)
        {
            $this->_userwidgetRepository->delete(array('user_id' => $userId));
            $this->_userwidgetRepository->saveWidgets($userId, $widgetsArray);
        } // end if


        $this->flashMessage('Změny uloženy.', 'success');

        return $this->redirect('default');
    }
    
}
