<?php

namespace Control\ContactForm;

class Translator implements \Nette\Localization\ITranslator
{
    private $_language;
    private $_mesages;

    public function __construct(){
        $this->_mesages['en'] = array(
            'Jméno a příjmení' => 'Name',
            'Zadejte prosím jméno a příjmení.' => 'Fill your name.',
            'Zadejte prosím jméno a příjmení (minimální délka 5 znaků).' => 'Fill your name (minimal length 5 chars).',
            'E-mail' => 'E-mail',
            'Zadejte prosím e-mail.' => 'Fill your e-mail.',
            'Email musí být ve správném tvaru např. jan.novak@seznam.cz' => 'Fill your e-mail (example john.doe@gmail.com).',
            'Telefon' => 'Phone',
            'Text' => 'Message',
            'Text zprávy' => 'Message',
            'Zadejte prosím text zprávy.' => 'Fill your text message.',
            'Odeslat' => 'Send',
        );
    }

    public function setLanguage($language){
        $this->_language = $language;
    }

    /**
    * Translates the given string.
    *
    * @param  string   message
    * @param  int      plural count
    * @return string
    */
    public function translate($message, $count = NULL)
    {
        if ($this->_language != 'cz') {
            if (isset($this->_mesages[$this->_language][$message])) {
                return $this->_mesages[$this->_language][$message];
            }
        }

        return $message;
    }

}