<?php

namespace App\AdminModule\CoreModule\Model\Forms;

use App\AdminModule\CoreModule\Model\Repository\Repository;

class ManyToMany {

    /**
     * @var string
     */
    protected $_mColumnName;
    /**
     * @var string
     */
    protected $_nColumnName;

    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\Repository
     */
    protected $_mRepository;

    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\Repository
     */
    protected $_nRepository;

    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\Repository
     */
    protected $_mnRepository;

    /**
     * Vybrane hodnoty z formulare.
     *
     * @var array
     */
    protected $_relations;

    /**
     * Konstruktor pro zadani zakladnich repository.
     *
     * @param $mnRepository
     * @param $mRepository
     * @param null $nRepository
     */
    public function __construct($mnRepository, $mRepository, $nRepository = null) {
        $this->setMnRepository($mnRepository);
        $this->setMRepository($mRepository);

        if (is_null($nRepository)) {
            $this->setNRepository($mRepository);
        } else {
            $this->setNRepository($nRepository);
        }
    }

    public function setMRepository(Repository $repository){
        $this->_mRepository = $repository;
    }

    public function setNRepository(Repository $repository){
        $this->_nRepository = $repository;
    }

    public function setMnRepository(Repository $repository){
        $this->_mnRepository = $repository;
    }

    /**
     * Nastavi nazev sloupce M.
     *
     * @param string $columnName
     */
    public function setMColumn($columnName){
        $this->_mColumnName = $columnName;
    }

    /**
     * Nastavi nazev sloupce N.
     *
     * @param string $columnName
     */
    public function setNColumn($columnName){
        $this->_nColumnName = $columnName;
    }

    /**
     * Metoda vrati souvisejici zaznamy k vybranemu zaznamu.
     *
     * @param string|int $value
     */
    public function getRelatedForColumnKeyAndValue($value) {
        $relatedSelection = $this->_mnRepository->findBy(array($this->_mColumnName => $value));

        $relatedIds = array();
        foreach ($relatedSelection as $relatedRow) {
            $nColumnKey = $this->_nColumnName;

            $relatedIds[] = $relatedRow->$nColumnKey;
        }

        return $relatedIds;
    }

    /**
     * Metoda nastavi data vybrane ve formularovem prvku.
     *
     * @param array $relationsIds
     */
    public function setRelations(array $relationsIds){
        $this->_relations = $relationsIds;
    }

    public function saveRelations($mRowId) {
        $mColumnName = $this->_mColumnName;
        $nColumnName = $this->_nColumnName;

        // TODO - musim nejak zobecnit do nejakeho nastroje, urcite se bude hodit!!!
        if (count($this->_relations) > 0) {
            $oldRelatedIdsSelections = $this->_mnRepository->findBy(array($mColumnName => $mRowId));

            if ($oldRelatedIdsSelections->count() > 0) {
                // odmazu ty, ktere nejsou vybrane
                $this->_mnRepository->delete(array(
                    $mColumnName => $mRowId,
                    $nColumnName.' NOT IN' => $this->_relations
                ));

                // jiz existujici vazby navkladam do pole pro pozdejsi praci
                $oldRelatedIds = array();
                foreach ($oldRelatedIdsSelections as $relatedRow) {
                    $oldRelatedIds[] = $relatedRow->$nColumnName;
                }

                // projdu vsechny vkladane
                foreach ($this->_relations as $relatedId) {
                    if (!in_array($relatedId, $oldRelatedIds)) {
                        // pokud neexistuje zaznam, vlozim jej
                        $this->_mnRepository->insert(array(
                            $mColumnName => $mRowId,
                            $nColumnName => $relatedId,
                        ));
                    }
                }
            } else {
                foreach ($this->_relations as $relatedId) {
                    $this->_mnRepository->insert(array(
                        $mColumnName => $mRowId,
                        $nColumnName => $relatedId,
                    ));
                }
            }
        } else {
            // pokud nejsou nastavene souvisejici, pokusim se odmazat nejake zaznamy pokud existuji
            $this->_mnRepository->delete(array($mColumnName => $mRowId));
        }
    }

}