<?php
namespace App\Model\Repository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Page extends RepositoryAbstract
{
    protected $_table = 'cms_page';

    /**
     * Metoda nacte a vrati stranku dle zadane URL adresy.
     *
     * @param string $url
     * @return mixed
     */
    public function findByUrl($url){
        return $this->findBy(array('url' => $url))->fetch();
    }

    /**
     * Metoda nacte a vrati stranku dle zadane URL adresy.
     *
     * @param string $url
     * @return mixed
     */
    public function findVisiblePageByUrl($url){
        return $this->findBy(array('view' => 1, 'url' => $url))->fetch();
    }

}