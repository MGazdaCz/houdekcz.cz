<?php

namespace App\AdminModule\EshopModule\Model\Order;

use Nette\Database\Table\ActiveRow;

class CompanyAddress extends Address {
    protected $_ico;
    protected $_dic;
    protected $_person;

    /**
     * Metoda inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     * @return \App\AdminModule\EshopModule\Model\Order\CompanyAddress
     */
    public static function createFromActiveRow(ActiveRow $activeRow){
        $address = new self;
        $address->initFromActiveRow($activeRow);

        return $address;
    }

    /**
     * Metoda inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     */
    public function initFromActiveRow(ActiveRow $activeRow){
        $this->_name   = $activeRow->companyName;
        $this->_person = $activeRow->companyPerson;
        $this->_street = $activeRow->companyStreet;
        $this->_city   = $activeRow->companyCity;
        $this->_zip    = $activeRow->companyZip;
        $this->_email  = $activeRow->email;
        $this->_phone  = $activeRow->phone;
        $this->_ico    = $activeRow->ico;
        $this->_dic    = $activeRow->dic;
    }

    /**
     * Metoda inicializuje objednavku z dat formulare.
     *
     * @param $values
     * @return \App\AdminModule\EshopModule\Model\Order\CompanyAddress
     */
    public static function createFromCheckoutFormData($values){
        $address = new self;
        $address->initFromCheckoutFormData($values);

        return $address;
    }

    /**
     * Metoda inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     */
    public function initFromCheckoutFormData($values){
        $this->_name   = $values->billingAddress->company;
        $this->_person = $values->billingAddress->person;
        $this->_street = $values->billingAddress->street;
        $this->_city   = $values->billingAddress->city;
        $this->_zip    = $values->billingAddress->zip;
        $this->_email  = $values->email;
        $this->_phone  = $values->phone;
        $this->_ico    = $values->billingAddress->ico;
        $this->_dic    = $values->billingAddress->dic;
    }

}