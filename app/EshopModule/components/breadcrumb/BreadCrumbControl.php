<?php
namespace App\EshopModule\Control\Product;

use Nette\Application\UI\Control;
use Nette\Database\Table\ActiveRow;

class BreadCrumbControl extends Control
{
    protected $_categoryRepo;
    protected $_productRepo;

    protected $_items;

    public function __construct(){
        $this->_items = array();
    }

    public function render()
    {
        $this->_categoryRepo = $this->getPresenter()->context->getService('eshopCategory');
        $this->_productRepo  = $this->getPresenter()->context->getService('eshopProduct');

        $this->_init();

        $this->template->items = $this->_items;
        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

    /**
     * Inicializace polozek pro eshop.
     */
    protected function _init(){
        $url = $this->getPresenter()->getParameter('url');

        // vlozeni linku na homepage
        $this->addItem('Home', $this->getPresenter()->link(':Homepage:'));

        // vlozeni linku na HP eshopu
        if ($this->getPresenter()->getName() == 'Eshop:Homepage') {
            $this->addItem('Eshop');
        } else {
            $this->addItem('Eshop', $this->getPresenter()->link(':Eshop:Homepage:'));
        }

        // pokud jsem v kategorii, generuji polozky pro kategorie
        if ($this->getPresenter()->getName() == 'Eshop:Category') {
            $category = $this->_categoryRepo->findVisibleByUrl($url);

            if ($category) {
                // pridam parent categorie
                $this->_initCategories($category->_parent);

                // nakonec pridam aktivni kategorii
                $this->addItem($category->name);
            }
        }

        // pokud jsem v produktu, generuji nazev produktu + polozky pro kategorie
        if ($this->getPresenter()->getName() == 'Eshop:Product') {
            $product = $this->_productRepo->findVisibleByUrl($url);

            if ($product) {
                // hledam kde je produkt dostupny (v jakych kategoriich)
                $productCategories = $product->related('eshop_category_product.eshop_product_id');

                // pokud je produkt zarazen v kategoriich, vezmu prvni a vlozim jeji podstrom kategorii
                if ($productCategories->count()) {
                    $categoryProductRow = $productCategories->fetch();

                    // pridam parent categorie
                    $this->_initCategories($categoryProductRow->eshop_category_id);
                }
                $this->addItem($product->name);
            }
        }
    }

    /**
     * Metoda nacte a vlozi parenty kategorii do BreadCrumbu.
     *
     * @param $categoryId
     */
    protected function _initCategories($categoryId){
        // nactu vsechny parenty
        $parents = array();
        $parents = $this->_categoryRepo->findAllParents($categoryId, $parents);
        $parents = array_reverse($parents);

        if (!empty($parents)) {
            // vlozim parenty do BreadCrumbu
            foreach ($parents as $parent) {
                $parentUrl = $this->getPresenter()->link(':Eshop:Category:default', array('url' => $parent->url));

                $this->addItem($parent->name, $parentUrl);
            }
        }
    }

    /**
     * Metoda prida polozku do renderovaneho menicka.
     *
     * @param $name
     * @param null $url
     * @param array $urlParams
     */
    public function addItem($name, $url = null){
        $item = new \stdClass();
        $item->name = $name;
        $item->url  = $url;

        $this->_items[] = $item;
    }

}