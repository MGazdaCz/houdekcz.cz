<?php
namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * @author Martin Hromádko <hromadkom@gmail.com>
 */
class Directories extends MainRepository {
	
	protected $_table = 'cms_directory';
}
