<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CmsModule\Model\Photogallery\Helper;
use App\AdminModule\CoreModule\Component\FulltextSearchControl;
use App\AdminModule\CoreModule\Model\Forms\Controls\ImageInput;
use App\AdminModule\CoreModule\Model\Forms\ImageUpload;
use App\AdminModule\CoreModule\Model\Forms\ManyToMany;
use App\AdminModule\CoreModule\Component\SortLink;
use App\AdminModule\CoreModule\Model\SeoFieldsTool;
use Nette,
    App\Model;


/**
 * Product presenter.
 */
class ProductPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_mainRepository;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Category
     */
    protected $_categoryRepo;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\CategoryProduct
     */
    protected $_categoryProductRepo;
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Photogallery
     */
    protected $_photogalleryRepo;
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\CisUnit
     */
    protected $_cisUnitRepo;
    /**
     * @var \App\AdminModule\CoreModule\Model\Forms\ManyToMany
     */
    protected $_mnCategoryProduct;

    // nastaveni razeni
    /**
     * @var \App\AdminModule\CoreModule\Component\SortLink
     */
    protected $_orderComponent;
    protected $_page = 1;


    protected function startup(){
        parent::startup();

        // konfigurace repositaru
        $this->_mainRepository      = $this->context->getService('eshopProduct');
        $this->_categoryRepo        = $this->context->getService('eshopCategory');
        $this->_categoryProductRepo = $this->context->getService('eshopCategoryProduct');
        $this->_photogalleryRepo    = $this->context->getService('cmsPhotogallery');
        $this->_cisUnitRepo         = $this->context->getService('eshopCisUnit');

        // sestaveni MkuN vazby na kategorie
        $this->_mnCategoryProduct = new ManyToMany($this->_categoryProductRepo, $this->_categoryRepo, $this->_mainRepository);
        $this->_mnCategoryProduct->setMColumn('eshop_product_id');
        $this->_mnCategoryProduct->setNColumn('eshop_category_id');

        $this->template->bodyClass = 'module-product';

        // konfigurace komponenty pro razeni
        $this->_orderComponent = $this->getComponent('listSortLink');
        $this->_orderComponent->setActualSortField('name');
        $this->_orderComponent->setActualSortFieldDirection(SortLink::DIRECTION_UP);

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Product/form.latte';
    }

    public function renderDefault()
    {
        $categoryId  = $this->getParameter('categoryId');
        $this->_page = $this->getParameter('page', 1);

        // ziskani podstromu kategorii
        $categoriesIds   = $this->_categoryRepo->getAllChildrenCategories($categoryId);
        $categoriesIds[] = $categoryId;

        // inicializace parametru razeni + ziskani sortovaci casti query
        $this->_orderComponent->setParameter('page', $this->_page);
        $this->_orderComponent->setParameter('q', $this->getParameter('q'));
        $orderForQuery = $this->_orderComponent->getOrderForQuery();

        // inicializace parametru strankovani
        $this->getComponent('listPaginator')->setParameter('q', $this->getParameter('q'));
        $this->getComponent('listPaginator')->setParameter('categoryId', $categoryId);

        // moznost sestaveni where
        $whereArray = array();
        if ($categoryId) {
            $whereArray['id IN (SELECT eshop_product_id FROM eshop_category_product WHERE eshop_category_id IN ?)'] = $categoriesIds;
        }

        // ziskani rozsireni z vyhledavani
        $this->getComponent('fulltextSearch')->extendWhereArray($whereArray);

        // vypocty strankovani
        $this->template->h1 = 'Produkty';

        $this->template->pageLimit     = 10;
        $this->template->page          = $this->_page;
        $this->template->pageLimitFrom = ($this->_page - 1) * $this->template->pageLimit;
        $this->template->items         = $this->_mainRepository
                                            ->findBy($whereArray)
                                            ->order($orderForQuery)
                                            ->limit($this->template->pageLimit, $this->template->pageLimitFrom);
        $this->template->itemsCount    = $this->_mainRepository->findBy($whereArray)->count('id');
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Nový produkt';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $item = $this->_mainRepository->findByPk($id);

        $this->template->id = $id;
        $this->template->h1       = 'Editace produktu: '.$item->name;
        $this->template->product  = $item;
        $this->template->variants = $item->related('eshop_product_variant.eshop_product_id');
    }

    protected function createComponentForm()
    {
        $categoriesArray   = $this->_categoryRepo->getCategoriesArrayForMultiSelect(0);
        $photoGalleryArray = $this->_photogalleryRepo->fetchPairs('id', 'name', true);
        $cisUnitArray      = $this->_cisUnitRepo->fetchPairs('id', 'name');

        $form = new PageBase();
        $form->templateEnable();
        $form->initFormItems();

        // musim nastavit manualne, protoze nevim, jak to nastavit automaticky pri pridani custom fieldu "ImageInput"
        $form->elementPrototype->enctype = 'multipart/form-data';

        unset($form['heading']);

        $form->addSelect('eshop_cis_unit_id', 'Jednotka', $cisUnitArray);
        $form->addMultiSelect('categoryProduct', 'Kategorie produktu', $categoriesArray, 5);

        $form->addText('sku', 'Kód produktu', 20);
        $form->addText('ean', 'EAN', 20);
        $form
            ->addText('externId', 'Extern ID', 20)
            ->setDisabled(true);
        $form->addText('price', 'Cena', 10);
        $form->addText('pricePerItem', 'Cena za kus', 10);
        $form->addText('vat', 'Sazba DPH', 5);
        $form->addCheckbox('priceWithVat', 'cena s DPH');
        $form->addText('stock', 'Skladem', 10);

        $form['image'] = new ImageInput('image', 'Hlavní obrázek');

        $form->addSelect('cms_photogallery_id', 'Fotogalerie', $photoGalleryArray);
        $form->addUpload('images', 'Ostatní obrázky', true);

        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);

        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            $form['image']
                //->setDeleteUrl('/admin/cms/project/deleteimage/'.$item->id)
                ->setPathDir(WWW_DIR.'/data/eshop/product/preview')
                ->setPathWww(WWW_WWW.'/data/eshop/product/preview');

            if (!is_null($item)) {
                $itemArray = $item->toArray();

                // sestaveni related do pole a vlozeni do formulare
                $itemArray['categoryProduct'] = $this->_mnCategoryProduct->getRelatedForColumnKeyAndValue($this->_itemId);

                $form->setDefaults($itemArray);
            } else {

                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        } else {
            $defaults = array(
                'price' => '0.00',
                'pricePerItem' => '0.00',
                'vat' => '0.00',
                'stock' => '0.00'
            );

            $categoryId  = $this->getParameter('categoryId');
            $categoryIds = array_keys($categoriesArray);
            if (in_array($categoryId, $categoryIds)) {
                $defaults['categoryProduct'] = array($categoryId);
            }

            $form->setDefaults($defaults);
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // prevzeti related clanku do "mezipameti"
        $this->_mnCategoryProduct->setRelations($values['categoryProduct']);
        unset($values['categoryProduct']);

        // osetreni seo poli
        SeoFieldsTool::sanitizeFieldsData($values['name'], $values);

        // urceni poradi
        if (empty($id)) {
            $values['_order'] = $this->_mainRepository->findAll()->count() + 1;
        }

        // nulovani "prazdnych galerii"
        if (empty($values['cms_photogallery_id'])) {
            $values['cms_photogallery_id'] = null;
        }

        try {
            // uploadovani hlavniho obrazku
            if (!empty($values['image'])) {
                $this->_uploadFile($id, $values, 'image', time());
            } else {
                unset($values['image']);
            }
        }
        catch (\Exception $e)
        {
            $this->flashMessage($e->getMessage(), 'error');
            unset($values['image']);
            //return $this->redirect('edit', array('id' => $id));
        } // end try/catch

        // zalozeni nove fotogalerie a jeji pripojeni k zaznamu
        try {
            $photoGallery = Helper::createGalleryFromFiles($this->context, $values['name'], $values['images']);
            if (!is_null($photoGallery)) {
                $values['cms_photogallery_id'] = $photoGallery->id;
            }
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'error');
        }
        unset($values['images']);
        // ---
    }

    protected function _afterSave($id, $values){
        // ulozeni related vazeb do DB
        $this->_mnCategoryProduct->saveRelations($id);
    }

    private function _uploadFile($id, &$values, $fileKey, $targetFileName) {
        // generovani nahledu fotky
        $file = $values[$fileKey];
        unset($values[$fileKey]);

        if ($file instanceof ImageUpload) {
            $values[$fileKey] = $targetFileName.'.'.$file->getSuffix();

            $file->generatePreviews(array(
                'dir' => WWW_DIR.'/data/eshop/product',
                'fileName' => $values[$fileKey],
                'previews' => $this->context->getParameters()['eshop']['product']['previews']
            ));
        }
    }

    /**
     * Metoda aktualizuje data - opravi URL adresy a SEO pole.
     */
    public function renderRepairproducts(){
        $items = $this->_mainRepository->findAll();

        foreach ($items as $item) {
            $updateData = array();

            // aktualizace URL adres kategorii
            if (empty($item->url)) {
                $updateData['url'] = '/'.Nette\Utils\Strings::webalize($item->name);
            }
            if (empty($item->heading))     $updateData['heading']     = $item->name;
            if (empty($item->title))       $updateData['title']       = $item->name;
            if (empty($item->keywords))    $updateData['keywords']    = $item->name;
            if (empty($item->description)) $updateData['description'] = $item->name;

            $this->_mainRepository->update($updateData, array('id' => $item->id));
        }

        $this->flashMessage('Aktualizovano '.$items->count().' záznamů');
        $this->redirect('default');
    }

    /**
     * Inicializace komponenty pro razeni.
     *
     * @return SortLink
     */
    protected function createComponentListSortLink() {
        return new SortLink(array(
            'name'  => SortLink::DIRECTION_UP,
            'sku'   => SortLink::DIRECTION_UP,
            'view'  => SortLink::DIRECTION_UP,
            'price' => SortLink::DIRECTION_UP,
            'pricePerItem' => SortLink::DIRECTION_UP,
        ));
    }

    /**
     * Udalost obsluhujici razeni.
     *
     * @param string $orderField
     * @param string $orderDirection
     * @param int $page
     */
    public function handleSort($orderField, $orderDirection, $page = 1, $q = ''){
        $this->_orderComponent->setActualSortField($orderField);
        $this->_orderComponent->setActualSortFieldDirection($orderDirection);

        $this->_page = $page;

        $this->redrawControl('table');
    }

    /**
     * Inicializace komponenty pro vyhledavani.
     *
     * @return FulltextSearchControl
     */
    protected function createComponentFulltextSearch() {
        return new FulltextSearchControl(array(
            'name', 'sku', 'price', 'pricePerItem'
        ));
    }

}
