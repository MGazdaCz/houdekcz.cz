<?php

namespace App\AdminModule\ErpModule;

use Model\Form\FormEditorDetail;
use Model\Helper\TimeHelper;
use \Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\DateTime;

/**
 * Guestbook presenter.
 */
class TaskPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    private $_projectRepository;
    private $_issueRepository;

    protected function startup(){
        parent::startup();
        $this->_mainRepository    = $this->context->taskRepository;
        $this->_projectRepository = $this->context->projectRepository;
        $this->_issueRepository   = $this->context->erpissueRepository;

        $this->template->taskRepository = $this->_mainRepository;

        $this->_orderDefault = 'dateFrom, dateTo';
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentTaskForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        // vsechny nedokoncené projekty
        $projects = $this->_projectRepository->getTable()->where('parent IS NULL AND rel_projectstatus < 100')->order('name');

        $issuesArray = array(0 => ' - ');
        foreach ($projects as $p)
        {
            //$issuesArray[$p->name] = array();
            //$issue->name.', '.$issue->ref('rel_project')->name;

            $issues = $p->related('erpissue.rel_project')->order('name');
            if ($issues->count() > 0)
            {
                foreach ($issues as $i) {
                    $issuesArray[$p->name][$i->id] = $i->name;
                } // end foreach
            } // end if
        } // end foreach

        $form->addGroup('Výkaz práce');
        $form
            ->addSelect('rel_issue', 'Úkol', $issuesArray)
            ->setRequired('Úkol musí být vybrán.');
        $form
            ->addText('dateFrom', 'Datum realizace od')
            ->setDefaultValue(date('d.m.Y'))
            ->setRequired('Datum realizace od musí být nastaveno.');
        $form
            ->addText('timeFrom', 'Hodiny a minuty')
            ->setRequired('Hodiny a minuty musí být nastaveny');
        $form
            ->addText('dateTo', 'Datum realizace do')
            ->setDefaultValue(date('d.m.Y'))
            ->setRequired('Datum realizace do musí být nastaveno');
        $form
            ->addText('timeTo', 'Hodiny a minuty')
            ->setRequired('Hodiny a minuty musí být nastaveny');

        $form->addTextArea('note', 'Poznámky', 75, 10);

        $form->addSubmit('submit', 'Uložit');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            if ($this->_itemId > 0)
            {
                $item = $this->_mainRepository->findByPk($this->_itemId);
                $task = Task::createFromActiveRow($item);

                /*
                // osetreni formatu datumu
                $date = new \Nette\DateTime($item->deadline);
                $item->deadline = $date->format('d.m.Y');
                // ---
                */

                $form->setDefaults($task->getArrayForForm());
            }
            else
            {
                $relProject = $this->getParameter('issue', 0);
                $form->setDefaults(array('rel_issue' => $relProject));
            } // end if
        } // end if

        $form->onSuccess[] = $this->taskFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function taskFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        $task   = Task::createFromFormRequest((array)$form->values);
        $values = $task->getArrayForSql();

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['insertDate'] = date('Y-m-d H:i:s');

            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    public function renderAdd(){
        $this->template->h1 = 'Přidat výkaz';
        $this->setView('form');
    }

    public function renderEdit($id){
        $this->template->h1 = 'Upravit výkaz';
        $this->_itemId = intval($id);
        $this->setView('form');
    }

}
