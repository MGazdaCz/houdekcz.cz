<?php

use Nette\Security,
	Nette\Utils\Strings,
    AdminModule\CoreModule\Model\Repository\UserRepository;


/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Security\IAuthenticator
{
    private $_userRepository;


	public function __construct(UserRepository $userRepository)
	{
		$this->_userRepository = $userRepository;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
		$row = $this->_userRepository->findByUsername($username);
        
		if (!$row) {
			throw new Security\AuthenticationException('Uživatelské jméno '.$username.' nenalezeno.', self::IDENTITY_NOT_FOUND);
		}

		//if ($row->password !== $this->calculateHash($password, $row->password)) {
		if ($row->password !== $password) {
			throw new Security\AuthenticationException('Zadané heslo není správné.', self::INVALID_CREDENTIAL);
		}

		$userArray = $row->toArray();
		unset($userArray['password']);
		return new Nette\Security\Identity($row['id'], array($row->core_usergroup_id), $userArray); // $row[self::COLUMN_ROLE]
	}


	/**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	public static function calculateHash($password, $salt = NULL)
	{
        if ($salt === null) {
            $salt = '$2a$07$' . Nette\Utils\Strings::random(22);
        }
        return crypt($password, $salt);
	}

}
