<?php

namespace App\AdminModule\CoreModule\Presenters;

/**
 * Presenter pro razeni polozek.
 * 
 */
class SortPresenter extends BasePresenter
{
    /** @var \Nette\Database\Context */
    private $_database;

    public function __construct(\Nette\Database\Context $database)
    {
        $this->_database = $database;
    }
    
    public function renderDefault()
    {
        $table = $this->getParameter('table', null);
        $ids   = $this->getParameter('ids', null);
        
        if (!empty($table) && !empty($ids) && is_array($ids)) {
            foreach ($ids as $order => $id) {
                $this->_database->query('UPDATE '.addslashes($table).' SET _order = ? WHERE id = ?', $order, $id);
            } // end foreach
        }
        else {
            throw new \Exception('Chyba pri razeni. Skontrolujte zadanou tabulku a preda IDs.');
        } // end if
        
        $this->sendJson(array('message' => 'OK'));
    } // end function
} // end class
