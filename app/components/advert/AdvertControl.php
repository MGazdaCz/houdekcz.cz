<?php
namespace App\Control\Advert;

use Nette\Application\UI\Control;

class AdvertControl extends Control
{
    public function renderBoxDemo()
    {
        $this->template->setFile(__DIR__.'/templates/boxDemo.latte');
        $this->template->render();
    }

    public function renderBox()
    {
        $this->template->setFile(__DIR__.'/templates/box.latte');
        $this->template->render();
    }

    public function renderBoxFromActiveRow($activeRow)
    {
        $this->template->activeBanner = null;

        if (!is_null($activeRow)) {
            $this->template->activeBanner = $activeRow;
        }

        $this->template->setFile(__DIR__.'/templates/box.latte');
        $this->template->render();
    }

    public function renderBoxWithZone($zoneId)
    {
        $bannerRepo = $this->parent->context->getService('cmsBanner');
        $zoneRepo   = $this->parent->context->getService('cmsBannerZone');

        $this->template->zoneId         = intval($zoneId);
        $this->template->banners        = array();
        $this->template->activeBannerId = null;
        $this->template->activeBanner   = null;

        // pokud jsem prihlasenej, nactu seznam banneru pro select
        if ($this->parent->user->isLoggedIn()) {
            $this->template->banners = $bannerRepo->fetchPairsWithWhere('id', 'name', array('cms_banner_type_id' => 1));
        }

        $zone = $zoneRepo->findByPk($zoneId);
        if (!is_null($zone) && $zone->cms_banner_id) {
            $this->template->activeBannerId = $zone->cms_banner_id;
            $this->template->activeBanner   = $zone->cms_banner;
        }

        // pokud nejsem prihlasenej,
        if (!$this->parent->user->isLoggedIn()) {

        }

        $this->template->setFile(__DIR__.'/templates/boxWithZone.latte');
        $this->template->render();
    }

    public function handleSubmit(){
        $values = $_POST;

        $zoneId   = intval($values['zoneId']);
        $bannerId = intval($values['bannerId']);

        if (empty($bannerId)) {
            $bannerId = null;
        }

        $zoneRepo = $this->parent->context->getService('cmsBannerZone');

        if ($zoneRepo->update(array('cms_banner_id' => $bannerId), array('id' => $zoneId))) {
            $this->parent->flashMessage('Banner byl úspěšně nastaven.');
            return;
        }

        $this->parent->flashMessage('Banner se nepovedlo nastavit. Zkuste to prosím později.', 'error info');
        return;
    }

}