<?php

namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * @author Martin Hromádko <hromadkom@gmail.com>
 */
class Photos extends MainRepository {

    protected $_table = 'cms_photo';

    public function findByGalleryId($id) {
        $sql = "SELECT p.*, f.name as path 
                FROM cms_photo p    
                JOIN cms_gallery_photo gp ON p.id = gp.cms_photo_id
                JOIN cms_file f ON f.id = p.cms_file_id
                WHERE gp.cms_gallery_id = ?";

        return $this->getDbContext()->query($sql, $id);
    }

    /**
     * Odstranuje fotografii z galerie
     * @param int $galleryId
     * @param int $photoId
     * @return \Nette\Database\ResultSet
     */
    public function removePhotoFromGallery($galleryId, $photoId) {
        $sql = "DELETE FROM cms_gallery_photo WHERE cms_gallery_id = ? AND cms_photo_id = ?";
        return $this->getDbContext()->query($sql, $galleryId, $photoId);
    }

    /**
     * Prida fotografii ke galerii skrze vazebni tabulku
     * @param int $galleryId
     * @param int $photoId
     * @return \Nette\Database\ResultSet
     */
    public function addPhotoToGallery($galleryId, $photoId) {
        $sql = "INSERT INTO cms_gallery_photo (cms_gallery_id, cms_photo_id) VALUES (?,?)";
        return $this->getDbContext()->query($sql, $galleryId, $photoId);
    }

    /**
     * Overuje zda fotografie je pripojena ke gallerii
     * @param int $gallery
     * @param int $photo
     * @return bool
     */
    public function checkPhotoInGallery($gallery, $photo) {
        $rslt = $this->getDbContext()->table('cms_gallery_photo')->where(array('cms_gallery_id' => $gallery, 'cms_photo_id' => $photo));
        return $rslt->count() == 1 ? true : false;
    }

    public function addPhotoByFileDirectory($gallery, $directory) {
        $sql = "SELECT p.id
            FROM cms_photo p
            JOIN cms_file f ON p.cms_file_id = f.id
            JOIN cms_directory d ON f.cms_directory_id = d.id
            WHERE d.id = ?";
        $photos = $this->getDbContext()->query($sql, $directory);
        foreach ($photos as $photo){
            if(!$this->checkPhotoInGallery($gallery, $photo->id)){
                $this->addPhotoToGallery($gallery, $photo->id);
            }
        }
    }

}
