<?php

namespace App\AdminModule\CoreModule\Presenters;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;

/**
 * Homepage presenter.
 */
class SettingsPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    protected function startup(){
        parent::startup();
        $this->_mainRepository = $this->context->coreSettingsRepository;

        $this->_listH1       = 'Nastavení systému';
        $this->_orderDefault = 'id';
    }
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    public function renderDefault() {
        $this->template->h1 = 'Nastavení systému';
        $this->template->formComponentName = 'defaultForm';
    } // end function

    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);
        $form->addHidden('id')->setValue(1);
        
        $form->addGroup('Základní nastavení');
        $form->addText('name', 'Název', 50);
        $form->addText('email', 'Email', 50);
        
        $form->addGroup('Nastavení meta tagů');
        $form->addText('defaultTitle', 'Titulek', 50);
        $form->addText('titleSuffix', 'Přípona titulku', 50);
        $form->addTextArea('defaultKeywords', 'Klíčová slova', 50, 5);
        $form->addTextArea('defaultDescription', 'Meta popisek', 50, 5);
        
        $form->addGroup('Externí služby');
        // sluzby v podobe analyticsu atd.
        
        $form->addSubmit('submit', 'Ulož');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            $item = $this->_mainRepository->findByPk(1);
            $form->setDefaults($item);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    public function defaultFormSubmitted(Form $form)
    {
        if ($form->values->id > 0)
        {
            $this->_mainRepository->update($form->values, array('id' => $form->values->id));
        }
        else
        {
            $this->_mainRepository->insert($form->values);
        } // end if
        
        $this->flashMessage('Změny uloženy.');

        return $this->redirect('default');
    }

}
