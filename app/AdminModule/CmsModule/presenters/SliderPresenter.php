<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CoreModule\Model\Forms\FormEditorDetail;
use Nette,
    App\Model;


/**
 * Slider presenter.
 */
class SliderPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\SliderItem
     */
    protected $_repositoryItems;

    protected function startup(){
        parent::startup();

        $this->_mainRepository  = $this->context->getService('cmsSlider');
        $this->_repositoryItems = $this->context->getService('cmsSliderItem');

        $this->_orderDefault = 'name';
        $this->_listH1       = 'Slidery';

        $this->template->bodyClass = 'module-slider';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Slider/form.latte';
    }

    public function renderAdd()
    {
        parent::renderAdd();

        $this->template->h1 = 'Nový slider';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1    = 'Editace slideru: '.$this->_mainRepository->findByPk($id)->name;
        $this->template->id    = $id;
        $this->template->items = $this->_repositoryItems->findBy(array('cms_slider_id' => $id))->order('name');
    }

    protected function createComponentForm()
    {
        $form = new FormEditorDetail();
        $form->addHidden('id');
        $form->addText('name', 'Název slideru', 50);
        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}