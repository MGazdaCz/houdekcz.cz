<?php

namespace App\AdminModule\CoreModule\Presenters;

use Model\Form\FormPara;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends \App\Presenters\BasePresenter
{
    function actionIn(){
        if ($this->user->isLoggedIn())
            return $this->redirect(':Admin:Core:Homepage:');

        $this->template->cmsName = $this->context->parameters['web']['cmsName'];
    }

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = new FormPara();
        $form->addText('username', 'Uživatelské jméno:')
            ->setRequired('Zadejte vaše uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Zadejte vaše heslo.');

        $form->addCheckbox('persistent', 'Zůstat trvale přihlášen');

        $form->addSubmit('send', 'Přihlásit se');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = $this->signInFormSucceeded;
        return $form;
    }


    public function signInFormSucceeded($form)
    {
        try {
            $user = $this->getUser();
            $values = $form->getValues();
            if ($values->persistent) {
                $user->setExpiration('+30 days', FALSE);
            }
            $user->login($values->username, $values->password);
            $this->flashMessage('Přihlášení bylo úspěšné.');
            $this->redirect(':Admin:Core:Homepage:');
        } catch (NS\AuthenticationException $e) {
            //$form->addError('Neplatné uživatelské jméno nebo heslo.');
            $form->addError($e->getMessage());
        }
    }


    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Uživatel byl bezpečně dohlášen ze systému.');
        $this->redirect(':Homepage:');
    }

}
