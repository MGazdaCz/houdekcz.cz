<?php

namespace App\EshopModule\Model;

use App\EshopModule\Model\Cart\Item;
use App\EshopModule\Model\Product;
use Nette\Http\Session;
use Nette\Object;

/**
 * Cart manager.
 */
class Cart extends Object
{
    const SESSION_SECTION_NAME = 'eshop_cart';

    protected $_products;

    protected $_cart;
    protected $_productRepository;


    public function __construct(Session $session, \App\AdminModule\EshopModule\Model\Repository\Product $productRepository){
        $this->_cart              = $session->getSection(self::SESSION_SECTION_NAME);
        $this->_productRepository = $productRepository;

        // inicializace kosiku
        $this->_initCart();

        // predani produktu do objektu kosiku
        $this->_products = &$this->_cart->products;
    }

    /**
     * Metoda provede inicializaci kosiku.
     *
     * @return $this
     */
    protected function _initCart(){
        if (!isset($this->_cart->products)) {
            $this->_cart->products = array();
        }

        return $this;
    }

    /**
     * Metoda prida produkt do kosiku.
     *
     * @param \Nette\Di\Container $context
     * @param int $productId
     * @param int $qty
     * @return \App\EshopModule\Model\Cart\Item | null
     */
    public function addProduct(\Nette\Di\Container $context, $productId, $qty = 1, $productVariantId = null){
        $product = $this->_productRepository->findByPk($productId);

        if (!is_null($product)) {
            $productKey                   = Item::getItemKey($productId, $productVariantId);
            $item                         = new \App\EshopModule\Model\Cart\Item($context, $productId, $qty, $productVariantId);
            $this->_products[$productKey] = $item;
            return $item;
        }

        return null;
    }

    /**
     * Metoda odebere produkt z kosiku.
     *
     * @param int $productId
     * @return bool
     */
    public function removeProduct($productId, $productVariantId = null){
        $productKey = Item::getItemKey($productId, $productVariantId);

        if (isset($this->_products[$productKey])) {
            unset($this->_products[$productKey]);

            return true;
        }

        return false;
    }

    /**
     * Metoda zmeni kvantitu zbozi v kosiku.
     *
     * @param int $productId
     * @param int $qty
     * @return bool
     */
    public function changeQuantity($productId, $qty, $productVariantId = null){
        $qty        = intval($qty);
        $productKey = Item::getItemKey($productId, $productVariantId);

        if ($qty <= 0) {
            $this->removeProduct($productId, $productVariantId);
        }

        if (isset($this->_products[$productKey])) {
            $this->_products[$productKey]->setQty($qty);

            return true;
        }

        return false;
    }

    /**
     * Metoda vrati seznam produktu v kosiku.
     *
     * @return array
     */
    public function getProducts(){
        return $this->_products;
    }

    /**
     * Metoda vrati seznam polozek (produktu) v kosiku.
     *
     * @return array
     */
    public function getItems(){
        return $this->getProducts();
    }

    /**
     * Metoda vrati pocet polozek v kosiku.
     *
     * @return int
     */
    public function getItemsCount(){
        $count = 0;

        foreach ($this->getProducts() as $product) {
            $count += $product->getQty();
        }

        return $count;
    }

    /**
     * Metoda vrati celkovou cenu zbozi v kosiku s DPH.
     *
     * @return float
     */
    public function getPriceWithVat(){
        $price = 0;

        foreach ($this->getProducts() as $product) {
            $price += $product->getPriceWithVat();
        }

        return $price;
    }

    /**
     * Metoda vrati celkovou cenu zbozi v kosiku bez DPH.
     *
     * @return float
     */
    public function getPriceWithoutVat(){
        $price = 0;

        foreach ($this->getProducts() as $product) {
            $price += $product->getPriceWithoutVat();
        }

        return $price;
    }

    /**
     * Metoda vraci true, pokud je kosik prazdny. Jinak vraci false.
     *
     * @return bool
     */
    public function isEmpty(){
        $products = $this->getProducts();

        return empty($products);
    }

    /**
     * Metoda provede vysypani kosiku.
     *
     * @return \App\EshopModule\Model\Cart
     */
    public function truncate(){
        $this->_products = array();
        return $this;
    }

}
