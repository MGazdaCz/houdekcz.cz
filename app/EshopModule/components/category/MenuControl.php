<?php
namespace App\EshopModule\Control\Category;

use Nette\Application\UI\Control;

class MenuControl extends Control
{
    public function render()
    {
        $categoryRepo = $this->getPresenter()->context->getService('eshopCategory');
        $url = $this->getPresenter()->getParameter('url');

        $this->template->items = $categoryRepo->findBy(array('_parent = ?' => 0, 'view = ?' => 1, 'delete = ?' => 0))->order('_order');

        if (!isset($this->template->activeCategory)) {
            $this->template->activeCategory = null;
        }

        $this->template->setFile(__DIR__.'/templates/menu.latte');
        $this->template->render();
    }

    public function handleopen($url){
        $categoryRepo = $this->getPresenter()->context->getService('eshopCategory');
        $category     = $categoryRepo->findBy(array('url' => $url))->fetch();

        if ($category) {
            $this->template->activeCategory = $category->id;
        }

        $this->redrawControl('eshopmenu');
    }

    public function handleclose($url){
        $this->redrawControl('eshopmenu');
    }

}