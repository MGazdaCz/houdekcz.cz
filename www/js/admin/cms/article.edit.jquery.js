/**
 * Created by MiG on 04.08.2015
 */

$(document).ready( function() {

    // implementace inicializace CKEditoru
    CKEDITOR.disableAutoInline = true;

    // ovladani direct editable poli
    $("div.wr-editable").each(function( index ) {
        var contentId       = $(this).attr('id');
        var elementHiddenId = $(this).attr('wr-data-element-id');

        CKEDITOR.inline( contentId, {
            on: {
                blur: function( event ) {
                    var data = event.editor.getData();
                    $("#" + elementHiddenId).val(data);
                }
            }
        } );
    });

    // po kliknuti na submit prenesu data do hidden poli a pak muzu ulozit
    $("input[type=submit]").click(function(){
        // projdu editable policka a ulozim
        $("div.wr-editable").each(function( index ) {
            var contentId       = $(this).attr('id');
            var data            = $(this).html();
            var elementHiddenId = $(this).attr('wr-data-element-id');

            $("#" + elementHiddenId).val(data);
        });
    });

    // implementace datepickeru
    //$(".date-picker").datepicker();

});