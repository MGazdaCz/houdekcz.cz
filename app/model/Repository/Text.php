<?php
namespace App\Model\Repository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Text extends RepositoryAbstract
{
    protected $_table = 'cms_text';
}