<?php

namespace App\Forms;

use App\Model\Repository\User;
use Nette,
	Nette\Application\UI\Form;


class ContactFormFactory extends Nette\Object
{
	/** @var User */
    /*
	private $_userRepository;

	public function __construct(User $userRepository)
	{
		$this->_userRepository = $userRepository;
	}
    */


	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new PForm();

        //$form->getElementPrototype()->class = 'ajax';

        $form
            ->addHidden('backUrl');
        $form
            ->addText('name', null, 50)
            ->setRequired('Vyplňte vaše jméno a příjmení.')
            ->setAttribute('placeholder', 'Vaše jméno a příjmení');
        $form
            ->addText('phone', null, 50)
            ->setAttribute('placeholder', 'Váš telefon');
        $form
            ->addText('email', null, 50)
            ->addRule(Form::EMAIL, 'Vyplňte váš e-mail.')
            ->setAttribute('placeholder', 'Váš e-mail');
        $form
            ->addTextArea('text', null, 30, 10)
            ->setRequired('Zadejte text vašeho dotazu.')
            ->setAttribute('placeholder', 'Váš dotaz');
        $form
            ->addSubmit('submit', 'Odeslat')
            ->setAttribute('class', 'btn size-1 blue icon next');

		//$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded($form, $values)
	{

	}

}
