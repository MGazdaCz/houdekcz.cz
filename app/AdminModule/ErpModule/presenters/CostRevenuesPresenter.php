<?php

namespace App\AdminModule\ErpModule;

use App\AdminModule\CoreModule\Model\Forms\FormDetail;
use App\AdminModule\CoreModule\Model\Forms\FormInline;
use \Nette\Application\UI\Form;

/**
 * Náklady a výnosy presenter.
 */
class CostRevenuesPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    private $_projectRepository;
    private $_issueRepository;
    private $_companyRepository;
    
    protected function startup(){
        parent::startup();
        $this->_mainRepository    = $this->context->erpCostRevenuesRepository;
        $this->_projectRepository = $this->context->projectRepository;
        $this->_issueRepository   = $this->context->erpissueRepository;
        $this->_companyRepository = $this->context->companyRepository;
    }
    
    /**
    * Metoda vrati uloziste filtru v session.
    * 
    * @author MiG
    * @param void
    * @return \Nette\Http\SessionSection
    */
    private function _getFilterSession() {
        $session = $this->context->container->getService('session');
        
        return $session->getSection('costRevenuesFilter');
    } // end function
    
    /**
     * Definujeme komponentu pro formular filtru.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentFilterForm($name) {
        $form = new FormInline($this, $name);
        $form->setMethod(FormInline::GET);
        
        // vytazeni parametru filtru
        $parameters = $this->_getFilterSession()->parameters;
        if ($form->isSubmitted())
        {
            $parameters = $this->getParameters();
        } // end if
        
        // nacteni ciselniku pro filtr
        $projects  = $this->_projectRepository->fetchPairs('id', 'name', true);
        $issue     = array();
        $companies = $this->_companyRepository->fetchPairs('id', 'name', true);
        
        if (isset($parameters['Project_id'])) {
            $projectId = $parameters['Project_id'];
            
            if (!empty($projectId)) {
                $issueItems = $this->_issueRepository
                            ->getTable()
                            ->where(array('rel_project = ?' => $projectId))
                            ->fetchPairs('id', 'name');
                            
                $issue = array(0 => ' - ') + $issueItems;
            } // end if
        } // end if
        
        if (empty($issue))
        {
            $issue = $this->_issueRepository->fetchPairs('id', 'name', true);
        } // end if
        
        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        // sestaveni formulare
        $form->addSelect('Project_id', 'Projekt', $projects);
        $form->addSelect('ErpIssue_id', 'Úkol', $issue);
        $form->addSelect('ErpCompany_id', 'Komu', $companies);
        //$form->addText('text', 'Název');

        $form->addSubmit('submit', 'filtruj');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            $filterSettings = $this->_getFilterSession();
            
            $form->setValues((!empty($parameters) ? $parameters : array()));
        }
        else
        {
            // ulozeni hodnot do session
            $this->filterFormSubmitted($form);
        } // end if

        //$form->onSuccess[] = $this->filterFormSubmitted;

        return $form;
    } // end function

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentDefaultForm($name) {
        $form = new FormDetail($this, $name);
        
        $projects  = $this->_projectRepository->fetchPairs('id', 'name', true);
        $issue     = $this->_issueRepository->fetchPairs('id', 'name', true);
        $companies = $this->_companyRepository->fetchPairs('id', 'name', true);
        
        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        $form->addGroup('Základní informace');
        $form->addSelect('Project_id', 'Projekt', $projects);
        $form->addSelect('ErpIssue_id', 'Úkol', $issue);
        $form->addSelect('type', 'Typ', array('cost' => 'náklad', 'revenue' => 'výnos'));
        $form->addSelect('ErpCompany_id', 'Komu', $companies);
        $form->addText('name', 'Název');
        $form->addCheckbox('pay', 'Zaplaceno');
        $form->addText('price', 'Cena');

        $form->addSubmit('submit', 'Uložit');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            if ($this->_itemId > 0)
            {
                $item = $this->_mainRepository->findByPk($this->_itemId);
                $itemArray = $item->toArray();

                $form->setDefaults($itemArray);
            }
            else
            {
                $form->setDefaults(array(
                    'type' => $this->getParam('type', null)
                ));
            } // end if
        } // end if
        

        $form->onSuccess[] = $this->filterFormSubmitted;

        return $form;
    }
    
    /**
    * Udalost po submitnuti filtru.
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function filterFormSubmitted(Form $form) {
        $filterSettings = $this->_getFilterSession();
        
        $filterSettings->parameters = (array)$form->values;
    } // end function

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function defaultFormSubmitted(Form $form) {
        $requestParams = $this->request->getParameters();

        $values = (array)$form->values;

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['_insertDate'] = date('Y-m-d H:i:s');
            $values['_insertUser'] = $this->getUser()->getId();
            
            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    public function renderDefault() {
        $filterParams = $this->_getFilterSession()->parameters;
        
        // sestaveni filtru
        $costsWhere    = array('type = ?' => 'cost');
        $revenuesWhere = array('type = ?' => 'revenue');
        if (!empty($filterParams)) {
            foreach ($filterParams as $column => $value) {
                if (!empty($value)) {
                    $costsWhere[$column.' = ?']    = $value;
                    $revenuesWhere[$column.' = ?'] = $value;
                } // end if
            } // end foreach
        } // end if
        // ---
        
        // nacteni polozek listu
        $this->template->costs    = $this->_mainRepository->findBy($costsWhere)->order('id');
        $this->template->revenues = $this->_mainRepository->findBy($revenuesWhere)->order('id');
    } // end function

} // end class
