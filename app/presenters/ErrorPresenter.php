<?php

namespace App\Presenters;

use Nette\Diagnostics\Debugger;


/**
 * Error presenter.
 */
class ErrorPresenter extends PagePresenter
{

	/**
	 * @param  Exception
	 * @return void
	 */
	public function renderDefault()
	{
        $code = $this->getParameter('code');

        $this->template->description = '';
        $this->template->keywords    = '';
        $this->template->title       = 'Chyba 404 - stránka nenalezena';
        $this->template->bodyClass   = 'error-404';
        
        // load template 403.latte or 404.latte or ... 4xx.latte
        $this->setView(in_array($code, array(403, 404, 405, 410, 500)) ? $code : '4xx');
	}

}
