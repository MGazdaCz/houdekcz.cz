<?php
namespace Control\Reservation;

use Control,
    Model\Helper,
    \Nette\Application\UI\Form,
    \Nette\Mail\Message;
use Model\Form\FormPara;

class ReservationControl extends Control\AbstractControl
{

    public function render()
    {
        //$this->template->rows = $this->_context->guestbookRepository->findBy(array('display' => 1));

        $this->template->setFile(__DIR__.'/reservation.latte');
        $this->template->render();
    }

    protected function createComponentForm()
    {
        $reservationConfig = $this->_context->parameters['reservation'];

        $form = new FormPara();

        $form->addHidden('backlink', $this->_url);
        $form->addText('dateFrom', $reservationConfig['date']['from'])
             ->setRequired('Vyberte prosím datum příjezdu.')
             ->setAttribute('class', 'datepicker');
        $form->addText('dateTo', $reservationConfig['date']['to'])
             ->setRequired('Vyberte prosím datum odjezdu.')
             ->setAttribute('class', 'datepicker');
        $form->addText('name', 'Jméno a příjmení')
             ->setRequired('Zadejte prosím jméno a příjmení.')
             ->addRule(Form::MIN_LENGTH, 'Zadejte prosím jméno a příjmení (minimální délka 5 znaků).', 5);

        if ($reservationConfig['person'] == true)
        {
            $form->addText('person', 'Počet osob')
                 ->setRequired('Zadejte prosím počet osob.')
                 ->addRule(Form::INTEGER, 'Zadejte prosím počet osob celým číslem.');
        } // end if

        $form->addSelect('object', $reservationConfig['object']['name'], $reservationConfig['object']['items']);
        $form->addText('phone', 'Telefon')
             ->setRequired('Zadejte prosím telefonní číslo.');
        $form->addText('email', 'E-mail')
            ->setRequired('Zadejte prosím e-mail.')
             ->addRule(Form::EMAIL, 'Zadejte prosím email ve správném tvaru.');
        $form->addTextArea('note', 'Poznámka');
        $form->addSubmit('submit', 'Odeslat rezervaci');

        $form->onSuccess[] = callback($this, 'processForm');

        return $form;
    }

    public function processForm(Form $form){
        $values = (array)$form->values;
        unset($values['backlink']);

        // preformatovani datumu pro ulozeni do DB
        $dateFrom           = new Helper\DatetimeHelper($values['dateFrom']);
        $dateTo             = new Helper\DatetimeHelper($values['dateTo']);
        $values['dateFrom'] = $dateFrom->Format('%Y-%m-%d %H:%i:%s');
        $values['dateTo']   = $dateTo->Format('%Y-%m-%d %H:%i:%s');
        // ---

        // ulozeni dat do DB
        $this->_context->reservationRepository->insert($values);
        $this->flashMessage('Rezervace byla přidána.');
        //$this->redirect(302, '/'.$form->values->backlink);
        //$this->redirect('this');

        // odeslani potvrzujiciho emailu
        $this->sendReservationEmail($form);

        header('Location: '.$form->values->backlink);
        return;
    }

    private function sendReservationEmail(Form $form){
        // sestaveni sablony odeslaneho formulare
        $template = new \Nette\Templating\FileTemplate(__DIR__.'/email.latte');
        $template->registerFilter(new \Nette\Latte\Engine);
        $template->registerHelperLoader('Nette\Templating\Helpers::loader');
        $template->formName          = $this->_context->parameters['web']['name'];
        $template->reservationConfig = $this->_context->parameters['reservation'];
        $template->formValues        = $form->values;

        // odeslani formulare
        $mail = new Message();
        $mail->setFrom($this->_context->parameters['email']['admin']);
        $mail->addTo($form->values->email);
        $mail->addTo($this->_context->parameters['email']['admin']);
        $mail->setSubject('Rezervace z '.$this->_context->parameters['web']['name']);
        $mail->setHtmlBody($template);
        $mail->send();
    }

} // end class