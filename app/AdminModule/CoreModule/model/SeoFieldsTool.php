<?php
namespace App\AdminModule\CoreModule\Model;

use Nette\Object;
use Nette\Utils\Strings;

/**
 * Trida pro optimalizaci Seo poli v modulech.
 *
 * @package Model\Form
 */
class SeoFieldsTool extends Object
{
    const FIELD_TITLE       = 'title';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_KEYWORDS    = 'keywords';
    const FIELD_URL         = 'url';

    /**
     * Metoda ostreju a pripadne doplnuje chybejici SEO tagy.
     *
     * @param string $name
     * @param array $values
     */
    public static function sanitizeFieldsData($name, &$values){
        // pokud neni prazdne pole $name
        if (!empty($name)) {
            // pokusim se opravit prazdna pole
            if (empty($values[self::FIELD_TITLE])) {
                $values[self::FIELD_TITLE] = $name;
            }
            if (empty($values[self::FIELD_DESCRIPTION])) {
                $values[self::FIELD_DESCRIPTION] = $name;
            }
            if (empty($values[self::FIELD_KEYWORDS])) {
                $values[self::FIELD_KEYWORDS] = $name;
            }
            if (empty($values[self::FIELD_URL])) {
                $values[self::FIELD_URL] = '/'.Strings::webalize($name);
            }
        }
    }

}