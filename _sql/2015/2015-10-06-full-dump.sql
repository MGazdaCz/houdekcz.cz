-- Adminer 4.0.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+02:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cms_banner`;
CREATE TABLE `cms_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cms_banner_type_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_banner_type_id` (`cms_banner_type_id`),
  CONSTRAINT `cms_banner_ibfk_1` FOREIGN KEY (`cms_banner_type_id`) REFERENCES `cms_banner_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_banner` (`id`, `cms_banner_type_id`, `name`, `image`, `url`, `_insertUser`, `_insertDate`) VALUES
(1,	1,	'Testovac',	'1439140858.jpg',	'http://www.novinky.cz/domaci/377193-kontroluji-se-ridici-i-cyklisti-o-opile-vodaky-se-nestara-nikdo.html',	1,	'2015-08-09 19:15:26'),
(2,	1,	'Testovac',	'1439140950.jpg',	'',	1,	'2015-08-09 19:22:30');

DROP TABLE IF EXISTS `cms_banner_type`;
CREATE TABLE `cms_banner_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_banner_type` (`id`, `name`, `width`, `height`, `_insertUser`, `_insertDate`) VALUES
(1,	'Banner v box',	390,	0,	1,	'2015-08-09 19:06:58'),
(2,	'Banner pro detail',	610,	0,	1,	'2015-08-09 19:07:36');

DROP TABLE IF EXISTS `cms_banner_zone`;
CREATE TABLE `cms_banner_zone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `cms_banner_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_banner_id` (`cms_banner_id`),
  CONSTRAINT `cms_banner_zone_ibfk_1` FOREIGN KEY (`cms_banner_id`) REFERENCES `cms_banner` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_banner_zone` (`id`, `name`, `cms_banner_id`) VALUES
(1,	'Homepage slot 1',	1),
(2,	'Homepage slot 2',	1),
(3,	'Homepage slot 3',	1),
(4,	'Magazine slot 1',	1),
(5,	'Magazine slot 2',	1);

DROP TABLE IF EXISTS `cms_directory`;
CREATE TABLE `cms_directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `FK_cms_directory_cms_directory` FOREIGN KEY (`parent_id`) REFERENCES `cms_directory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_file`;
CREATE TABLE `cms_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_directory_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `mime_type` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` bigint(20) DEFAULT NULL,
  `is_image` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_directory_id` (`cms_directory_id`),
  CONSTRAINT `FK_cms_file_cms_directory` FOREIGN KEY (`cms_directory_id`) REFERENCES `cms_directory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_gallery`;
CREATE TABLE `cms_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_gallery_directory`;
CREATE TABLE `cms_gallery_directory` (
  `cms_gallery_id` int(11) NOT NULL,
  `cms_directory_id` int(11) NOT NULL,
  UNIQUE KEY `cms_directory_id` (`cms_directory_id`),
  UNIQUE KEY `cms_gallery_id` (`cms_gallery_id`),
  UNIQUE KEY `unique` (`cms_gallery_id`,`cms_directory_id`),
  CONSTRAINT `cms_gallery_directory_ibfk_1` FOREIGN KEY (`cms_gallery_id`) REFERENCES `cms_gallery` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cms_gallery_directory_ibfk_2` FOREIGN KEY (`cms_directory_id`) REFERENCES `cms_directory` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_gallery_photo`;
CREATE TABLE `cms_gallery_photo` (
  `cms_gallery_id` int(11) NOT NULL,
  `cms_photo_id` int(11) NOT NULL,
  PRIMARY KEY (`cms_gallery_id`,`cms_photo_id`),
  KEY `cms_gallery_id` (`cms_gallery_id`),
  KEY `cms_photo_id` (`cms_photo_id`),
  CONSTRAINT `cms_gallery_photo_ibfk_1` FOREIGN KEY (`cms_gallery_id`) REFERENCES `cms_gallery` (`id`),
  CONSTRAINT `cms_gallery_photo_ibfk_2` FOREIGN KEY (`cms_photo_id`) REFERENCES `cms_photo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_menu`;
CREATE TABLE `cms_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_menu` (`id`, `name`, `description`, `_insertUser`, `_insertDate`) VALUES
(1,	'Hlavní',	'Hlavní',	NULL,	NULL);

DROP TABLE IF EXISTS `cms_menu_item`;
CREATE TABLE `cms_menu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `aClass` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `absolute` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `cms_page_id` int(10) unsigned DEFAULT NULL,
  `_order` int(10) unsigned DEFAULT NULL,
  `core_language_id` char(2) COLLATE utf8_czech_ci NOT NULL DEFAULT 'cz',
  `cms_menu_id` int(10) unsigned DEFAULT NULL,
  `newWindow` char(1) COLLATE utf8_czech_ci DEFAULT '0',
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_page_id` (`cms_page_id`),
  KEY `core_language_id` (`core_language_id`),
  KEY `cms_menu_id` (`cms_menu_id`),
  CONSTRAINT `cms_menu_item_ibfk_1` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_page` (`id`),
  CONSTRAINT `cms_menu_item_ibfk_2` FOREIGN KEY (`core_language_id`) REFERENCES `core_language` (`id`),
  CONSTRAINT `cms_menu_item_ibfk_3` FOREIGN KEY (`cms_menu_id`) REFERENCES `cms_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_menu_item` (`id`, `name`, `title`, `aClass`, `url`, `view`, `delete`, `absolute`, `cms_page_id`, `_order`, `core_language_id`, `cms_menu_id`, `newWindow`, `_insertUser`, `_insertDate`) VALUES
(2,	'Eshop',	'Eshop',	'',	'',	'1',	'0',	'0',	6,	NULL,	'cz',	1,	'0',	1,	'2015-07-17 22:18:08'),
(4,	'Proč zvolit nás',	'Proč zvolit nás',	'',	'',	'1',	'0',	'0',	NULL,	NULL,	'cz',	1,	'0',	1,	'2015-07-17 22:18:08'),
(6,	'Vše o nákupu',	'Vše o nákupu',	'',	'',	'1',	'0',	'0',	NULL,	NULL,	'cz',	1,	'0',	1,	'2015-10-06 12:03:35'),
(7,	'Kontakt',	'Kontakt',	'',	'',	'1',	'0',	'0',	NULL,	NULL,	'cz',	1,	'0',	1,	'2015-10-06 12:04:13'),
(8,	'Přihlášení',	'Přihlášení',	'',	'',	'1',	'0',	'0',	NULL,	NULL,	'cz',	1,	'0',	1,	'2015-10-06 12:04:31');

DROP TABLE IF EXISTS `cms_news`;
CREATE TABLE `cms_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `showHeading` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  `annotation` text COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `datePublication` date DEFAULT NULL,
  `showDatePublication` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `dateFrom` date DEFAULT NULL,
  `dateTo` date DEFAULT NULL,
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `_insertUser` (`_insertUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `cms_newsletter_email`;
CREATE TABLE `cms_newsletter_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `enable` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE `cms_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_parent` int(10) unsigned DEFAULT NULL,
  `_level` int(10) unsigned NOT NULL DEFAULT '0',
  `_order` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `homepage` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `showHeading` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  `component` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `annotation` text COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `cms_photogallery_id` int(10) unsigned DEFAULT NULL,
  `datePublication` date DEFAULT NULL,
  `showDatePublication` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `dateFrom` date DEFAULT NULL,
  `dateTo` date DEFAULT NULL,
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime DEFAULT NULL,
  `core_language_id` char(2) COLLATE utf8_czech_ci DEFAULT NULL,
  `template` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `_insertUser` (`_insertUser`),
  KEY `_parent` (`_parent`),
  KEY `CmsPhotogallery_id` (`cms_photogallery_id`),
  KEY `core_language_id` (`core_language_id`),
  CONSTRAINT `cms_page_ibfk_2` FOREIGN KEY (`_insertUser`) REFERENCES `core_user` (`id`),
  CONSTRAINT `cms_page_ibfk_5` FOREIGN KEY (`_parent`) REFERENCES `cms_page` (`id`),
  CONSTRAINT `cms_page_ibfk_6` FOREIGN KEY (`cms_photogallery_id`) REFERENCES `cms_photogallery` (`id`),
  CONSTRAINT `cms_page_ibfk_7` FOREIGN KEY (`core_language_id`) REFERENCES `core_language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_page` (`id`, `_parent`, `_level`, `_order`, `name`, `heading`, `view`, `homepage`, `showHeading`, `component`, `annotation`, `text`, `url`, `title`, `description`, `keywords`, `delete`, `cms_photogallery_id`, `datePublication`, `showDatePublication`, `dateFrom`, `dateTo`, `_insertUser`, `_insertDate`, `core_language_id`, `template`) VALUES
(1,	NULL,	0,	0,	'Úvod',	'Úvod',	'1',	'1',	'1',	NULL,	'',	'',	'/',	'Úvod',	'Úvod',	'Úvod',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	1,	'2015-05-07 10:35:05',	'cz',	''),
(2,	NULL,	0,	0,	'Magaz',	'Magaz',	'1',	'0',	'1',	NULL,	'',	'',	'/magazin',	'Magaz',	'',	'',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	1,	'2015-07-19 12:26:23',	'cz',	''),
(3,	NULL,	0,	0,	'Podcasty',	'Podcasty',	'1',	'0',	'1',	NULL,	'',	'',	'/podcasty',	'Podcasty',	'',	'',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	1,	'2015-07-19 12:28:20',	'cz',	''),
(4,	NULL,	0,	0,	'Akce',	'Nadpis k akc',	'1',	'0',	'1',	NULL,	'',	'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n',	'/akce',	'Akce',	'',	'',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	1,	'2015-07-19 12:35:27',	'cz',	''),
(5,	NULL,	0,	0,	'Auto',	'Nadpis k autor',	'1',	'0',	'1',	NULL,	'',	'<div class=\"author\">\n<div class=\"image\"><a href=\"#na\" title=\"Milan Gazda\"><img alt=\"Milan Gazda\" src=\"/dixie.cz/www/data/user/pw2/DSC2032-ctverec.jpg\" /></a></div>\n\n<h2><a href=\"/dixie.cz/magazin/autor/milan-gazda\" title=\"Milan Gazda\">Milan Gazda</a></h2>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n\n<p class=\"more\"><a href=\"/dixie.cz/magazin/autor/milan-gazda\" title=\"V',	'/autori',	'Auto',	'',	'',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	1,	'2015-07-19 12:36:18',	'cz',	''),
(6,	NULL,	0,	0,	'Eshop',	'Eshop',	'1',	'0',	'1',	NULL,	'',	'',	'/eshop',	'Eshop | Radiodixie.cz',	'',	'',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	1,	'2015-09-04 22:54:51',	'cz',	'');

DROP TABLE IF EXISTS `cms_page_file`;
CREATE TABLE `cms_page_file` (
  `cms_file_id` int(11) NOT NULL,
  `cms_page_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cms_file_id`,`cms_page_id`),
  KEY `cms_file_id` (`cms_file_id`),
  KEY `cms_page_id` (`cms_page_id`),
  CONSTRAINT `cms_page_file_ibfk_1` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_page` (`id`),
  CONSTRAINT `FK_cms_page_file_cms_file` FOREIGN KEY (`cms_file_id`) REFERENCES `cms_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_page_gallery`;
CREATE TABLE `cms_page_gallery` (
  `cms_gallery_id` int(11) NOT NULL,
  `cms_page_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cms_gallery_id`,`cms_page_id`),
  KEY `cms_gallery_id` (`cms_gallery_id`),
  KEY `cms_page_id` (`cms_page_id`),
  CONSTRAINT `cms_page_gallery_ibfk_1` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_cms_page_gallery_cms_gallery` FOREIGN KEY (`cms_gallery_id`) REFERENCES `cms_gallery` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_photo`;
CREATE TABLE `cms_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_file_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_cms_photo_cms_file_id` (`cms_file_id`),
  KEY `cms_file_id` (`cms_file_id`),
  CONSTRAINT `FK_cms_photo_cms_file` FOREIGN KEY (`cms_file_id`) REFERENCES `cms_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_photogallery`;
CREATE TABLE `cms_photogallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `cms_photogalleryitem`;
CREATE TABLE `cms_photogalleryitem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `file` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `cms_photogallery_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_photogallery_id` (`cms_photogallery_id`),
  CONSTRAINT `cms_photogalleryitem_ibfk_1` FOREIGN KEY (`cms_photogallery_id`) REFERENCES `cms_photogallery` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `cms_text`;
CREATE TABLE `cms_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_text` (`id`, `name`, `text`, `view`, `_insertUser`, `_insertDate`) VALUES
(1,	'Footer - kontakt',	'<p>Z',	'1',	0,	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `core_language`;
CREATE TABLE `core_language` (
  `id` char(2) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `core_language` (`id`, `name`, `active`) VALUES
('cz',	'',	'1'),
('de',	'deutsch',	'0'),
('en',	'english',	'1');

DROP TABLE IF EXISTS `core_settings`;
CREATE TABLE `core_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `defaultTitle` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `titleSuffix` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `defaultKeywords` text COLLATE utf8_czech_ci NOT NULL,
  `defaultDescription` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `core_settings` (`id`, `name`, `email`, `defaultTitle`, `titleSuffix`, `defaultKeywords`, `defaultDescription`) VALUES
(1,	'WeboveReseni.cz',	'info@webovereseni.cz',	'',	'',	'',	'');

DROP TABLE IF EXISTS `core_user`;
CREATE TABLE `core_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_czech_ci NOT NULL,
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `core_user_role_id` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  `annotation` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `core_usergroup_id` (`core_user_role_id`),
  CONSTRAINT `core_user_ibfk_1` FOREIGN KEY (`core_user_role_id`) REFERENCES `core_user_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `core_user` (`id`, `name`, `surname`, `alias`, `username`, `password`, `active`, `photo`, `core_user_role_id`, `delete`, `_insertUser`, `_insertDate`, `annotation`) VALUES
(1,	'Milan',	'Gazda',	'milan-gazda',	'milan.gazda',	'$2y$10$TYOeeQS//6gGWVBQS4c/BuotpRE2kikqkvyg6cl7XU1KUI/0h7O36',	'1',	'DSC2032-ctverec.jpg',	'administrator',	'0',	0,	'2015-07-17 22:28:05',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
(2,	'Milan',	'Gutveis',	'',	'milan.gutveis',	'$2y$10$TYOeeQS//6gGWVBQS4c/BuotpRE2kikqkvyg6cl7XU1KUI/0h7O36',	'1',	'',	'administrator',	'0',	0,	'2015-07-17 22:28:05',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

DROP TABLE IF EXISTS `core_usermodule`;
CREATE TABLE `core_usermodule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `module` varchar(30) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `User_id` (`user_id`),
  CONSTRAINT `core_usermodule_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `core_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `core_usermodule` (`id`, `user_id`, `module`) VALUES
(193,	5,	'cms-page'),
(194,	5,	'cms-photogallery'),
(195,	5,	'cms-textbox'),
(196,	5,	'core-settings'),
(201,	4,	'cms-page'),
(202,	4,	'cms-photogallery'),
(203,	4,	'cms-textbox'),
(204,	4,	'core-settings'),
(209,	1,	'cms-page'),
(210,	1,	'cms-photogallery'),
(211,	1,	'cms-textbox'),
(212,	1,	'core-settings');

DROP TABLE IF EXISTS `core_userwidget`;
CREATE TABLE `core_userwidget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `widget` varchar(30) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `User_id` (`user_id`),
  CONSTRAINT `core_userwidget_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `core_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `core_user_role`;
CREATE TABLE `core_user_role` (
  `id` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `core_user_role` (`id`, `name`, `description`) VALUES
('administrator',	'administr',	'Nejvy'),
('employee',	'zam',	'M'),
('guest',	'host',	'Host - pouze pro prohl'),
('supervisor',	'vedouc',	'Spr');

DROP TABLE IF EXISTS `eshop_category`;
CREATE TABLE `eshop_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_parent` int(10) unsigned NOT NULL,
  `_order` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `showHeading` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `annotation` text COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_category` (`id`, `_parent`, `_order`, `name`, `heading`, `view`, `showHeading`, `delete`, `annotation`, `text`, `url`, `title`, `description`, `keywords`, `template`, `_insertUser`, `_insertDate`) VALUES
(1,	0,	0,	'Trika',	'',	'1',	'0',	'0',	'',	'',	'/trika',	'Trika',	'Trika',	'Trika',	'',	1,	'2015-09-13 03:24:54'),
(2,	0,	0,	'Dopl',	'',	'1',	'0',	'0',	'',	'',	'/doplnky',	'Dopl',	'Dopl',	'Dopl',	'',	1,	'2015-09-13 03:27:42'),
(3,	0,	0,	'Hudba',	'',	'1',	'0',	'0',	'',	'',	'/hudba',	'Hudba',	'Hudba',	'Hudba',	'',	1,	'2015-09-13 03:28:06'),
(4,	0,	0,	'Plak',	'',	'1',	'0',	'0',	'',	'',	'/plakaty',	'Plak',	'Plak',	'Plak',	'',	1,	'2015-09-13 03:29:06');

DROP TABLE IF EXISTS `eshop_category_product`;
CREATE TABLE `eshop_category_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eshop_category_id` int(10) unsigned NOT NULL,
  `eshop_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eshop_category_id` (`eshop_category_id`),
  KEY `eshop_product_id` (`eshop_product_id`),
  CONSTRAINT `eshop_category_product_ibfk_1` FOREIGN KEY (`eshop_category_id`) REFERENCES `eshop_category` (`id`),
  CONSTRAINT `eshop_category_product_ibfk_2` FOREIGN KEY (`eshop_product_id`) REFERENCES `eshop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_category_product` (`id`, `eshop_category_id`, `eshop_product_id`) VALUES
(7,	1,	1),
(8,	1,	2),
(9,	1,	3),
(10,	1,	4);

DROP TABLE IF EXISTS `eshop_delivery`;
CREATE TABLE `eshop_delivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_delivery` (`id`, `name`, `price`, `active`, `_insertUser`, `_insertDate`) VALUES
(1,	'Balík do ruky - ČR',	100.00,	'1',	1,	'2015-05-25 13:41:35'),
(2,	'Balík na poštu',	100.00,	'0',	1,	'2015-05-25 13:41:58'),
(3,	'Geis - ČR',	100.00,	'1',	1,	'2015-05-25 13:42:18'),
(4,	'Osobní odběr',	0.00,	'0',	1,	'2015-05-25 13:42:34'),
(5,	'Balík do ruky - SK',	200.00,	'1',	NULL,	'0000-00-00 00:00:00'),
(6,	'Geis - SK',	200.00,	'1',	NULL,	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `eshop_delivery_payment`;
CREATE TABLE `eshop_delivery_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eshop_delivery_id` int(10) unsigned DEFAULT NULL,
  `eshop_payment_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `eshop_delivery_id` (`eshop_delivery_id`),
  KEY `eshop_payment_id` (`eshop_payment_id`),
  CONSTRAINT `eshop_delivery_payment_ibfk_3` FOREIGN KEY (`eshop_delivery_id`) REFERENCES `eshop_delivery` (`id`) ON DELETE CASCADE,
  CONSTRAINT `eshop_delivery_payment_ibfk_4` FOREIGN KEY (`eshop_payment_id`) REFERENCES `eshop_payment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_delivery_payment` (`id`, `eshop_delivery_id`, `eshop_payment_id`, `name`, `price`) VALUES
(12,	1,	2,	'Převodem',	0.00),
(13,	1,	NULL,	'Dobírka Česká Pošta',	50.00),
(14,	2,	NULL,	'Dobírka Česká Pošta',	50.00),
(15,	2,	2,	'Převodem',	0.00),
(16,	3,	NULL,	'Dobírka Geis',	50.00),
(17,	3,	2,	'Převodem',	0.00),
(18,	4,	1,	'Hotově',	0.00),
(19,	5,	NULL,	'Převodem',	0.00),
(20,	6,	NULL,	'Převodem',	0.00);

DROP TABLE IF EXISTS `eshop_feature`;
CREATE TABLE `eshop_feature` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `search` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_feature` (`id`, `name`, `search`) VALUES
(1,	'velikost',	'0');

DROP TABLE IF EXISTS `eshop_feature_value`;
CREATE TABLE `eshop_feature_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eshop_feature_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eshop_feature_id` (`eshop_feature_id`),
  CONSTRAINT `eshop_feature_value_ibfk_2` FOREIGN KEY (`eshop_feature_id`) REFERENCES `eshop_feature` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_feature_value` (`id`, `eshop_feature_id`, `name`, `alias`) VALUES
(1,	1,	'XS',	'xs'),
(2,	1,	'S',	's'),
(3,	1,	'M',	'm'),
(4,	1,	'L',	'l'),
(5,	1,	'XL',	'xl'),
(6,	1,	'XXL',	'xxl');

DROP TABLE IF EXISTS `eshop_order`;
CREATE TABLE `eshop_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_insertDate` datetime NOT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `zip` char(6) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci,
  `ico` varchar(15) COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(15) COLLATE utf8_czech_ci DEFAULT NULL,
  `companyName` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `companyPerson` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `companyStreet` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `companyCity` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `companyZip` char(6) COLLATE utf8_czech_ci DEFAULT NULL,
  `discountCode` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `eshop_delivery_id` int(10) unsigned NOT NULL,
  `deliveryName` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `deliveryPrice` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `eshop_delivery_payment_id` int(10) unsigned NOT NULL,
  `paymentName` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `paymentPrice` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `itemCount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `priceWithoutVat` decimal(8,2) NOT NULL DEFAULT '0.00',
  `priceWithVat` decimal(8,2) NOT NULL DEFAULT '0.00',
  `totalPrice` decimal(8,2) NOT NULL DEFAULT '0.00',
  `content` text COLLATE utf8_czech_ci NOT NULL,
  `shipped` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `eshop_order_status_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `eshop_delivery_id` (`eshop_delivery_id`),
  KEY `eshop_delivery_payment` (`eshop_delivery_payment_id`),
  KEY `eshop_order_status_id` (`eshop_order_status_id`),
  CONSTRAINT `eshop_order_ibfk_1` FOREIGN KEY (`eshop_delivery_id`) REFERENCES `eshop_delivery` (`id`),
  CONSTRAINT `eshop_order_ibfk_2` FOREIGN KEY (`eshop_delivery_payment_id`) REFERENCES `eshop_delivery_payment` (`id`),
  CONSTRAINT `eshop_order_ibfk_3` FOREIGN KEY (`eshop_delivery_payment_id`) REFERENCES `eshop_delivery_payment` (`id`),
  CONSTRAINT `eshop_order_ibfk_4` FOREIGN KEY (`eshop_order_status_id`) REFERENCES `eshop_order_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `eshop_order_item`;
CREATE TABLE `eshop_order_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eshop_order_id` int(10) unsigned NOT NULL,
  `eshop_product_id` int(10) unsigned NOT NULL,
  `eshop_product_variant_id` int(11) DEFAULT NULL,
  `itemCount` decimal(10,2) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nameVariant` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `priceWithVat` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `priceWithoutVat` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `vat` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eshop_order_id` (`eshop_order_id`),
  KEY `eshop_product_id` (`eshop_product_id`),
  KEY `eshop_product_variant_id` (`eshop_product_variant_id`),
  CONSTRAINT `eshop_order_item_ibfk_1` FOREIGN KEY (`eshop_order_id`) REFERENCES `eshop_order` (`id`),
  CONSTRAINT `eshop_order_item_ibfk_2` FOREIGN KEY (`eshop_product_id`) REFERENCES `eshop_product` (`id`),
  CONSTRAINT `eshop_order_item_ibfk_3` FOREIGN KEY (`eshop_product_variant_id`) REFERENCES `eshop_product_variant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_order_item` (`id`, `eshop_order_id`, `eshop_product_id`, `eshop_product_variant_id`, `itemCount`, `name`, `nameVariant`, `priceWithVat`, `priceWithoutVat`, `vat`, `_insertDate`) VALUES
(1,	1,	3,	NULL,	3.00,	'Triko Mustang P-51D',	NULL,	2070.00,	1710.74,	21.00,	'2015-10-06 14:05:24'),
(2,	1,	1,	NULL,	1.00,	'Triko Rough Rider Sue',	NULL,	599.00,	520.87,	15.00,	'2015-10-06 14:05:24');

DROP TABLE IF EXISTS `eshop_order_status`;
CREATE TABLE `eshop_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nameLong` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_order_status` (`id`, `name`, `nameLong`) VALUES
(-1,	'stornovaná',	''),
(1,	'nepotvrzená',	''),
(2,	'potvrzená',	''),
(3,	'expedováno',	'zboží expedováno / uzavřená');

DROP TABLE IF EXISTS `eshop_payment`;
CREATE TABLE `eshop_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_payment` (`id`, `name`, `price`, `active`, `_insertUser`, `_insertDate`) VALUES
(1,	'Hotově',	0.00,	'1',	1,	'2015-05-25 13:53:10'),
(2,	'Převodem',	0.00,	'1',	1,	'2015-05-25 13:53:24');

DROP TABLE IF EXISTS `eshop_product`;
CREATE TABLE `eshop_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_order` int(10) unsigned NOT NULL,
  `sku` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ean` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `showHeading` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  `delete` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `annotation` text COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `vat` decimal(5,2) NOT NULL DEFAULT '0.00',
  `priceWithVat` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `stock` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `image` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `cms_photogallery_id` int(10) unsigned DEFAULT NULL,
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_photogallery_id` (`cms_photogallery_id`),
  CONSTRAINT `eshop_product_ibfk_1` FOREIGN KEY (`cms_photogallery_id`) REFERENCES `cms_photogallery` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_product` (`id`, `_order`, `sku`, `ean`, `name`, `heading`, `view`, `showHeading`, `delete`, `annotation`, `text`, `url`, `title`, `description`, `keywords`, `template`, `price`, `vat`, `priceWithVat`, `stock`, `image`, `cms_photogallery_id`, `_insertUser`, `_insertDate`) VALUES
(1,	0,	'triko-001',	'',	'Triko Rough Rider Sue',	'Triko Rough Rider Sue',	'1',	'0',	'0',	'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis non massa in laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>\n',	'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis non massa in laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam eu felis sed arcu dignissim pretium vel vel sapien. Aenean elementum tellus eu diam dapibus, nec volutpat odio varius. Nulla ac pellentesque orci. Vestibulum sit amet nibh vel magna consequat ultricies. Cras accumsan auctor neque, id ultrices libero auctor quis. Vestibulum quis nunc vel tellus aliquam interdum. Integer a consequat tortor, in facilisis sem. Suspendisse aliquam efficitur nisl, sit amet hendrerit dui placerat sed.</p>\n\n<p>Phasellus rutrum diam in ullamcorper egestas. Mauris vel mi quis diam feugiat finibus. Aliquam et ultricies justo. Nunc quis finibus felis, non rutrum nisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi ac orci fermentum, porttitor ipsum euismod, suscipit tortor. Quisque aliquam, libero id ultricies semper, enim lectus lobortis elit, at finibus quam nisi ac turpis.</p>\n',	'/triko-rough-rider-sue',	'Triko Rough Rider Sue',	'Triko Rough Rider Sue',	'Triko Rough Rider Sue',	'',	599.00,	15.00,	'1',	92.0000,	'1444130916.jpg',	NULL,	1,	'2015-09-07 12:00:00'),
(2,	1,	NULL,	NULL,	'Triko Ford Mustang Boxx 302 - ',	'',	'1',	'0',	'0',	'',	'',	'/triko-ford-mustang-boxx-302-zlute',	'Triko Ford Mustang Boxx 302 - ',	'Triko Ford Mustang Boxx 302 - ',	'Triko Ford Mustang Boxx 302 - ',	'',	690.00,	21.00,	'1',	8.0000,	NULL,	NULL,	1,	'2015-09-13 04:30:26'),
(3,	2,	NULL,	NULL,	'Triko Mustang P-51D',	'',	'1',	'0',	'0',	'',	'',	'/triko-mustang-p-51d',	'Triko Mustang P-51D',	'Triko Mustang P-51D',	'Triko Mustang P-51D',	'',	690.00,	21.00,	'1',	2.0000,	NULL,	NULL,	1,	'2015-09-13 05:45:33'),
(4,	3,	NULL,	NULL,	'Triko South Will Rise Again 1',	'',	'1',	'0',	'0',	'',	'',	'/triko-south-will-rise-again-1',	'Triko South Will Rise Again 1',	'Triko South Will Rise Again 1',	'Triko South Will Rise Again 1',	'',	790.00,	21.00,	'1',	10.0000,	NULL,	NULL,	1,	'2015-09-13 05:47:19');

DROP TABLE IF EXISTS `eshop_product_variant`;
CREATE TABLE `eshop_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eshop_product_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `size_id` int(10) unsigned DEFAULT NULL,
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `stock` decimal(10,4) unsigned NOT NULL,
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eshop_product_id` (`eshop_product_id`),
  KEY `size` (`size_id`),
  CONSTRAINT `eshop_product_variant_ibfk_1` FOREIGN KEY (`eshop_product_id`) REFERENCES `eshop_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `eshop_product_variant_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `eshop_feature_value` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE `guestbook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `display` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `authorized` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `person` int(10) unsigned NOT NULL,
  `object` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `note` text COLLATE utf8_czech_ci NOT NULL,
  `insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2015-10-06 14:29:52
