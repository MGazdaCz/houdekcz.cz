<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CoreModule\Model\Forms\FormEditorDetail;
use Nette,
    App\Model;


/**
 * Slider Item presenter.
 */
class SlideritemPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Slider
     */
    protected $_repositorySlider;

    protected function startup(){
        parent::startup();

        $this->_mainRepository   = $this->context->getService('cmsSliderItem');
        $this->_repositorySlider = $this->context->getService('cmsSlider');

        $this->template->bodyClass = 'module-slideritem';
        $this->template->sliderId  = $this->getParameter('cms_slider_id');

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Slideritem/form.latte';
    }

    public function renderDefault(){
        $this->redirect(':Admin:Cms:Slider:');
    }

    public function renderAdd() {
        parent::renderAdd();

        $this->template->h1 = 'Nový obrázek slideru';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $menu = $this->_mainRepository->findByPk($id);
        $this->template->sliderId = $menu->cms_slider_id;
    }

    protected function createComponentDefaultForm()
    {
        $sliders = $this->_repositorySlider->fetchPairs('id', 'name', true);

        $form = new FormEditorDetail();
        $form->addHidden('id');
        $form->addSelect('cms_slider_id', 'Slider', $sliders);
        $form->addText('name', 'Název', 80);
        $form->addText('heading', 'Nadpis', 80);
        $form->addCheckbox('view', 'Zobrazit');
        $form
            ->addTextArea('annotation', 'Anotace', 80, 5)
            //->setAttribute('height', '100px')
            ->setAttribute('class', 'ckeditor');
        $form
            ->addUpload('file', 'Obrázek');
            //->addRule(Nette\Application\UI\Form::IMAGE, 'Soubor musí být obrázkem!');
        $form
            ->addText('url', 'Url', 80);
        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        } else {
            $form->setDefaults(array(
                'cms_slider_id' => $this->getParameter('cms_slider_id')
            ));
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // ulozeni obrazku a aktualizace nazvu obrazku
        $this->_saveImage($id, $values);
    }

    /**
     * Metoda se pokusi zpracovat vlozeny obrazek.
     *
     * @param int $id
     * @param object $values
     */
    private function _saveImage($id, &$values){
        /**
         * @var \Nette\Http\FileUpload $file
         */
        $fileName = null;
        $file     = $values->file;



        // zpracovani vlozene fotky
        if ($file->isOk()) {
            // zjisteni pripony a sestaveni nazvu
            $ext      = pathinfo($file->getName(), PATHINFO_EXTENSION);
            $fileName = $id.'.'.$ext;

            // presunuti stahnute fotky
            $file->move(DATA_DIR.DS.'slider'.DS.$fileName);

            // vytvoreni nahledu
            $image = Nette\Utils\Image::fromFile(DATA_DIR.DS.'slider'.DS.$fileName);
            $image->resize(160,120);
            $image->save(DATA_DIR.DS.'slider'.DS.'tn_'.$fileName);

            // nahrazeni souboru za nazev souboru
            $values->file = $fileName;
        } else {
            unset($values['file']);
        }
    }

}