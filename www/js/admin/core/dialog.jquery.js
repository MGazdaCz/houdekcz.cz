// customizace UI pro formulare ... podpora HTML v titlu (hlavicce) formulare
$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
    _title: function(title) {
        if (!this.options.title ) {
            title.html("&#160;");
        } else {
            title.html(this.options.title);
        }
    }
}));
// ---

$(document).ready(function(){

    $("a.mg-ajax").on("click", function(){
        
        $.ajax($(this).attr("href"), {
            async: true,
            success: function(data) {
                // zpracovani popupu
                if (data.popup.content != undefined)
                {
                    $( "#mg-dialog-confirm" ).html(data.popup.content);
                    
                    data.popup.buttons[0].click = function(){
                        $(this).dialog("close");
                    };
                    
                    data.popup.buttons[1].click = function(){
                        $(this).dialog("close");
                    };
                    
                    $( "#mg-dialog-confirm" ).removeClass('hide').dialog(data.popup);
                } // end if
                
                // zpracovani aktualizaci obsahu
                $.each(data.contents, function( index, item ) {
                    $(item.selector).replaceWith(item.content);
                });
                
            }
        });
        
        return false;
    });
    
});