<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Advice Category presenter.
 */
class AdvicecategoryPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\CmsModule\Model\Repository\AdviceCategory
     */
    protected $_repository;

    protected function startup(){
        parent::startup();

        $this->_repository = $this->context->cmsAdviceCategory;

        $this->template->bodyClass = 'module-advice';
    }

    public function renderDefault()
    {
        $this->template->items = $this->_repository->findAll()->order('name');
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Přidání kategorie';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace kategorie: '.$this->_repository->findByPk($id)->name;
    }

    protected function createComponentForm()
    {
        $form = new PageBase();
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_repository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}
