<?php
namespace App\AdminModule\CmsModule\Model\Form;

use Nette\Application\UI\Form;

/**
 * Trida definujici zakladni formular, ktery bude spolecny pro vsechny moduly majici zakladni obsah jako stranky.
 *
 * @package App\CmsModule\Model\Form
 */
class PageBase extends Form {
    const SUBMIT_DEFAULT_LABEL = 'Uložit';

    protected $_languages;
    protected $_templateEnable;

    /**
     * Konstruktor formulare.
     *
     * @param \Nette\ComponentModel\IContainer $parent
     * @param null $name
     */
    public function __construct(\Nette\ComponentModel\IContainer $parent = NULL, $name = NULL){
        parent::__construct();

        $this->_templateEnable = false;
    }

    public function initFormItems(){
        $this->addHidden('id');
        $this
            ->addText('name', '', 80)
            ->setAttribute('placeholder', 'Název')
            ->setRequired('Zadejte název stránky');
        $this
            ->addText('heading', '', 80)
            ->setAttribute('placeholder', 'Nadpis')
            ->setRequired('Zadejte nadpis');
        $this->addCheckbox('view', 'schváleno');
        $this
            ->addCheckbox('showHeading', 'zobrazit nadpis')
            ->setDefaultValue(1);

        if (!empty($this->_languages)) {
            $this->addSelect('core_language_id', 'Jayzková mutace', $this->_languages);
        }

        $this
            ->addTextArea('annotation', 'Anotace', 80, 5)
            //->setAttribute('height', '100px')
            ->setAttribute('class', 'ckeditor');
        $this
            ->addTextArea('text', 'Text', 80, 10)
            ->setAttribute('class', 'ckeditor');

        if ($this->_templateEnable) {
            $this
                ->addText('template', 'Šablona stránky', 80)
                ->setAttribute('placeholder', 'default.latte');
        }

        $this
            ->addText('title', '', 80)
            ->setAttribute('placeholder', 'Titulek');
        $this
            ->addText('url', '', 80)
            ->setAttribute('placeholder', 'URL adresa');
        $this
            ->addTextArea('keywords', '', 80, 5)
            ->setAttribute('placeholder', 'Klíčová slova');
        $this
            ->addTextArea('description', '', 80, 5)
            ->setAttribute('placeholder', 'Meta popisek');

        $this->addSubmit('save', self::SUBMIT_DEFAULT_LABEL);
    }

    /**
     * Metoda nastavi mnozinu jazyku.
     *
     * @param $languages
     */
    public function setLanguages($languages) {
        $this->_languages = $languages;
    }

    /**
     * Metoda umozni vlozit tlacitko pro nastaveni template "stranky", produktu atd.
     *
     * Je nam potom umozneno napr. ze stranky vytvorit cokoli tj. produkt eshopu, atd.
     */
    public function templateEnable() {
        $this->_templateEnable = true;
    }

    /**
     *
     * @return bool
     */
    public function isTemplateEnabled(){
        return $this->_templateEnable;
    }

    /**
     * Metoda nastavi data do values, pokud jsou prazdna.
     *
     * @param $values
     */
    public static function setSeoData(&$values){
        if (empty($values['title'])) {
            $values['title'] = $values['name'];
        }
        if (empty($values['keywords'])) {
            $values['keywords'] = $values['name'];
        }
        if (empty($values['description'])) {
            $values['description'] = $values['name'];
        }
        if (empty($values['url'])) {
            $values['url'] = '/'.\Nette\Utils\Strings::webalize($values['name']);
        }
    }

}