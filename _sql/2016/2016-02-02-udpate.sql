SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+01:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `eshop_cis_unit`;
CREATE TABLE `eshop_cis_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_cis_unit` (`id`, `name`) VALUES
(1,	'ks'),
(2,	'bal'),
(3,	'kg');

ALTER TABLE eshop_product ADD COLUMN eshop_cis_unit_id int(11) NOT NULL;

ALTER TABLE `eshop_product` ADD FOREIGN KEY (`eshop_cis_unit_id`) REFERENCES `eshop_cis_unit` (`id`) ON DELETE NO ACTION ON UPDATE RESTRICT;

UPDATE `eshop_product` SET eshop_cis_unit_id = 1;