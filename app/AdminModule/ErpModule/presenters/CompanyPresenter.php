<?php

namespace App\AdminModule\ErpModule;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;

/**
 * Guestbook presenter.
 */
class CompanyPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    protected function startup(){
        parent::startup();
        $this->_mainRepository = $this->context->companyRepository;
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        $form->addGroup('Základní informace');
        $form->addText('name', 'Název firmy');

        $form->addGroup('Kontaktní osoba');
        $form->addText('contactPerson', 'Jméno a příjmení');
        $form->addText('contactEmail', 'Email');
        $form->addText('contactPhone', 'Telefon');

        $form->addGroup('Fakturační údaje');
        $form->addText('faName', 'Název');
        $form->addText('faStreet', 'Ulice');
        $form->addText('faCity', 'Město');
        $form->addText('faZip', 'PSČ');
        $form->addText('ico', 'IČO');
        $form->addText('dic', 'DIČ');

        $form->addGroup('Rozšířené informace');
        $form->addText('insertDate', 'Datum vložení')
            ->setAttribute('class', 'datepicker');
        $form->addTextArea('note', 'Poznámka');

        $form->addSubmit('submit', 'Uložit');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_mainRepository->findByPk($this->_itemId);
            $itemArray = $item->toArray();

            // osetreni formatu datumu
            $insertDate = new \Nette\DateTime($itemArray['insertDate']);
            $itemArray['insertDate'] = $insertDate->format('d.m.Y');
            // ---

            $form->setDefaults($itemArray);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni formatu datumu
        $values = (array)$form->values;

        $insertDate = new \Nette\DateTime($values['insertDate']);
        $values['insertDate'] = $insertDate->format('Y-m-d H:i:s');
        // ---

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    public function renderDefault()
    {
        $this->template->reservationConfig = $this->context->parameters['reservation'];
        $this->template->items             = $this->_mainRepository->getTable()->order('name');
        
        $this->template->projectRepo = $this->context->projectRepository;
    }

}
