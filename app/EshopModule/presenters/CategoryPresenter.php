<?php

namespace App\EshopModule\Presenters;

use Nette;


/**
 * Category presenter.
 */
class CategoryPresenter extends BasePresenter
{
    const PAGE_LIMIT = 12;

    protected function startup(){
        parent::startup();

        $this->template->bodyClass = 'eshop-category';
    }

    /**
     * Metoda pro renderovani homepage.
     */
    public function renderDefault()
	{
        $url  = $this->getParameter('url');
        $page = $this->getParameter('page', 1);

        if (!empty($url)) {
            $url          = $url;
            $categoryRepo = $this->context->getService('eshopCategory');

            $category = $categoryRepo->findBy(array('url' => $url, 'view' => 1, 'delete' => 0))->fetch();

            if ($category) {
                $this->template->text = 'V kategorii '.Nette\Utils\Strings::lower($category->name).' najdete tyto produkty:';

                // hledani vsech podkategorii
                // TODO - toto by slo mozna cachovat k jednotlivym kategoriim produktu?!
                $childrens = $this->context->getService('eshopCategory')->getAllChildrenCategories($category->id);
                $childrens[] = $category->id;

                // nacteni poctu produktu v kategorii
                $count =  $this->template->products = $this->context->getService('eshopProduct')->findBy(array(
                    'id IN (SELECT eshop_product_id FROM eshop_category_product WHERE eshop_category_id IN ?)' => $childrens,
                    'view' => 1,
                    'delete' => 0
                ))->count('id');

                // nacteni produktu v podkategoriich
                $this->template->products = $this->context->getService('eshopProduct')->findBy(array(
                    'id IN (SELECT eshop_product_id FROM eshop_category_product WHERE eshop_category_id IN ?)' => $childrens,
                    'view' => 1,
                    'delete' => 0
                ))->order('price')->limit(12, ($page - 1) * self::PAGE_LIMIT);

                $this->template->title       = $category->title;
                $this->template->description = $category->description;
                $this->template->keywords    = $category->keywords;
                $this->template->category    = $category;

                // nastaveni paginatoru
                $paginatorSettings = new \stdClass();
                $paginatorSettings->limit = self::PAGE_LIMIT;
                $paginatorSettings->page  = $page;
                $paginatorSettings->count = $count;

                $this->template->paginator = $paginatorSettings;
                // ---
            } else {
                throw new \Nette\Application\BadRequestException('Kategorie nenalezena.', 404);
            }
        } else {
            throw new \Nette\Application\BadRequestException('Kategorie nenalezena.', 404);
        }
	}

    /*********************************************
     *      Registrace komponent
     ********************************************/

}
