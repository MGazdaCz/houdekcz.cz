<?php
namespace App\EshopModule\Control\Product;

use Nette\Application\UI\Control;
use App\EshopModule\Model\Product;
use Nette\Database\Table\ActiveRow;

class AddProductControl extends Control
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_productRepository;

    public function render($productId)
    {
        $this->_productRepository = $this->presenter->context->getService('eshopProduct');
        //$this->_cart              = $this->getParent()->context->cart;
        //$this->template->cart     = $this->_cart;

        // nacteni detailu o produktu
        $productActiveRow = $this->_productRepository->findByPk($productId);

        // inicializace produktu do sablony
        $this->template->product = null;
        if (!is_null($productActiveRow)) {
            $this->template->product  = Product::getFromActiveRow($productActiveRow);;
            $this->template->variants = $productActiveRow->related('eshop_product_variant.eshop_product_id')->where(array('view' => 1));
        } else {
            throw new \Exception('Chyba - produkt s ID = '.$productId.' nenalezen');
        }

        // init a renderovani sablony
        $this->template->setFile(__DIR__.'/templates/addProduct.latte');
        $this->template->render();
    }

    /**
     * Renderuje tlacitka z aktivniho zaznamu.
     *
     * Optimalizace rychlosti!!!
     *
     * @param ActiveRow $productActiveRow
     */
    public function renderFromActiveRow(ActiveRow $productActiveRow, $template = 'addProduct.latte')
    {
        // inicializace produktu do sablony
        $this->template->productActiveRow = $productActiveRow;
        $this->template->product          = Product::getFromActiveRow($productActiveRow);
        $this->template->variants         = $productActiveRow->related('eshop_product_variant.eshop_product_id')->where(array('view' => 1));

        // init a renderovani sablony
        $this->template->setFile(__DIR__.'/templates/'.$template);
        $this->template->render();
    }

}