<?php
namespace App\AdminModule\CoreModule\Component;

use \Nette\Application\UI\Control;

class YesNo extends Control
{
    public function render($value)
    {
        $this->template->spanText  = 'ne';
        $this->template->spanClass = 'red';
        if ($value)
        {
            $this->template->spanText  = 'ano';
            $this->template->spanClass = 'green';
        } // end if
        
        $this->template->setFile(__DIR__.'/templates/yesNo.latte');
        $this->template->render();
    }

} // end class