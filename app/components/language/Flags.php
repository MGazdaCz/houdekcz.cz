<?php
namespace FrontModule\Control\Language;

use Control;
use FrontModule\Model\Language\Flags;

class FlagsControl extends Control\AbstractControl
{
    public function render()
    {
        /** @var \AdminModule\CoreModule\Model\Repository\CoreLanguage $languageRepository **/
        $flags = new Flags();
        $flags->setLanguageRepostiory($this->parent->context->coreLanguageRepository);
        $flags->setPageRepostiory($this->parent->context->cmspageRepository);
        $flags->loadLanguages();

        // samotne renderovani
        $this->template->flags = $flags;
        $this->template->setFile(__DIR__.'/flags.latte');
        $this->template->render();
    }

} // end class