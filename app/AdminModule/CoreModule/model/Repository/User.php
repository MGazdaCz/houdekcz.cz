<?php
namespace App\AdminModule\CoreModule\Model\Repository;
use Nette;

class User extends Repository
{
    protected $_table = 'core_user';
    
    /**
     * Metoda nacte uzivatele podle uzivatelskeho jmena,
     * @param string $username
     * @return Nette\Database\Table\ActiveRow
     */
    public function findByUsername($username){
        return $this->findAll()->where('username', $username)->fetch();
    }
}

