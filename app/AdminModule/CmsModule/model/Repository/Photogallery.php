<?php
namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository;
use Nette;

class Photogallery extends Repository
{
    protected $_table = 'cms_photogallery';
}