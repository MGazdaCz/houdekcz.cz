<?php
namespace Control\ReservationList;

use Control,
    Model\Helper,
    \Nette\Application\UI\Form,
    \Nette\Mail\Message;
use Model\Form\FormPara;
use \Model\Calendar\Calendar;
use \Model\Reservation\Reservation;

class ReservationListControl extends Control\AbstractControl
{
    private $_reservationRepository;

    public function render()
    {
        //$this->template->rows = $this->_context->guestbookRepository->findBy(array('display' => 1));
        $this->_reservationRepository = $this->_context->reservationRepository;

        $reservationConfig = $this->_context->parameters['reservation'];

        // nastaveni parametru (i z filtru)
        //$year = date('Y');
        $year = $reservationConfig['defaultYear'];

        $object = 'apartman-a';
        if (isset($this->presenter->params['object']))
            $object = $this->presenter->params['object'];
        // ---

        // sestaveni seznamu rezervovanych dni
        $reservation = new Reservation;
        $reservation->setRepository($this->_reservationRepository);
        $reservedDays = $reservation->getReservedDays($object, $year);

        $this->template->object        = $object;
        $this->template->objectName    = $reservationConfig['object']['items'][$object];
        $this->template->objectName{0} = strtolower($this->template->objectName{0});
        $this->template->year          = $year;
        $this->template->reserverDays  = $reservedDays;
        $this->template->monthsData    = array();
        $this->template->months        = $this->_context->parameters['months']['small'];
        $this->template->days          = $this->_context->parameters['days']['short'];

        for ($i=1;$i<=12;$i++)
        {
            $this->template->monthsData[$i] = Calendar::getMonthMatrix($i, $year);
        } // end for

        $this->template->setFile(__DIR__.'/list.latte');
        $this->template->render();
    }

    /**
     * Metoda registruje komponentu pro filtr nad kalendarem.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentCalendarFilterForm($name)
    {
        $reservationConfig = $this->_context->parameters['reservation'];

        $object = '';
        if (isset($this->presenter->params['object']))
            $object = $this->presenter->params['object'];

        $form = new FormPara($this, $name);
        $form->setMethod(Form::GET);
        //$form->setAction($this->_url);

        $form->addHidden('backlink', $this->_url);
        $form->addSelect('object', $reservationConfig['object']['name'], $reservationConfig['object']['items'])->setDefaultValue($object);
        $form->addSubmit('submit', 'Změnit');

        $form->onSuccess[] = callback($this, 'processCalendarFilterForm');

        //echo '<pre>'.$this->_context->httpRequest->url.'</pre>';
        //die;

        return $form;
    }

    public function processCalendarFilterForm(Form $form){
        // potrebuju URL bez parametru
        $url = $form->values->backlink;
        if (strpos($url, '?') !== false)
        {
            $url = substr($url, 0, strpos($url, '?'));
        } // end if

        //die($url);

        header('Location: '.$url.'?object='.$form->values->object);
        return;
    }

} // end class