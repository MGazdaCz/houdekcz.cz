SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+02:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

-- implementace rozbalovaciho menu
ALTER TABLE `cms_menu_item` ADD `dropdownContent` text COLLATE 'utf8_czech_ci' NULL AFTER `newWindow`;
ALTER TABLE `cms_menu_item` ADD `dropdownEnable` char(1) COLLATE 'utf8_czech_ci' NULL DEFAULT '0' AFTER `newWindow`;


-- implementace textu do paticky
DROP TABLE IF EXISTS `cms_text`;
CREATE TABLE `cms_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_text` (`id`, `name`, `text`, `view`, `_insertUser`, `_insertDate`) VALUES
  (1,	'Footer - kontakt',	'<p>Rychnov nad Kněžnou, 516 01, Pod Budínem 1221</p>\n<p><a href=\"tel:+420602712603\" title=\"Telefonické objednávky\">Tel. 602 712 603</a></p>\n<p><a href=\"mailto:velkoobchod.houdek@seznam.cz\" title=\"Dotazy\">E-mail: velkoobchod.houdek@seznam.cz</a></p>\n',	'1',	0,	'0000-00-00 00:00:00'),
  (2,	'Footer - důležité informace',	'<ul class=\"footer-menu\">\n                    <li class=\"footer-menu__item\"><a title=\"Vše o nákupním procesu\" href=\"/houdekcz.cz/obchodni-podminky\">Vše o nákupu</a></li>\n                    <li class=\"footer-menu__item\"><a title=\"Benefity\" href=\"/houdekcz.cz/kontakt\">Naše výhody</a></li>\n                    <li class=\"footer-menu__item\"><a title=\"Tvorba internetových obchodů\" href=\"http://www.webovereseni.cz\">Autoři e-shopu</a></li>\n                </ul>',	'1',	1,	'2016-08-03 10:07:49'),
  (3,	'Footer - možnosti platby',	'<ul class=\"footer-menu\">\n                    <li class=\"footer-menu__item\">Platba na dobírku</li>\n                    <li class=\"footer-menu__item\">Platba hotově</li>\n                    <li class=\"footer-menu__item\">Pltaba předem bankovním převodem</li>\n                </ul>',	'1',	1,	'2016-08-03 10:08:23');


-- implementace nejnovejsich slideru
DROP TABLE IF EXISTS `cms_slider`;
CREATE TABLE `cms_slider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `_insertUser` int(10) unsigned DEFAULT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `_insertUser` (`_insertUser`),
  CONSTRAINT `cms_slider_ibfk_1` FOREIGN KEY (`_insertUser`) REFERENCES `core_user` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_slider` (`id`, `name`, `_insertUser`, `_insertDate`) VALUES
  (1,	'Slider na HP',	1,	'2016-03-08 23:18:05');

DROP TABLE IF EXISTS `cms_slider_item`;
CREATE TABLE `cms_slider_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cms_slider_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `view` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `annotation` text COLLATE utf8_czech_ci,
  `file` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_slider_id` (`cms_slider_id`),
  KEY `_insertUser` (`_insertUser`),
  CONSTRAINT `cms_slider_item_ibfk_1` FOREIGN KEY (`cms_slider_id`) REFERENCES `cms_slider` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cms_slider_item_ibfk_2` FOREIGN KEY (`_insertUser`) REFERENCES `core_user` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `cms_slider_item` (`id`, `cms_slider_id`, `name`, `heading`, `view`, `annotation`, `file`, `url`, `_insertUser`, `_insertDate`) VALUES
  (1,	1,	'HoudekCz.cz',	'',	'1',	'',	'1.jpg',	'http://www.houdekcz.cz',	1,	'2016-03-08 23:44:49');

-- 2016-08-03 10:34:53
