<?php

namespace App\AdminModule\ErpModule\ReservationModule;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;
//use \Control\Calendar;
use \Model\Calendar\Calendar;
use \Model\Reservation\Reservation;
use \Model\Form\FormPara;
use Nette\Config\Configurator;
use Nette\DateTime;
use Nette\Utils\Neon;

//use Nette\Config\Configurator;

/**
 * Guestbook presenter.
 */
class ReservationPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    protected $_itemId;
    protected $_reservationRepository;

    protected function startup(){
        parent::startup();
        $this->_reservationRepository = $this->context->reservationRepository;
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentReservationForm($name)
    {
        $reservationConfig = $this->context->parameters['reservation'];

        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        $form->addText('dateFrom', $reservationConfig['date']['from'])
            ->setAttribute('class', 'datepicker');
        $form->addText('dateTo', $reservationConfig['date']['to'])
            ->setAttribute('class', 'datepicker');
        $form->addText('name', 'Jméno a příjmení')
            ->addRule(Form::MIN_LENGTH, 'Zadejte prosím jméno a příjmení (minimální délka 5 znaků).', 5);
        $form->addCheckbox('authorized', 'schválená rezervace');

        if ($reservationConfig['person'] == true)
        {
            $form->addText('person', 'Počet osob')
                ->addRule(Form::INTEGER, 'Zadejte prosím počet osob celým číslem.');
        } // end if

        $form->addSelect('object', $reservationConfig['object']['name'], $reservationConfig['object']['items']);
        $form->addText('phone', 'Telefon')
            ->setRequired('Zadejte prosím telefonní číslo.');
        $form->addText('email', 'E-mail')
            ->addRule(Form::EMAIL, 'Zadejte prosím email ve správném tvaru.');
        $form->addTextArea('note', 'Poznámka');
        $form->addSubmit('submit', 'Odeslat rezervaci');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_reservationRepository->findByPk($this->_itemId);

            // osetreni formatu datumu
            $dateFrom = new \Nette\DateTime($item->dateFrom);
            $dateTo   = new \Nette\DateTime($item->dateTo);
            $item->dateFrom = $dateFrom->format('d.m.Y');
            $item->dateTo   = $dateTo->format('d.m.Y');
            // ---

            $form->setDefaults($item);
        } // end if

        $form->onSuccess[] = $this->reservationFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function reservationFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni formatu datumu
        $values = (array)$form->values;

        $dateFrom = new \Nette\DateTime($values['dateFrom']);
        $dateTo   = new \Nette\DateTime($values['dateTo']);
        $values['dateFrom']   = $dateFrom->format('Y-m-d H:i:s');
        $values['dateTo']     = $dateTo->format('Y-m-d H:i:s');
        $values['insertDate'] = date('Y-m-d H:i:s');
        // ---

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_reservationRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $this->_reservationRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    /**
     * Metoda registruje komponentu pro filtr nad kalendarem.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentCalendarFilterForm($name)
    {
        $reservationConfig = $this->context->parameters['reservation'];

        $years = array();
        for ($i=date('Y')-1;$i<=date('Y')+2;$i++)
            $years[$i] = $i;

        $actualYear = date('Y');
        if (isset($this->params['year']))
            $actualYear = $this->params['year'];

        $object = '';
        if (isset($this->params['object']))
            $object = $this->params['object'];

        $form = new FormPara($this, $name);
        $form->setMethod(Form::GET);

        $form->addSelect('object', $reservationConfig['object']['name'], $reservationConfig['object']['items'])->setDefaultValue($object);
        $form->addSelect('year', 'Rok', $years)->setDefaultValue($actualYear);
        $form->addSubmit('submit', 'Změnit');

        return $form;
    }

    /*
    protected function createComponentCalendar($name){
        return new Calendar\CalendarControl($this, $name);
    }

    protected function createComponentMonth($name){
        return new Calendar\MonthControl($this, $name);
    }
    */

    public function renderDefault()
    {
        $this->template->reservationConfig = $this->context->parameters['reservation'];
        $this->template->items             = $this->_reservationRepository->getTable()->order('dateFrom, dateTo');

        //$p = new Page\Page();
    }

    public function renderEdit($id){
        $this->_itemId = intval($id);
    }

    public function renderDelete($id){
        $this->_reservationRepository->delete(array('id' => $id));
        return $this->redirect('default');
    }

    public function renderCalendar()
    {
        // nastaveni parametru (i z filtru)
        $year = date('Y');
        if (isset($this->params['year']))
            $year = $this->params['year'];

        $object = 'apartman-a';
        if (isset($this->params['object']))
            $object = $this->params['object'];
        // ---

        // sestaveni seznamu rezervovanych dni
        $reservation = new Reservation;
        $reservation->setRepository($this->_reservationRepository);
        $reservedDays = $reservation->getReservedDays($object, $year);

        $reservationConfig = $this->context->parameters['reservation'];

        $this->template->object        = $object;
        $this->template->objectName    = $reservationConfig['object']['items'][$object];
        $this->template->objectName{0} = strtolower($this->template->objectName{0});
        $this->template->year          = $year;
        $this->template->reserverDays  = $reservedDays;
        $this->template->monthsData    = array();
        $this->template->months        = $this->getContext()->parameters['months']['small'];
        $this->template->days          = $this->getContext()->parameters['days']['short'];

        for ($i=1;$i<=12;$i++)
        {
            $this->template->monthsData[$i] = Calendar::getMonthMatrix($i, $year);
        } // end for

        if ($this->isAjax())
        {
            // prerenderovani ajaxem -> pouze snippetu months
            $this->invalidateControl('months');
        } // end if
    }

    public function handleSetdate(){
        $object   = $this->params['object'];
        $date     = $this->params['date'];
        $dtDate   = new DateTime($date);
        $datetime = $dtDate->format('Y-m-d H:i:s');

        $reservation = new Reservation;
        $reservation->setRepository($this->_reservationRepository);

        if ($reservation->isReserved($object, $datetime))
            $reservation->removeSystemReserve($object, $datetime);
        else
            $reservation->reserveAsSystem($object, $datetime);

        if ($this->isAjax())
        {
            // prerenderovani ajaxem
            $this->params['year'] = $dtDate->format('Y');
            return $this->renderCalendar();
        } // end if

        return $this->redirect('calendar?object='.$object.'&year='.$dtDate->format('Y'));
    }

}
