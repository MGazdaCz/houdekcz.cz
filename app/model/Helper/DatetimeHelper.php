<?php
namespace Model\Helper;

use Nette;

class DatetimeHelper extends Nette\Object
{
    const SPECIAL = '%';

    const DEN = 'd'; // 01 .. 31
    const DEN_KRATKY = 'j'; // 0 .. 31
    const DEN_NAZEV = 'l'; // pondělí ... neděle
    const DEN_NAZEV_KRATKY = 'D'; // po .. ut

    const MESIC = 'm'; // 01 .. 12
    const MESIC_KRATKY = 'n'; // 1 .. 12
    const MESIC_NAZEV = 'F'; // leden .. prosinec

    const ROK = 'Y'; // 1980 .. 2020
    const ROK_KRATKY = 'y'; // 80 .. 20

    const HODINA = 'H'; // 00 .. 23
    const HODINA_KRATKY = 'G'; // 0 .. 23
    const MINUTA = 'i'; // 00 .. 59
    const SEKUNDA = 's'; // 00 .. 59

    private $datum;
    private $datumArray;
    private $timestamp;
    private $data;
    private $format;

    /**
     * Konstruktor tridy.
     *
     * @author MiG
     * @param date | datetime $datum
     * @return void
     * @since 2011-
     */
    function __construct($datum)
    {
        $this->datum = $datum;
        $this->datumArray = array();
        $this->data = array();

        $this->initFormat()
            ->datumExplode()
            ->initTimestamp()
            ->prepareData();

        return;
    } // end function

    /**
     * Metoda se pokusi zjistit, v jakem formatu je zadane datum v konstruktoru.
     *
     * @author MiG
     * @param void
     * @return Helper_Datum
     * @since 2011-
     */
    private function initFormat()
    {
        if (preg_match('/[a-zA-Z]{3}, \d{1,2} [a-zA-Z]{3} \d{4} \d{2}:\d{2}:\d{2}/', $this->datum))
            $this->format = 'RFC 822';
        elseif (strlen($this->datum) == 19)
            $this->format = 'datetime';
        elseif (strlen($this->datum) == 16 && strpos($this->datum, '.') !== false)
            $this->format = 'datetimeuser';
        elseif (strlen($this->datum) >= 9 && strlen($this->datum) <= 10 && strpos($this->datum, '.') === false)
            $this->format = 'date';
        elseif (strlen($this->datum) == 10 && strpos($this->datum, '.') !== false)
            $this->format = 'dateuser';

        return $this;
    } // end function

    /**
     * Metoda rozseka datum do pole.
     *
     * @author MiG
     * @param void
     * @return Helper_Datum
     * @since 2011-
     */
    private function datumExplode()
    {
        $datum = $this->datum;

        if ($this->format == 'RFC 822') {
            $datumTmp = date('Y-m-d H:i:s', strtotime($datum));
            $datumTmp = explode(' ', $datumTmp);

            $this->datumArray = array_merge($this->datumArray, explode('-', $datumTmp[0]));
            $this->datumArray = array_merge($this->datumArray, explode(':', $datumTmp[1]));
        } elseif ($this->format == 'datetime') {
            $datumTmp = explode(' ', $datum);

            $this->datumArray = array_merge($this->datumArray, explode('-', $datumTmp[0]));
            $this->datumArray = array_merge($this->datumArray, explode(':', $datumTmp[1]));
        } elseif ($this->format == 'datetimeuser') {
            $datumTmp = explode(' ', $datum);

            $this->datumArray = array_merge($this->datumArray, array_reverse(explode('.', $datumTmp[0])));
            $this->datumArray = array_merge($this->datumArray, explode(':', $datumTmp[1]));
        } elseif ($this->format == 'date') {
            $this->datumArray = explode('-', $datum);
        } elseif ($this->format == 'dateuser') {
            $this->datumArray = explode('.', $datum);
            $this->datumArray = array_reverse($this->datumArray);
        } // end if

        return $this;
    } // end function

    /**
     * Metoda vytvori z pole casove razitko.
     *
     * @author MiG
     * @param void
     * @return Helper_Datum
     * @since 2011-
     */
    private function initTimestamp()
    {
        $this->timestamp = mktime(
            @intval($this->datumArray[3]),
            @intval($this->datumArray[4]),
            @intval($this->datumArray[5]),
            @intval($this->datumArray[1]),
            @intval($this->datumArray[2]),
            @intval($this->datumArray[0])
        );

        return $this;
    } // end function

    /**
     * Metoda pripravi data pro generator.
     *
     * @author MiG
     * @param void
     * @return Helper_Datum
     * @since 2011-
     */
    private function prepareData()
    {
        /**
         * 2011-08-24 14:02 - MiG - doplneno pouzite funkce NVL pro umozneni pouziti helperu v editoru.
         */
        /*
        $daysLong = nvl($GLOBALS['days']);
        if (isset($GLOBALS['days_long']))
            $daysLong = $GLOBALS['days_long'];
        */

        $this->data = array(
            self::DEN => date(self::DEN, $this->timestamp),
            self::DEN_KRATKY => date(self::DEN_KRATKY, $this->timestamp),
            //self::DEN_NAZEV => nvl($GLOBALS['days_long'][date('N', $this->timestamp)]),
            //self::DEN_NAZEV_KRATKY => nvl($GLOBALS['days'][date('N', $this->timestamp)]),

            self::MESIC => date(self::MESIC, $this->timestamp),
            self::MESIC_KRATKY => strtolower(date(self::MESIC_KRATKY, $this->timestamp)),
            //self::MESIC_NAZEV => strtolower(nvl($GLOBALS['months'][date('n', $this->timestamp)])),

            self::ROK => date(self::ROK, $this->timestamp),
            self::ROK_KRATKY => date(self::ROK_KRATKY, $this->timestamp),

            self::HODINA => date(self::HODINA, $this->timestamp),
            self::HODINA_KRATKY => date(self::HODINA_KRATKY, $this->timestamp),
            self::MINUTA => date(self::MINUTA, $this->timestamp),
            self::SEKUNDA => date(self::SEKUNDA, $this->timestamp),
        );

        /*
        // MiG - fix pro nahrazovani velkych ceskych pismen
        $this->data[self::MESIC_KRATKY] = str_replace(array('Č', 'Ř'), array('č', 'ř'), $this->data[self::MESIC_KRATKY]);
        $this->data[self::MESIC_NAZEV] = str_replace(array('Č', 'Ř'), array('č', 'ř'), $this->data[self::MESIC_NAZEV]);
        // ---
        */

        return $this;
    } // end function

    /**
     * Metoda provede formatovani datumu dle zadaneho formatu.
     *
     * @author MiG
     * @param string $format
     * @return string
     * @since 2011-
     */
    public function Format($format)
    {
        $out = $format;
        foreach ($this->data as $key => $data) {
            $out = str_replace(self::SPECIAL . $key, $data, $out);
        } // end foreach

        return $out;
    } // end function

    /**
     * Funkce vrati casove razitko vytvorene funkci mktime
     *
     * @author MiG
     * @param void
     * @return int
     * @since 2011-
     */
    function GetTimeStamp()
    {
        return $this->timestamp;
    } // end function

} // end class