<?php

namespace App\AdminModule\CoreModule\Presenters;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;

/**
 * Homepage presenter.
 */
class UsergroupPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();
        $this->_mainRepository = $this->context->coreUsergroupRepository;

        $this->_listH1       = 'Skupiny uživatelů';
        $this->_orderDefault = 'id';
    }

    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);
        //$form->addHidden('id');
        $form->addText('id', 'ID', 50);
        $form->addText('name', 'Název', 50);
        $form->addTextArea('description', 'Popis skupiny', 50, 5);
        $form->addSubmit('submit', 'Ulož');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && !empty($this->_itemId))
        {
            $user = $this->_mainRepository->findByPk($this->_itemId);
            $form->setDefaults($user);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        if (isset($requestParams['id']) && !empty($requestParams['id']))
        {
            $this->_mainRepository->update($form->values, array('id' => $requestParams['id']));
        }
        else
        {
            $this->_mainRepository->insert($form->values);
        } // end if

        return $this->redirect('default');
    }

}
