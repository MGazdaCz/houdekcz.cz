<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;
use App\AdminModule\CoreModule\Model\Forms\Controls\ImageInput;


/**
 * Text presenter.
 */
class ProjectPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsProject');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Projekty';
        $this->_orderDefault = 'id';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace projektu: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $form = new Nette\Application\UI\Form();

        // musim nastavit manualne, protoze nevim, jak to nastavit automaticky pri pridani custom fieldu "ImageInput"
        $form->elementPrototype->enctype = 'multipart/form-data';

        $form->addHidden('id');
        $form
            ->addText('name', 'Název', 80)
            ->setAttribute('placeholder', 'Název projektu')
            ->setRequired('Zadejte název projektu');
        $form
            ->addCheckbox('view', 'zobrazit');
        $form
            ->addTextArea('annotation', 'Anotace', 80, 5)
            //->setAttribute('height', '100px')
            ->setAttribute('class', 'ckeditor');
        $form
            ->addText('area', 'Plocha', 10)
            //->setAttribute('placeholder', 'Plocha')
            ->setValue(0)
            ->setRequired('Zadejte plochu projektu');
        $form
            ->addText('price', 'Cena', 10)
            //->setAttribute('placeholder', 'Plocha')
            ->setValue(0)
            ->setRequired('Zadejte cenu projektu');

        $form['previewImage']  = new ImageInput('previewImage', 'Náhled');
        $form['platformImage'] = new ImageInput('platformImage', 'Půdorys');

        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            $form['previewImage']
                //->setDeleteUrl('/admin/cms/project/deleteimage/'.$item->id)
                ->setPathDir(WWW_DIR.'/data/project/pw1')
                ->setPathWww(WWW_WWW.'/data/project/pw1');
            $form['platformImage']
                //->setDeleteUrl('/admin/cms/project/deleteimage/'.$item->id)
                ->setPathDir(WWW_DIR.'/data/project/pw1')
                ->setPathWww(WWW_WWW.'/data/project/pw1');

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // osetreni vkladani plochy a cen
        $values['area'] = str_replace(',', '.', $values['area']);
        $values['price'] = str_replace(',', '.', $values['price']);

        try {
            $this->_uploadFile($id, $values, 'previewImage');
            $this->_uploadFile($id, $values, 'platformImage');
        }
        catch (\Exception $e)
        {
            $this->flashMessage($e->getMessage(), 'error');
            return $this->redirect('edit', array('id' => $id));
        } // end try/catch
    }

    private function _uploadFile($id, &$values, $fileKey) {
        // generovani nahledu fotky
        $file = $values[$fileKey];
        unset($values[$fileKey]);

        /*
        echo '<pre>'.print_r($file, true).'</pre>';
        die;
        */

        $values[$fileKey] = $file->getSanitizedName();
        $file->generatePreviews(array(
            'dir' => WWW_DIR.'/data/project',
            'previews' => array(
                'p1' => array(
                    'directory' => 'pw1',
                    'x' => 335,
                    'y' => 0,
                ),
                'p2' => array(
                    'directory' => 'pw2',
                    'x' => 640,
                    'y' => 480,
                )
            )
        ));
    }

}
