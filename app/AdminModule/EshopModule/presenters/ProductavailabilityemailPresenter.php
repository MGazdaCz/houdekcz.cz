<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Product Availability Email presenter.
 */
class ProductavailabilityemailPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_productRepo;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\ProductVariant
     */
    protected $_variantRepo;

    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('eshopProductAvailableEmail');
        $this->_variantRepo    = $this->context->getService('eshopProductVariant');
        $this->_productRepo    = $this->context->getService('eshopProduct');

        $this->template->bodyClass = 'module-product-availableEmail';
        //$this->template->productId = $this->getParameter('productId');
        //$this->template->product   = $this->_productRepo->findBy(array('id' => $this->template->productId))->fetch();

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Productavailabilityemail/form.latte';
    }

    public function renderDefault()
    {
        $this->template->h1 = 'Dotazy k dostupnosti produktů';
        $this->template->items = $this->_mainRepository->findAll()->order('id');
    }

    /*
    public function renderAdd()
    {
        $this->setView('form');
        $this->template->h1 = 'Nová varianta k produktu '.$this->template->product->name;
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $item = $this->_mainRepository->findByPk($id);

        $this->template->id        = $id;
        $this->template->h1        = 'Editace varianty: '.$item->name;
        $this->template->variant   = $item;
        $this->template->productId = $item->eshop_product_id;
    }

    protected function createComponentForm()
    {
        $products = $this->_productRepo->fetchPairs('id', 'name');
        $sizes    = $this->_featureValueRepo->fetchPairsWithWhere('id', 'name', array('eshop_feature_id' => 1), true);

        $form = new Nette\Application\UI\Form();

        $form->addHidden('id');
        $form->addSelect('eshop_product_id', 'Produkt', $products);
        $form->addText('name', 'Název', 50);
        $form->addTextArea('description', 'Popis', 50, 5);
        $form->addCheckbox('view', 'zobrazit');
        $form->addSelect('size_id', 'Velikost', $sizes);
        $form->addText('price', 'Cena', 10);
        $form->addText('stock', 'Sklad', 10);

        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);

        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {

                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        } else {
            $form->setDefaults(array(
                'eshop_product_id' => $this->template->productId,
                'price' => '0.00',
                'stock' => '0.00',
            ));
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        if (empty($values['size_id'])) {
            $values['size_id'] = null;
        }
    }
    */

}
