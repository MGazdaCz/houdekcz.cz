<?php
namespace App\AdminModule\Control\Widget\Task;

use \Nette\Application\UI\Control;
use App\AdminModule\Control\Widget\Base\BaseControl;

class TaskControl extends Control
{
    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/task.latte');

        // vložíme do šablony parametry
        $head    = '<a href="'.$this->presenter->link(':Admin:Erp:Task:').'" titlee="Výkazy práce">Výkazy práce</a>';
        $content = (string)$template;
        
        $baseControl = $this->getParent()->getComponent('widgetbase');
        $baseControl->setHead($head);
        $baseControl->setContent($content);
        $baseControl->render();
    } // end function
} // end class