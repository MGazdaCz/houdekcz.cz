/* ======================================================================================
GULPFILE.js
====================================================================================== */

/*
REQUIRE TASKS
--------------------------------------------------------------------------------------- */
var gulp 		  = require('gulp'),
	sass 		  = require('gulp-sass'),
	autoprefixer  = require('gulp-autoprefixer'),
	minifyCss 	  = require('gulp-minify-css'),
	plumber 	  = require('gulp-plumber'),
	gutil 		  = require('gulp-util'),
	livereload	  = require('gulp-livereload');

/*
Paths
--------------------------------------------------------------------------------------- */
var dest 		= "./www/",
	src 		  = "./www/",
	//customSrc	= "./www/",
	sassSrc 	= src + "./sass/**/*.scss",
	sassDest 	= dest + "./css";

module.exports = {

	// SASS
	sass: {
		src: src + "/sass/*.{sass,scss}",
		dest: dest + "/css"
	},

	// Images
	images: {
		src: "/images/**",
		dest: "/images"
	},

	//
	/*customImages: {
		src: customSrc + "/images/**",
		dest: dest + "./skins/blue/images"
	} */
};

/*
onError
--------------------------------------------------------------------------------------- */
var onError = function(err) {
	gutil.log(gutil.colors.red(err));
};


/*
SASS KOMPILACE
--------------------------------------------------------------------------------------- */
gulp.task('sass', function() {
		gulp.src('./**/*.scss')
		.pipe(plumber({
			errorHandler: onError
		}))
		// .pipe(sourcemaps.init())
		.pipe(sass())		
		//.pipe(sourcemaps.write())
		.pipe(autoprefixer({
			browsers: ['last 2 version']
		}))
		.pipe(minifyCss())
		.pipe(gulp.dest('./'))
		/*.pipe(browserSync.reload({
			stream: true
		}));*/
		.pipe(livereload());
});


/*
WATCH
--------------------------------------------------------------------------------------- */
gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('./sass/**/*.scss', ['sass']);
	
});


/*
DEFAULT TASK
--------------------------------------------------------------------------------------- */
gulp.task('default', ['sass', 'watch']);