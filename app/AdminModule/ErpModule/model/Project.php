<?php
    namespace App\AdminModule\ErpModule;

    use Model\Helper\TimeHelper;
    use Model\Repository\TaskRepository;
    use Nette\Database\Table\ActiveRow;

    class Project
    {
        private $_level;
        private $_project;
        private $_company;
        private $_projectStatus;

        private $_taskRepository;

        public function __construct()
        {
            $this->_level         = null;
            $this->_project       = null;
            $this->_company       = null;
            $this->_projectStatus = null;
        }

        public static function createFromActiveRow(ActiveRow $row){
            $project = new self;
            $project->_project       = $row;
            $project->_company       = $project->_project->ref('rel_company');
            $project->_projectStatus = $project->_project->ref('rel_projectstatus');

            return $project;
        } // end function

        public function get($attr, $default = null){
            // pokusim se vratit atribut ze self
            $attrPrefix = '_'.$attr;
            if (isset($this->$attrPrefix))
                return $this->$attrPrefix;

            // pokusim se vratit atribut ze zakladniho objektu project
            if (isset($this->_project->$attr))
                return $this->_project->$attr;

            // pokusim se vratit atribut z relace
            $attrArray = explode('_', $attr);
            $attrLong  = $attr;
            if (count($attrArray) == 2)
            {
                $selfAtribute = '_'.$attrArray[0];
                $attr         = $attrArray[1];

                if (isset($this->$selfAtribute) && isset($this->$selfAtribute->$attr))
                    return $this->$selfAtribute->$attr;
            } // end if

            // vratim defaultni hodnotu
            return $default;
        }

        public function __get($attr){
            return $this->get($attr);
        }

        public function getTimeSpent()
        {
            $time = $this->_taskRepository->getTable()->where('rel_project = ?', $this->_project->id)->sum('timeLength');
            return $time;
        }

        public function getTimeSpentPercent(){
            $timeSpent = $this->getTimeSpent();

            if ($this->_project->time > 0)
            {
                return round($timeSpent / ($this->_project->time * 3600) * 100);
            } // end if
            return intval(0);
        }

        public function getTimeSpentClass(){
            $percent = $this->getTimeSpentPercent();

            if ($percent < 50)
                return 'danger';
            elseif ($percent < 75)
                return 'warning';
            return 'success';
        }

        public function setTaskRepository(TaskRepository $taskRepository){
            $this->_taskRepository = $taskRepository;
            return $this;
        }

        public function setLevel($level)
        {
            $this->_level = $level;
        } // end function
    }