<?php
namespace App\AdminModule\CoreModule\Component\Buttons;

use \Nette\Application\UI\Control;

class BackButton extends Control
{
    public function render($urlBack = null)
    {
        // moznost prenastaveni back URL
        $this->template->backUrl = $this->getParent()->template->urlBack;
        if (!is_null($urlBack)) {
            $this->template->backUrl = $urlBack;
        }

        $this->template->setFile(__DIR__.'/templates/backButton.latte');
        $this->template->render();
    }

}