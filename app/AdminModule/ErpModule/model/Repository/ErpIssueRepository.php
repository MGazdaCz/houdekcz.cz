<?php
namespace App\AdminModule\ErpModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;
use Nette;

class ErpIssueRepository extends MainRepository
{
    protected $_table = 'erp_issue';

    /**
     * Metoda vrati odpracovany cas v milisekundach pro uvedene issue.
     *
     * @param integer $issueId
     * @return integer
     */
    public function getTimeSpent($issueId)
    {
        return $this->_dbContext->table('task')->where('ErpIssue_id = ?', $issueId)->sum('timeLength');
    } // end if

}