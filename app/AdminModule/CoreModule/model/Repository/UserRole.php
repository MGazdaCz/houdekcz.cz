<?php
namespace App\AdminModule\CoreModule\Model\Repository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class UserRole extends \App\AdminModule\CoreModule\Model\Repository\Repository
{
    protected $_table = 'core_user_role';
}