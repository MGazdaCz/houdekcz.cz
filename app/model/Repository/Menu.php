<?php
namespace App\Model\Repository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Menu extends RepositoryAbstract
{
    protected $_table = 'cms_menu';
}