<?php
namespace App\AdminModule\CoreModule\Component;

use \Nette\Application\UI\Control;

class SortLink extends Control
{
    const DIRECTION_UP   = 'up';
    const DIRECTION_DOWN = 'down';

    protected $_actualSortFiled;
    protected $_actualSortFiledDirection;
    protected $_sortDirectionSettings;

    protected $_parameters;


    public function __construct(array $configDirection){
        $this->_sortDirectionSettings = $configDirection;
    }

    /**
     * Komponenta pro vypsani odkazu pro razeni ve vybranem modulu.
     *
     * @param string $name - nazev odkazu (text mezi acky)
     * @param string $column - nazev sloupce dle ktereho radime
     */
    public function render($name, $column)
    {
        if (!empty($this->_actualSortFiled)) {
            if ($this->_actualSortFiledDirection == 'up') {
                $this->_sortDirectionSettings[$this->_actualSortFiled] = 'down';
            }
        }

        $this->template->name                  = $name;
        $this->template->column                = $column;
        $this->template->actualSortFiled       = $this->_actualSortFiled;
        $this->template->sortDirectionSettings = $this->_sortDirectionSettings;
        $this->template->params                = $this->_parameters;

        // sestaveni parametru pro vytvoreni odkazu
        $this->template->linkParameters = array(
            $column,
            $this->_sortDirectionSettings[$column]
        );

        // moznost pridani volitelnych parametru
        if (!empty($this->_parameters)) {
            foreach ($this->_parameters as $paramName => $paramValue) {
                $this->template->linkParameters[] = $paramValue;
            }
        }

        $this->template->setFile(__DIR__.'/templates/sortLink.latte');
        $this->template->render();
    }

    public function setActualSortField($fieldName){
        $this->_actualSortFiled = $fieldName;
        return $this;
    }

    public function setActualSortFieldDirection($direction){
        $this->_actualSortFiledDirection = $direction;
        return $this;
    }

    /**
     * Metoda nastavi specialni parametr predavany v sortu.
     *
     * @param string $parameterName
     * @param mixed $value
     */
    public function setParameter($parameterName, $value){
        $this->_parameters[$parameterName] = $value;
        return $this;
    }

    /**
     * Metoda nastavi specialni parametry predavane v sortu.
     *
     * @param array $parameters
     */
    public function setParameters(array $parameters){
        $this->_parameters = $parameters;
        return $this;
    }

    /**
     * Metoda vrati string pro SQL order.
     *
     * @return string|null
     */
    public function getOrderForQuery(){
        if (!empty($this->_actualSortFiled)) {
            $sort = $this->_actualSortFiled;

            if ($this->_actualSortFiledDirection == 'up') {
                $sort .= ' ASC';
            } else {
                $sort .= ' DESC';
            }

            return $sort;
        }
        return null;
    }

}