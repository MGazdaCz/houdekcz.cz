<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use Model\Form\FormEditorDetail;
use App\AdminModule\CoreModule\Model\SeoFieldsTool;
use \Nette\Application\UI\Form;

/**
 * Homepage presenter.
 */
class PagePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    private $_photogalleryRepository;

    protected function startup(){
        parent::startup();
        $this->_mainRepository         = $this->context->getService('cmsPage');
        $this->_photogalleryRepository = $this->context->getService('cmsPhotogallery');

        $this->_listH1       = 'Stránky';
        $this->_orderDefault = '_order';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Page/form.latte';
    }
    
    protected function createComponentForm($name)
    {
        // sestaveni dat pro mutace
        $languages = $this->_languageRepository->fetchPairs('id', 'name');

        // zalozeni formulare
        $form = new PageBase($this, $name);
        $form->setLanguages($languages);
        $form->templateEnable();
        $form->initFormItems();

        // sestaveni dat pro policka formular
        $parents = $this->_mainRepository->getTable()->where('id != '.intval($this->_itemId).' AND delete = 0')->fetchPairs('id', 'name');
        $parents = array_reverse($parents, true);
        $parents[0] = ' - ';
        $parents = array_reverse($parents, true);

        $componentArray = array(
            '' => ' - ',
            'photogallery'    => 'Fotogalerie',
            'contactForm'     => 'Kontaktní formulář',
            'guestbook'       => 'Návštěvní kniha',
            'reservation'     => 'Rezervace - formulář',
            'reservationList' => 'Rezervace - výpis',
        );
        
        $galleries = $this->_photogalleryRepository->fetchPairs('id', 'name', true);
        // ---

        // rozsireni formulare
        $form->addCheckbox('homepage', ' homepage');
        $form->addSelect('_parent', 'Nadřízená stránka', $parents);

        $form->addSelect('component', 'Zobrazit komponentu', $componentArray);

        $form->addSelect('cms_photogallery_id', 'Fotogalerie', $galleries);
        $form->addText('datePublication', 'Datum publikace')->setAttribute('class', 'date-picker');
        $form->addCheckbox('showDatePublication', ' zobrazit');
        $form->addText('dateFrom', 'Datum od')->setAttribute('class', 'date-picker');
        $form->addText('dateTo', 'Datum do')->setAttribute('class', 'date-picker');
        
        //$form->addSubmit('submit', 'Ulož')->setAttribute('class', 'btn btn-xs btn-info');

        // nastaveni defaultnich dat do formulare
        if ($this->_itemId > 0)
        {
            $page = $this->_mainRepository->findByPk($this->_itemId);
            $page = $page->toArray();
            
            if (!empty($page['datePublication'])) $page['datePublication'] = $page['datePublication']->format('d.m.Y');
            if (!empty($page['dateFrom']))        $page['dateFrom']        = $page['dateFrom']->format('d.m.Y');
            if (!empty($page['dateTo']))          $page['dateTo']          = $page['dateTo']->format('d.m.Y');
            
            $form->setDefaults($page);
        }

        $form->onSubmit[] = $this->defaultFormSubmitted;

        return $form;
    }

    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();
        $formValues    = (array)$form->values;

        // osetreni seo poli
        SeoFieldsTool::sanitizeFieldsData($formValues['name'], $formValues);

        if ($formValues['cms_photogallery_id'] == 0)
            $formValues['cms_photogallery_id'] = null;
        if ($formValues['_parent'] == 0) {
            $formValues['_parent'] = null;
            $formValues['_level']  = 0;
        }
        else {
            $formValues['_level'] = count($this->_getParents($formValues['_parent']));
        } // end if

        if (!empty($formValues['datePublication']))
            $formValues['datePublication'] = date('Y-m-d', strtotime($formValues['datePublication']));
        else
            $formValues['datePublication'] = null;

        if (!empty($formValues['dateFrom']))
            $formValues['dateFrom'] = date('Y-m-d', strtotime($formValues['dateFrom']));
        else
            $formValues['dateFrom'] = null;

        if (!empty($formValues['dateTo']))
            $formValues['dateTo'] = date('Y-m-d', strtotime($formValues['dateTo']));
        else
            $formValues['dateTo'] = null;

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $pageId = $requestParams['id'];

            $this->_mainRepository->update($formValues, array('id' => $requestParams['id']));
        }
        else
        {
            $formValues['url']         = '/'.\Nette\Utils\Strings::webalize($formValues['name']);
            $formValues['title']       = $formValues['name'];
            $formValues['_insertUser'] = $this->user->getId();
            $formValues['_insertDate'] = date('Y-m-d H:i:s');

            $pageId = $this->_mainRepository->insert($formValues);
        } // end if

        $this->flashMessage('Stránka byla uložena.', 'success');

        return $this->redirect('edit', array('id' => $pageId));
    }

}
