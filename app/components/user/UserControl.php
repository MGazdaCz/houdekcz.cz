<?php
namespace App\Control\User;

use Nette\Application\UI\Control;

class UserControl extends Control
{
    public function renderDemo()
    {
        $this->template->setFile(__DIR__.'/templates/demo.latte');
        $this->template->render();
    }

    public function render($user)
    {
        $this->template->user = $user;

        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

    public function renderAuthor($user)
    {
        $this->template->user = $user;

        $this->template->userImage  = $user->photo;
        $this->template->userName   = $user->name.' '.$user->surname;
        $this->template->annotation = $user->annotation;
        $this->template->alias      = $user->alias;

        $this->template->setFile(__DIR__.'/templates/author.latte');
        $this->template->render();
    }

    public function renderAuthorList()
    {
        $userRepo = $this->getParent()->context->getService('coreUser');

        $this->template->authors = $userRepo->findBy(array(
            'active' => 1
        ));

        $this->template->setFile(__DIR__.'/templates/authorList.latte');
        $this->template->render();
    }

}