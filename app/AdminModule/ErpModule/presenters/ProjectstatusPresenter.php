<?php

namespace App\AdminModule\ErpModule;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;
use App\AdminModule\ErpModule\ProjectModule;
use Nette\DateTime;

/**
 * Faktury presenter.
 */
class ProjectstatusPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    protected function startup(){
        parent::startup();
        $this->_mainRepository  = $this->context->erpprojectstatusRepository;

        $this->_listH1 = 'Číselník stavů projektů';

        $this->_orderDefault = 'id';
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        //$form->addGroup('Základní informace');
        $form->addText('id', 'Číslo (ID)');
        $form->addText('name', 'Název');

        $form->addTextArea('note', 'Poznámky', 75, 10);

        $form->addSubmit('submit', 'Uložit')->setAttribute('class', 'btn btn-sm btn-success');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_mainRepository->findByPk($this->_itemId);
            $itemArray = $item->toArray();

            $form->setDefaults($itemArray);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni formatu datumu
        $values = (array)$form->values;

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['insertDate'] = date('Y-m-d H:i:s');

            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

}