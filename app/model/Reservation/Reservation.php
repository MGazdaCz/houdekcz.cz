<?php
namespace Model\Reservation;

use Nette;

class Reservation {

    const SYSTEM_NAME = 'SYSTEM';

    private $_repository;

    /**
     * Metoda nastavi uloziste.
     *
     * @param \Model\Repository\ReservationRepository $repository
     */
    public function setRepository(\Model\Repository\ReservationRepository $repository){
        $this->_repository = $repository;
    }

    /**
     * @param string object
     * @retunr array seznam obsazenych dni (dvourozmerne pole $reservedDays[$month][$dayNumber];
     */
    public function getReservedDays($object, $year){
        $reservedDays = array();

        $selection = $this->_repository->getTable()->where("object = ? AND (dateFrom LIKE ? OR dateTo LIKE ?)", $object, "$year-%", "$year-%")->order('dateFrom');

        if ($selection->count() > 0)
        {
            while ($row = $selection->fetch())
            {
                $dtFrom = new Nette\DateTime($row->dateFrom);
                $tsFrom = $dtFrom->getTimestamp();
                $dtTo   = new Nette\DateTime($row->dateTo);
                $tsTo   = $dtTo->getTimestamp();

                for ($i=$tsFrom;$i<=$tsTo;$i+=Nette\DateTime::DAY)
                {
                    $actualDay = new Nette\DateTime(date('Y-m-d', $i));

                    $month = $actualDay->format('n');
                    $day   = $actualDay->format('j');

                    if ($actualDay->format('Y') == $year)
                        $reservedDays[$month][$day] = $day;
                    //echo $actualDay->format('Y-m-d')." ";
                } // end for
            } // end while
        } // end if

        return $reservedDays;
    } // end function

    /**
     * Metoda vraci true, pokud je jiz objekt na tento den rezervovan.
     *
     * @param string $object
     * @param string $datetime
     * @return bool
     */
    public function isReserved($object, $datetime){
        $selection = $this->_repository->getTable()->where("object = ? AND (dateFrom <= ? AND dateTo >= ?)", $object, $datetime, $datetime);

        if ($selection->count() > 0)
            return true;
        return false;
    }

    /**
     * @param string $object
     * @param string $datetime
     */
    public function reserveAsSystem($object, $datetime){
        $prev = $this->getPrevSystemRec($object, $datetime);
        $next = $this->getNextSystemRec($object, $datetime);

        if ($prev || $next)
        {
            if ($prev && $next)
            {
                // musim sloucit 2 sousedni zaznamy
                $prevArray = $prev->toArray();

                // slouceni (natazeni)
                $prevArray['dateTo'] = $next->dateTo;
                $prevArray['insertDate'] = $next->dateTo;
                $this->_repository->update($prevArray, array('id' => $prev->id));

                // vymazani nasledujiciho intervalu
                $this->_repository->delete(array('id' => $next->id));
            }
            elseif ($prev)
            {
                // provedu update predchoziho zaznamu
                $prevArray = $prev->toArray();

                $prevArray['dateTo']     = $datetime;
                $prevArray['insertDate'] = date('Y-m-d H:i:s');
                $this->_repository->update($prevArray, array('id' => $prev->id));
            }
            else
            {
                // provedu update nasledujiho zaznamu
                $nextArray = $next->toArray();

                $nextArray['dateFrom']   = $datetime;
                $nextArray['insertDate'] = date('Y-m-d H:i:s');
                $this->_repository->update($nextArray, array('id' => $next->id));
            } // end if
        }
        else
        {
            // nemam systemove sousedy, mohu zalozit novy zaznam
            $data = array(
                'dateFrom'   => $datetime,
                'dateTo'     => $datetime,
                'name'       => self::SYSTEM_NAME,
                'authorized' => 1,
                'person'     => 1,
                'object'     => $object,
                'note'       => 'Rezervace vložena přes modul rezervace (kalendář)',
                'insertDate' => date('Y-m-d H:i:s'),
            );
            $this->_repository->insert($data);
        } // end if
    }

    public function removeSystemReserve($object, $datetime){
        $selection = $this->_repository->getTable()->where("name = ? AND object = ? AND (dateFrom <= ? AND dateTo >= ?)", self::SYSTEM_NAME, $object, $datetime, $datetime);

        if ($selection->count() > 0)
        {
            $row = $selection->fetch();

            if ($row->name != self::SYSTEM_NAME)
            {
                // nemuzu vymazat, jedna se o zaznam z registrace z FE nebo z BE pres tabulkovy mod
            }
            else
            {
                if ($row->dateFrom == $row->dateTo)
                {
                    $selection->delete();
                }
                else
                {
                    $dtDatetime = new Nette\DateTime($datetime);
                    $rowArray   = $row->toArray();

                    if ($row->dateFrom == $datetime)
                    {
                        // oriznu interval zepredu
                        $rowArray['dateFrom']   = date('Y-m-d H:i:s', ($dtDatetime->getTimestamp() + Nette\DateTime::DAY));
                        $rowArray['insertDate'] = date('Y-m-d H:i:s');

                        $this->_repository->update($rowArray, array('id' => $row->id));
                    }
                    elseif ($row->dateTo == $datetime)
                    {
                        // oriznu interval zezadu
                        $rowArray['dateTo']     = date('Y-m-d H:i:s', ($dtDatetime->getTimestamp() - Nette\DateTime::DAY));
                        $rowArray['insertDate'] = date('Y-m-d H:i:s');

                        $this->_repository->update($rowArray, array('id' => $row->id));
                    }
                    else
                    {
                        // musim rozdelit interval
                        $rowArray2 = $rowArray;

                        $rowArray['dateTo']     = date('Y-m-d H:i:s', ($dtDatetime->getTimestamp() - Nette\DateTime::DAY));
                        $rowArray['insertDate'] = date('Y-m-d H:i:s');

                        $this->_repository->update($rowArray, array('id' => $row->id));

                        unset($rowArray2['id']);
                        $rowArray2['dateFrom']   = date('Y-m-d H:i:s', ($dtDatetime->getTimestamp() + Nette\DateTime::DAY));
                        $rowArray2['insertDate'] = date('Y-m-d H:i:s');

                        $this->_repository->insert($rowArray2);
                    } // end if

                    //$dtDate = new Nette\DateTime($datetime);
                } // end if
            } // end if
        } // end if
    }

    private function getPrevSystemRec($object, $datetime)
    {
        $dtDate     = new Nette\DateTime($datetime);
        $prevDateTs = $dtDate->getTimestamp() - Nette\DateTime::DAY;

        $selection = $this->_repository->getTable()->where("name = ? AND object = ? AND dateTo = ?", self::SYSTEM_NAME, $object, date('Y-m-d H:i:s', $prevDateTs));
        if ($selection->count() > 0)
            return $selection->fetch();

        return false;
    }

    private function getNextSystemRec($object, $datetime)
    {
        $dtDate = new Nette\DateTime($datetime);
        $nextDateTs = $dtDate->getTimestamp() + Nette\DateTime::DAY;

        $selection = $this->_repository->getTable()->where("name = ? AND object = ? AND dateFrom = ?", self::SYSTEM_NAME, $object, date('Y-m-d H:i:s', $nextDateTs));
        if ($selection->count() > 0)
            return $selection->fetch();

        return false;
    }

} // end class