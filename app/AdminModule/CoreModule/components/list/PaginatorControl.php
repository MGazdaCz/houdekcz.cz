<?php
namespace App\AdminModule\CoreModule\Component;

use \Nette\Application\UI\Control;

class PaginatorControl extends Control
{
    /**
     * Pocet polozek v paginatoru.
     *
     * @var int
     */
    protected $_pocet = 4;

    /**
     * Atribut pro nastaveni parametru, ktere lze predavat v pageru.
     *
     * @var array
     */
    protected $_parameters = array();

    /**
     * Renderovani defaultniho paginatoru.
     *
     * @param $limit
     * @param $page
     * @param $count
     */
    public function render($limit, $page, $count)
    {
        $this->_initItems($limit, $page, $count);

        $this->template->setFile(__DIR__.'/templates/paginator.latte');
        $this->template->render();
    }

    /**
     * Metoda provede inicializaci prvku pro paginator.
     *
     * @param $limit
     * @param $page
     * @param $count
     */
    protected function _initItems($limit, $page, $count){
        $this->template->page    = $page;
        $this->template->prevUrl = '#';
        $this->template->nextUrl = '#';
        $this->template->items   = array();

        $pageCount = ceil($count / $limit);

        if ($page > 1) {
            $this->template->prevUrl = $this->_getLink($page - 1);
        }

        if ($page < $pageCount) {
            $this->template->nextUrl = $this->_getLink($page + 1);
        }

        if ($pageCount > 10) {
            $this->_generateSlimItems($page, $pageCount);
        } else {
            $this->_generateAllItems($pageCount);
        }
    }

    /**
     * Vypocet vsech polozek paginatoru.
     *
     * @param $pageCount
     */
    protected function _generateAllItems($pageCount){
        for ($i = 1; $i <= $pageCount; $i++) {
            $this->template->items[$i] = $this->_getLink($i);
        }
    }

    /**
     * Vypocet omezeneho poctu polozke v paginatoru doplnenych o separatory.
     *
     * @param int $page
     * @param int $pageCount
     */
    protected function _generateSlimItems($page, $pageCount){
        // zakladni nastaveni, urcuje rozsah polozek v paginatoru
        $pocet   = $this->_pocet;
        $rozptyl = $pocet / 2;

        // vypocet startu intervalu
        if (($page - $rozptyl) <= 1) {
            $from = 2;
        } else {
            $from = $page - $rozptyl;
        }

        // vypocet konce intervalu
        if (($page + $rozptyl) > $pageCount) {
            $to = $pageCount - 1;
            $from = $pageCount - $pocet;
        } else {
            $to = $from + $pocet;
        }

        // strankovani musi vzdy obsahovat odkaz na prvni stranku
        $this->template->items[1] = $this->_getLink(1);

        // doplneni odsazeni ze zacatku
        if ($from > 2) {
            $this->template->items['...'] = '#';
        }

        // vypocet polozek v paginatoru
        for ($i = $from; $i <= $to; $i++) {
            $this->template->items[$i] = $this->_getLink($i);
        }

        // doplneni odsazeni z konce
        if ($to < ($pageCount - 1)) {
            $this->template->items['....'] = '#';
        }

        // strankovani musi vzdy obsahovat odkaz na posledni stranku
        $this->template->items[$pageCount] = $this->_getLink($pageCount);
    }

    protected function _getLink($pageNumber){
        $parameters         = $this->_parameters;
        $parameters['page'] = $pageNumber;

        return $this->presenter->link('default', $parameters);
    }

    /**
     * Metoda nastavi parametr predavany v pageru.
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setParameter($name, $value){
        $this->_parameters[$name] = $value;

        return $this;
    }

} // end class