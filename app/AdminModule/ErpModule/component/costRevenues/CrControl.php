<?php
namespace App\AdminModule\ErpModule\Control\CostRevenues;

use \Nette\Application\UI\Control;

class CrControl extends Control
{
    private $_costRevenuesRepositiory;

    public function render($projectId)
    {
        // init repository
        $this->_costRevenuesRepositiory = $this->parent->getContext()->erpCostRevenuesRepository;
        
        $whereCost    = array('type = ?' => 'cost',    'Project_id = ?' => $projectId);
        $whereRevenue = array('type = ?' => 'revenue', 'Project_id = ?' => $projectId);
        
        // nacteni polozek listu
        $this->template->costs    = $this->_costRevenuesRepositiory->findBy($whereCost)->order('id');
        $this->template->revenues = $this->_costRevenuesRepositiory->findBy($whereRevenue)->order('id');
        
        $template = $this->template;
        $template->setFile(__DIR__ . '/default.latte');

        // a vykreslíme ji
        $template->render();
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function handleAdd() {
        return $this->parent->redirect(':Admin:Erp:Costrevenues:add', array(
            'Project_id' => 19,
            'type'       => $this->getParam('type')
        ));
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function handleEdit() {
        return $this->parent->redirect(':Admin:Erp:Costrevenues:edit', array('id' => $this->getParam('id')));
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function handleDelete() {
        return $this->parent->redirect(':Admin:Erp:Costrevenues:delete', array('id' => $this->getParam('id')));
    } // end function

    protected function createComponentListActionButtons()
    {
        $component = new \AdminModule\CoreModule\Component\ActionButtons();

        return $component;
    }
    
} // end class