<?php

namespace App\Model\Homepage;
use App\Model\ColumnListAbstract;
use Nette\Database\Table\Selection;
use Nette\DI\Container;

/**
 * Created by PhpStorm.
 * User: Milan
 * Date: 31.7.15
 * Time: 21:07
 */
class Homepage extends ColumnListAbstract {
    //const FIRST_PAGE_ITEMS         = 16;
    const FIRST_PAGE_SPECIAL_ITEMS = 6;
    const FIRST_PAGE_LIMIT         = 9;
    const PAGE_LIMIT               = 10;

    const POSITION_PODCAST    = 3;
    const POSITION_ADVERT1    = 4;
    const POSITION_ACTION     = 6;
    const POSITION_NEWSLETTER = 9;
    const POSITION_ADVERT2    = 12;
    const POSITION_ADVERT3    = 13;

    /**
     * Metoda sestavi dotaz pro nacteni hlavniho clanku a vrati selection.
     *
     * @param Container $context
     * @return mixed
     */
    public function getMainArticle(Container $context) {
        $articleRepo = $context->getService('cmsArticle');

        $whereArray = array(
            'delete' => 0,
            'mainArticle' => 1,
        );
        //if (!$this->user->isLoggedIn()) {
        $whereArray['view'] = 1;
        $whereArray[]       = '(datePublication IS NULL OR datePublication <= NOW())';
        //}

        return $articleRepo->findBy($whereArray)->order('datePublication DESC')->limit(1)->fetch();
    }

    /**
     * Metoda vrati pocet polozek na HP.
     *
     * @param Selection $acticlesSelection
     * @return int count
     */
    public function getItemsCount(Selection $acticlesSelection){
        return self::FIRST_PAGE_SPECIAL_ITEMS + $acticlesSelection->count();
    }

    /**
     * Metoda nacte clanky pro homepage.
     *
     * @param Container $context
     * @return mixed
     */
    public function getArticlesForFirstPage(Container $context, $pageNumber = 1){
        $articleRepo = $context->getService('cmsArticle');

        if ($pageNumber < 1) {
            $limit = self::FIRST_PAGE_LIMIT;
        } else {
            $limit = self::FIRST_PAGE_LIMIT + (self::PAGE_LIMIT * ($pageNumber - 1));
        }

        $whereArray = array(
            'delete' => 0,
            'showOnHomepage' => 1,
        );
        if (!$this->_user->isLoggedIn()) {
            $whereArray[]       = '(datePublication IS NULL OR datePublication <= NOW())';
            $whereArray['view'] = 1;
        }

        //return $articleRepo->findBy($whereArray)->order('datePublication DESC')->limit($limit);
        return $articleRepo->findBy($whereArray)->order('_order')->limit($limit);
    }

    /**
     * Metoda prerozdeli data do sloupcu.
     *
     * @param Container $context
     * @param int $pageWidth
     */
    public function getColumnsData(Container $context, $pageWidth, $pageNumber = 1){
        $articleSelection = $this->getArticlesForFirstPage($context, $pageNumber);

        // init poctu sloupcu podle rozliseni
        $columnCount = $this->getColumnCount($pageWidth);

        $columnIndex = 0;
        $bannerZone  = 1;
        $itemsCount  = $this->getItemsCount($articleSelection);

        // inicializace navratoveho pole
        $returnData = $this->getReturnArray($columnCount);

        for ($i = 1; $i <= $itemsCount; $i++) {
            if ($i == self::POSITION_PODCAST) {
                $returnData[$columnIndex][] = array(
                    'control' => 'podcastControl',
                    'method'  => 'box',
                    'param'   => null,
                );
            } elseif ($i == self::POSITION_ACTION) {
                $returnData[$columnIndex][] = array(
                    'control' => 'actionControl',
                    'method'  => 'box',
                    'param'   => null,
                );
            } elseif ($i == self::POSITION_NEWSLETTER) {
                $returnData[$columnIndex][] = array(
                    'control' => 'newsletterControl',
                    'method'  => 'box',
                    'param'   => null,
                );
            } elseif (in_array($i, array(self::POSITION_ADVERT1, self::POSITION_ADVERT2, self::POSITION_ADVERT3))) {
                $returnData[$columnIndex][] = array(
                    'control' => 'advertControl',
                    'method'  => 'boxWithZone',
                    'param'   => $bannerZone++,
                );
            } else {
                $returnData[$columnIndex][] = array(
                    'control' => 'articleBoxControl',
                    'method'  => null,
                    'param'   =>  $articleSelection->fetch(),
                );
            }

            // posunuti iteratoru sloupce
            $columnIndex++;
            if ($columnIndex == $columnCount) $columnIndex = 0;
        }

        return $returnData;
    }

    /**
     * Metoda ulozi vysledek razeni do DB.
     *
     * @param Container $context
     * @param array $columnsData
     */
    public function sort(Container $context, $columnsData){
        $articleRepo = $context->getService('cmsArticle');

        // projdu sloupce abych si urcil max. pocet radku
        $columns = count($columnsData);
        $rows    = 0;
        for ($i = 0; $i < $columns; $i++) {
            $columnData = $columnsData[$i];
            $count      = count($columnData);
            if ($count > $rows) {
                $rows = $count;
            }
        }

        // projdu jednotlive radky a sloupce, tim urcim poradi clanku
        $order = 0;
        for ($i = 0; $i <= $rows; $i++) {
            for ($j = 0; $j < $columns; $j++) {
                if (isset($columnsData[$j][$i]) && !empty($columnsData[$j][$i])) {
                    $articleId = $columnsData[$j][$i];

                    //$articlesIds[] = $articleId;

                    $articleRepo->update(array('_order' => $order++), array('id' => $articleId));
                }
            }
        }
        /*
        echo '<pre>'.print_r($columnsData, true).'</pre>';
        echo '<pre>'.print_r($articlesIds, true).'</pre>';
        die;
        */
    }

}