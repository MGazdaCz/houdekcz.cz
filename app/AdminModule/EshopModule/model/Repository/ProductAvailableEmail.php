<?php

namespace App\AdminModule\EshopModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class ProductAvailableEmail extends MainRepository
{
    protected $_table = 'eshop_product_available_email';
}