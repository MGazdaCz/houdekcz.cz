<?php
namespace App\AdminModule\Control\Filter;

use \Nette\Application\UI\Control;

class FilterControl extends Control
{
    /**
     * @var \App\AdminModule\Model\Filter\Quick
     */
    private $_quickFilter;

    /**
     * @param \App\AdminModule\Model\Filter\Quick $quickFilter
     * @return \App\AdminModule\Control\Filter\FilterControl
     */
    public function setQuickFilter(\App\AdminModule\Model\Filter\Quick $quickFilter){
        $this->_quickFilter = $quickFilter;
        return $this;
    }

    public function render()
    {

    }

    public function renderQuick()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/quick.latte');

        // vložíme do šablony parametry
        $template->name   = $this->_quickFilter->getName();
        $template->active = $this->_quickFilter->getActualFilter();
        $template->items  = $this->_quickFilter->getItems();

        // a vykreslíme ji
        $template->render();
    }

} // end class