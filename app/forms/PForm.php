<?php
namespace App\Forms;

use Nette\Application\UI\Form;
use Nette\Forms\Controls;

/**
 * Trida preteyujici formulare tak, aby se mi automaticky jednotlive elementy generovaly do odstavcu.
 * @package Model\Form
 */
class PForm extends Form
{
    public function __construct(\Nette\ComponentModel\IContainer $parent = NULL, $name = NULL){
        parent::__construct($parent, $name);

        // setup form rendering
        $renderer = $this->getRenderer();

        $renderer->wrappers['group']['label'] .= ' class="lighter blue"';
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'p class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container']      = 'span class=input';
        $renderer->wrappers['control']['description']    = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        $renderer->wrappers['label']['container'] = 'span class="label"';

        // make form and controls compatible with Twitter Bootstrap
        $this->getElementPrototype()->class('form-horizontal');
    }

}