<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Settings presenter.
 */
class SettingsPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Settings
     */
    protected $_mainRepository;

    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('eshopSettings');

        $this->template->bodyClass = 'module-category';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Category/form.latte';
    }

    public function renderDefault()
    {
        $this->template->h1 = 'Nastavení eshopu';
        $this->template->items = $this->_mainRepository->findAll();
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Nová vlastnost';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $item = $this->_mainRepository->findByPk($id);

        $this->template->id = $id;
        $this->template->h1 = 'Editace kategorie: '.$item->name;
        $this->template->variants = array();
    }

    protected function createComponentForm()
    {
        $form = new PageBase();
        $form->templateEnable();
        $form->initFormItems();

        unset($form['heading']);

        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);

        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {

                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        } else {

        }

        return $form;
    }

    /**
     * Metoda aktualizuje data, ktera lze cachovat pro rychlejsi generovani FE.
     */
    public function renderPreparemenu(){
        $items = $this->_mainRepository->findAll();

        foreach ($items as $item) {
            // hledani vsech podkategorii
            // TODO - toto by slo mozna cachovat k jednotlivym kategoriim produktu?!
            $childrens = $this->context->getService('eshopCategory')->getAllChildrenCategories($item->id);
            $childrens[] = $item->id;

            // nacteni produktu v podkategoriich
            $countProducts = $this->context->getService('eshopProduct')->findBy(array(
                'id IN (SELECT eshop_product_id FROM eshop_category_product WHERE eshop_category_id IN ?)' => $childrens,
                'view' => 1,
                'delete' => 0
            ))->count();

            $updateData = array(
                'countChildren' => $item->related('eshop_category._parent')->count(),
                'countProducts' => $countProducts
            );

            // aktualizace URL adres kategorii
            if (empty($item->url)) {
                $updateData['url'] = '/'.Nette\Utils\Strings::webalize($item->name);
            }

            $this->_mainRepository->update($updateData, array('id' => $item->id));
        }

        $this->flashMessage('Aktualizovano '.$items->count().' záznamů');
        $this->redirect('default');
    }

}
