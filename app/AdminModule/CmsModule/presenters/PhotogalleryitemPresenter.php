<?php

namespace App\AdminModule\CmsModule\Presenters;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;
use \Nette\Utils\Strings;
use App\AdminModule\CoreModule\Model\Forms\Controls\ImageInput;

/**
 * Photogallery presenter.
 */
class PhotogalleryitemPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    private $_photogalleryRepository;
    
    protected function startup(){
        parent::startup();
        $this->_mainRepository = $this->context->getService('cmsPhotogalleryItem');
        $this->_photogalleryRepository = $this->context->getService('cmsPhotogallery');

        $this->_orderDefault = 'name';
    }

    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);
        
        // musim nastavit manualne, protoze nevim, jak to nastavit automaticky pri pridani custom fieldu "ImageInput"
        $form->elementPrototype->enctype = 'multipart/form-data';

        //$form->addHidden('id');
        $form->addSelect('cms_photogallery_id', 'Fotogalerie', $this->_photogalleryRepository->getTable()->fetchPairs('id', 'name'));
        $form->addText('name', 'Název', 75);
        $form->addCheckbox('view', 'Shváleno');
        //$form->addUpload('file', 'Fotografie');
        $form['file'] = new ImageInput('file', 'Fotografie');
        $form->addTextArea('description', 'Popis', 75, 5);
        $form->addSubmit('submit', 'Ulož')->setAttribute('class', 'btn btn-sm btn-info');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_mainRepository->findByPk($this->_itemId);
            
            $form['file']
                ->setDeleteUrl('/admin/photogalleryitem/deleteimage/'.$item->id)
                ->setPathDir(WWW_DIR.'/data/photogallery/'.$item->cms_photogallery->alias.'/pw1')
                ->setPathWww(WWW_WWW.'/data/photogallery/'.$item->cms_photogallery->alias.'/pw1');
            
            $form->setDefaults($item);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }
 
    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();
        $formValues    = $form->getValues(true);
        
        // nacteni detailu o fotogalerii
        $photogalery = $this->_photogalleryRepository->findByPk($formValues['cms_photogallery_id']);
        
        // generovani nahledu fotky
        $file = $formValues['file'];
        unset($formValues['file']);
        
        if ($file->isOk()) {
            $formValues['file'] = $file->getSanitizedName();
            try {
                $file->generatePreviews(array(
                    'dir' => WWW_DIR.'/data/photogallery/'.$photogalery->alias,
                    'previews' => $this->context->parameters['admin']['cms']['photogallery']['previews']
                ));
            }
            catch (\Exception $e)
            {
                $this->flashMessage($e->getMessage(), 'error');
                return $this->redirect('edit', array('id' => $requestParams['id']));
            } // end try/catch
        } // end if
        
        $photogalId = null;
        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $photogalId = $requestParams['id'];
            $this->_mainRepository->update($formValues, array('id' => $requestParams['id']));
            
            $formValues = $this->_mainRepository->findByPk($photogalId)->toArray();
            
            $this->flashMessage('Fotografie byla změněna.', 'success');
        }
        else
        {
            $photogalId = $this->_mainRepository->insert($formValues);
            
            $this->flashMessage('Fotografie byla uložena.', 'success');
        } // end if

        return $this->redirect('edit', array('id' => $photogalId));
    } // end function
    
    public function renderEdit($id){
        $this->template->h1 = 'Upravit položku';
        $this->template->formComponentName = 'defaultForm';
        
        $this->template->item = $this->_mainRepository->findByPk($id);

        $this->_itemId = $id;
    }
    
    public function renderDelete($id)
    {
        $photo = $this->_mainRepository->findByPk($id);
        
        if (!is_null($photo))
        {
            $this->_mainRepository->update(array('delete' => '1'), array('id' => $id));
            
            $this->flashMessage('Položka byla přesunuta do koše.');
            
            return $this->redirect('Photogallery:edit', array(
                'id' => $photo->cms_photogallery_id
            ));
        } // end if
        
        $this->flashMessage('Položka nebyla nalezena.');

        return $this->redirect(':Admin:Cms:Photogallery:');
    }

}
