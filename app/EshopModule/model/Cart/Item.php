<?php

namespace App\EshopModule\Model\Cart;
use App\EshopModule\Model\Cart;
use App\EshopModule\Model\ProductWithVariant;

/**
 * Product v kosiku
 */
class Item extends ProductWithVariant
{
    /**
     * @var float
     */
    protected $_qty;

    /**
     * @param $context
     * @param $id
     * @param $qty
     * @param null $variantId
     */
    public function __construct($context, $id, $qty, $variantId = null){
        $this->_qty = floatval($qty);

        // nacteni detailu o produktu
        $productRow = $context->getService('eshopProduct')->findByPk($id);

        if (!is_null($productRow)) {
            // inicializace zakladnich atributu
            $this->initFromActiveRow($productRow);

            // inicializace varinty produktu
            if (!is_null($variantId)) {
                // nacteni varianty produktu
                $variantRow = $context->getService('eshopProductVariant')->findByPk($variantId);

                // inicializace dle nactene varianty produktu
                $this->initFromVariantActiveRow($variantRow);
            }
        }
    }

    /**
     * Metoda vrati ID produktu.
     *
     * @return int
     */
    public function getId(){
        return $this->_id;
    }

    /**
     * Metoda generuje klic polozky.
     *
     * @return string
     */
    /*
    public function getItemKey(){
        return self::getProductKey($this->getId(), $this->getVariantId());
    }
    */

    /**
     * Metoda generuje klic k produktu.
     *
     * @param $productId
     * @param $productVariantId
     * @return string
     */
    public static function getItemKey($productId, $productVariantId){
        return intval($productId).'_'.intval($productVariantId);
    }

    /**
     * Metoda vrati nazev produktu.
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Metoda vrati info o mnozstvi zbozi v kosiku.
     *
     * @return float
     */
    public function getQty(){
        return $this->_qty;
    }

    /**
     * Metoda vrati data o produktu.
     *
     * @return \App\EshopModule\Model\ProductWithVariant
     */
    public function getProduct(){
        return $this;
    }

    /**
     * Metoda upravi mnozstvi produktu.
     *
     * @param $qty
     */
    public function setQuantity($qty){
        $this->_qty = floatval($qty);
    }

    /**
     * Metoda upravi mnozstvi produktu.
     *
     * @param $qty
     */
    public function setQty($qty){
        return $this->setQuantity($qty);
    }

    /**
     * Metoda vrati cenu produktu s DPH.
     *
     * @return float
     */
    public function getPriceWithVat() {
        return parent::getPriceWithVat() * $this->_qty;
    }

    /**
     * Metoda vrati centu produktu bez DPH.
     *
     * @return float
     */
    public function getPriceWithoutVat() {
        return parent::getPriceWithoutVat() * $this->_qty;
    }

}
