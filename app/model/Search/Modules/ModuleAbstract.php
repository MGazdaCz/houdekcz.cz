<?php

namespace App\Model\Search;

abstract class ModuleAbstract {
    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\Repository
     */
    protected $_mainRepository;

    protected $_context;

    public abstract function init();
    public abstract function search($queryString);

    public function setContext($context) {
        $this->_context = $context;
    }

    /**
     * Metoda pripravi vystupni data.
     *
     * @param string $name
     * @param \Nette\Database\Table\Selection $items
     * @return array
     */
    public function prepareReturnData($name, $items) {
        return array(
            'name' => $name,
            'items' => $items
        );
    }

}