<?php

namespace App\AdminModule\CoreModule\Presenters;

use Nette,
    App\Model;


/**
 * User role presenter.
 */
class UserrolePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('coreUserRole');
    }

    public function renderDefault()
    {
        if ($this->getUser()->isInRole('superadmin')) {
            $this->template->items = $this->_mainRepository->findAll();
        } else {
            $this->template->items = $this->_mainRepository->findBy(array(
                'id NOT' => array('superadmin')
            ));
        }
    }

    protected function createComponentDefaultForm()
    {
        $form = new Nette\Application\UI\Form();
        $form
            ->addText('id', 'Alias', 50)
            ->setRequired('Zadejte alias role.');
        $form
            ->addText('name', 'Název', 50)
            ->setRequired('Zadejte název role.');
        $form
            ->addTextArea('description', 'Popis', 80,5);
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}
