function initialize()
{
  var myLatlng = new google.maps.LatLng(49.8610768, 16.321669);

  var mapOptions = {
    scrollwheel: false,
    center: new google.maps.LatLng(49.8610768, 16.321669),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("google_map"),
      mapOptions);

  var contentString =
      '<h1 id="firstHeading" class="firstHeading">MAYDAYSped</h1>' +
      '<p><strong>Spediční společnost</strong><br />Trstěnická 932<br />57001  Litomyšl</p>'

  var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 300
  });

  var image = 'http://www.maydaysped/img/logo-mapa.png';

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      animation: google.maps.Animation.DROP,
      title:"MAYDAYSped",
      icon: image
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });

} // end function