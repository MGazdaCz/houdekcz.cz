<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CmsModule\Model\Photogallery\Helper;
use App\AdminModule\CoreModule\Model\Forms\Controls\ImageInput;
use App\AdminModule\CoreModule\Model\Forms\ImageUpload;
use App\AdminModule\CoreModule\Model\Forms\ManyToMany;
use Nette,
    App\Model;


/**
 * Import presenter.
 */
class ImportexportPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_mainRepository;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Category
     */
    protected $_categoryRepo;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\CategoryProduct
     */
    protected $_categoryProductRepo;

    protected function startup(){
        parent::startup();

        // konfigurace repositaru
        $this->_mainRepository      = $this->context->getService('eshopProduct');
        $this->_categoryRepo        = $this->context->getService('eshopCategory');
        $this->_categoryProductRepo = $this->context->getService('eshopCategoryProduct');

        $this->template->bodyClass = 'module-import';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Import/form.latte';
    }

    public function renderDefault()
    {
        $this->template->h1    = 'Importní / exportní rozhraní';
        //$this->template->items = $this->_mainRepository->findBy($whereArray)->order('_order');
    }

    public function renderImportcategory()
    {
        $this->template->h1 = 'Nový import kategorií';
    }

    protected function createComponentCategoryImportForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addUpload('file', 'Importní soubor');
        $form->addSubmit('submit', 'Importuj');

        $form->onSuccess[] = array($this, 'formCategoryImportSucceeded');

        return $form;
    }

    public function formCategoryImportSucceeded($form, &$values){
        try {
            $file = $values['file'];
            $file->move(TEMP_DIR);

            echo '<pre>'.print_r($values, true).'</pre>';
            die;
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'danger');
        }
    }

}
