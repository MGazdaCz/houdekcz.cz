<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Newsletter presenter.
 */
class NewsletterPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Newsletter
     */
    protected $_repository;

    protected function startup(){
        parent::startup();

        $this->_repository = $this->context->cmsNewsletter;

        $this->template->bodyClass = 'module-newsletter';
    }

    public function renderDefault()
    {
        $this->template->items = $this->_repository->findAll();
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1   = 'Přidání emailu';
        $this->template->edit = false;
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1   = 'Editace emailu: '.$this->_repository->findByPk($id)->email;
        $this->template->edit = true;
    }

    protected function createComponentForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addText('email', 'Email', 50)
            ->setRequired('Zadejte email');
        $form
            ->addText('name', 'Jméno', 50);
        $form
            ->addText('surname', 'Příjmení', 50);
        $form->addSubmit('save', PageBase::SUBMIT_DEFAULT_LABEL);
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_repository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}
