<?php
namespace App\Model\Helper;

class PriceHelper
{
    /**
     * Metoda preformatuje cislo do formatu meny.
     *
     * @param float $price
     * @return string
     */
    public static function format($price)
    {
        $returnPrice = number_format($price, 2, ',', ' ');

        return str_replace(' ', '&nbsp;', $returnPrice);
    }
}