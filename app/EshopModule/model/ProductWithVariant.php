<?php

namespace App\EshopModule\Model;
use Nette\Database\Table\ActiveRow;

/**
 * Trida pro praci s produktem s vybranou variantou.
 */
class ProductWithVariant extends Product
{
    /**
     * @var string
     */
    protected $_variantName;

    /**
     * @var int
     */
    protected $_variantId;

    /**
     * Metoda inicializuje vlastnosti z active row.
     *
     * @param ActiveRow $variantRow
     */
    public function initFromVariantActiveRow(ActiveRow $variantRow){
        $this->_variantId   = $variantRow->id;
        $this->_variantName = $variantRow->name;

        if (empty($this->_variantName) && !empty($variantRow->size_id)) {
            $this->_variantName = $variantRow->size->name;
        }

        // prenastaveni ceny
        if ($variantRow->price > 0) {
            $this->_price = $variantRow->price;
        }
    }

    public function getVariantId() {
        return $this->_variantId;
    }

    public function getVariantName() {
        return $this->_variantName;
    }

}
