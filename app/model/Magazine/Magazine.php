<?php

namespace App\Model\Magazine;

use App\AdminModule\CoreModule\Model\Repository\User;
use App\Model\ColumnListAbstract;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\DI\Container;

/**
 * Created by PhpStorm.
 * User: Milan
 * Date: 31.7.15
 * Time: 21:07
 */
class Magazine extends ColumnListAbstract {
    const FIRST_PAGE_SPECIAL_ITEMS = 3;
    const PAGE_LIMIT               = 10;

    const POSITION_ADVERT1    = 4;
    const POSITION_NEWSLETTER = 6;
    const POSITION_ADVERT2    = 9;

    protected $_author;
    protected $_labels;
    protected $_sort;


    /**
     * Metoda vrati pocet polozek v magazinu.
     *
     * @param Selection $acticlesSelection
     * @return int count
     */
    public function getItemsCount(Selection $acticlesSelection){
        return self::FIRST_PAGE_SPECIAL_ITEMS + $acticlesSelection->count();
    }

    /**
     * Metoda nacte clanky pro magazin.
     *
     * @param Container $context
     * @return mixed
     */
    public function getArticles(Container $context, $pageNumber = 1){
        $articleRepo = $context->getService('cmsArticle');

        if ($pageNumber < 1) {
            $limit = self::PAGE_LIMIT;
        } else {
            $limit = self::PAGE_LIMIT + (self::PAGE_LIMIT * ($pageNumber - 1));
        }

        $whereArray = array(
            'delete' => 0,
        );
        if (!$this->_user->isLoggedIn()) {
            $whereArray[]       = '(datePublication IS NULL OR datePublication <= NOW())';
            $whereArray['view'] = 1;
        }

        if (!empty($this->_labels)) {
            $whereArray['id IN (SELECT cms_article_id FROM cms_article_labels WHERE cms_article_label_id IN (SELECT id FROM cms_article_label WHERE alias IN ?))'] = $this->_labels;
        }
        if (!is_null($this->_author)) {
            $whereArray['author_id = ?'] = $this->_author->id;
        }

        // sestaveni nazvu sloupce pro razeni
        $sort = $this->getSortColumn($this->_sort);

        //return $articleRepo->findBy($whereArray)->order('datePublication DESC')->limit($limit);
        return $articleRepo->findBy($whereArray)->order($sort)->limit($limit);
    }

    /**
     * Metoda prerozdeli data do sloupcu.
     *
     * @param Container $context
     * @param int $pageWidth
     */
    public function getColumnsData(Container $context, $pageWidth, $pageNumber = 1){
        $articleSelection = $this->getArticles($context, $pageNumber);

        // init poctu sloupcu podle rozliseni
        $columnCount = $this->getColumnCount($pageWidth);

        $columnIndex = 0;
        $bannerZone  = 4;
        $itemsCount  = $this->getItemsCount($articleSelection);

        // inicializace navratoveho pole
        $returnData = $this->getReturnArray($columnCount);

        for ($i = 1; $i <= $itemsCount; $i++) {
            if ($i == self::POSITION_NEWSLETTER) {
                $returnData[$columnIndex][] = array(
                    'control' => 'newsletterControl',
                    'method'  => 'box',
                    'param'   => null,
                );
            } elseif (in_array($i, array(self::POSITION_ADVERT1, self::POSITION_ADVERT2))) {
                $returnData[$columnIndex][] = array(
                    'control' => 'advertControl',
                    'method'  => 'boxWithZone',
                    'param'   => $bannerZone++,
                );
            } else {
                $returnData[$columnIndex][] = array(
                    'control' => 'articleBoxControl',
                    'method'  => null,
                    'param'   =>  $articleSelection->fetch(),
                );
            }

            // posunuti iteratoru sloupce
            $columnIndex++;
            if ($columnIndex == $columnCount) $columnIndex = 0;
        }

        return $returnData;
    }

    /**
     * Metoda generuje odkaz pro tlacitko dalsi clanky.
     *
     * @param $basePath
     * @param $page
     * @param $labels
     * @param $sort
     * @return string
     */
    public function getMoreUrl($basePath, $page, $labels, $sort){
        $url = $basePath.'/magazin?do=moreArticles&page='.$page;

        if (!empty($labels)) {
            foreach ($labels as $label) {
                $url .= '&labels[]='.$label;
            }
        }

        if (!empty($sort)) {
            $url .= '&sort='.$sort;
        }

        return $url;
    }

    /**
     * Metoda vrati nazev sloupce se smerem poradi dle zadaneho aliasu.
     *
     * @param $sortAlias
     * @return string
     */
    public function getSortColumn($sortAlias){
        if ($sortAlias == 'most-read') {
            return 'showCount DESC';
        }

        return 'datePublication DESC';
    }

    /**
     * Metoda vrati pole, definujici tabulku pro moznosti razeni.
     * @return array
     */
    public function getSortArray(){
        return array(
            'latest'    => 'Nejnovější',
            'most-read' => 'Nejčtenější',
        );
    }

    /**********************************************************************
     *   Settery a gettery
     *********************************************************************/
    public function setAuthor($author){
        $this->_author = $author;
    }

    /**
     * Metoda se pokusi nastavit autora podle aliasu autora.
     *
     * @param string $authorAlias
     * @throws \Nette\Application\BadRequestException
     */
    public function setAuthorByAlias($authorAlias, User $userRepository) {
        // nacteni detailu autora
        $authorRow = null;
        if (!empty($authorAlias)) {
            $authorRow = $userRepository->findBy(array('alias' => $authorAlias))->fetch();

            if (is_null($authorRow)) {
                throw new \Nette\Application\BadRequestException('Články autora '.$authorAlias.' se nepovedlo nalézt.', 404);
            }

            $this->setAuthor($authorRow);
        }
    }

    /**
     * @return ActiveRow
     */
    public function getAuthor() {
        return $this->_author;
    }

    public function setLabels(array $labels){
        $this->_labels = $labels;
    }

    public function setSort($sort){
        $this->_sort = $sort;
    }

}