<?php

namespace App\EshopModule\Presenters;

use App\EshopModule\Control\Product\InCategoriesControl;
use App\EshopModule\Model\Product;
use Nette;


/**
 * Product presenter.
 */
class ProductPresenter extends BasePresenter
{

    protected function startup(){
        parent::startup();

        $this->template->bodyClass = 'eshop-detail';
    }

    /**
     * Metoda pro renderovani homepage.
     */
    public function renderDefault()
	{
        $url = $this->getParameter('url');

        if (!empty($url)) {
            $url         = $url;
            $productRepo = $this->context->getService('eshopProduct');

            $product = $productRepo->findBy(array('url' => $url, 'view' => 1, 'delete' => 0))->fetch();

            if ($product) {
                $this->template->title         = $product->title;
                $this->template->description   = $product->description;
                $this->template->keywords      = $product->keywords;
                $this->template->product       = $product;
                $this->template->productDetail = Product::getFromActiveRow($product);
                $this->template->photogallery = null;

                /*
                // inicializace skladovych zasob
                $this->template->stock = $product->stock;
                $variants = $product->related('eshop_product_variant.eshop_product_id');
                if ($variants->count() > 0) {
                    $stock = 0;
                    foreach ($variants as $variant) {
                        $stock += $variant->stock;
                    }
                    $this->template->stock = $stock;
                }
                // ---
                */

                if ($product->cms_photogallery_id) {
                    $this->template->photogallery = $product->cms_photogallery;
                }
            } else {
                throw new \Nette\Application\BadRequestException('Produkt nenalezen.', 404);
            }
        } else {
            throw new \Nette\Application\BadRequestException('Produkt nenalezen.', 404);
        }
	}

    /*********************************************
     *      Registrace komponent
     ********************************************/

    public function createComponentInCategories(){
        return new InCategoriesControl();
    }

}
