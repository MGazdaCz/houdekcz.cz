SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+01:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `eshop_action`;
CREATE TABLE `eshop_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `products` text COLLATE utf8_czech_ci NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  `_insertUser` int(10) unsigned NOT NULL,
  `_insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_action` (`id`, `name`, `products`, `count`, `_insertUser`, `_insertDate`) VALUES
(1,	'Doporučené produkty na Homepage',	'4,5,7,972',	4,	1,	'2015-11-06 13:23:39');

DROP TABLE IF EXISTS `eshop_action_product`;
CREATE TABLE `eshop_action_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eshop_action_id` int(10) unsigned NOT NULL,
  `eshop_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eshop_product_id` (`eshop_product_id`),
  KEY `eshop_action_id` (`eshop_action_id`),
  CONSTRAINT `eshop_action_product_ibfk_3` FOREIGN KEY (`eshop_action_id`) REFERENCES `eshop_action` (`id`) ON DELETE CASCADE,
  CONSTRAINT `eshop_action_product_ibfk_2` FOREIGN KEY (`eshop_product_id`) REFERENCES `eshop_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `eshop_action_product` (`id`, `eshop_action_id`, `eshop_product_id`) VALUES
(9,	1,	4),
(10,	1,	5),
(11,	1,	7),
(12,	1,	972);

-- 2015-11-06 13:31:22