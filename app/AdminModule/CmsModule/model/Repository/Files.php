<?php

namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * @author Martin Hromádko <hromadkom@gmail.com>
 */
class Files extends MainRepository {

    protected $_table = 'cms_file';

    public function findFilesWithPhotoIdByDirectory($directory_id) {
        if (is_null($directory_id)) {
            $sql = "SELECT f.name as name, p.id as id
            FROM cms_file f
            JOIN cms_photo p ON p.cms_file_id = f.id
            WHERE f.cms_directory_id IS NULL";
            return $this->getDbContext()->query($sql);
        }

        $sql = "SELECT f.name as name, p.id as id
            FROM cms_file f
            JOIN cms_photo p ON p.cms_file_id = f.id
            WHERE f.cms_directory_id = ?";
        return $this->getDbContext()->query($sql, $directory_id);
    }
}
