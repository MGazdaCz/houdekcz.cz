<?php

namespace  App\AdminModule\CmsModule\Presenters;

use App\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * News presenter.
 */
class NewsPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\CmsModule\Model\Repository\Page
     */
    protected $_repository;

    protected function startup(){
        parent::startup();

        $this->_repository = $this->context->cmsNews;

        $this->template->bodyClass = 'module-news';
    }

    public function renderDefault()
    {
        $this->template->items = $this->_repository->findAll()->order('name');
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Přidání novinky';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace novinky: '.$this->_repository->findByPk($id)->name;
    }

    protected function createComponentForm()
    {
        $form = new PageBase();
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_repository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    public function formSucceeded(Nette\Application\UI\Form $form, $values){
        $id = $values['id'];

        unset($values['id']);

        if (empty($id)) {
            $result = $this->_repository->insert($values);
        } else {
            $result = $this->_repository->update($values, array(
                'id' => $id
            ));
        }

        $this->flashMessage('Záznam byl uložen.');

        $this->redirect('default');

        /*
        echo '<pre>'.print_r($this->getParameters(), true).'</pre>';
        echo '<pre>'.print_r($result, true).'</pre>';
        echo '<pre>'.print_r($values, true).'</pre>';
        die;
        */
    }

}
