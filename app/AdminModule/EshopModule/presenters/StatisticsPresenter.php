<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CmsModule\Model\Photogallery\Helper;
use App\AdminModule\CoreModule\Model\Forms\Controls\ImageInput;
use App\AdminModule\CoreModule\Model\Forms\ImageUpload;
use App\AdminModule\CoreModule\Model\Forms\ManyToMany;
use Nette,
    App\Model;


/**
 * Statistics presenter.
 */
class StatisticsPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_mainRepository;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Category
     */
    protected $_categoryRepo;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\CategoryProduct
     */
    protected $_categoryProductRepo;

    protected function startup(){
        parent::startup();

        // konfigurace repositaru
        $this->_mainRepository      = $this->context->getService('eshopProduct');
        $this->_categoryRepo        = $this->context->getService('eshopCategory');
        $this->_categoryProductRepo = $this->context->getService('eshopCategoryProduct');

        $this->template->bodyClass = 'module-eshop-statistics';

        // nastaveni defaultni sablony formulare
        //$this->_formTemplate = __DIR__.'/../templates/Product/form.latte';
    }

    public function renderDefault()
    {
        $this->template->h1 = 'Statistiky eshopu - základní přehled';

        $this->template->productsCount             = $this->_mainRepository->findAll()->count('id');
        $this->template->productsViewsCount        = $this->_mainRepository->findBy(array('view' => 1))->count('id');
        $this->template->categoriesCount           = $this->_categoryRepo->findAll()->count('id');
        $this->template->categoriesViewsCount      = $this->_categoryRepo->findBy(array('view' => 1))->count('id');
        $this->template->productsInCategoriesCount = $this->_categoryProductRepo->findAll()->count('id');
    }

    public function renderProductsincategories()
    {
        $this->template->h1 = 'Statistiky eshopu - produkty v kategoriích';

        $dotaz = 'SELECT eshop_product_id AS id, p.name, COUNT(eshop_category_id) AS count
                    FROM eshop_category_product cp
                    JOIN eshop_product p ON p.id = cp.eshop_product_id
                    GROUP BY eshop_product_id
                    ORDER BY count DESC, id';
        //HAVING count > 1

        $this->template->products = $this->_mainRepository->getDbContext()->query($dotaz);
    }

}
