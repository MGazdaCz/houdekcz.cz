<?php
namespace App\AdminModule\ErpModule;

use Model\Helper\TimeHelper;
use Nette\Database\Table\ActiveRow;
use Nette\DateTime;

class Task
{
    private $_id;
    private $_dateFrom;
    private $_timestampFrom;
    private $_dateTo;
    private $_timestampTo;
    private $_timeLength;
    private $_insertDate;
    private $_note;
    private $_issuetId;
    private $_issue;

    public function __construct()
    {
        $this->_project = null;
    }

    public static function createFromFormRequest(array $data)
    {
        $dateFrom = new \Nette\DateTime($data['dateFrom'].' '.$data['timeFrom']);
        $dateTo   = new \Nette\DateTime($data['dateTo'].' '.$data['timeTo']);

        $task = new self;
        $task->_dateFrom      = $dateFrom->format('Y-m-d H:i:s');
        $task->_timestampFrom = $dateFrom->getTimestamp();
        $task->_dateTo        = $dateTo->format('Y-m-d H:i:s');
        $task->_timestampTo   = $dateTo->getTimestamp();
        $task->_issuetId      = $data['rel_issue'];
        $task->_note          = $data['note'];

        $task->_timeLength = $task->_timestampTo - $task->_timestampFrom;

        return $task;
    } // end function

    public static function createFromActiveRow(ActiveRow $row)
    {
        $task = new self;
        $task->_id            = $row->id;
        $task->_dateFrom      = $row->dateFrom;
        $task->_timestampFrom = $row->timestampFrom;
        $task->_dateTo        = $row->dateTo;
        $task->_timestampTo   = $row->timestampTo;
        $task->_timeLength    = $row->timeLength;
        $task->_insertDate    = $row->insertDate;
        $task->_issuetId      = $row->rel_issue;
        $task->_note          = $row->note;

        $task->_issue = $row->ref('rel_issue');

        return $task;
    }

    public function getArrayForSql()
    {
        return array(
            'rel_issue'     => $this->_issuetId,
            //'rel_project'   => null,
            'dateFrom'      => $this->_dateFrom,
            'timestampFrom' => $this->_timestampFrom,
            'dateTo'        => $this->_dateTo,
            'timestampTo'   => $this->_timestampTo,
            'timeLength'    => $this->_timeLength,
            'note'          => $this->_note,
        );
    }

    public function getArrayForForm()
    {
        $dateFrom = new \Nette\DateTime($this->_dateFrom);
        $dateTo   = new \Nette\DateTime($this->_dateTo);

        return array(
            'rel_issue'   => $this->_issuetId,
            'dateFrom'    => $dateFrom->format('d.m.Y'),
            'timeFrom'    => $dateFrom->format('H:i'),
            'dateTo'      => $dateTo->format('d.m.Y'),
            'timeTo'      => $dateTo->format('H:i'),
            'note'        => $this->_note,
        );
    }
    
    public function get($attr, $default = null){
        // pokusim se vratit atribut ze zakladniho objektu
        $attrPrefix = '_'.$attr;
        if (isset($this->$attr))
            return $this->$attr;
        if (isset($this->$attrPrefix))
            return $this->$attrPrefix;

        // pokusim se vratit atribut z relace
        $attrArray = explode('_', $attr);
        $attrLong  = $attr;
        if (count($attrArray) == 2)
        {
            $selfAtribute = '_'.$attrArray[0];
            $attr         = $attrArray[1];

            if (isset($this->$selfAtribute) && isset($this->$selfAtribute->$attr))
                return $this->$selfAtribute->$attr;
        } // end if

        // vratim defaultni hodnotu
        return $default;
    }

    public function __get($attr){
        return $this->get($attr);
    }

    public function getTimeSpent(){
        return $this->_timeLength;
    }
}