<?php
namespace App\EshopModule\Control\Product;

use Nette\Application\UI\Control;
use Nette\Database\Table\ActiveRow;

/**
 * Komponenta pro generovani boxiku pro detail produktu s kategoriemi produktu.
 *
 *  "Produkt je zařazen v následujících kategoriích"
 *
 * @package App\EshopModule\Control\Product
 */
class InCategoriesControl extends Control
{
    public function render(ActiveRow $product)
    {
        $categoryRepo = $this->getPresenter()->context->getService('eshopCategory');

        $categories = $categoryRepo->findBy(array(
            'id IN (SELECT eshop_category_id FROM eshop_category_product WHERE eshop_product_id = ?)' => $product->id,
            'view'   => 1,
            'delete' => 0
        ));


        $this->template->items = $categories;
        $this->template->setFile(__DIR__.'/templates/inCategoriesDefault.latte');
        $this->template->render();
    }
}