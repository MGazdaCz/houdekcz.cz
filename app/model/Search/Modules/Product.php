<?php

namespace App\Model\Search;

use Nette\Utils\Strings;

class Product extends ModuleAbstract {

    public function init()
    {
        $this->_mainRepository = $this->_context->getService('eshopProduct');
    }

    public function search($queryString)
    {
        $queryString = '%'.addslashes(Strings::lower($queryString)).'%';

        $condition  = 'delete = 0 AND view = 1 AND ';
        $condition .= '(LOWER(name) LIKE ? OR LOWER(heading) LIKE ? OR LOWER(annotation) LIKE ? OR LOWER(text) LIKE ? OR LOWER(sku) LIKE ?)';

        $dbSelection = $this->_mainRepository->getTable()->where($condition, $queryString, $queryString, $queryString, $queryString, $queryString);

        return $this->prepareReturnData('Produkty eshopu', $dbSelection);
    }

}