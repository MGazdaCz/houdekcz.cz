/**
 * Created by Milan on 6.8.15.
 */
CKEDITOR.plugins.add( 'wrdixierelated', {
    //icons: 'related',
    init: function( editor ) {
        // Plugin logic goes here...

        editor.ui.addButton( 'Related', {
            label: 'Vložit související články',
            command: 'insertRelatedCode',
            toolbar: 'wr',
            icon: this.path + 'icons/related.png'
        });

        editor.ui.addButton( 'Banner', {
            label: 'Vložit banner do článku',
            command: 'insertBannerCode',
            toolbar: 'wr',
            icon: this.path + 'icons/banner.png'
        });

        editor.addCommand( 'insertRelatedCode',
        {
            exec : function( editor )
            {
                editor.insertHtml('<div class="related-inline"><h2>Mohlo by se vám také líbit</h2>{souvisejici-clanky}</div>');
            }
        });

        editor.addCommand( 'insertBannerCode',
            {
                exec : function( editor )
                {
                    editor.insertHtml('<p class="banner-inline">{banner}</p>');
                }
            });
    }
});