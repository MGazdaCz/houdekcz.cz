<?php
namespace App\Control\Usermenu;

use Control;

class UserMenuControl extends Control\AbstractControl
{
    public function render()
    {
        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

    public function renderHomepage() {
        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

    public function renderMagazine($pageId) {
        $this->template->pageId = $pageId;

        $this->template->setFile(__DIR__.'/templates/magazine.latte');
        $this->template->render();
    }

    public function renderArticle($articleId, $authorId, $photogalleryId) {
        $this->template->articleId      = $articleId;
        $this->template->authorId       = $authorId;
        $this->template->photogalleryId = $photogalleryId;

        $this->template->setFile(__DIR__.'/templates/article.latte');
        $this->template->render();
    }

}