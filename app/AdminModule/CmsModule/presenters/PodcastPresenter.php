<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;


/**
 * Podcast presenter.
 */
class PodcastPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Podcast
     */
    protected $_podcastCategoryRepo;

    protected function startup(){
        parent::startup();

        $this->_mainRepository      = $this->context->getService('cmsPodcast');
        $this->_podcastCategoryRepo = $this->context->getService('cmsPodcastCategory');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Podcasty';
        $this->_orderDefault = 'id DESC';
    }

    protected function _beforeRenderDefault(&$querySelection){
        $categoryId = $this->getParameter('categoryId');

        if (!empty($categoryId)) {
            $querySelection->where('cms_podcast_category_id = ?', $categoryId);
        }
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace podcastu: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $categoryArray = $this->_podcastCategoryRepo->fetchPairs('id', 'name');

        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addSelect('cms_podcast_category_id', 'Kategorie', $categoryArray);
        $form
            ->addText('name', 'Název', 80)
            ->setAttribute('placeholder', 'Název podcastu')
            ->setRequired('Zadejte název podcastu');
        $form
            ->addText('datePublication', 'Datum publikace')
            ->setAttribute('class', 'date-picker')
            ->setRequired('Vyberte datum publikace.');
        $form
            ->addText('timePublication', 'Čas publikace')
            ->setAttribute('placeholder', 'HH:MM')
            ->addRule(Nette\Application\UI\Form::PATTERN, 'Zadejte čas ve správném formátu HH:MM (např. 08:20).', '([0-9]){2}:([0-9]){2}');
        $form
            ->addCheckbox('view', 'zobrazit');
        $form
            ->addUpload('file', 'Podcast');
        $form
            ->addTextArea('text', 'Text', 80, 10)
            ->setAttribute('class', 'ckeditor');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $itemArray = $item->toArray();

                if (!empty($itemArray['datePublication'])) $itemArray['timePublication'] = $itemArray['datePublication']->format('H:i');
                if (!empty($itemArray['datePublication'])) $itemArray['datePublication'] = $itemArray['datePublication']->format('d.m.Y');

                $form->setDefaults($itemArray);
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // osetreni data a casu publikace
        $values['datePublication'] = Nette\Utils\DateTime::from($values['datePublication'].' '.$values['timePublication'])->format('Y-m-d H:i:s');
        unset($values['timePublication']);

        // osetreni uploadu podcastu
        if ($values['file']->isOk()) {
            $fileNameArray = explode('.', $values['file']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['file']->move(DATA_DIR . "/podcast/$filename");
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['file'] = $filename;
        } else {
            unset($values['file']);
        }
    }

}
