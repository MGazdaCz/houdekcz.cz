<?php

namespace App\AdminModule\EshopModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Delivery extends MainRepository
{
    protected $_table = 'eshop_delivery';
}