<?php

namespace App\AdminModule\CmsModule\Presenters;

use Model\Form\FormEditorDetail;
use App\AdminModule\CoreModule\Model\Forms\ImageUpload;
use \Nette\Application\UI\Form;
use \Nette\Utils\Strings;

/**
 * Photogallery presenter.
 */
class PhotogalleryPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    private $_itemRepository;
    
    protected function startup(){
        parent::startup();
        $this->_mainRepository = $this->context->getService('cmsPhotogallery');
        $this->_itemRepository = $this->context->getService('cmsPhotogalleryItem');

        $this->_orderDefault = 'name';
        $this->_listH1 = 'Fotogalerie';
    }

    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        //$form->addHidden('id');
        $form->addText('name', 'Název', 75)->setRequired();
        $form->addText('alias', 'Alias')->setDisabled();
        $form->addCheckbox('view', 'Shváleno');
        $form->addTextArea('description', 'Popis', 75, 5);
        $form->addUpload('photos', 'Fotografie', true);
        $form->addSubmit('submit', 'Ulož')->setAttribute('class', 'btn btn-sm btn-info');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $page = $this->_mainRepository->findByPk($this->_itemId);
            $form->setDefaults($page);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        $formValues = $form->getValues(true);
        $photos     = $formValues['photos'];
        unset($formValues['photos']);
        
        $photogalId = null;
        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $photogalId = $requestParams['id'];
            $this->_mainRepository->update($formValues, array('id' => $requestParams['id']));
            
            $formValues = $this->_mainRepository->findByPk($photogalId)->toArray();
            
            $this->flashMessage('Fotogalerie byla změněna.', 'success');
        }
        else
        {
            $formValues['alias'] = Strings::webalize($formValues['name']);
            $photogalId          = $this->_mainRepository->insert($formValues);
            
            $this->flashMessage('Fotogalerie byla založena.', 'success');
        } // end if
        
        // overeni existence adresare galerie
        $dirPath = WWW_DIR.'/data/photogallery/'.$formValues['alias'];
        if (!empty($formValues['alias']) && !is_dir($dirPath))
        {
            if (!mkdir($dirPath))
            {
                $this->flashMessage('Chyba při vytváření adresáře "'.$formValues['alias'].'" (path: '.$dirPath.').');
            } // end if
        } // end if
        
        // samotne zpracovani fotek
        if (!empty($photos))
        {
            foreach ($photos as $fileUpload) {
                $imageUpload = new ImageUpload($fileUpload);
                
                $state = false;
                try {
                    $imageUpload->generatePreviews(array(
                        'dir' => WWW_DIR.'/data/photogallery/'.$formValues['alias'],
                        'previews' => $this->context->parameters['admin']['cms']['photogallery']['previews']
                    ));
                    
                    $state = true;
                } catch (\Exception $e) {
                    $this->flashMessage($e->getMessage());
                } // end try/catch
                
                if ($state)
                {
                    $this->_itemRepository->insert(array(
                        'file' => $imageUpload->getSanitizedName(),
                        'cms_photogallery_id' => $photogalId
                    ));
                } // end if
                
                unset($imageUpload);
            } // end foreach
        } // end if

        return $this->redirect('default');
    } // end function
    
    public function renderEdit($id){
        $this->template->h1                = 'Upravit položku';
        $this->template->formComponentName = 'defaultForm';

        $this->_itemId = $id;
        
        $this->template->photogallery = $this->_mainRepository->findByPk($id);
    }
    
}
