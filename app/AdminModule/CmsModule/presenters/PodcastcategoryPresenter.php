<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Podcastcategory presenter.
 */
class PodcastcategoryPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\User
     */
    private $_userRepository;

    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsPodcastCategory');
        $this->_userRepository = $this->context->getService('coreUser');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Kategorie podcastů';
        $this->_orderDefault = 'id';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace kateogrie: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $authors = $this->_userRepository->fetchPairsWithWhere('id', 'name surname', array('active' => 1), false, ' ');
        //$form = new Nette\Application\UI\Form();
        $form = new PageBase();
        $form->initFormItems();

        unset($form['heading']);
        unset($form['showHeading']);

        $form
            ->addSelect('author_id', 'Autor', $authors);
        $form
            ->addText('alias', 'Alias', 80)
            ->setAttribute('placeholder', 'Alias kategorie');
        $form
            ->addUpload('logo', 'Logo kategorie');
        $form
            ->addUpload('image', 'Obrázek kategorie');
        $form
            ->addUpload('categoryBanner', 'Banner podcastu pro HP');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        if (empty($values['alias'])) {
            $values['alias'] = Nette\Utils\Strings::webalize($values['name']);
        }
        if (empty($values['title'])) {
            $values['title'] = $values['name'];
        }
        if (empty($values['url'])) {
            $values['url'] = '/'.Nette\Utils\Strings::webalize($values['name']);
        }
        $values['heading']     = $values['name'];
        $values['showHeading'] = 1;

        // osetreni uploadu LOGA
        if ($values['logo']->isOk()) {
            $fileNameArray = explode('.', $values['logo']->getName());

            $filename = 'logo_'.time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['logo']->move(DATA_DIR . "/podcastcategory/$filename");
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['logo'] = $filename;
        } else {
            unset($values['logo']);
        }

        // osetreni uploadu OBRAZKU
        if ($values['image']->isOk()) {
            $image = $values['image']->toImage();

            $fileNameArray = explode('.', $values['image']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['image']->move(DATA_DIR . "/podcastcategory/$filename");

                if ($image) {
                    $image->resize(120,90);
                    $image->save(DATA_DIR . "/podcastcategory/preview/$filename");
                }
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['image'] = $filename;
        } else {
            unset($values['image']);
        }

        // osetreni uploadu OBRAZKU banneru kategorie
        if ($values['categoryBanner']->isOk()) {
            $image = $values['categoryBanner']->toImage();

            $fileNameArray = explode('.', $values['categoryBanner']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['categoryBanner']->move(DATA_DIR . "/podcastcategory/banner/$filename");

                if ($image) {
                    $image->resize(120,90);
                    $image->save(DATA_DIR . "/podcastcategory/banner/preview/$filename");
                }
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['categoryBanner'] = $filename;
        } else {
            unset($values['categoryBanner']);
        }
    }

}
