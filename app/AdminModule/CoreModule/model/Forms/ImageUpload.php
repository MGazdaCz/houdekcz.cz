<?php
namespace App\AdminModule\CoreModule\Model\Forms;

use Nette\Utils\Strings;

class ImageUpload
{               
    private $_fileUpload;
    
    /**
    * Konstruktor vytvori objekt z predaneho objektu typu FileUpload
    * 
    * @author MiG
    * @param \Nette\Http\FileUpload $file
    * @return void
    */
    function __construct($file) {
        if (is_null($file))
        {
            $this->_fileUpload = new \Nette\Http\FileUpload(null);
        }
        else
        {
            $this->_fileUpload = $file;
        } // end if
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param array $options
    * @return void
    */
    function generatePreviews(array $options) {
        if (!$this->_fileUpload->isImage())
        {
            throw new \Exception('Soubor '.$this->_fileUpload->getName().' není obrázkem.');
        } // end if
        
        if ($this->_fileUpload->getSize() > 3000000)
        {
            throw new \Exception('Souboru '.$this->_fileUpload->getName().' není možné změnit velikost. Je větší než 3 MB.');
        } // end if)
        
        $dirRoot = $options['dir'];
        if (!is_dir($dirRoot))
        {
            if (mkdir($dirRoot))
            {
                chmod($dirRoot, 0777);
            }
            else
            {
                throw new \Exception('Adresář neexistuje: '.$dirRoot.'.');
            } // end if
        } // end if

        $fileName = $this->_fileUpload->getSanitizedName();
        if (isset($options['fileName'])) {
            $fileName = $options['fileName'];
        }
        
        if (!empty($options['previews']))
        {
            $image = $this->_fileUpload->toImage();
            
            foreach ($options['previews'] as $preview) {
                $dir = $dirRoot.'/'.$preview['directory'].'/';
                
                if (!is_dir($dir))
                {
                    if (mkdir($dir))
                    {
                        chmod($dir, 0777);
                    }
                    else
                    {
                        throw new \Exception('Adresář neexistuje: '.$dirRoot.'.');
                    } // end if
                } // end if
                
                $pw = clone $image;
                $pw->resize($preview['x'], $preview['y']);
                $pw->save($dir.$fileName);
                
                unset($pw);
            } // end foreach
        } // end if
        
        // ulozeni originalu
        $this->_fileUpload->move($dirRoot.'/original/'.$this->_fileUpload->getSanitizedName());
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return string
    */
    function getSanitizedName() {
        return $this->_fileUpload->getSanitizedName();
    } // end function

    /**
     * put your comment there...
     *
     * @author MiG
     * @param void
     * @return string
     */
    function getName() {
        return $this->_fileUpload->getName();
    }

    /**
     * Metoda vrati priponu souboru.
     *
     * @return string
     */
    function getExtension() {
        $nameParts = explode('.', $this->getName());

        if (count($nameParts) > 1) {
            return $nameParts[count($nameParts) - 1];
        }

        return 'null';
    }

    /**
     * Metoda vrati priponu souboru.
     *
     * @return string
     */
    public function getSuffix(){
        return $this->getExtension();
    }

    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return bool
    */
    function isOk() {
        return $this->_fileUpload->isOk();
    } // end function
    
} // end class