<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;


/**
 * Articlecolor presenter.
 */
class ArticlecolorPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsArticleColor');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Barvy efektů';
        $this->_orderDefault = '_order';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace barvy: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addText('name', 'Název', 80)
            ->setAttribute('placeholder', 'Název štítku')
            ->setRequired('Zadejte název štítku');
        $form->addText('alias', 'Alias', 50);
        $form
            ->addCheckbox('view', 'zobrazit');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        if (empty($values['alias'])) {
            $values['alias'] = Nette\Utils\Strings::webalize($values['name']);
        }
    }

}
