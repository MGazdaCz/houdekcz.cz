<?php
namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository;

use Nette;

class Page extends Repository
{
    protected $_table = 'cms_page';

    /**
     * Metoda nacte a vrati homepage pro prislusnou mutaci.
     *
     * @param string $language
     * @return Nette\Database\Table\Selection
     */
    public function getHomepage($language = 'cz'){
        return $this->findBy(array(
            'homepage = 1', 'view = 1', 'delete = 0', 'core_language_id = ?' => $language,
            '(dateFrom IS NULL AND dateTo IS NULL) OR (dateFrom <= CURDATE() AND dateTo IS NULL) OR (dateFrom IS NULL AND dateTo >= CURDATE()) OR (dateFrom <= CURDATE() AND dateTo >= CURDATE())'
        ))->limit(1);
    }

    /**
     * Metoda sestavi dotaz pro nacteni podstranek.
     *
     * @param int $pageId
     * @return Nette\Database\Table\Selection
     */
    public function getSubpages($pageId) {
        return $this->findBy(array(
            '_parent = ?' => $pageId, 'view = 1', 'delete = 0',
            '(dateFrom IS NULL AND dateTo IS NULL) OR (dateFrom <= CURDATE() AND dateTo IS NULL) OR (dateFrom IS NULL AND dateTo >= CURDATE()) OR (dateFrom <= CURDATE() AND dateTo >= CURDATE())'
        ));
    }

    public function findVisiblePageByUrl($url) {
        return $this->findBy(array(
            'url = ?' => $url, 'view = 1', 'delete = 0',
            '(dateFrom IS NULL AND dateTo IS NULL) OR (dateFrom <= CURDATE() AND dateTo IS NULL) OR (dateFrom IS NULL AND dateTo >= CURDATE()) OR (dateFrom <= CURDATE() AND dateTo >= CURDATE())'
        ));
    }

}

