<?php

namespace App\EshopModule\Presenters;

use Nette,
	App\Model;


/**
 * Cart presenter.
 */
class CartPresenter extends BasePresenter
{

    public function renderDefault(){
        return $this->redirect(301, 'Checkout:default');
    }

    /**
     * Metoda pro pridani zbori do kosiku.
     */
    public function renderAdd($productId, $qty = 1)
	{
        $productVariantId = $this->getParameter('variantId');

        $cartItem = $this->context->getService('cart')->addProduct($this->context, $productId, $qty, $productVariantId);

        if (!is_null($cartItem)) {
            //$this->flashMessage('Produkt přidán do nákupního košíku.');
        } else {
            $this->flashMessage('Produkt se nepovedlo přidat do košíku. Zkuste to prosím později.');
        }

        $this->template->product = $cartItem;
        $this->template->deliveryFreeFrom = null;

        if (defined('DELIVERY_FREE_FROM') && DELIVERY_FREE_FROM > 0) {
            $this->template->deliveryFreeFrom    = DELIVERY_FREE_FROM;
            $this->template->priceToDeliveryFree = $this->context->getService('checkout')->getPriceToDeliveryFree();
        }

        if ($this->isAjax()) {
            // prekresleni KOMPONENTY kosiku v hlavicce
            $this['cartMiniControl']->invalidateControl();
            $this->redrawControl('dialog');
            $this->redrawControl('flashMessages');
        } else {
            $this->setView('null');
            $this->redirect(301, ':Eshop:Homepage:');
        }

        //$this->redirect(301, 'Checkout:default');
	    //echo '<pre>'.print_r($_SERVER["REQUEST_URI"], true).'</pre>';
	    //echo '<pre>'.print_r($page, true).'</pre>';
        //die;
	}

    /**
     * Metoda pro pridani zbori do kosiku.
     */
    public function renderRemove($productId, $variantId)
    {
        $this->context->getService('cart')->removeProduct($productId, $variantId);

        // prekresleni KOMPONENTY kosiku v hlavicce
        $this['cartMiniControl']->invalidateControl();

        //$this->redirect(301, 'Checkout:default');
        //echo '<pre>'.print_r($_SERVER["REQUEST_URI"], true).'</pre>';
        //echo '<pre>'.print_r($page, true).'</pre>';
        //die;
    }

    /**
     * Metoda pro pridani zbori do kosiku.
     */
    public function renderChangeqty($productId, $variantId, $qty)
    {
        $this->setView('null');

        $this->context->getService('cart')->changeQuantity($productId, $qty, $variantId);

        if ($this->isAjax()) {
            // prekresleni KOMPONENTY kosiku v hlavicce
            $this['cartMiniControl']->invalidateControl();
            $this->redrawControl('flashMessages');
        } else {
            $this->redirect(301, ':Eshop:Homepage:');
        }

        //$this->redirect(301, 'Checkout:default');
        //echo '<pre>'.print_r($_SERVER["REQUEST_URI"], true).'</pre>';
        //echo '<pre>'.print_r($page, true).'</pre>';
        //die;
    }

    /**
     * Metoda pro pridani zbori do kosiku.
     */
    public function renderTruncate()
    {
        $this->setView('null');

        $this->context->getService('cart')->truncate();

        $this->redirect(301, ':Eshop:Homepage:');
        //echo '<pre>'.print_r($_SERVER["REQUEST_URI"], true).'</pre>';
        //echo '<pre>'.print_r($page, true).'</pre>';
        //die;
    }

}
