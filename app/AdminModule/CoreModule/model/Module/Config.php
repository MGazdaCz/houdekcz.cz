<?php
namespace App\AdminModule\CoreModule\Model\Module;
use Nette;

/**
 * Prace s konfiguraci modulu.
 */
class Config extends Nette\Object implements \ArrayAccess
{
    protected $_config;
    protected $_presenter;
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param mixed $value
    * @return self
    */
    function setPresenter($presenter) {
        $this->_config    = array();
        $this->_presenter = $presenter;
        return $this;
    } // end function
    
    /**
    * Metoda vytahne konfiguraci z configu.
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function load() {
        $nameArray = explode(':',$this->_presenter->name);

        if (!empty($nameArray) && count($nameArray) == 3)
        {
            $confTmp = $this->_presenter->context->container->parameters;
            foreach ($nameArray as $name)
            {
                $nameLower = strtolower($name);
                if (!isset($confTmp[$nameLower]))
                    return $this;

                $confTmp = $confTmp[strtolower($name)];
            } // end foreach

            $this->_config = $confTmp;
        }
        else
        {
            //throw new \Exception('Chyba při načítání konfigu. Jméno presenteru se neskládá ze tří částí - '.$this->name);
        } // end if
        
        return;
    } // end function
    
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_config[] = $value;
        } else {
            $this->_config[$offset] = $value;
        }
    }
    public function offsetExists($offset) {
        return isset($this->_config[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->_config[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->_config[$offset]) ? $this->_config[$offset] : null;
    }
    
} // end class