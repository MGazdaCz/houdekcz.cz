$(document).ready(function(){

    $("a.mg-delete").click(function(){
        
        // moznost vlozeni custom hlasky pro confirm dialog
        var attrDataConfirm = $(this).attr("data-confirm");
        var confirmMessage  = 'Opravdu chcete záznam vymazat?';
        
        if (attrDataConfirm != undefined && attrDataConfirm.length)
        {
            confirmMessage = attrDataConfirm;
        } // end if
        
        if (confirm(confirmMessage))
        {
            return true;
        } // end if
        
        return false;
    });
    
});