<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;


/**
 * Banner presenter.
 */
class BannerPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsBanner');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Typy bannerů';
        $this->_orderDefault = 'id';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace typu: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $bannerTypeRepo = $this->context->getService('cmsBannerType');
        $types = $bannerTypeRepo->fetchPairs('id', 'name');

        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addText('name', 'Název', 80)
            ->setAttribute('placeholder', 'Orientační název banneru')
            ->setRequired('Zadejte název banneru');
        $form
            ->addText('url', 'Url adresa', 80)
            ->setAttribute('placeholder', 'Url adresa ve formátu http://www.mojedomena.com')
            ->addCondition(Nette\Application\UI\Form::FILLED)
            ->addRule(Nette\Application\UI\Form::URL, 'Zadejte url adresu ve správném tvaru');
        $form
            ->addSelect('cms_banner_type_id', 'Typ banneru', $types);
        $form
            ->addUpload('image', 'Obrázek');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        if ($values['image']->isOk()) {
            $image = $values['image']->toImage();

            $fileNameArray = explode('.', $values['image']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['image']->move(DATA_DIR . "/banner/$filename");

                if ($image) {
                    $image->resize(120,90);
                    $image->save(DATA_DIR . "/banner/preview/$filename");
                }
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['image'] = $filename;
        } else {
            unset($values['image']);
        }
    }

}
