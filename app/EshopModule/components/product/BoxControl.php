<?php
namespace App\EshopModule\Control\Product;

use App\EshopModule\Model\Product;
use App\Model\Helper\PriceHelper;
use Nette\Application\UI\Control;
use Nette\Database\Table\ActiveRow;

class BoxControl extends Control
{
    private $_cmsAction;

    public function render(ActiveRow $row)
    {
        $this->template->productRow = $row;
        $this->template->product    = Product::getFromActiveRow($row);

        $this->template->registerHelper('wrPrice', function($s){
            return PriceHelper::format($s);
        });

        $this->template->setFile(__DIR__.'/templates/box.latte');
        $this->template->render();
    }

    public function renderDemo()
    {
        $this->_cmsAction = $this->getParent()->context->getService('cmsAction');

        $this->template->wwwRoot = WWW_ROOT;
        $this->template->item    = $this->_cmsAction->findByPk(1);

        $this->template->setFile(__DIR__.'/templates/boxDemo.latte');
        $this->template->render();
    }

    protected function createComponentProductAddControl() {
        return new \App\EshopModule\Control\Product\AddProductControl();
    }

}