<?php
namespace App\AdminModule\CoreModule\Component;

use \Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class FulltextSearchControl extends Control
{
    protected $_settings;

    /**
     * Konstruktor nastavi sloupce, ve kterych se bude vyhledavat.
     *
     * @param array $settings
     */
    public function __construct($settings){
        $this->_settings = $settings;
    }

    /**
     * Komponenta pro vygenerovani formulare pro fulltextove vyhledavani.
     */
    public function render()
    {
        $this->template->settings = $this->_settings;

        $this->template->setFile(__DIR__.'/templates/fulltextSearch.latte');
        $this->template->render();
    }

    /**
     * Vytvoreni komponenty vyhledavaciho formulare.
     *
     * @return Form
     */
    protected function createComponentForm(){
        $form = new \Nette\Application\UI\Form();
        $form->setMethod(Form::GET);
        $form
            ->addText('q', 'Hledaný text')
            ->addRule(Form::MIN_LENGTH, 'Hledaný řetězec musí mít alespoň %d znaky.', 2);
        //$form->addCheckboxList('fields', 'Prohledávané pole', $this->_settings);

        $form->setValues(array(
            'q' => $this->parent->getParameter('q')
        ));

        $form->addSubmit('search', 'Hledej');

        return $form;
    }

    /**
     * Metoda vrati string pro SQL order.
     *
     * @deprecated
     * @return string
     */
    public function getWhereForQuery(){
        $searchText = $this->parent->getParameter('q');

        if (!is_null($searchText)) {
            $returnArray = array();
            foreach ($this->_settings as $colName) {
                $returnArray[] = "LOWER($colName) LIKE '%$searchText%'";
            }

            if (!empty($returnArray)) {
                return implode(' OR ', $returnArray);
            }
        }

        return '';
    }

    /**
     * Metoda rozsiri podminkove pole pro SQL dotaz.
     *
     * @param array $whereArray
     */
    public function extendWhereArray(&$whereArray){
        // ziskani hledaneho stringu
        $searchText = $this->parent->getParameter('q');

        if (!is_null($searchText)) {
            // sestaveni podminky
            $whereArray = array();
            $dataArray  = array();
            foreach ($this->_settings as $colName) {
                $returnArray[] = "LOWER($colName) LIKE ?";
                $dataArray[]   = "%$searchText%";
            }

            // predani podminky do vyhledavaciho pole
            if (!empty($returnArray)) {
                $whereArray[implode(' OR ', $returnArray)] = $dataArray;
            }
        }
    }

}