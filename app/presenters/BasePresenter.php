<?php

namespace App\Presenters;

use App\Component\Paginator\PaginatorControl;
use App\Control\Article\BoxControl;
use App\Control\Cart\MiniControl;
use App\Control\Photogallery\PhotogalleryControl;
use App\Control\Textbox\TextboxControl;
use App\Model\Helper\PriceHelper;
use Nette\Utils\Strings;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
    /** @var \App\Forms\RegisterFormFactory @inject */
    public $registerFormFactory;

    /** @var \App\Forms\LoginFormFactory @inject */
    public $loginFormFactory;

    /** @var \App\Forms\ContactFormFactory @inject */
    public $contactFormFactory;

    protected function startup(){
        parent::startup();

        $this->template->wwwRoot     = WWW_ROOT;
        $this->template->ogUrl       = WWW_ROOT;
        $this->template->bottomClass = '';
    }

    /**
     * Metoda vrati URL stranky.
     *
     * @return string
     */
    public function getPageUrlString(){
        return '/'.$this->getParameter('url', '');
    }

    /**
     * Metoda vrati cisty nazev presenteru.
     *
     * @return string
     */
    public function getPureName()
    {
        $pos = strrpos($this->name, ':');
        if (is_int($pos)) {
            return substr($this->name, $pos + 1);
        }

        return $this->name;
    }

    /**
     * put your comment there...
     *
     * @author MiG
     * @param void
     * @return void
     */
    function getPage() {
        return null;
    } // end function

    /**
     * Metoda detekuje, zda se jedna o proverovany modul.
     *
     * @param $module
     * @return bool
     */
    public function isModuleCurrent($module)
    {
        $module = trim((string) $module);

        $pos = strrpos($this->getName(), ':');
        $current = $pos !== FALSE ? substr($this->getName(), 0, $pos + 1) : NULL;

        if ($current === NULL) {
            return $module === '';
        }

        return Strings::startsWith($current, $module . ':');
    }

    public function beforeRender()
    {
        if ($this->isAjax()) {
            $this->redrawControl('flashMessages');
        }
    }

    /*************************************************************************
     * !!! REGISTRACE KOMPONENT !!!
     ************************************************************************/

    protected function createComponentMainMenu()
    {
        return new \App\Control\Mainmenu\MainmenuControl();
    }

    /**
     * Vytvoreni komponenty pro generovani textoveho boxiku.
     *
     * @return TextboxControl
     */
    public function createComponentTextbox(){
        return new TextboxControl();
    }

    public function createComponentPhotogalleryControl(){
        return new PhotogalleryControl();
    }

    protected function createComponentUserMenuControl() {
        return new \App\Control\Usermenu\UserMenuControl();
    }

    protected function createComponentUserControl()
    {
        return new \App\Control\User\UserControl();
    }

    protected function createComponentNewsletterControl() {
        return new \App\Control\Newsletter\NewsletterControl();
    }

    protected function createComponentCartMiniControl() {
        return new \App\EshopModule\Control\Cart\MiniControl();
    }

    protected function createComponentProductAddControl() {
        return new \App\EshopModule\Control\Product\AddProductControl();
    }

    protected function createComponentPaginator() {
        return new PaginatorControl();
    }

    protected function createComponentContactForm()
    {
        $form = $this->contactFormFactory->create();
        //$form->setAction($this->template->basePath.$this->getPageUrlString());
        $form->setValues(array(
            'backUrl' => $this->getPageUrlString()
        ));

        $form->onSuccess[] = function (\Nette\Application\UI\Form $form, $values) {
            $backUrl = $values->backUrl;

            // nastaveni sablone emailu
            $template = $this->createTemplate()->setFile(__DIR__.'/../templates/emails/contactForm.latte');
            $template->formValues = $values;
            $template->pageUrl = $this->template->basePath.$backUrl;

            $mail = new \Nette\Mail\Message();
            $mail
                ->setFrom('Gelmodel.cz <eurocat@domacilekarna.cz>')
                ->addTo($this->context->parameters['contactFormEmail'])
                ->addTo($values->email)
                ->addBcc($this->context->parameters['copy1Email'])
                ->setSubject('Kontaktní formulář')
                ->setHtmlBody($template);

            $mailer = new \Nette\Mail\SendmailMailer();
            $mailer->send($mail);

            $this->flashMessage('Formulář byl odeslán.', 'success');

            $this->redirectUrl($this->template->basePath.$backUrl.'?'.self::FLASH_KEY.'='.$this->getParameter(self::FLASH_KEY).'#contactsformheader');
            return;
        };

        return $form;
    }

    /*************************************************************************
     * !!! REGISTRACE KOMPONENT !!!
     ************************************************************************/
    /**
     * !!! REGISTRACE HELPERU !!!
     *
     * @param null $class
     * @return \Nette\Templating\ITemplate
     */
    protected function createTemplate($class = null)
    {
        $template = parent::createTemplate($class);

        $template->registerHelper('time', function($s){
            return \Model\Helper\TimeHelper::secondToHourAndMinute($s);
        });

        $template->registerHelper('wrPrice', function($s){
            return PriceHelper::format($s);
        });

        return $template;
    }

}
