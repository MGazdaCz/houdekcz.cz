<?php

namespace App\AdminModule\CmsModule\Presenters;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;

/**
 * Guestbook presenter.
 */
class GuestbookPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    protected $_itemId;
    protected $_guestbookRepository;

    protected function startup(){
        parent::startup();
        $this->_guestbookRepository = $this->context->guestbookRepository;
    }

    protected function createComponentGuestbookForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        //$form->addHidden('id');
        $form->addText('name', 'Jméno a příjmení', 100);
        $form->addText('email', 'E-mail', 100);
        $form->addText('date', 'Datum vložení', 20)
            ->setAttribute('class', 'datepicker');;
        $form->addCheckbox('display', 'Zobrazit');
        $form->addTextArea('text', 'Text', 75, 8);
        $form->addSubmit('submit', 'Ulož');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_guestbookRepository->findByPk($this->_itemId);

            // formatovani datumu
            $date = new \Nette\DateTime($item->date);
            $item->date = $date->format('d.m.Y');

            $form->setDefaults($item);
        } // end if

        $form->onSuccess[] = $this->guestbookFormSubmitted;

        return $form;
    }

    public function guestbookFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni formatu datumu
        $values = (array)$form->values;

        $date = new \Nette\DateTime($values['date']);
        $values['date'] = $date->format('Y-m-d H:i:s');
        // ---

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_guestbookRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $this->_guestbookRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

	public function renderDefault()
	{
		$this->template->items = $this->_guestbookRepository->findAll()->order("date DESC");

        //$p = new Page\Page();
	}

    public function renderEdit($id){
        $this->_itemId = intval($id);
    }

    public function renderDelete($id){
        $this->_guestbookRepository->delete(array('id' => $id));
        return $this->redirect('default');
    }

}
