<?php
namespace App\AdminModule\CoreModule\Model\Repository;
use Nette;

class CoreSettings extends Repository
{
    protected $_table = 'core_settings';
} // end class