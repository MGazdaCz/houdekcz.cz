<?php

namespace App\AdminModule\CoreModule\Presenters;

use App\AdminModule\ErpModule\Control\Issue\ListControl;
use App\AdminModule\Control\Widget\Base\BaseControl;
use App\AdminModule\Control\Widget\Task\TaskControl;
use App\AdminModule\Control\Widget\Domain\DomainControl;
use App\AdminModule\Control\Widget\Client\ClientControl;
use App\AdminModule\Control\Widget\Notes\NotesControl;
use App\AdminModule\Control\Widget\Pomodoro\PomodoroControl;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
    const APP_NAME  = 'Google Analytics Statistics';
    const APP_EMAIL = '295117945293-rloa51a3thlip4p76t3fss5v7081ncf4@developer.gserviceaccount.com';
    const CLIENT_ID = '295117945293-rloa51a3thlip4p76t3fss5v7081ncf4.apps.googleusercontent.com';
    //const PATH_TO_PRIVATE_KEY_FILE = APPLICATION_PATH.'/data/e674cecd37107cef913b28411c2deba0d04f1c2d-privatekey.p12';

    //private $_issueRepository;
    //private $_userWidgetRepository;

    public function startup(){
        parent::startup();

        //$this->_issueRepository      = $this->context->erpissueRepository;
        //$this->_userWidgetRepository = $this->context->coreUserwidgetRepository;
    } // end function
    
    public function renderDefault()
    {
        //$this->template->widgetsEnable = $this->_userWidgetRepository->fetchAllForUser($this->user->getId(), true);
        $this->template->widgetsEnable = array();

        /*
        $this->template->issues = $this->_issueRepository
                                            ->getTable()
                                            ->where('rel_assigned = '.$this->getUser()->id.' AND rel_issuestatus <= 50')
                                            ->order('deadline');
        */
	} // end function

    /**
     * Registrace komponent.
     */
    
    /**
     * Registrace komponenty pro vypis
     *
     * @return \AdminModule\Control\Filter\FilterControl
     */
    protected function createComponentList()
    {
        $filter = new ListControl();

        return $filter;
    } // end function 
     
    /**
     * @return 
     */
    protected function createComponentWidgetbase()
    {
        $control = new BaseControl();

        return $control;
    } // end function

    /**
     * @return 
     */
    protected function createComponentWidgettask()
    {
        $control = new TaskControl();

        return $control;
    } // end function

    protected function createComponentWidgetdomain()
    {
        $control = new DomainControl();

        return $control;
    } // end function

    protected function createComponentWidgetclient()
    {
        $control = new ClientControl();

        return $control;
    } // end function

    protected function createComponentWidgetnotes()
    {
        $control = new NotesControl();

        return $control;
    } // end function
    
    protected function createComponentWidgetpomodoro()
    {
        $control = new PomodoroControl();

        return $control;
    } // end function

} // end class
