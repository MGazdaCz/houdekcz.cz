<?php
namespace App\Control\Newsletter;

use App\Forms\RegisterNewsletterFactory;
use Nette\Application\UI\Control;
use Nette\Utils\Validators;

class NewsletterControl extends Control
{
    /**
     * @var \App\Forms\RegisterNewsletterFactory @inject
     */
    protected $_registerForm;

    public function render()
    {

    }

    public function renderBox()
    {
        $formFactory = new RegisterNewsletterFactory();

        $this->template->form = $formFactory->create();
        //$this->template->form->onSuccess[] = array($this, 'save');

        $this->template->setFile(__DIR__.'/templates/box.latte');
        $this->template->render();
    }

    public function handleSave() {
        if (isset($_POST['email']) && Validators::isEmail($_POST['email'])) {
            $email     = $_POST['email'];
            $emailRepo = $this->getParent()->context->getService('cmsNewsletterEmail');

            $emailSelection = $emailRepo->findBy(array('email' => $email));

            if ($emailSelection->count()) {
                // uz existuje, zrusim registraci

                $emailRow = $emailSelection->fetch();
                $emailRepo->update(array('enable' => !$emailRow->enable), array('id' => $emailRow->id));

                if ($emailRow->enable) {
                    $this->getParent()->flashMessage('Email byl vyřazen z databáze.');
                } else {
                    $this->getParent()->flashMessage('Email byl uložen do databáze.');
                }

            } else {
                // neexistuje, zaregistruju
                $emailRepo->insert(array('email' => $email));

                $this->getParent()->flashMessage('Email byl uložen do databáze.');
            }
        } else {
            $this->getParent()->flashMessage('Zadejte email ve správném formátu.');
        }
    }

}