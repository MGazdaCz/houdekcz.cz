<?php
namespace App\AdminModule\Control\Widget\Domain;

use \Nette\Application\UI\Control;
use App\AdminModule\Control\Widget\Base\BaseControl;

class DomainControl extends Control
{
    public function render()
    {
        /*
        $template = $this->template;
        $template->setFile(__DIR__ . '/domain.latte');
        */

        // vložíme do šablony parametry
        $content = '<p class="alert alert-info">Výčet aktuálních domén.</p>';
        
        $baseControl = $this->getParent()->getComponent('widgetbase');
        $baseControl->setHead('Správa domén');
        $baseControl->setContent($content);
        $baseControl->render();
    } // end function
} // end class