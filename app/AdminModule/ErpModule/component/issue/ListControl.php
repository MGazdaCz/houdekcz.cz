<?php
namespace App\AdminModule\ErpModule\Control\Issue;

use \Nette\Application\UI\Control;

class ListControl extends Control
{

    public function render($items, $templateName = 'listLight')
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/'.$templateName.'.latte');

        // registrace helperu time
        $template->registerHelper('time', function($s){
            return \Model\Helper\TimeHelper::secondToHourAndMinute($s);
        });

        // vložíme do šablony parametry
        $template->items           = $items;
        $template->issueRepository = $this->getParent()->context->erpissueRepository;

        // a vykreslíme ji
        $template->render();
    }
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param \Nette\Database\Table\Selection $items
    * @return string
    */
    public function renderCount($items) {
        echo '('.$items->count().')';
    } // end function

} // end class