<?php

namespace App\AdminModule\ErpModule;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;
use App\AdminModule\ErpModule\ProjectModule;
use Nette\DateTime;

/**
 * Guestbook presenter.
 */
class DomainPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    private $_companyRepository;

    protected function startup(){
        parent::startup();
        $this->_mainRepository  = $this->context->domainRepository;
        $this->_companyRepository = $this->context->companyRepository;

        $this->_listH1 = 'Správa domén';

        $this->_orderDefault = 'dateExpire';
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        $company = $this->_companyRepository->getTable()->order('name');
        $companyArray = array(0 => ' - ');
        foreach ($company as $c)
        {
            $companyArray[$c->id] = $c->name;
        } // end foreach

        //$form->addGroup('Základní informace');
        $form->addText('name', 'Název domény');
        $form->addText('dateBuy', 'Datum zakoupení')->setAttribute('class', 'form-controll date-picker');
        $form->addText('dateExpire', 'Datum expirace')->setAttribute('class', 'form-controll date-picker');
        $form->addSelect('rel_company', 'Firma', $companyArray);
        $form->addTextArea('description', 'Popis', 75, 10);

        $form->addSubmit('submit', 'Uložit')->setAttribute('class', 'btn btn-sm btn-success');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_mainRepository->findByPk($this->_itemId);
            $itemArray = $item->toArray();

            // osetreni formatu datumu
            if (!is_null($item->dateBuy))
            {
                $date = new DateTime($item->dateBuy);
                $itemArray['dateBuy'] = $date->format('d.m.Y');
            } // end if

            if (!is_null($item->dateExpire))
            {
                $date = new \Nette\DateTime($item->dateExpire);
                $itemArray['dateExpire'] = $date->format('d.m.Y');
            } // end if
            // ---

            $form->setDefaults($itemArray);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni formatu datumu
        $values = (array)$form->values;

        if (!empty($values['dateBuy']))
        {
            $date = new \Nette\DateTime($values['dateBuy']);
            $values['dateBuy'] = $date->format('Y-m-d');
            unset($date);
        }
        else
        {
            $values['dateBuy'] = null;
        }// end if

        if (!empty($values['dateExpire']))
        {
            $date = new \Nette\DateTime($values['dateExpire']);
            $values['dateExpire'] = $date->format('Y-m-d');
            unset($date);
        }
        else
        {
            $values['dateExpire'] = null;
        } // end if
        // ---

        if (empty($values['rel_company']))
            $values['rel_company'] = null;

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['insertDate'] = date('Y-m-d H:i:s');

            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

}