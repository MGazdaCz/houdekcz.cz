$(document).ready(function () {

    // nastaveni datepickeru
    $('.date-picker').datepicker({
        autoclose:true,
        format: "dd.mm.yyyy",
        weekStart: 1,
        language:"cs"
    }).next().on(ace.click_event, function (){
        $(this).prev().focus();
    });

    // nastaveni timepickeru
    $('.time-picker').timepicker({
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

})
;