<?php
namespace App\Model\Repository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class MenuItem extends RepositoryAbstract
{
    protected $_table = 'cms_menu_item';
}