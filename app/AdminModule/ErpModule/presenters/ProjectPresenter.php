<?php

namespace App\AdminModule\ErpModule;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;

/**
 * Guestbook presenter.
 */
class ProjectPresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    private $_companyRepository;
    private $_projectRepository;
    private $_projectstatusRepository;
    private $_userRepository;
    private $_taskRepository;
    private $_issueRepository;

    protected function startup(){
        parent::startup();
        $this->_companyRepository       = $this->context->companyRepository;
        $this->_projectRepository       = $this->context->projectRepository;
        $this->_projectstatusRepository = $this->context->erpprojectstatusRepository;
        $this->_userRepository          = $this->context->userRepository;
        $this->_taskRepository          = $this->context->taskRepository;
        $this->_issueRepository         = $this->context->erpissueRepository;
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentProjectForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare

        // nacteni spolecnosti
        $companyArray = $this->_companyRepository->getTable()->order('name')->fetchPairs('id', 'name');
        $companyArray = array(0 => ' - ') + $companyArray;

        // nacteni stavu projektu
        $projectStatusArray = $this->_projectstatusRepository->getTable()->order('id')->fetchPairs('id', 'name');
        $projectStatusArray = array(0 => ' - ') + $projectStatusArray;

        // nacteni uzivatelu
        $usersArray = $this->_userRepository->getTable()->where("surname <> ''")->order('id')->fetchPairs('id', 'surname');
        $usersArray = array(0 => ' - ') + $usersArray;

        // nacteni projektu pro select parent
        $whereSuffix = '';
        if ($this->getParameter('id') > 0)
            $whereSuffix = ' AND id <> '.intval($this->getParameter('id'));

        //$projects = $this->_projectRepository->getTable()->where('parent IS NULL'.$whereSuffix)->order('name');
        $projects = array();
        $this->getProjects($projects, 0, 'parent IS NULL'.$whereSuffix, 'name');
        $projectsArray = array(null => ' - ');
        foreach ($projects as $p)
        {
            $namePrefix = '';
            for ($i = 0; $i < $p->level; $i++)
            {
                $namePrefix .= "---";
            } // end for

            $projectsArray[$p->id] = $namePrefix.' '.$p->name;
        } // end foreach
        // ---

        $form->addGroup('Základní informace');
        $form->addText('name', 'Název projektu')->setAttribute('class', 'width-50');
        //$form->addSelect('parent', 'Nadřízený projekt', $projectsArray)->setAttribute('class', 'width-50');
        $form->addSelect('rel_manager', 'Manažer projektu', $usersArray)->setAttribute('class', 'width-50');
        $form->addSelect('rel_company', 'Firma', $companyArray)->setAttribute('class', 'width-50');
        $form->addText('deadline', 'Termín dokončení')
            ->setAttribute('class', 'datepicker');
        $form->addText('time', 'Časový odhad');
        $form->addSelect('rel_projectstatus', 'Stav projektu', $projectStatusArray)->setAttribute('class', 'width-50');
        $form->addSelect('rel_assigned', 'Přiřazeno', $usersArray)->setAttribute('class', 'width-50');
        $form->addSubmit('submit', 'Uložit');

        /*
        if ($this->getUser()->isInRole('administrator'))
        {
            $form->addGroup('Finance');
            $form->addText('price', 'Cena');
        } // end if
        */
        
        $form->addGroup('Rozšířené informace');
        $form->addTextArea('description', 'Popis', 75, 10)->setAttribute('class', 'ckeditor');
        $form->addTextArea('analysis', 'Analýza', 75, 10)->setAttribute('class', 'ckeditor');

        $form->addSubmit('submit2', 'Uložit');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            if ($this->_itemId > 0)
            {
                $item = $this->_projectRepository->findByPk($this->_itemId);
                $item = $item->toArray();

                // osetreni formatu datumu
                $date = new \Nette\DateTime($item['deadline']);
                $item['deadline'] = $date->format('d.m.Y');
                // ---

                $form->setDefaults($item);
            }
            else
            {
                $form->setDefaults(array('parent' => $this->getParameter('parent')));
            } // end if
        } // end if

        $form->onSuccess[] = $this->companyFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function companyFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni dat
        $values = (array)$form->values;

        if (empty($values['parent']))       $values['parent']       = null;
        if (empty($values['rel_manager']))  $values['rel_manager']  = null;
        if (empty($values['rel_assigned'])) $values['rel_assigned'] = null;

        $deadline = new \Nette\DateTime($values['deadline']);
        $values['deadline'] = $deadline->format('Y-m-d H:i:s');
        // ---

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_projectRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['insertDate'] = date('Y-m-d H:i:s');

            $this->_projectRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentPreviewProjectForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare

        // nacteni stavu projektu
        $projectStatusArray = $this->_projectstatusRepository->getTable()->order('id')->fetchPairs('id', 'name');
        $projectStatusArray = array(0 => ' - ') + $projectStatusArray;

        //$form->addGroup('Základní informace');
        $form->addSelect('rel_projectstatus', 'Stav projektu', $projectStatusArray);
        $form->addSubmit('submit', 'Uložit');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            if ($this->_itemId > 0)
            {
                $item = $this->_projectRepository->findByPk($this->_itemId);

                $form->setDefaults($item);
            } // end if
        } // end if

        $form->onSuccess[] = $this->companyFormSubmitted;

        return $form;
    }

    public function renderDefault()
    {
        $this->template->items       = array();
        $this->template->sumEstimate = 0;
        $this->template->sumComplete = 0;

        // nove vyuziti filtru
        $this->template->activeFilter = $this->getParameter('filter', 'actual');
        $this->_quickFilter->setActualFilter($this->template->activeFilter);
        $where = $this->_quickFilter->getWhere();
        // ---

        $this->template->projectRepository = $this->_projectRepository;
        $this->template->items             = $this->_projectRepository->getTable()->order('deadline');
        
        if (!empty($where)) {
            $this->template->items->where($where);
        } // end if
    } // end function

    /**
     * Metoda pro rekurzivni nacitani projektu.
     *
     * @param array $itemsArray
     * @param int $level
     * @param string $where
     * @param string $orderBy
     * @param string $whereGlobal defaultne prazdny retezec
     */
    private function getProjects(&$itemsArray, $level, $where, $orderBy, $whereGlobal = '')
    {
        $items = $this->_projectRepository->getTable()->where($where.$whereGlobal)->order($orderBy);

        foreach ($items as $item)
        {
            $project = Project::createFromActiveRow($item);
            $project->setLevel($level);

            $itemsArray[] = $project;

            $this->getProjects($itemsArray, $level + 1, 'parent = '.intval($item->id).$whereGlobal, $orderBy);
        } // end foreach
    }

    public function renderAdd(){
        $this->setView('add');
    }

    public function renderEdit($id)
    {
        $this->_itemId = intval($id);
    }

    public function renderView($id)
    {
        $this->_itemId = $id;
        if ($this->_itemId > 0)
        {
            $project = $this->_projectRepository->findByPk($this->_itemId);
            $this->template->item = $project;

            $this->template->h1 = 'Projekt: '.$project->name;
            
            // nacteni issue
            $this->template->issues = $this->_issueRepository->getTable()->where('rel_project = '.$this->_itemId); // .' AND rel_issuestatus IN (10,20,50)' ->order($sortColumn)
        } // end if
    } // end function

    public function renderDelete($id){
        $this->_projectRepository->delete(array('id' => $id));
        return $this->redirect('default');
    }
    
    /**
     * Registrace komponenty pro vypis
     *
     * @return \AdminModule\Control\Filter\FilterControl
     */
    protected function createComponentIssueList() {
        $control = new \AdminModule\ErpModule\Control\Issue\ListControl();

        return $control;
    }
    
    protected function createComponentCostRevenuesList() {
        $control = new \AdminModule\ErpModule\Control\CostRevenues\CrControl();

        return $control;
    }

}
