<?php
namespace App\Component\Paginator;

class PaginatorControl extends \App\AdminModule\CoreModule\Component\PaginatorControl
{
    /**
     * Renderovani defaultniho paginatoru.
     *
     * @param $limit
     * @param $page
     * @param $count
     */
    public function render($limit, $page, $count)
    {
        $this->_initItems($limit, $page, $count);

        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

    protected function _getLink($pageNumber){
        $url = $this->getPresenter()->getParameter('url');
        return $this->presenter->link('default', array('url' => $url, 'page' => $pageNumber));
    }

}