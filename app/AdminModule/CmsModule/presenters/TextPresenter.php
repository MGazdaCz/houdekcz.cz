<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;


/**
 * Text presenter.
 */
class TextPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsText');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Texty';
        $this->_orderDefault = 'name';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace textu: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addText('name', '', 80)
            ->setAttribute('placeholder', 'Orientační název textu')
            ->setRequired('Zadejte název');
        $form
            ->addCheckbox('view', 'zobrazit');
        $form
            ->addTextArea('text', 'Text', 80, 10)
            ->setAttribute('class', 'ckeditor');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}
