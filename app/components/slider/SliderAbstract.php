<?php
namespace App\Control\Slider;

use \Nette\Application\UI\Control;

abstract class SliderControlAbstract extends Control
{
    protected $_options;
    protected $_items;

    /**
     * Metoda provede inicializaci parametru slideru.
     *
     * @param array $options
     * @throws \Exception
     */
    protected function _initOptions(array $options){
        $this->_options = $options;

        if (isset($this->_options['items'])) {
            $this->_items = &$this->_options['items'];
        }
        else {
            throw new \Exception('Chyba inicializace slideru - nejsou nastaveny polozky slideru (items).');
        }
    }

    public function render(array $options) {

    }

} // end class