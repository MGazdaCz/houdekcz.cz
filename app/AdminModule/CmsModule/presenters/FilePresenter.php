<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette\Application\UI\Form;

/**
 * @author Martin Hromádko <hromadkom@gmail.com>
 */
class FilePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter {

    /**
     * @var id vybraneho adresare
     */
    private $directoryId;

    /**
     * @inject
     * @var \App\AdminModule\CmsModule\Model\Repository\Directories
     */
    public $directoryRepository;

    /**
     * @inject
     * @var \App\AdminModule\CmsModule\Model\Repository\Files
     */
    public $fileRepository;

    /**
     * @inject
     * @var \App\AdminModule\CmsModule\Model\Repository\Photos
     */
    public $photoRepository;

    protected function startup() {
        parent::startup();

        $this->template->_listH1   = 'module-file';
        $this->template->bodyClass = 'module-file';
    }

    public function actionDefault($id) {
        $this->directoryId = $id;
    }

    public function renderDefault() {
        $this->template->directories = $this->directoryRepository->findBy(array('parent_id' => $this->directoryId));

        $this->template->files = $this->fileRepository->findBy(array('cms_directory_id' => $this->directoryId));
        if ($this->directoryId) {
            $this->template->activeDirectory = $this->directoryRepository->findByPk($this->directoryId);
        }
    }

    /*
     * Správa souborů
     */

    /**
     * Formulář pro přidání souborů
     * @return Form
     */
    public function createComponentFileUploadForm() {
        $form = new Form();
        $form->addUpload('file');
        $form->addSubmit('send', 'Odeslat');
        $form->onSuccess[] = $this->fileUploadFormSuccess;
        return $form;
    }

    public function fileUploadFormSuccess(Form $form) {
        $parameters = $this->context->getParameters();
        $uploadDir = $parameters['paths']['uploadDir'];
        $values = $form->values;
        $file = $values->file;

        if ($file->isOk()) {
            $file->move($uploadDir . '/' . $file->getSanitizedName());
            $row = $this->fileRepository->insert(array('cms_directory_id' => $this->directoryId, 'name' => $file->getSanitizedName(), 'mime_type' => $file->getContentType(), 'file_size' => $file->getSize(), 'is_image' => $file->isImage()));
            if ($file->isImage()) {
                $file_id = $row->id;
                $this->photoRepository->insert(array('cms_file_id' => $file_id));
            }
            if ($this->isAjax()) {
                $this->redrawControl('itemsContainer');
            }
        } else {
            throw new \ErrorException('File is not ok.');
        }
    }

    /**
     * Zpracovani signalu smazani souboru
     * @param int $fileId
     */
    public function handleDeleteFile($fileId) {
        $file = $this->fileRepository->findByPk($fileId);
        $this->removeFile($file);
        $this->flashMessage('Soubor smazán.');
        $this->redirect(301, 'File:default', array('id' => $file->cms_directory_id));
    }

    /**
     * Zpracovani smazani souboru
     * @param $file
     */
    private function removeFile($file) {
        $parameters = $this->context->getParameters();
        $uploadDir = $parameters['paths']['uploadDir'];
        @unlink($uploadDir . '/' . $file->name);
        $this->fileRepository->delete(array('id' => $file->id));
    }

    /*
     * Správa adresářů
     */

    /**
     * Vytvoření kostry pro vyváření a editaci adresářů
     * @return Form
     */
    private function createDirectoryForm() {
        $form = new Form();
        $form->addText('name', 'Název', null, 128)
                ->setRequired('Prosím vyplňte název nového adresáře.');
        $form->addHidden('parent_id', $this->directoryId);
        return $form;
    }

    /**
     * Formulář pro vytvoření adresáře
     * @return Form
     */
    public function createComponentAddDirectoryForm() {
        $form = $this->createDirectoryForm();
        $form->addSubmit('create', 'Vytvořit');
        $form->onSuccess[] = $this->addDirectoryFormSuccess;
        return $form;
    }

    /**
     * Callback funkce zpracování formuláře pro vytvoření adresáře
     * @param Form $form
     */
    public function addDirectoryFormSuccess(Form $form) {
        $values = $form->values;
        if ($values['parent_id'] == '') {
            $values['parent_id'] = null;
        }
        unset($values['create']);
        $this->directoryRepository->insert($values);
        $this->flashMessage('Adresář úspěšně vytvořen');
        $this->redirect('this');
    }

    /**
     * Akce úpravy adresáře
     * @param int $id
     */
    public function actionEditDirectory($id) {
        $this->directoryId = $id;
    }

    /**
     * Formulář pro úpravu adresáře
     * @return Form
     */
    public function createComponentEditDirectoryForm() {
        $activeDirectory = $this->directoryRepository->findByPk($this->directoryId);
        $form = $this->createDirectoryForm();
        $form->addSubmit('edit', 'Uložit');
        $form->setDefaults(array('name' => $activeDirectory->name));
        $form->onSuccess[] = $this->editDirectoryFormSuccess;
        return $form;
    }

    /**
     * Callback funkce zpracování formuláře pro úpravu adresáře
     * @param Form $form
     */
    public function editDirectoryFormSuccess(Form $form) {
        $name = $form->values['name'];
        $this->directoryRepository->update(array('name' => $name), array('id' => $this->directoryId));
        $directory = $this->directoryRepository->findByPk($this->directoryId);
        $this->flashMessage('Adresář úspěšně upraven');
        $this->redirect(301, 'File:default', array('id' => $directory->parent_id));
    }

    /**
     * Zpracování signálu smazání adresáře
     * @param int $id
     */
    public function handleDeleteDirectory($id) {
        $directory = $this->directoryRepository->findByPk($id);
        $this->deleteDirectory($directory);
        $this->flashMessage('Adresář úspěšně smazán');
        $this->redirect(301, 'File:default', array('id' => $directory->parent_id));
    }

    /**
     * Zpracovani smazani adresare
     * @param $directory
     */
    public function deleteDirectory($directory) {
        $children = $this->directoryRepository->findBy(array('parent_id' => $directory->id));
        foreach ($children as $dir) {
            $this->deleteDirectory($dir);
        }

        $files = $this->fileRepository->findBy(array('cms_directory_id' => $directory->id));
        foreach ($files as $f) {
            $this->removeFile($f);
        }

        $this->directoryRepository->delete(array('id' => $directory->id));
    }

    /*
     * jQuery File Tree custom implementace
     * 
     *
      <ul class="jqueryFileTree" style="display: none;">
      <li class="directory collapsed"><a href="#" rel="/this/folder/">Folder Name</a></li>
      (additional folders here)
      <li class="file ext_txt"><a href="#" rel="/this/folder/filename.txt">filename.txt</a></li>
      (additional files here)
      </ul>
     */

    public function actionGetDir() {
        if ($this->isAjax()) {
            $post = $this->getRequest()->getPost();
            $dir = $post['dir'];
            if ($dir == "/") {
                $dir = null;
            } else {
                $dir = str_replace('/', '', $dir);
            }

            $this->template->directories = $this->directoryRepository->findBy(array('parent_id' => $dir));
            $this->template->files = $this->fileRepository->findFilesWithPhotoIdByDirectory($dir);
        } else {
            $this->redirect(':Homepage:default');
        }
    }

    public function actionDirInfo($dir) {
        if (!$this->isAjax()) {
            $this->redirect(':Homepage:default');
        }

        if (!is_numeric($dir)) {
            $this->payload->status = 0;
            $this->payload->message = "Chyba parametru";
            $this->sendPayload();
            return;
        }
        
        $directory = $this->directoryRepository->findByPk($dir);
        $this->payload->name = $directory->name;
        $this->payload->imageCount = $this->fileRepository->count(array('cms_directory_id'=>$dir, 'is_image'=>1));
        
        $this->sendPayload();
    }

}
