<?php

namespace App\AdminModule\EshopModule\Model;

use App\AdminModule\EshopModule\Model\Order\Address;
use App\AdminModule\EshopModule\Model\Order\CompanyAddress;
use App\EshopModule\Model\Checkout;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\ArrayHash;

class Order {
    protected $_id;
    protected $_insertDate;
    protected $_discountCode;
    protected $_deliveryId;
    protected $_deliveryName;
    protected $_deliveryPrice;
    protected $_paymentId;
    protected $_paymentName;
    protected $_paymentPrice;
    protected $_itemCount;
    protected $_priceWithoutVat;
    protected $_priceWithVat;
    protected $_content;
    protected $_shipped;
    protected $_text;

    /**
     * @var \App\AdminModule\EshopModule\Model\Order\Address
     */
    protected $_primaryAddress;

    /**
     * @var \App\AdminModule\EshopModule\Model\Order\Address
     */
    protected $_companyAddress;


    protected $_items;

    // ceny
    protected $_productPriceWithVat;
    protected $_productPriceWithoutVat;
    protected $_shippingPriceWithVat;
    protected $_shippingPriceWithoutVat;
    protected $_totalPriceWithVat;
    protected $_totalPriceWithoutVat;
    protected $_totalPrice;


    /**
     * Metoda inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     */
    public static function createFromActiveRow(ActiveRow $activeRow){
        $order = new self;
        $order->_id              = $activeRow->id;
        $order->_insertDate      = $activeRow->_insertDate;
        $order->_discountCode    = $activeRow->discountCode;
        $order->_deliveryId      = $activeRow->deliveryId;
        $order->_deliveryName    = $activeRow->deliveryName;
        $order->_deliveryPrice   = $activeRow->deliveryPrice;
        $order->_paymentId       = $activeRow->paymentId;
        $order->_paymentName     = $activeRow->paymentName;
        $order->_paymentPrice    = $activeRow->paymentPrice;
        $order->_itemCount       = $activeRow->itemCount;
        $order->_priceWithoutVat = $activeRow->priceWithoutVat;
        $order->_priceWithVat    = $activeRow->priceWithVat;
        $order->_content         = $activeRow->content;
        $order->_shipped         = $activeRow->shipped;

        $order->_primaryAddress = Address::createFromActiveRow($activeRow);
        $order->_companyAddress = CompanyAddress::createFromActiveRow($activeRow);

        return $order;
    }

    /**
     * Metoda inicializuje objednavku z dat formulare.
     *
     * @param array $values
     */
    public static function createFromCheckoutFormData(ArrayHash $values){
        $order = new self;
        $order->_insertDate = date('Y-m-d H:i:s');
        $order->_deliveryId = $values->delivery;
        $order->_paymentId  = $values->payment;
        $order->_text       = $values->text;

        if (isset($values->voucher)) {
            $order->_discountCode = $values->voucher;
        }

        $order->_primaryAddress = Address::createFromCheckoutFormData($values);
        $order->_companyAddress = CompanyAddress::createFromCheckoutFormData($values);

        return $order;
    }


    protected function _setItems(Checkout $checkout){
        $this->_itemCount = $checkout->getItemsCount();
        $this->_items     = $checkout->getItems();
    }

    protected function _setPrices(Checkout $checkout){
        $this->_productPriceWithVat     = $checkout->getCart()->getPriceWithVat();
        $this->_productPriceWithoutVat  = $checkout->getCart()->getPriceWithoutVat();
        $this->_shippingPriceWithVat    = $checkout->getShippingPriceWithVat();
        $this->_shippingPriceWithoutVat = $checkout->getShippingPriceWithoutVat();
        $this->_totalPriceWithVat       = $checkout->getTotalPriceWithVat();
        $this->_totalPriceWithoutVat    = $checkout->getTotalPriceWithoutVat();
        $this->_totalPrice              = $checkout->getTotalPrice();
    }

    public function setDataFromCheckout(Checkout $checkout){
        $this->_setItems($checkout);
        $this->_setPrices($checkout);

        $this->_deliveryId    = $checkout->getDeliveryId();
        $this->_deliveryName  = $checkout->getDelivery()['name'];
        $this->_deliveryPrice = $checkout->getDeliveryPrice();

        $this->_paymentId    = $checkout->getPaymentId();
        $this->_paymentName  = $checkout->getPayment()['name'];
        $this->_paymentPrice = $checkout->getPaymentPrice();
    }

    /**
     * Metoda ulozi data o objednavce do DB.
     */
    public function save(\Nette\DI\Container $context){
        if (empty($this->_id)) {
            // insert noveho zaznamu
            // sestaveni dat pro insert
            $orderData = array(
                '_insertDate'               => date('Y-m-d H:i:s'),
                'name'                      => $this->_primaryAddress->name,
                'street'                    => $this->_primaryAddress->street,
                'city'                      => $this->_primaryAddress->city,
                'zip'                       => $this->_primaryAddress->zip,
                'email'                     => $this->_primaryAddress->email,
                'phone'                     => $this->_primaryAddress->phone,

                'ico'                       => $this->_companyAddress->ico,
                'dic'                       => $this->_companyAddress->dic,
                'companyName'               => $this->_companyAddress->company,
                'companyPerson'             => $this->_companyAddress->person,
                'companyStreet'             => $this->_companyAddress->street,
                'companyCity'               => $this->_companyAddress->city,
                'companyZip'                => $this->_companyAddress->zip,

                'text'                      => $this->_text,
                'discountCode'              => $this->_discountCode,

                'eshop_delivery_id'         => $this->_deliveryId,
                'deliveryName'              => $this->_deliveryName,
                'deliveryPrice'             => $this->_deliveryPrice,

                'eshop_delivery_payment_id' => $this->_paymentId,
                'paymentName'               => $this->_paymentName,
                'paymentPrice'              => $this->_paymentPrice,

                'itemCount'                 => $this->_itemCount,
                'priceWithoutVat'           => $this->_productPriceWithoutVat,
                'priceWithVat'              => $this->_productPriceWithVat,
                'totalPrice'                => $this->_totalPrice,
            );

            $result = $context->getService('eshopOrder')->insert($orderData);

            if (is_object($result)) {
                // nastaveni ID objednavky
                $this->_id = $result->id;

                // ulozeni polozek objednavky do DB
                $orderItemRepo = $context->getService('eshopOrderItem');
                foreach ($this->_items as $item) {
                    $orderItemRepo->insert(array(
                        'eshop_order_id'           => $this->_id,
                        'eshop_product_id'         => $item->getId(),
                        'eshop_product_variant_id' => $item->getVariantId(),
                        'itemCount'                => $item->getQty(),
                        'name'                     => $item->name,
                        'nameVariant'              => $item->variantName,
                        'priceWithVat'             => $item->getPriceWithVat(),
                        'priceWithoutVat'          => $item->getPriceWithoutVat(),
                        'vat'                      => $item->vat,
                        '_insertDate'              => date('Y-m-d H:i:s'),
                    ));
                }
                // ---

                return true;
            }
        } else {
            // update zaznamu
            $orderData = array(
                //'_insertDate'               => date('Y-m-d H:i:s'),
                //'name'                      => $this->_primaryAddress->name,
                //'street'                    => $this->_primaryAddress->street,
                //'city'                      => $this->_primaryAddress->city,
                //'zip'                       => $this->_primaryAddress->zip,
                //'email'                     => $this->_primaryAddress->email,
                //'phone'                     => $this->_primaryAddress->phone,
                //
                //'ico'                       => $this->_companyAddress->ico,
                //'dic'                       => $this->_companyAddress->dic,
                //'companyName'               => $this->_companyAddress->company,
                //'companyPerson'             => $this->_companyAddress->person,
                //'companyStreet'             => $this->_companyAddress->street,
                //'companyCity'               => $this->_companyAddress->city,
                //'companyZip'                => $this->_companyAddress->zip,
                //
                //'text'                      => $this->_text,
                //'discountCode'              => $this->_discountCode,
                //
                //'eshop_delivery_id'         => $this->_deliveryId,
                //'deliveryName'              => $this->_deliveryName,
                //'deliveryPrice'             => $this->_deliveryPrice,
                //
                //'eshop_delivery_payment_id' => $this->_paymentId,
                //'paymentName'               => $this->_paymentName,
                //'paymentPrice'              => $this->_paymentPrice,
                //
                //'itemCount'                 => $this->_itemCount,
                //'priceWithoutVat'           => $this->_productPriceWithoutVat,
                //'priceWithVat'              => $this->_productPriceWithVat,
                //'totalPrice'                => $this->_totalPrice,
                'content' => $this->_content,
            );

            $result = $context->getService('eshopOrder')->update($orderData, array('id' => $this->_id));

            if ($result) {
                return true;
            }
        }

        return false;
    }

    /**
     * Metoda provede aktualizaci skladovych zasob.
     */
    public function updateStock(\Nette\DI\Container $context){
        $productRepo = $context->getService('eshopProduct');
        $variantRepo = $context->getService('eshopProductVariant');

        // aktualizace skladovych zasob
        foreach ($this->_items as $item) {
            if ($item->getVariantId()) {
                // aktualizace varianty
                $id   = $item->getVariantId();
                $repo = $variantRepo;
            } else {
                // aktualizace produktu
                $id   = $item->getId();
                $repo = $productRepo;
            }

            // provedeni update
            $repo->update(array(
                'stock' => new \Nette\Database\SqlLiteral('stock - '.$item->getQty())
            ), array('id' => $id));
        }
    }

    public function setValuesToTemplate(&$template){
        $template->orderId      = $this->_id;
        //$template->formValues   = $values; // nepouziva se

        $template->products           = $this->_items;
        $template->shippingAddress    = $this->_primaryAddress;
        $template->showBillingAddress = (!empty($this->_companyAddress->name) ? true : false);
        $template->billingAddress     = $this->_companyAddress;

        $template->voucher = $this->_discountCode;
        $template->phone   = $this->_primaryAddress->phone;
        $template->email   = $this->_primaryAddress->email;
        $template->text    = $this->_text;

        $template->deliveryName  = $this->_deliveryName;
        $template->deliveryPrice = $this->_deliveryPrice;
        $template->paymentName   = $this->_paymentName;
        $template->paymentPrice  = $this->_paymentPrice;

        $template->productPriceWithVat     = $this->_productPriceWithVat;
        $template->productPriceWithoutVat  = $this->_productPriceWithoutVat;
        $template->shippingPriceWithVat    = $this->_shippingPriceWithVat;
        $template->shippingPriceWithoutVat = $this->_shippingPriceWithoutVat;
        $template->totalPriceWithVat       = $this->_totalPriceWithVat;
        $template->totalPriceWithoutVat    = $this->_totalPriceWithoutVat;
        $template->totalPrice              = $this->_totalPrice;
    }

    public function getId(){
        return $this->_id;
    }

    /**
     * Metoda nastavi obsah objednavky v HTML kodu.
     *
     * @param $content
     * @return $this
     */
    public function setContent($content){
        $this->_content = $content;
        return $this;
    }

    public function sendEmailSummary($subject, $from, $to, $emailContent, $bccEmails = array()){
        // odeslani emailu
        $mail = new \Nette\Mail\Message();
        $mail
            ->setFrom($from)
            ->addTo($to)
            ->addBcc($this->_primaryAddress->email)
            ->setSubject($subject.$this->_id)
            ->setHtmlBody($emailContent);

        if (!empty($bccEmails)) {
            foreach ($bccEmails as $email) {
                $mail->addBcc($email);
            }
        }

        $mailer = new \Nette\Mail\SendmailMailer();
        $mailer->send($mail);
    }

}