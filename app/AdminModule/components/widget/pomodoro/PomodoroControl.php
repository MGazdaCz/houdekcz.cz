<?php
namespace App\AdminModule\Control\Widget\Pomodoro;

use \Nette\Application\UI\Control;
use App\AdminModule\Control\Widget\Base\BaseControl;

class PomodoroControl extends Control
{
    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/pomodoro.latte');

        // vložíme do šablony parametry
        $head    = 'Pomodoro - efektivita prace';
        $content = (string)$template;
        
        $baseControl = $this->getParent()->getComponent('widgetbase');
        $baseControl->setHead($head);
        $baseControl->setContent($content);
        $baseControl->render();
    } // end function
} // end class