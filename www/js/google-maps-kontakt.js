function initialize()
{
  var myLatlng = new google.maps.LatLng(49.8610768, 16.321669);

  var mapOptions = {
    scrollwheel: false,
    center: new google.maps.LatLng(49.8610768, 16.321669),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("google_map"),mapOptions);

  var contentString =
  '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">MAYDAY Sped s.r.o.</h1>' +
            '<div id="bodyContent">' +
            '<p>Trstěnická 932, 57001  Litomyšl</p>' +
            '<p>CZECH REPUBLIC</p>' +
            '<p><br />Mob.: +420 <strong>603 968 803</strong><br />E-mail: <a href="mailto:maydaysped@maydaysped.cz" title="Kontaktní e-mail">maydaysped@maydaysped.cz</p>' +
            '</div>' +
  '</div>';
    
  var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 400
  });

  var image = '/www/img/logo-mapa.png';

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      animation: google.maps.Animation.DROP,
      icon: image,
      title:"MAYDAYSped"
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });

} // end function  

google.maps.event.addDomListener(window, 'load', initialize);