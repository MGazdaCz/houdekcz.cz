<?php

namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Article extends MainRepository
{
    protected $_table = 'cms_article';

    /**
     * Metoda nacte clanky
     *
     * @param string $language
     * @return \Nette\Database\Table\Selection
     */
    public function getArticles(){
        return $this->findBy(array(
            'view = 1',
            'delete = 0',
            '(datePublication IS NULL OR datePublication <= CURDATE())',
            '(dateFrom IS NULL AND dateTo IS NULL) OR (dateFrom <= CURDATE() AND dateTo IS NULL) OR (dateFrom IS NULL AND dateTo >= CURDATE()) OR (dateFrom <= CURDATE() AND dateTo >= CURDATE())'
        ))->order('id DESC');
    }

}