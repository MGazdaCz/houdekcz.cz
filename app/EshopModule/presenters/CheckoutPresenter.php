<?php

namespace App\EshopModule\Presenters;

use App\AdminModule\EshopModule\Model\Order;
use App\EshopModule\Model\Cart\Item;
use Nette,
	App\Model;


/**
 * Checkout presenter.
 */
class CheckoutPresenter extends BasePresenter
{
    /**
     * @var \App\EshopModule\Model\Checkout
     */
    protected $_checkout;

    public function startup(){
        parent::startup();

        // nastaveni sluzby checkoutu
        $this->_checkout = $this->context->getService('checkout');

        // inicializace cen a kosiku
        $this->template->isSend                  = false;
        $this->template->showBillingAddress      = false;
        $this->template->deliveries              = $this->_checkout->getPossibleDeliveries();
        $this->template->shippingPriceWithVat    = $this->_checkout->getShippingPriceWithVat();
        $this->template->shippingPriceWithoutVat = $this->_checkout->getShippingPriceWithoutVat();
        $this->template->productPriceWithVat     = $this->_cart->getPriceWithVat();
        $this->template->productPriceWithoutVat  = $this->_cart->getPriceWithoutVat();
        $this->template->totalPrice              = $this->_checkout->getTotalPriceWithVat();
        $this->template->totalPriceWithVat       = $this->_checkout->getTotalPriceWithVat();
        $this->template->totalPriceWithoutVat    = $this->_checkout->getTotalPriceWithoutVat();
        $this->template->priceToDeliveryFree     = $this->_checkout->getPriceToDeliveryFree();

        // cislo aktivniho kroku
        $this->template->numStepActive = 1;
    }

    /**
     * Metoda najde a vrati aktualni zobrazovanou stranku.
     *
     * !!! Pretizeni kvuli zvyraznovani homepage v kosiku !!!
     *
     * @return object | null
     */
    public function getPage(){
        return null;
    }

    /**
     * Metoda pro renderovani homepage.
     */
    public function renderDefault()
	{
        $this->template->bodyClass = 'checkout';

	    //echo '<pre>'.print_r($this->_checkout->getPriceWithVat(), true).'</pre>';
	    //echo '<pre>'.print_r($this->_checkout->getPriceWithoutVat(), true).'</pre>';
	    //echo '<pre>'.print_r($page, true).'</pre>';
        //die;
	}

    public function renderThankyoupage() {
        $pageRepository = $this->context->getService('cmsPage');
        $page           = $pageRepository->findBy(array('url' => '/thank-you-page'))->fetch();

        // ziskani promennych pro sablonu
        $this->template->orderId  = $this->getParameter('orderId');
        $this->template->delivery = $this->_checkout->getDelivery();
        $this->template->payment  = $this->_checkout->getPayment();
        $this->template->products = $this->_checkout->getProducts();

        // vysypani kosiku
        $this->_cart->truncate();

        $this->template->page = null;
        if (!is_null($page)) {
            $this->template->page = $page;
        } else {
            throw new \Exception('Děkovací stránka nenalezena.');
        }
    }

    /**
     * Vytvoreni komponenty pro formular pokladny.
     *
     * @return Nette\Application\UI\Form
     */
    public function createComponentCheckoutForm(){
        $form = new Nette\Application\UI\Form();

        // tlačítka pokladny
        $form
            ->addSubmit('recalculate', 'Přepočítat')
            ->setAttribute('class', 'upravit')
            ->setValidationScope(false)
            ->onClick[] = array($this, 'recalculateItems');
        $form
            ->addSubmit('submit', 'Odeslat objednávku')
            ->setAttribute('class', 'sub_text');
            //->setValidationScope(false);

        // generovani inputu pro produkty z kosiku
        $products = $form->addContainer('products');
        foreach ($this->_cart->getItems() as $item) {
            $itemKey     = Item::getItemKey($item->getId(), $item->getVariantId());
            $cntProducts = $products->addContainer($itemKey);

            $cntProducts->addHidden('id', $item->getId());
            $cntProducts->addHidden('variantId', $item->getVariantId());
            $cntProducts
                ->addText('qty', 'Počet kusů:')
                ->setDefaultValue($item->getQty());
        }

        // generovani dopravy
        $deliveryArray = array();
        $deliveries = $this->_checkout->getPossibleDeliveries();
        if (!empty($deliveries)) {
            foreach ($deliveries as $item) {
                $deliveryArray[$item->id] = $item->name.' ('.$item->price.'&nbsp;Kč)';
            }
        }
        $defaultValue = null;
        if (in_array($this->_checkout->getDeliveryId(), array_keys($deliveryArray))) {
            $defaultValue = $this->_checkout->getDeliveryId();
        }
        $form
            ->addRadioList('delivery', '', $deliveryArray)
            ->setRequired('Vyberte dopravu.')
            ->setDefaultValue($defaultValue);

        // dorucovaci adresa
        $address1 = $form->addContainer('shippingAddress');
        $address1
            ->addText('name', 'Jméno a příjmení')
            ->setRequired('Vyplňte jméno a příjmení.');
        $address1
            ->addText('street', 'Ulice a č. p.')
            ->setRequired('Vyplňte ulici.');
        $address1
            ->addText('city', 'Město')
            ->setRequired('Vyplňte město.');
        $address1
            ->addText('zip', 'PSČ')
            ->setRequired('Vyplňte poštovní směrovací číslo.');

        // fakturacni adresa
        $form
            ->addCheckbox('showBillingAddress', 'Zobrazit fakturační adresu');
        $address2 = $form->addContainer('billingAddress');
        $address2
            ->addText('company', 'Společnost')
            ->addConditionOn($form['showBillingAddress'], Nette\Application\UI\Form::EQUAL, true)
            ->setRequired('Vyplňte název společnosti.');
        $address2
            ->addText('person', 'Kontaktní osoba')
            ->addConditionOn($form['showBillingAddress'], Nette\Application\UI\Form::EQUAL, true)
            ->setRequired('Vyplňte jméno kontaktní osoby.');
        $address2
            ->addText('ico', 'IČ')
            ->addConditionOn($form['showBillingAddress'], Nette\Application\UI\Form::EQUAL, true)
            ->setRequired('Vyplňte číslo IČ.');
        $address2
            ->addText('dic', 'DIC');
        $address2
            ->addText('street', 'Ulice a č. p.')
            ->addConditionOn($form['showBillingAddress'], Nette\Application\UI\Form::EQUAL, true)
            ->setRequired('Vyplňte ulici.');
        $address2
            ->addText('city', 'Město')
            ->addConditionOn($form['showBillingAddress'], Nette\Application\UI\Form::EQUAL, true)
            ->setRequired('Vyplňte město.');
        $address2
            ->addText('zip', 'PSČ')
            ->addConditionOn($form['showBillingAddress'], Nette\Application\UI\Form::EQUAL, true)
            ->setRequired('Vyplňte poštovní směrovací číslo.');

        // generovani plateb
        $paymentArray = array();
        $payments = $this->_checkout->getPossiblePayments();
        if (!empty($payments)) {
            foreach ($payments as $item) {
                if (!empty($item->eshop_payment_id)) {
                    $paymentArray[$item->id] = $item->eshop_payment->name.' ('.$item->eshop_payment->price.'&nbsp;Kč)';
                } else {
                    $paymentArray[$item->id] = $item->name.' ('.$item->price.'&nbsp;Kč)';
                }
            }
        }
        $form
            ->addRadioList('payment', '', $paymentArray)
            ->setRequired('Vyberte způsob platby.')
            ->setDefaultValue($this->_checkout->getPaymentId($paymentArray));

        $form
            ->addText('voucher', 'Zadejte kód slevového či dárkového kupónu', 42);
        $form
            ->addSubmit('voucherSubmit', 'Ověřit zadaný kód')
            ->setAttribute('class', 'submit');

        // kontaktní informace
        $form
            ->addText('email', 'E-mail')
            ->setRequired('Zadejte váš email.')
            ->addRule(Nette\Application\UI\Form::EMAIL, 'Zadejte váš email ve správném tvaru.');
        $form
            ->addText('phone', 'Mobil nebo telefon')
            ->setRequired('Zadejte váše telefonní číslo.');
        $form
            ->addTextArea('text', 'Vaše poznámka k objednávce');

        // metoda nastavi defaultni data z requestu po odeslani formulare
        $this->_setFormData($form, $this->getParameters());

        $form->onSuccess[] = array($this, 'formSucceeded');

        return $form;
    }

    public function recalculateItems(Nette\Forms\Controls\SubmitButton $button){
        $values = $button->getForm()->getValues();

        // uprava mnostvi produktu
        if (isset($values['products']) && !empty($values['products'])) {
            foreach ($values['products'] as $product) {
                $this->_cart->changeQuantity($product['id'], $product['qty'], $product['variantId']);
            }
        }

        $this->redirect(':Eshop:Checkout:');
        return;
    }

    /**
     * Metoda volana po odeslani formulare pokladny.
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     */
    public function formSucceeded(Nette\Application\UI\Form $form, $values){
        // uprava mnostvi produktu
        if (isset($values['products']) && !empty($values['products'])) {
            foreach ($values['products'] as $product) {
                $this->_cart->changeQuantity($product['id'], $product['qty']);
            }
        }

        // predani dat do sablony
        $this->template->showBillingAddress = $values['showBillingAddress'];

        // vytvoreni objednavky
        $order = Order::createFromCheckoutFormData($values);
        $order->setDataFromCheckout($this->_checkout);

        // ulozeni objednavky do DB
        if ($form->isValid()) {
            if ($order->save($this->context)) {
                // sestaveni obsahu pro ulozeni
                $templateString = (string)$this->_getEmailSummaryTemplate($order);

                // nastaveni obsahu pro ulozeni
                $order->setContent($templateString);

                // ulozeni zmen
                $order->save($this->context);

                // aktualizace skladovych zasob
                $order->updateStock($this->context);

                // metody odeslou emaily
                $eshopParams   = $this->context->getParameters()['eshop'];
                $summaryParams = $eshopParams['summary'];

                $order->sendEmailSummary($summaryParams['subject'], $summaryParams['from'], $summaryParams['to'], $templateString, $eshopParams['bcc']);
                //$order->sendEmailClientInfo();

                $this->redirect('thankyoupage', array(
                    'orderId' => $order->getId(),
                ));
            } else {
                $this->flashMessage('Něco se u nás pokazilo. Objednávku se nepovedlo uložit.');
            }
        } else {
            $this->flashMessage('Něco se u nás pokazilo. Zkuste prosím dokončit objednávku později.');
        }

        $order->setValuesToTemplate($this->template);

        // prekresleni kosiku
        $this->redrawControl('checkout');

        // prekresleni KOMPONENTY kosiku v hlavicce
        $this['cartMiniControl']->invalidateControl();

        return;

        echo '<pre>'.print_r($order, true).'</pre>';
        die;
    }

    /**
     * Metoda nastavi defaultni data do formulare.
     *
     * @param $form
     * @param array $data
     */
    protected function _setFormData(Nette\Application\UI\Form &$form, array $data){
        if (isset($data['payment']) && $data['payment'] != $this->_checkout->getPaymentId()) {
            unset($data['payment']);
        }

        $form->setDefaults($data);
    }

    public function handleremove($productId, $variantId = null){
        // odstraneni produktu z kosiku
        $this->_cart->removeProduct($productId, $variantId);

        // inicializace cen kosiku
        $this->template->shippingPriceWithVat    = $this->_checkout->getShippingPriceWithVat();
        $this->template->shippingPriceWithoutVat = $this->_checkout->getShippingPriceWithoutVat();
        $this->template->productPriceWithVat     = $this->_cart->getPriceWithVat();
        $this->template->productPriceWithoutVat  = $this->_cart->getPriceWithoutVat();
        $this->template->totalPrice              = $this->_checkout->getTotalPriceWithVat();
        $this->template->totalPriceWithVat       = $this->_checkout->getTotalPriceWithVat();
        $this->template->totalPriceWithoutVat    = $this->_checkout->getTotalPriceWithoutVat();
        $this->template->priceToDeliveryFree     = $this->_checkout->getPriceToDeliveryFree();

        // prekresleni kosiku
        $this->redrawControl('checkout');

        // prekresleni KOMPONENTY kosiku v hlavicce
        $this['cartMiniControl']->invalidateControl();

        return;
    }

    public function handleremoveall(){
        // odstraneni produktu z kosiku
        $this->_cart->truncate();

        // prekresleni kosiku
        $this->redrawControl('checkout');

        // prekresleni KOMPONENTY kosiku v hlavicce
        $this['cartMiniControl']->invalidateControl();

        return;
    }

    public function handlesetdelivery($delivery){
        //die($delivery);
        $this->_checkout->setDelivery($delivery);

        $this->template->numStepActive        = 2;

        $this->template->productPriceWithVat     = $this->_cart->getPriceWithVat();
        $this->template->productPriceWithoutVat  = $this->_cart->getPriceWithoutVat();
        $this->template->shippingPriceWithVat    = $this->_checkout->getShippingPriceWithVat();
        $this->template->shippingPriceWithoutVat = $this->_checkout->getShippingPriceWithoutVat();
        $this->template->totalPriceWithVat       = $this->_checkout->getTotalPriceWithVat();
        $this->template->totalPriceWithoutVat    = $this->_checkout->getTotalPriceWithoutVat();

        $this->template->totalPrice              = $this->_checkout->getTotalPrice();

        // prekresleni kosiku
        $this->redrawControl('checkout');

        return;
    }

    public function handlesetpayment($payment){
        //die($payment);
        $this->_checkout->setPayment($payment);

        $this->template->numStepActive        = 2;

        $this->template->productPriceWithVat     = $this->_cart->getPriceWithVat();
        $this->template->productPriceWithoutVat  = $this->_cart->getPriceWithoutVat();
        $this->template->shippingPriceWithVat    = $this->_checkout->getShippingPriceWithVat();
        $this->template->shippingPriceWithoutVat = $this->_checkout->getShippingPriceWithoutVat();
        $this->template->totalPriceWithVat       = $this->_checkout->getTotalPriceWithVat();
        $this->template->totalPriceWithoutVat    = $this->_checkout->getTotalPriceWithoutVat();

        $this->template->totalPrice              = $this->_checkout->getTotalPrice();

        // prekresleni kosiku
        $this->redrawControl('checkout');

        return;
    }

    /**
     * Metoda generuje HTML vystup sumarizace objednavky pro email.
     *
     * @param $values
     * @return ITemplate
     */
    protected function _getEmailSummaryTemplate(Order $order){
        // nastaveni sablony emailu
        $template = $this->createTemplate()->setFile(__DIR__.'/../templates/Checkout/emailSummary.latte');

        // naplneni dat
        $order->setValuesToTemplate($template);

        return $template;
    }

    public function renderDebug(){
        echo '<pre>'.print_r($this->_cart, true).'</pre>';
        die;
    }

}
