<?php
namespace App\AdminModule\CoreModule\Model\Ajax;

class Data
{
    protected $_message;
    protected $_popup;
    protected $_contents;
    
    /**
    * Konstruktor pouze nastavi vnitrni atributy objektu.
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function __construct() {
        $this->_message  = array();
        $this->_popup    = array();
        $this->_contents = array();
    } // end function
    
    /**
    * Metoda prida zpravu (flash message).
    * 
    * @author MiG
    * @param string
    * @param string
    * @param int
    * @param string
    * @return \AdminModule\CoreModule\Model\Ajax\Data
    */
    function setMessage($message, $title = '', $delay = 0, $type = 'info') {
        $this->_message = array(
            'title'   => $title,
            'delay'   => $delay,
            'type'    => $type,
            'message' => $message
        );
        return $this;
    } // end function
    
    /**
    * Metoda prida data pro aktualizaci pomoci ajaxoveho zpracovani.
    * 
    * @author MiG
    * @param string $content HTML content
    * @param string $method update nebo replace
    * @param string $selector selector, pomoci nehoz zacilim v JS aktualizovany (nahrazovany) objekt
    * @return \AdminModule\CoreModule\Model\Ajax\Data
    */
    function addContentUpdate($content, $method = 'replace', $selector = NULL) {
        $this->_contents[] = array(
            'method'   => $method,
            'selector' => $selector,
            'content'  => $content,
        );
        return $this;
    } // end function
    
    /**
    * Metoda sestavi data pro popup okno.
    * 
    * @author MiG
    * @param void
    * @return \AdminModule\CoreModule\Model\Ajax\Data
    */
    function setPopup($content, $title, Array $buttons = array(), Array $options = array()) {
        $this->_popup = array(
            "content"    => $content,
            "resizable"  => false,
            "modal"      => true,
            'title'      => '<div class="widget-header"><h4 class="smaller"><i class="icon-warning-sign red"></i>'.$title.'</h4></div>',
            'title_html' => true,
            'buttons' => array(
                array(
                    'html'  => "<i class='icon-trash bigger-110'></i>&nbsp; Delete all items",
                    'class' => 'btn btn-danger btn-xs',
                    'event' => 'close'
                ),
                array(
                    'html'  => "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
                    'class' => 'btn btn-xs',
                    'event' => 'close'
                )
            )
        );
        return $this;
    } // end function
    
    /**
    * Metoda sestavi data pro popup okno.
    * 
    * @author MiG
    * @param void
    * @return \AdminModule\CoreModule\Model\Ajax\Data
    */
    function setConfirm($content, $title, Array $buttons = array(), Array $options = array()) {
        $this->_popup = array(
            "content"    => $content,
            "resizable"  => false,
            "modal"      => true,
            'title'      => '<div class="widget-header"><h4 class="smaller"><i class="icon-warning-sign red"></i>'.$title.'</h4></div>',
            'title_html' => true,
            'buttons' => array(
                array(
                    'html'  => "<i class='icon-check bigger-110'></i>&nbsp; Ano",
                    'class' => 'btn btn-success btn-xs',
                    'event' => 'close'
                ),
                array(
                    'html'  => "<i class='icon-remove bigger-110'></i>&nbsp; Ne",
                    'class' => 'btn btn-xs',
                    'event' => 'close'
                )
            )
        );
        return $this;
    } // end function
    
    /**
    * Magicka metoda umoznujici pretypovani objektu na pole.
    * 
    * @author MiG
    * @param void
    * @return array
    */
    function __toArray() {
        return array(
            'message'  => $this->_message,
            'popup'    => $this->_popup,
            'contents' => $this->_contents,
        );
    } // end function
    
    /**
    * Metoda vrati data v poli pro odeslani v JSON objektu.
    * 
    * @author MiG
    * @param void
    * @return array
    */
    function getArray() {
        return $this->__toArray();
    } // end function
    
} // end class