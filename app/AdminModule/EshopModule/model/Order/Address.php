<?php

namespace App\AdminModule\EshopModule\Model\Order;

use Nette\Database\Table\ActiveRow;

class Address {
    protected $_name;
    protected $_street;
    protected $_city;
    protected $_zip;
    protected $_email;
    protected $_phone;

    /**
     * Metoda vytvori instanci objednavky a inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     */
    public static function createFromActiveRow(ActiveRow $activeRow){
        $address = new self;
        $address->initFromActiveRow($activeRow);

        return $address;
    }

    /**
     * Metoda inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     */
    public function initFromActiveRow(ActiveRow $activeRow){
        $this->_name   = $activeRow->name;
        $this->_street = $activeRow->street;
        $this->_city   = $activeRow->city;
        $this->_zip    = $activeRow->zip;
        $this->_email  = $activeRow->email;
        $this->_phone  = $activeRow->phone;
    }

    /**
     * Metoda inicializuje objednavku z dat formulare.
     *
     * @param array $values
     * @return \App\AdminModule\EshopModule\Model\Order\Address
     */
    public static function createFromCheckoutFormData($values){
        $address = new self;
        $address->initFromCheckoutFormData($values);

        return $address;
    }

    /**
     * Metoda inicializuje objednavku z active row.
     *
     * @param ActiveRow $activeRow
     */
    public function initFromCheckoutFormData($values){
        $this->_name   = $values->shippingAddress->name;
        $this->_street = $values->shippingAddress->street;
        $this->_city   = $values->shippingAddress->city;
        $this->_zip    = $values->shippingAddress->zip;
        $this->_email  = $values->email;
        $this->_phone  = $values->phone;
    }

    public function __get($attrName){
        $attrNameWithPrefix = '_'.$attrName;
        if (isset($this->$attrNameWithPrefix)) {
            return $this->$attrNameWithPrefix;
        }
        return '## CHYBA - nezámý atribut '.$attrName.' ##';
    }

}