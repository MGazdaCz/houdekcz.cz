<?php

namespace App\AdminModule\EshopModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Product extends MainRepository
{
    protected $_table = 'eshop_product';

    /**
     * Nacte kategorii dle zadane url a pouzije rozsiri vhere o view a delete.
     *
     * @param $url
     * @return \Nette\Database\Table\ActiveRow
     */
    public function findVisibleByUrl($url){
        return $this->findBy(array(
            'url'    => $url,
            'view'   => 1,
            'delete' => 0
        ))->fetch();
    }
}