<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Payment presenter.
 */
class PaymentPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Payment
     */
    protected $_repository;

    protected function startup(){
        parent::startup();

        $this->_repository = $this->context->eshopPayment;

        $this->template->bodyClass = 'module-payment';
    }

    public function renderDefault()
    {
        $this->template->h1 = 'Způsoby platby';
        $this->template->items = $this->_repository->findAll()->order('name');
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Nový způsob platby';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace platby: '.$this->_repository->findByPk($id)->name;
    }

    protected function createComponentForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form->addText('name', 'Název', 50);
        $form->addText('price', 'Cena', 5);
        $form->addCheckbox('active', 'aktivní');
        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);

        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_repository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {

                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        } else {
            $form->setDefaults(array(
                'price' => '0.00'
            ));
        }

        return $form;
    }

    public function formSucceeded(Nette\Application\UI\Form $form, $values){
        $id = $values['id'];

        // metoda predpripravi data pro ulozeni zaznamu
        $this->_prepareValuesForSave($values, true);

        if (empty($id)) {
            $result = $this->_repository->insert($values);

            if (isset($result->id)) {
                $id = $result->id;

                $this->flashMessage('Záznam byl úspěšně založen.');

                $this->redirect('edit', array('id' => $id));
                return;
            }

            $this->redirect('add');
            return;
        } else {
            $result = $this->_repository->update($values, array(
                'id' => $id
            ));

            $this->flashMessage('Záznam byl úspěšně uložen.');
        }

        $this->redirect('edit', array('id' => $id));

        /*
        echo '<pre>'.print_r($this->getParameters(), true).'</pre>';
        echo '<pre>'.print_r($result, true).'</pre>';
        echo '<pre>'.print_r($values, true).'</pre>';
        die;
        */
    }

}
