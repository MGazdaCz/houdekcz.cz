<?php
namespace App\AdminModule\Model\Filter;

/**
 * Class Quick
 *
 * Trida pro praci s quick filtrem. Jeho konfigurace se provadi v konfiguracnich souborech jednotlivych modulu.
 *
 * @package AdminModule\Model\Filter
 */
class Quick
{
    private $_name;
    private $_defaultFilter;
    private $_actualFilter;
    private $_items;
    private $_config;

    private $_params;


    public function __construct(){
        $this->_actualFilter   = '';
        $this->_defaultlFilter = '';
    }

    public function initFromArray(array $configArray)
    {
        $this->_config = $configArray;

        $this->_name          = $this->_config['name'];
        $this->_defaultFilter = $this->_config['default'];
        $this->_items         = $this->_config['items'];

        //die('<pre>' . print_r($this, true) . '</pre>');

        return $this;
    } // end function

    /**
     * @return string
     */
    public function getName(){
        return $this->_name;
    }

    /**
     * @return string
     */
    public function getDefaultFilter(){
        return $this->_defaultlFilter;
    }

    /**
     * @param $actualFilter
     * @return \AdminModule\Model\Filter\Quick
     */
    public function setActualFilter($actualFilter)
    {
        $this->_actualFilter = $actualFilter;
        return $this;
    }

    /**
     * Metoda nastavi parametry, ktere je mozne vyuzivat pri sestavovani podminek where.
     *
     * @param array $params
     * @return \AdminModule\Model\Filter\Quick
     */
    public function setParams(array $params)
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * @return string
     */
    public function getActualFilter()
    {
        if (!empty($this->_actualFilter) && isset($this->_items[$this->_actualFilter]))
        {
            return $this->_actualFilter;
        } // end if

        if (!empty($this->_defaultFilter) && isset($this->_items[$this->_defaultFilter]))
        {
            return $this->_defaultFilter;
        } // end if

        return '';
    }

    /**
     * @return string
     */
    public function getWhere()
    {
        $actualFilter = $this->getActualFilter();
        if (!empty($actualFilter) && isset($this->_items[$actualFilter]))
        {
            // seznam podporovanych promennych
            $variablesArray = array(
                'idUser'
            );

            $where = $this->_items[$actualFilter]['where'];

            // prochazeni podporovanych promennych a nahrazovani hodnotami v SQL podmince where
            foreach ($variablesArray as $var) {
                $varTmp = "!$var!";
                $where = str_replace($varTmp, $this->_params[$var], $where);
            } // end foreach

            return $where;
        } // end if

        return '';
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->_items;
    }

} // end class