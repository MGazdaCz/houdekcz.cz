<?php
namespace Control;

use \Nette\Application\UI\Control;

abstract class AbstractControl extends Control
{
    protected $_url;
    protected $_context;

    public function setUrl($url){
        $this->_url = $url;
    }

    public function setContext($context){
        $this->_context = $context;
    }

    abstract public function render();

} // end class