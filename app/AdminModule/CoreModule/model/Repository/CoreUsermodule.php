<?php
namespace App\AdminModule\CoreModule\Model\Repository;
use Nette;

class CoreUsermodule extends Repository
{
    protected $_table = 'core_usermodule';

    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function saveModules($userId, array $modules) {
        foreach ($modules as $moduleId) {
            $this->insert(array(
                'User_id' => intval($userId),
                'module'  => $moduleId
            ));
        } // end foreach
        return $this;
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function deleteModules($userId) {
        return $this->delete(array('User_id' => $userId));
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param string $userId
    * @return void
    */
    function fetchAllForUser($userId, $onlyIds = false) {
        $rows = $this->findBy(array('User_id' => $userId));
        
        if ($onlyIds)
        {
            $data = array();
            
            foreach ($rows as $row) {
                $data[] = $row->module;
            } // end foreach
            
            return $data;
        } // end if
        
        return $rows;
    } // end function
    
} // end class