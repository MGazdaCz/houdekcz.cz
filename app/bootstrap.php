<?php

// prozatimni definovani cest
define('WWW_ROOT', 'http://server.mgazda.cz/houdekcz.cz');
define('DS', '/');
define('TEMP_DIR', __DIR__ . '/../temp');
define('WWW_DIR', __DIR__ . '/../www');
define('DATA_DIR', WWW_DIR . '/data');
define('WWW_WWW', WWW_ROOT);
define('DATA_WWW', WWW_WWW . '/data');

// TODO - definice dopravy zdarma
define('DELIVERY_FREE_FROM', 1999);

// Load Nette Framework or autoloader generated by Composer
require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

// Enable Nette Debugger for error visualisation & logging
//$configurator->setDebugMode(true);
//$configurator->setDebugMode(array('213.19.58.110'));
$configurator->setDebugMode(true);
$configurator->enableDebugger(__DIR__ . '/../log');

// Specify folder for cache
$configurator->setTempDirectory(__DIR__ . '/../temp');

// Enable RobotLoader - this will load all classes automatically
$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

// Create Dependency Injection container from config.neon file
$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon'); // none section

/**
 * Include konfiguracnich souboru jednotlivych modulu.
 */
$moduleConfigFiles = array(
    __DIR__ . '/AdminModule/CmsModule/config/config.neon',
    __DIR__ . '/AdminModule/CoreModule/config/config.neon',
    __DIR__ . '/AdminModule/ImModule/config/config.neon',
    __DIR__ . '/AdminModule/EshopModule/config/config.neon',
    __DIR__ . '/AdminModule/ErpModule/config/config.neon',
);
foreach ($moduleConfigFiles as $moduleConfigPath) {
    if (file_exists($moduleConfigPath)) {
        $configurator->addConfig($moduleConfigPath);
    }
}

$container = $configurator->createContainer();

return $container;