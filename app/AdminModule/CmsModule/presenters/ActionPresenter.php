<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;


/**
 * Action presenter.
 */
class ActionPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\Article
     */
    protected $_articleRepo;

    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsAction');
        $this->_articleRepo    = $this->context->getService('cmsArticle');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Akce';
        $this->_orderDefault = 'date DESC';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace akce: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $articles = $this->_articleRepo->fetchPairsWithWhereAndOrder('id', 'name', array(), 'name', true);

        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addTextArea('name', 'Název', 80, 3)
            ->setAttribute('placeholder', 'Název akce')
            ->setRequired('Zadejte název akce');
        $form->addUpload('image', 'Obrázek');
        $form
            ->addText('url', 'URL adresa', 80)
            ->setAttribute('placeholder', 'Webová adresa akce');
        $form
            ->addText('date', 'Datum', 10)
            ->setAttribute('class', 'date-picker');
        $form
            ->addText('time', 'Čas', 10)
            ->addCondition(Nette\Application\UI\Form::FILLED)
            ->addRule(Nette\Application\UI\Form::PATTERN, 'Čas musí být ve tvaru hh:mm např. 12:30.', '([0-9]){2}:([0-9]){2}');
            //->setAttribute('placeholder', date('H:i'));
        $form
            ->addText('urlShow', 'URL pro zobrazení', 80)
            ->setAttribute('placeholder', 'Webová adresa akce, která se má zobrazi. Pokud není zadaná, použije se z předchozího pole.');
        $form
            ->addCheckbox('view', 'zobrazit');
        $form
            ->addCheckbox('showOnHomepage', 'zobrazit na homepage');
        $form
            ->addSelect('cms_article_id', 'Související článek', $articles);
        $form
            ->addTextArea('text', 'Text', 80, 5);
            //->setAttribute('class', 'ckeditor');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $itemArray = $item->toArray();

                // preformatovani datumu
                $date              = $item->date;
                $itemArray['date'] = $date->format('d.m.Y');
                $itemArray['time'] = $date->format('H:i');

                $form->setDefaults($itemArray);
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // osetreni data a casu
        $date = $values['date'];
        $time = $values['time'];
        $date = $date.' '.$time;

        $values['date'] = Nette\Utils\DateTime::from($date)->format('Y-m-d H:i:s');
        unset($values['time']);

        if (empty($values['cms_article_id'])) {
            $values['cms_article_id'] = null;
        }

        // pokud je vybran priznak zobrazit na HP, resetnu ostatni akce
        if (!empty($values['showOnHomepage'])) {
            $this->_mainRepository->update(array('showOnHomepage' => 0), array());
        }

        // osetreni uploadu obrazku
        if ($values['image']->isOk()) {
            $image  = $values['image']->toImage();
            $image2 = clone $image;

            $fileNameArray = explode('.', $values['image']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['image']->move(DATA_DIR . "/action/$filename");

                if ($image) {
                    $image->resize(120,90);
                    $image->save(DATA_DIR . "/action/preview/$filename");
                }

                if ($image2) {
                    $image2->resize(390,null);
                    $image2->save(DATA_DIR . "/action/list/$filename");
                }
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['image'] = $filename;
        } else {
            unset($values['image']);
        }
    }

}
