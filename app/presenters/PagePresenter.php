<?php

namespace App\Presenters;
use Nette\Application\Application;

/**
 * Homepage presenter.
 */
class PagePresenter extends BasePresenter
{
    /**
     * @var \Nette\Http\Session|Nette\Http\SessionSection $_language
     */
    private $_language;
    private $_page;
    /**
     * @var \AdminModule\CmsModule\Model\Repository\CmspageRepository
     */
    private $_pageRepository;
    private $_componentName;

    protected function startup()
    {
        parent::startup();
        $this->_pageRepository = $this->context->getService('cmsPage');

        $this->template->registerHelper('textbox', 'TextboxHelper::textbox');
        $this->template->bodyClass = 'page';
    } // end function

	public function renderDefault()
	{
        $url   = $this->getParameter('url');
        $pages = $this->_pageRepository->findBy(array(
            'view' => 1, 'url' => '/'.$url, 'delete = 0',
            '(dateFrom IS NULL AND dateTo IS NULL) OR (dateFrom <= CURDATE() AND dateTo IS NULL) OR (dateFrom IS NULL AND dateTo >= CURDATE()) OR (dateFrom <= CURDATE() AND dateTo >= CURDATE())'
        ))->limit(1);

        if ($pages->count() > 0)
        {
            $this->_page = $pages->fetch();

            // ulozeni mutace do session
            $this->_language     = $this->getSession('feLanguage');
            $this->_language->id = $this->_page->core_language_id;

            // nazev komponenty si ulozim do session abych si jej pamatoval i po odeslani formu
            $componentManager       = $this->getSession('componentManager');
            $componentManager->name = $this->_componentName = $this->_page->component;

            //echo var_dump($pageData); die;

            // pokud to neni homepage, nastavim jiny pohled i classu na body
            //$this->template->bodyClass = 'uvod';
            if (!$this->_page->homepage)
            {
                $this->setView('page');
                $this->template->bodyClass .= ' page-'.$this->_page->id;
            }

            // pokud je obsah stranky prazdny, nastavim fixed na paticku
            if (empty($this->_page->text)) {
                $this->template->bottomClass = ' fixed';
            }

            // init atributu pro template
            $this->template->title       = $this->_page->title;
            $this->template->keywords    = $this->_page->keywords;
            $this->template->description = $this->_page->description;
            $this->template->page        = $this->_page;
            $this->template->subpages    = $this->_pageRepository->getSubpages($this->_page->id);
            $this->template->componensts = $this->getComponents();
            $this->template->language    = $this->_language;

            // inicializace URL homepage
            $homepage = $this->_pageRepository->getHomepage($this->_language->id)->fetch();
            $this->template->homepageUrl = '/';
            if (!empty($homepage)) {
                $this->template->homepageUrl = $homepage->url;
            } // end if
            // ---

            $this->template->pluginTemplate = 'test.latte';
        }
        else
        {
            throw new \Nette\Application\BadRequestException('Stránka nenalezena.', 404);
        } //end if
    }
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function getPage() {
        return $this->_page;
    } // end function

    protected function createComponentPageComponent()
    {
        // nazev komponenty ze session
        $componentManager = $this->getSession('componentManager');
        $componentName    = (!empty($componentManager->name) ? $componentManager->name : 'def');

        // init komponenty
        $componentNameUp    = $componentName;
        $componentNameUp{0} = strtoupper($componentName{0});
        $className = '\Control\\'.$componentNameUp.'\\'.$componentNameUp.'Control';

        $control = new $className();
        $control->setUrl($_SERVER['REQUEST_URI']);
        $control->setContext($this->context);

        return $control;
    }

    protected function createComponent($name)
    {
        $component = parent::createComponent($name);

        $componentNameUp    = $name;
        $componentNameUp{0} = strtoupper($componentNameUp{0});

        $className = $className = '\Control\\'.$componentNameUp.'\\'.$componentNameUp.'Control';

        if ($component === NULL && class_exists($className)) {
            $component = new $className;
            $component->setUrl($_SERVER['REQUEST_URI']);
            $component->setContext($this->context);
        }
        return $component;
    }

    protected function createComponentArticlesControl()
    {
        return new \App\Control\Article\ArticleControl();
    }

    protected function createComponentActionControl()
    {
        return new \App\Control\Action\ActionControl();
    }


}
