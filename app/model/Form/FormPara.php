<?php
namespace Model\Form;

use Nette\Application\UI\Form;

/**
 * Trida preteyujici formulare tak, aby se mi automaticky jednotlive elementy generovaly do odstavcu.
 * @package Model\Form
 */
class FormPara extends Form
{
    public function __construct(){
        parent::__construct();

        $renderer = $this->getRenderer();

        $renderer->wrappers['controls']['container'] = 'fieldset';
        $renderer->wrappers['pair']['container']     = 'p';
        $renderer->wrappers['pair']['.required']     = 'item required';
        $renderer->wrappers['pair']['.optional']     = 'item';
        $renderer->wrappers['pair']['.odd']          = 'odd';
        $renderer->wrappers['label']['container']    = null;
        $renderer->wrappers['control']['container']  = null;
    }
}