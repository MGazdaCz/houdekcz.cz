<?php

namespace App\Model\Search;

use Nette\Utils\Strings;

class Category extends ModuleAbstract {

    public function init()
    {
        $this->_mainRepository = $this->_context->getService('eshopCategory');
    }

    public function search($queryString)
    {
        $queryString = '%'.addslashes(Strings::lower($queryString)).'%';

        $condition = 'delete = 0 AND view = 1 AND (LOWER(name) LIKE ? OR LOWER(heading) LIKE ? OR LOWER(annotation) LIKE ? OR LOWER(text) LIKE ?)';

        $dbSelection = $this->_mainRepository->getTable()->where($condition, $queryString, $queryString, $queryString, $queryString);

        return $this->prepareReturnData('Kategorie eshopu', $dbSelection);
    }

}