<?php
namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;
use Nette;

class GuestbookRepository extends MainRepository
{
    public function insertRecord(array $data){
        $data['date']    = date('Y-m-d H:i:s');
        $data['display'] = intval(0);

        return $this->insert($data);
    }
}

