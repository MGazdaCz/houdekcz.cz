<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Advice Category presenter.
 */
class AdvicePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\CmsModule\Model\Repository\Advice
     */
    protected $_repository;
    /**
     * @var \App\CmsModule\Model\Repository\AdviceCategory
     */
    protected $_repositoryCategory;

    protected function startup(){
        parent::startup();

        $this->_repository         = $this->context->cmsAdvice;
        $this->_repositoryCategory = $this->context->cmsAdviceCategory;

        $this->template->bodyClass = 'module-advice';
    }

    public function renderDefault()
    {
        $categoryId = $this->getParameter('advice_category_id', null);

        if (!is_null($categoryId)) {
            $this->template->items = $this->_repository->findBy(array(
                'cms_advice_category_id' => $categoryId
            ))->order('name');
        } else {
            $this->template->items = $this->_repository->findAll()->order('name');
        }
    }

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Přidání příspěvku';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace příspěvku: '.$this->_repository->findByPk($id)->name;
    }

    protected function createComponentForm()
    {
        $categories = $this->_repositoryCategory->fetchPairs('id', 'name', true);

        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form->addSelect('cms_advice_category_id', 'Kategorie', $categories);
        $form
            ->addText('name', '', 80)
            ->setAttribute('placeholder', 'Název')
            ->setRequired('Zadejte stránky');
        $form
            ->addText('heading', '', 80)
            ->setAttribute('placeholder', 'Nadpis')
            ->setRequired('Zadejte nadpis');
        $form->addCheckbox('view', 'schváleno');

        $form
            ->addText('uName', '', 80)
            ->setAttribute('placeholder', 'Jméno tazatele');
        $form
            ->addText('uEmail', '', 80)
            ->setAttribute('placeholder', 'Email tazatele');
        $form
            ->addTextArea('question', 'Otázka', 80, 5);
            //->setAttribute('height', '100px')
            //->setAttribute('class', 'ckeditor');
        $form
            ->addTextArea('answer', 'Odpověď', 80, 5);
        //->setAttribute('height', '100px')
        //->setAttribute('class', 'ckeditor');

        $form->addSubmit('save', PageBase::SUBMIT_DEFAULT_LABEL);
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_repository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}
