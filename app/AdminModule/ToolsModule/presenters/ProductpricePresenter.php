<?php

namespace App\AdminModule\ToolsModule\Presenters;

use App\AdminModule\CoreModule\Model\Forms\FormEditorDetail;
use Nette,
    App\Model;


/**
 * Product price presenter.
 */
class ProductpricePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_mainRepository;


    protected function startup(){
        parent::startup();

        // konfigurace repositaru
        $this->_mainRepository      = $this->context->getService('eshopProduct');

        $this->template->bodyClass = 'tool-product-price';

        // nastaveni defaultni sablony formulare
        //$this->_formTemplate = __DIR__.'/../templates/Product/form.latte';
    }

    public function renderDefault() {
        // vypocty strankovani
        $this->template->h1 = 'Optimalizace cen produktů';

        $this->template->items      = $this->_mainRepository->findAll();
        $this->template->itemsCount = $this->template->items->count('id');
    }

    protected function createComponentPricePerItemForm() {
        $form = new FormEditorDetail();

        $form
            ->addText('nameMask', 'Maska jména produktu', 50)
            //->setValue('[%d]');
            ->setDisabled();

        $form
            ->addCheckbox('onlyEmptyPricePerItem', 'pouze u produktů, které mají prázdnou cenu za kus');

        $form->addSubmit('submit', 'Aktualizuj ceny');

        $form->onSuccess[] = array($this, 'formPricePerItemSucceeded');

        $form->setValues(array(
            'nameMask' => '/\[(\d+).+/i',
            'onlyEmptyPricePerItem' => 1
        ));

        return $form;
    }

    public function formPricePerItemSucceeded($form, $values){
        $selection = $this->_mainRepository;
        if (isset($values->onlyEmptyPricePerItem) && $values->onlyEmptyPricePerItem) {
            $products = $selection->findBy(array('pricePerItem = 0 OR pricePerItem IS NULL'));
        } else {
            $products = $selection->findAll();
        }

        $count = 0;
        if ($products->count() > 0) {
            foreach ($products as $product) {
                $array  = array();
                $result = preg_match('/\[(\d+).+/i', $product->name, $array);

//                echo '<pre>'.print_r($array, true).'</pre>';
//                echo '<pre>'.print_r($product->name, true).'</pre>';
//                echo '<pre>'.print_r($product->price, true).'</pre>';
//                echo '<pre>'.print_r($product->pricePerItem, true).'</pre>';

                if ($result && !empty($array) && isset($array[1])) {
                    $itemsInPackage = intval($array[1]);

                    $pricePerItem = $product->price / $itemsInPackage;

                    if ($pricePerItem > 0) {
                        $selection->update(array(
                            'pricePerItem' => $pricePerItem
                        ), array('id' => $product->id));

                        $count++;
                    }
                }
                //die;
            }
        }

        $this->flashMessage('Aktualizováno '.$count.' produktů');

//        echo '<pre>'.print_r($products->count(), true).'</pre>';
//        die;
    }

}
