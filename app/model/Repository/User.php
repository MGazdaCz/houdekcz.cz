<?php
namespace App\Model\Repository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class User extends RepositoryAbstract
{
    protected $_table = 'cms_user';
}