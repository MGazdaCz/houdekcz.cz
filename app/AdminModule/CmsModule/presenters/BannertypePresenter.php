<?php

namespace App\AdminModule\CmsModule\Presenters;

use Nette,
    App\Model;


/**
 * Bannertype presenter.
 */
class BannertypePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected function startup(){
        parent::startup();

        $this->_mainRepository = $this->context->getService('cmsBannerType');

        //$this->template->bodyClass  = 'module-text';

        $this->_listH1       = 'Typy bannerů';
        $this->_orderDefault = 'id';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1 = 'Editace typu: '.$this->_mainRepository->findByPk($id)->name;
    }

    protected function createComponentDefaultForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addText('name', '', 80)
            ->setAttribute('placeholder', 'Orientační název typu banneru')
            ->setRequired('Zadejte název typu banneru');
        $form
            ->addText('width', '', 10)
            ->setAttribute('placeholder', 'Šířka')
            ->addRule(Nette\Application\UI\Form::INTEGER, 'Šířka musí být celé číslo v pixelech')
            ->setRequired('Zadejte šířku banneru');
        $form
            ->addText('height', '', 10)
            ->setAttribute('placeholder', 'Výška')
            ->addRule(Nette\Application\UI\Form::INTEGER, 'Výška musí být celé číslo v pixelech')
            ->setRequired('Zadejte výšku banneru');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

}
