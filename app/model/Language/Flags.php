<?php
namespace FrontModule\Model\Language;

use Nette\Object;

/**
 * Trida pro nacteni vlajecek pro prepinani mutaci.
 * Navic trida inicializuje URL adrey pro jednotlive vlajecky.
 *
 * User: Milan
 * Date: 11.9.14
 * Time: 17:23
 */
class Flags {

    /** @var array $_languages **/
    private $_languages = array();
    /** @var \AdminModule\CoreModule\Model\Repository\CoreLanguage $_langRepository **/
    private $_langRepository;
    /** @var \AdminModule\CmsModule\Model\Repository\CmspageRepository $_pageRepository **/
    private $_pageRepository;

    /**
     * Metoda nastavi repository pro praci s mutacemi.
     *
     * @param \AdminModule\CoreModule\Model\Repository\CoreLanguage $repository
     * @return \FrontModule\Model\Language\Flags
     */
    public function setLanguageRepostiory(\AdminModule\CoreModule\Model\Repository\CoreLanguage $repository){
        $this->_langRepository = $repository;
        return $this;
    }

    /**
     * Metoda nastavi repository pro praci se strankami.
     *
     * @param \AdminModule\CmsModule\Model\Repository\CmspageRepository $repository
     * @return \FrontModule\Model\Language\Flags
     */
    public function setPageRepostiory(\AdminModule\CmsModule\Model\Repository\CmspageRepository $repository){
        $this->_pageRepository = $repository;
        return $this;
    }

    /**
     * Metoda vrati pocet nactenych jazykovych mutaci.
     *
     * @return int
     */
    public function count(){
        return count($this->_languages);
    }

    /**
     * Metoda nacte a inicializuje adresy homepage jazykovych mutaci.
     *
     * @return \FrontModule\Model\Language\Flags
     */
    public function loadLanguages()
    {
        $languageRepository = $this->_langRepository->findBy(array('active = ?' => 1))->fetchAll();

        foreach ($languageRepository as $lang) {
            $langItem = $lang->toArray();
            $langItem['url'] = $this->_pageRepository->getHomepage($lang->id)->fetch()->url;

            $this->_languages[] = $langItem;
        } // end foreach

        return $this;
    }

    /**
     * Metoda vrati pole nactenych jazykovych mutaci.
     *
     * @return array
     */
    public function getLanguages()
    {
        return $this->_languages;
    }

} // end class