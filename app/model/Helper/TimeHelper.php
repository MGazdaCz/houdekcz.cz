<?php
namespace Model\Helper;

class TimeHelper
{
    /**
     * Metoda preformatuje cas v sekundach na vystupni format HODINY:MINUTY
     *
     * @param integer $time cas v sekundach
     * @return string vystupni format H:s
     */
    public static function secondToHourAndMinute($time)
    {
        $hour = floor($time / 3600);
        $min  = floor(($time - $hour * 3600) / 60);

        return sprintf('%02d:%02d', $hour, $min);
    }
} // end class