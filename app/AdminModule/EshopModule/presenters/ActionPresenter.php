<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CoreModule\Model\Forms\FormEditorDetail;
use Nette,
    App\Model;


/**
 * Action presenter.
 */
class ActionPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    protected $_productsArray;

    protected function startup(){
        parent::startup();

        // konfigurace repositaru
        $this->_mainRepository = $this->context->getService('eshopAction');

        $this->template->bodyClass = 'module-eshop-action';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Action/form.latte';
    }

    public function renderDefault()
    {
        $this->template->h1 = 'Akce v eshopu';
        $this->template->items = $this->_mainRepository->findAll();
        $this->template->itemsCount = $this->template->items->count();
    }

    protected function createComponentForm()
    {
        $products = $this->context->getService('eshopProduct')->fetchPairs('id', 'name');

        $form = new FormEditorDetail();
        $form->addHidden('id');
        $form
            ->addText('name', 'Název', 50)
            ->setRequired();
        $form
            ->addMultiSelect('products', 'Produkty v akci', $products)
            ->setAttribute('class', 'select2-multiple width-100');
        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);

        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $itemArray = $item->toArray();

                if (!empty($itemArray['products'])) {
                    $itemArray['products'] = explode(',', $item->products);
                } else {
                    unset($itemArray['products']);
                }

                $form->setDefaults($itemArray);
            } else {

                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // uchovani naklikanych produktu v TMP pro ulozeni po ulozeni hlavniho zaznamu
        $this->_productsArray = $values['products'];

        $values['count']    = count($values['products']);
        $values['products'] = implode(',', $this->_productsArray);
    }

    protected function _afterSave($id, $values){
        $actionProductRepo = $this->context->getService('eshopActionProduct');
        $actionProductRepo->delete(array('eshop_action_id' => $id));

        // ulozim vazby na produkty
        if (!empty($this->_productsArray)) {
            foreach ($this->_productsArray as $productId) {
                $actionProductRepo->insert(array(
                    'eshop_action_id'  => $id,
                    'eshop_product_id' => $productId
                ));
            }
        }
    }

}
