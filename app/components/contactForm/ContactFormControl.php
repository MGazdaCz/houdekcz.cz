<?php
namespace Control\ContactForm;

use Control,
    Control\ContactForm\Translator,
    \Nette\Application\UI\Form,
    \Nette\Mail\Message,
    \Nette\Mail\SendmailMailer;
use Model\Form\FormPara;

class ContactFormControl extends Control\AbstractControl
{
    private $_formMessage;

    public function render($fields = 'name,email,phone,text')
    {
        $session            = $this->parent->context->getByType('Nette\Http\Session');
        $this->_formMessage = $session->getSection('formMessage');

        $this->template->setFile(__DIR__.'/form.latte');
        $this->template->formMessage = $this->_formMessage;
        $this->template->render();

        $this->_formMessage->message = '';
    }

    protected function createComponentForm()
    {
        // nacteni aktualni jazykove mutace
        $language = $this->getParent()->getSession('feLanguage');

        // sestaveni formulare
        $form = new FormPara();
        $form->addHidden('backlink', $this->_url);
        $form
            ->addText('name', 'Jméno a příjmení')
            ->setAttribute('placeholder', 'vaše jméno')
            ->setRequired('Zadejte prosím jméno a příjmení.')
            ->addRule(Form::MIN_LENGTH, 'Zadejte prosím jméno a příjmení (minimální délka 5 znaků).', 5);
        $form
            ->addText('email', 'E-mail')
            ->setAttribute('placeholder', 'váš e-mail')
            ->setRequired('Zadejte prosím e-mail.')
            ->addRule(Form::EMAIL, 'Email musí být ve správném tvaru např. jan.novak@gmail.com');
        $form->addText('phone', 'Telefon');
             //->setRequired('Zadejte prosím telefon.');
        //->setRequired('Zadejte prosím email ve správném tvaru.');
        $form
            ->addTextArea('text', 'Text zprávy')
            ->setAttribute('placeholder', 'text zprávy')
            ->setRequired('Zadejte prosím text zprávy.');
        $form
            ->addSubmit('submit', 'Odeslat')
            ->setAttribute('class', 'button')
            ->setAttribute('onclick', "_gaq.push(['_trackEvent','formular', 'odeslano']);");

        // inicializace translatoru
        $translator = new Translator();
        $translator->setLanguage($language->id);

        // nastaveni translatoru do formulare
        $form->setTranslator($translator);
        $form->onSuccess[] = callback($this, 'processForm');

        return $form;
    }

    public function processForm(Form $form){
        $values = (array)$form->values;
        unset($values['backlink']);

        // sestaveni sablony odeslaneho formulare
        $template = new \Nette\Templating\FileTemplate(__DIR__.'/email.latte');
        $template->registerFilter(new \Nette\Latte\Engine);
        $template->registerHelperLoader('Nette\Templating\Helpers::loader');
        $template->formName   = $this->_context->parameters['web']['name'];
        $template->formValues = $form->values;

        // odeslani formulare
        $mail = new Message();
        $mail->setFrom($this->_context->parameters['email']['admin']);
        $mail->addTo($this->_context->parameters['email']['admin']);
        $mail->addCc($form->values->email);

        if (isset($this->_context->parameters['email']['copy1'])) {
            $mail->addBcc($this->_context->parameters['email']['copy1']);
        }

        if (isset($this->_context->parameters['email']['copy2'])) {
            $mail->addBcc($this->_context->parameters['email']['copy2']);
        }

        $mail->setSubject('Kontaktní formulář z '.$this->_context->parameters['web']['name']);
        $mail->setHtmlBody($template);

        $mailer = new SendmailMailer();

        $session            = $this->parent->context->getByType('Nette\Http\Session');
        $this->_formMessage = $session->getSection('formMessage');

        try {
            $mailer->send($mail);
        } catch (\Exception $e) {
            $this->_formMessage->message = 'Formulář se nepodařio odeslat. Zkuste to prosím později.';
            $this->_formMessage->class   = 'error';

            header('Location: '.$form->values->backlink);
        }

        $this->_formMessage->message = 'Formulář byl úspěšně odeslán.';
        $this->_formMessage->class   = 'success';

        header('Location: '.$form->values->backlink);
        return;
    }

} // end class