<?php
namespace App\Control\Photogallery;

use \Nette\Application\UI\Control;

class PhotogalleryControl extends Control
{
    private $_cmsPhotogallery;

    public function render($id)
    {
        $this->renderWithTemplateAndLimit($id, 'default.latte', 0);
    }

    public function renderWithTemplate($id, $template)
    {
        $this->renderWithTemplateAndLimit($id, $template, 0);
    }

    public function renderWithLimit($id, $limit) {
        $this->renderWithTemplateAndLimit($id, 'default.latte', $limit);
    }

    public function renderWithTemplateAndLimit($id, $template, $limit){
        $this->_cmsPhotogallery = $this->getParent()->context->getService('cmsPhotogallery');

        // sestavovani query
        $selection = $this->_cmsPhotogallery->findBy(array('view' => 1, 'id = '.$id));

        if ($limit > 0) {
            $selection->limit($limit);
        }

        if (strpos($template, 'latte') === false) {
            $template .= '.latte';
        }

        $this->template->photogallery  = $selection->fetch();
        $this->template->setFile(__DIR__.'/'.$template);
        $this->template->render();
    }

}