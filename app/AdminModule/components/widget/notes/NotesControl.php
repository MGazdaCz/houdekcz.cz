<?php
namespace App\AdminModule\Control\Widget\Notes;

use \Nette\Application\UI\Control;
use App\AdminModule\Control\Widget\Base\BaseControl;

class NotesControl extends Control
{
    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/static.latte');

        // vložíme do šablony parametry
        $head    = 'Moje poznámky';
        $content = (string)$template;
        
        $baseControl = $this->getParent()->getComponent('widgetbase');
        $baseControl->setHead($head);
        $baseControl->setContent($content);
        $baseControl->render();
    } // end function
} // end class