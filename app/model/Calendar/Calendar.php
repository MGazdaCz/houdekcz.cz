<?php

namespace Model\Calendar;

use Nette;


class Calendar {

    /**
     * Metoda vraci true, pokud se jedna o presupny rok. Jinak FALSE.
     *
     * @author MiG
     * @param int $year
     * @return bool
     * @since 2013-
     */
    public static function isTransitive($year)
    {
        return (boolean) date("L", mktime(0,0,0,1,1,$year));
    } // end function

    /**
     * Metoda vraci pocet dnu v zadanem mesici.
     *
     * @author MiG
     * @param int $month
     * @param int $year
     * @return int pocet dni v mesici
     * @since 2013-
     */
    public static function getDayCount($month, $year)
    {
        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    } // end function

    /**
     *
     *
     * @author MiG
     * @param int $mesic
     * @param int $rok
     * @return int cislo dne v mesici
     * @since 2013-
     */
    public static function getFirstDay($month, $year)
    {
        $enOrder = date("w", mktime(0, 0, 0, $month, 1, $year));
        return ($enOrder==0) ? 7 : $enOrder;
    } // end function

    /**
     *
     *
     * @author MiG
     * @param int $month
     * @param int $year
     * @return int
     * @since 2013-
     */
    public static function getWeeksInMonth($month, $year)
    {
        $dayCount = self::getDayCount($month, $year);

        return date("W", mktime(0, 0, 0, $month, $dayCount-7, $year)) - date("W", mktime(0, 0, 0, $month, 1+7, $year)) + 3;
    } // end function

    /**
     * Metoda generuje matici, ktera interpretuje kalendarni mesic
     *
     * @author MiG
     * @param int
     * @param int
     * @return array dvojrozmerne pole
     * @since 2013-
     */
    public static function getMonthMatrix($month, $year)
    {
        $daysCount  = self::getDayCount($month, $year);
        $firstDay   = self::getFirstDay($month, $year);
        $weeksCount = self::getWeeksInMonth($month, $year);

        $matrix = array();
        $dayNum = 0;
        for($i=1;$i<=$weeksCount;$i++)
        {
            for($j=1;$j<=7;$j++)
            {
                $matrix[$i][$j] = '';
                if (($i == 1 && $firstDay > $j) || ($dayNum >= $daysCount))
                {
                    // prazdny den
                }
                else
                {
                    $matrix[$i][$j] = ++$dayNum;
                } // end if
            } // end for
        } // end for

        return $matrix;
    } // end function

} 