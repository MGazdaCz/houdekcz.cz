<?php

namespace App\Model\Search;

class Page extends ModuleAbstract {

    public function init()
    {
        $this->_mainRepository = $this->_context->getService('cmsPage');
    }

    public function search($queryString)
    {
        $queryString = '%'.addslashes($queryString).'%';

        $condition = 'delete = 0 AND view = 1 AND (name LIKE ? OR heading LIKE ? OR annotation LIKE ? OR text LIKE ?)';

        $dbSelection = $this->_mainRepository->getTable()->where($condition, $queryString, $queryString, $queryString, $queryString);

        return $this->prepareReturnData('Stránky', $dbSelection);
    }

}