<?php
namespace Control\Def;

use Control;

class DefControl extends Control\AbstractControl
{
    public function render()
    {
        $this->template->setFile(__DIR__.'/empty.latte');
        $this->template->render();
    }

} // end class