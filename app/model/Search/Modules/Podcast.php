<?php

namespace App\Model\Search;

class Podcast extends ModuleAbstract {

    public function init()
    {
        $this->_mainRepository = $this->_context->getService('cmsPodcast');
    }

    public function search($queryString)
    {
        $queryString = '%'.addslashes($queryString).'%';

        $condition = 'delete = 0 AND view = 1 AND (name LIKE ? OR text LIKE ?)';

        $dbSelection = $this->_mainRepository->getTable()->where($condition, $queryString, $queryString);

        return $this->prepareReturnData('Podcasty', $dbSelection);
    }

}