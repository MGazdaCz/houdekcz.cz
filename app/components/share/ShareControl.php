<?php
namespace App\Control\Share;

use Nette\Application\UI\Control;

class ShareControl extends Control
{
    public function render($pageUrl)
    {
        $this->template->pageUrl = $pageUrl;
        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

}