<?php

namespace App\Forms;

use App\Model\Repository\User;
use Nette,
	Nette\Application\UI\Form;


class LoginFormFactory extends Nette\Object
{
    /** @var User */
    private $user;


	public function __construct(Nette\Security\User $user)
	{
        $this->user = $user;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new PForm();
        $form
            ->addHidden('backUrl');
        $form
            ->addText('username', null, 50)
            ->setRequired('Vyplňte vaši emailovou adresu.')
            ->setAttribute('placeholder', 'Emailová adresa');
        $form
            ->addPassword('password', null)
            ->setRequired('Vyplňte heslo.')
            ->setAttribute('placeholder', 'Heslo');
        $form
            ->addSubmit('submit', 'Přihlásit se')
            ->setAttribute('class', 'btn size-2 green');

		//$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}

}
