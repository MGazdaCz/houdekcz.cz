<?php
namespace App\AdminModule\CoreModule\Model\Repository;
use Nette;

/**
 * Provádí operace nad databázovou tabulkou.
 */
abstract class Repository extends Nette\Object
{
    protected $_table;
    
    /** @var Nette\Database\Context */
    protected $_dbContext;

    public function __construct(Nette\Database\Context $db)
    {
        $this->_dbContext = $db;
    }

    /**
     * Vrací objekt reprezentující databázovou tabulku.
     * @return \Nette\Database\Table\Selection
     */
    public function getTable()
    {
        if (empty($this->_table)) {
            // název tabulky odvodíme z názvu třídy
            preg_match('#(\w+)Repository$#', get_class($this), $m);
            
            if (isset($m[1]) && !empty($m[1])) {
                $this->_table = $m[1];
            }
            else {
                // moznast parsovat tridy bez postfixu repository
                preg_match('#(\w+)$#', get_class($this), $m);
                
                if (isset($m[1]) && !empty($m[1]))
                    $this->_table = $m[1];
            } // end if
        } // end if
        
        return $this->_dbContext->table(lcfirst($this->_table));
    }

    /**
     * Metoda vrati nazev DB tabulky.
     *
     * @return string
     */
    public function getTableName(){
        return $this->_table;
    }

    /**
     * Metoda vrati DB context.
     *
     * @return Nette\Database\Context
     */
    public function getDbContext(){
        return $this->_dbContext;
    }

    /**
     * Vrací všechny řádky z tabulky.
     * @return Nette\Database\Table\Selection
     */
    public function findAll()
    {
        return $this->getTable();
    }

    /**
     * Vrací řádky podle filtru, např. array('name' => 'John').
     * @return Nette\Database\Table\Selection
     */
    public function findBy(array $by)
    {
        return $this->getTable()->where($by);
    }

    /**
    * put your comment there...
    * 
    * @param mixed $pk
    * @return \Nette\Database\Table\Selection
    */
    public function findByPk($pk)
    {
        $rows = $this->getTable()->wherePrimary($pk);
        if (isset($rows[$pk]))
            return $rows[$pk];
        return null;
    } // end function

    public function insert($data)
    {
        return $this->getTable()->insert($data);
    }

    public function update($data, array $where)
    {
        return $this->getTable()->where($where)->update($data);
    }

    public function delete(array $where)
    {
        return $this->getTable()->where($where)->delete();
    }

    /**
    * Metoda nacte a vrati sparovane hodnoty pro select.
    * 
    * @author MiG
    * @param string
    * @param string
    * @param bool
    * @return array
    */
    function fetchPairs($columnKey, $columnName, $nullValue = false) {
        $pairs = $this->getTable()->fetchPairs($columnKey, $columnName);
        
        if ($nullValue)
        {
            $tmpPairs = array(0 => ' - ');
            
            foreach ($pairs as $key => $value) {
                $tmpPairs[$key] = $value;
            } // end foreach
            
            return $tmpPairs;
        } // end if
        
        return $pairs;
    } // end function

    /**
     * Metoda nacte a vrati sparovane hodnoty pro select.
     *
     * @author MiG
     * @param string
     * @param string
     * @param array
     * @param bool
     * @return array
     */
    function fetchPairsWithWhere($columnKey, $columnsName, array $where, $nullValue = false, $delimiter = ' ') {
        $tmpPairs = array();
        $rows     = $this->getTable()->where($where);

        if ($nullValue) {
            $tmpPairs = array(0 => ' - ');
        }

        $columnsArray = explode($delimiter, $columnsName);

        foreach ($rows as $row) {
            $valueArray = array();

            foreach ($columnsArray as $column) {
                $valueArray[] = $row->$column;
            }

            $tmpPairs[$row->$columnKey] = implode($delimiter, $valueArray);
        } // end foreach

        return $tmpPairs;
    } // end function

    /**
     * Metoda nacte a vrati sparovane hodnoty pro select.
     *
     * @author MiG
     * @param string
     * @param string
     * @param array
     * @param string
     * @param bool
     * @return array
     */
    function fetchPairsWithWhereAndOrder($columnKey, $columnsName, array $where, $order, $nullValue = false, $delimiter = ' ') {
        $tmpPairs = array();
        $rows     = $this->getTable()->where($where)->order($order);

        if ($nullValue) {
            $tmpPairs = array(0 => ' - ');
        }

        $columnsArray = explode($delimiter, $columnsName);

        foreach ($rows as $row) {
            $valueArray = array();

            foreach ($columnsArray as $column) {
                $valueArray[] = $row->$column;
            }

            $tmpPairs[$row->$columnKey] = implode($delimiter, $valueArray);
        } // end foreach

        return $tmpPairs;
    } // end function

    /**
     * Metoda spocita pocet vysledku dotazu.
    * 
    * @author MiG
    * @param string where
    * @return integer
    */
    function count($where) {
        return $this->getTable()->where($where)->count('id');
    } // end function
    
}