<?php
namespace FrontModule\Control\Slider\TinyCarousel;

use \FrontModule\Control\Slider\SliderControlAbstract;

class TinyCarouselControl extends SliderControlAbstract
{
    public function render(array $options)
    {
        parent::_initOptions($options);

        // ziskani mutace do session
        $feLanguage = $this->parent->getSession('feLanguage');

        $lang = 'cz';
        if (!is_null($feLanguage->id))
            $lang = $feLanguage->id;

        $this->template->language = $lang;
        $this->template->items    = $this->_items;
        $this->template->setFile(__DIR__.'/default.latte');
        $this->template->render();
    }

}