<?php

namespace App\Presenters;
use App\Model\Search\EshopCategory;

/**
 * Search presenter.
 */
class SearchPresenter extends BasePresenter
{

    protected function startup()
    {
        parent::startup();
        //$this->template->registerHelper('textbox', 'TextboxHelper::textbox');
        $this->template->bodyClass = 'search';
    }

	public function renderDefault()
	{
        $queryString = $this->getParameter('q');

        $this->template->title       = 'Vyhledávání výrazu: '.$queryString;
        $this->template->description = '';
        $this->template->keywords    = '';
        $this->template->results     = array();

        if (!empty($queryString)) {
            $searchedModule = array(
                'Category',
                'Product',
                'Page',
                //'Podcast'
            );

            foreach ($searchedModule as $module) {
                $className = 'App\Model\Search\\'.$module;

                if (class_exists($className)) {
                    $oModule = new $className;
                    $oModule->setContext($this->context);
                    $oModule->init();

                    $this->template->results[$module] = $oModule->search($queryString);
                }
            }
        } else {
            $this->flashMessage('Hledaný výraz nesmí být prázdný.');
        }
    }
    
}
