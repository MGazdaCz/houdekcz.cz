<?php

namespace App\AdminModule\ErpModule\Model;
use Nette\Database\Connection;

/**
 * Created by PhpStorm.
 * User: Milan
 * Date: 15.5.15
 * Time: 14:01
 */
class ProjectManager {

    /**
     * Klic k session, kde jsou ulozena data o vybranem projektu.
     */
    const SESSION_SECTION_KEY = 'erpProject';

    /**
     * ID vybraneho projektu
     *
     * @var int
     */
    private $_projectId;

    /**
     * Detail o projektu nacteny z DB.
     *
     * @var
     */
    private $_projectDetail;

    /**
     * @var
     */
    private $_dbConnection;
    /**
     *
     * @var \Nette\Http\Session
     */
    private $_session;
    /**
     *
     * @var \Nette\DI\Container
     */
    private $_context;

    /**
     * Konstruktor tridy provede inicializaci potrebnych dat.
     *
     * @param \Nette\Http\Session $session
     * @param \Nette\DI\Container
     */
    public function __construct(\Nette\Http\Session $session, \Nette\DI\Container $context) {
        $this->_session      = $session;
        $this->_dbConnection = null;
        $this->_context      = $context;

        // nacteni dat ze session
        if (isset($this->_session->getSection(self::SESSION_SECTION_KEY)->id)) {
            $this->_projectId = $this->_session->getSection(self::SESSION_SECTION_KEY)->id;
        }
    }

    /**
     * Metoda vrati ID vybraneho projektu
     *
     * @return int
     */
    public function getProjectId(){
        return $this->_projectId;
    }

    /**
     * Metoda nacte a vrati detail o projektu.
     *
     * @return mixed
     */
    public function getProjectDetail(){
        if (is_null($this->_projectDetail)) {
            $this->_projectDetail = $this->_context->erpProject->findByPk($this->getProjectId());

            if (is_null($this->_projectDetail)) {
                throw new \Exception('Chyba, projekt nenalezen! (id: '.$this->getProjectId().')');
            }
        }
        return $this->_projectDetail;
    }

    /**
     * Metoda nastavi project ID.
     * @param $projectId
     */
    public function setProjectId($projectId){
        $this->_projectId = $projectId;

        // ulozeni dat do session
        $this->_saveSettingsToSession();
    }

    /**
     * Metoda provede ulozeni dat do session.
     */
    protected  function _saveSettingsToSession(){
        $this->_session->getSection(self::SESSION_SECTION_KEY)->id = $this->_projectId;
    }

    public function getDbConnection(){
        if (is_null($this->_dbConnection)) {
            $pDetail = $this->getProjectDetail();

            /*
            echo '<pre>'.print_r($this->getProjectDetail()->toArray(), true).'</pre>';
            die;
            */

            $this->_dbConnection = new Connection('mysql:host='.$pDetail->dbServer.';dbname='.$pDetail->dbName, $pDetail->dbUser, $pDetail->dbPassword);
        }
        return $this->_dbConnection;
    }

}