/**
 * Created by Milan on 15.5.15.
 */
$(document).ready(function(){

    // defaultni skryti anotace
    $("#page-annotation").hide();
    $("#page-annotation").removeClass("hidden");

    // kliknuti na tlacitko pro zobrazeni anotace
    $("a.show-annotation").click(function(){

        var configString = $(this).attr("data-wr-config");
        var config       = JSON.parse(configString);

        //console.log(config.titleShow);

        if ($("#page-annotation").is(":visible")) {
            $(this).html(config.titleShow);
        } else {
            $(this).html(config.titleHide);
        }

        //$("#page-annotation").removeClass("hidden");
        $("#page-annotation").toggle();
        return false;
    });

    // pokud se zmeni nazev stranky a jsou prazdna dalsi policka, dovyplnim je
    $("#frm-form-name").change(function(){
        var pageName = $(this).val();

        if ($("#frm-form-heading").length && $("#frm-form-heading").val().length == 0) {
            $("#frm-form-heading").val(pageName);
        }

        if ($("#frm-form-title").length && $("#frm-form-title").val().length == 0) {
            $("#frm-form-title").val(pageName);
        }

        if ($("#frm-form-keywords").length && $("#frm-form-keywords").val().length == 0) {
            $("#frm-form-keywords").val(pageName);
        }

        if ($("#frm-form-description").length && $("#frm-form-description").val().length == 0) {
            $("#frm-form-description").val(pageName);
        }
        
        if ($("#frm-form-url").length && $("#frm-form-url").val().length == 0) {
            $("#frm-form-url").val("/" + wrWebalize(pageName));
        }

        console.log(pageName);
    });

});