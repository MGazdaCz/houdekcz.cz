<?php
namespace App\AdminModule\CoreModule\Model\Repository;
use Nette;

class CoreUserwidget extends Repository
{
    protected $_table = 'core_userwidget';

    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function saveWidgets($userId, array $widgets) {
        foreach ($widgets as $widgetId) {
            $this->insert(array(
                'User_id' => intval($userId),
                'widget'  => $widgetId
            ));
        } // end foreach
        return $this;
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param string $userId
    * @return void
    */
    function fetchAllForUser($userId, $onlyIds = false) {
        $rows = $this->findBy(array('User_id' => $userId));
        
        if ($onlyIds)
        {
            $data = array();
            
            foreach ($rows as $row) {
                $data[] = $row->widget;
            } // end foreach
            
            return $data;
        } // end if
        
        return $rows;
    } // end function
    
} // end class