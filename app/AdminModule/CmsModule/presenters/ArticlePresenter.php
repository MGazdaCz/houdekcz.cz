<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use App\AdminModule\CoreModule\Model\Forms\ManyToMany;
use \Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

/**
 * Article presenter.
 */
class ArticlePresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\ArticleRelated
     */
    private $_articleRelatedRepo;

    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\ArticleLabel
     */
    private $_articleLabelRepo;

    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\ArticleLabels
     */
    private $_articleLabelsRepo;

    /**
     * @var \App\AdminModule\CoreModule\Model\Forms\ManyToMany
     */
    private $_mnArticleRelated;
    /**
     * @var \App\AdminModule\CoreModule\Model\Forms\ManyToMany
     */
    private $_mnArticleLabel;

    /**
     * @var \App\AdminModule\CmsModule\Forms\ArticleFormFactory @inject
     */
    public $articleForm;


    protected function startup(){
        parent::startup();

        // konfigurace repositaru
        $this->_mainRepository         = $this->context->getService('cmsArticle');
        $this->_articleRelatedRepo     = $this->context->getService('cmsArticleRelated');
        $this->_articleLabelsRepo      = $this->context->getService('cmsArticleLabels');
        $this->_articleLabelRepo       = $this->context->getService('cmsArticleLabel');

        // sestaveni MkuN vazby na clanky - souvisejici
        $this->_mnArticleRelated = new ManyToMany($this->_articleRelatedRepo, $this->_mainRepository);
        $this->_mnArticleRelated->setMColumn('cms_article_id');
        $this->_mnArticleRelated->setNColumn('related_id');

        // sestaveni MkuN vazby na stitky
        $this->_mnArticleLabel = new ManyToMany($this->_articleLabelsRepo, $this->_mainRepository, $this->_articleLabelRepo);
        $this->_mnArticleLabel->setMColumn('cms_article_id');
        $this->_mnArticleLabel->setNColumn('cms_article_label_id');

        // defaultni nastaveni promenny BE controlleru
        $this->_listH1       = 'Články';
        $this->_orderDefault = 'id DESC';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Article/form.latte';
    }

    protected function createComponentForm($name)
    {
        // zalozeni formulare
        $form = $this->articleForm->create();
        $this->articleForm->setPresenter($this);

        // nastaveni defaultnich dat do formulare
        if ($this->_itemId > 0)
        {
            $page = $this->_mainRepository->findByPk($this->_itemId);
            $page = $page->toArray();

            if (!empty($page['datePublication'])) $page['datePublication'] = $page['datePublication']->format('d.m.Y');
            if (!empty($page['dateFrom']))        $page['dateFrom']        = $page['dateFrom']->format('d.m.Y');
            if (!empty($page['dateTo']))          $page['dateTo']          = $page['dateTo']->format('d.m.Y');

            // sestaveni related do pole a vlozeni do formulare
            $page['related'] = $this->_mnArticleRelated->getRelatedForColumnKeyAndValue($this->_itemId);
            $page['labels'] = $this->_mnArticleLabel->getRelatedForColumnKeyAndValue($this->_itemId);
            // ---

            $form->setDefaults($page);
        }

        $form->onSuccess[] = array($this->articleForm, 'formSucceeded');

        return $form;
    }

    protected function _beforeSave($id, &$values){
        // "validace hodnot" pro ulozeni do DB
        if (empty($values['cms_photogallery_id'])) {
            $values['cms_photogallery_id'] = null;
        }

        if (empty($values['cms_podcast_id'])) {
            $values['cms_podcast_id'] = null;
        }

        if (empty($values['heading'])) {
            $values['heading'] = $values['name'];
        }

        if (empty($values['title'])) {
            $values['title'] = $values['name'];
        }

        if (empty($values['url'])) {
            $values['url'] = '/'.Strings::webalize($values['heading']);
        }

        // nulovani prazdnych datumu
        if (empty($values['datePublication'])) {
            $values['datePublication'] = null;
        } else {
            $values['datePublication'] = DateTime::from($values['datePublication'])->format('Y-m-d');
        }
        if (empty($values['dateFrom'])) {
            $values['dateFrom'] = null;
        }
        if (empty($values['dateTo'])) {
            $values['dateTo'] = null;
        }

        // prevzeti related clanku do "mezipameti"
        $this->_mnArticleRelated->setRelations($values['related']);
        unset($values['related']);

        // prevzeti stitku do "mezipameti"
        $this->_mnArticleLabel->setRelations($values['labels']);
        unset($values['labels']);

        // osetreni uploadu obrazku
        if ($values['mainImage']->isOk()) {
            $image = $values['mainImage']->toImage();

            $fileNameArray = explode('.', $values['mainImage']->getName());

            $filename = time();
            if (!empty($fileNameArray)) {
                $filename .= '.'.$fileNameArray[count($fileNameArray) - 1];
            }

            try {
                $values['mainImage']->move(DATA_DIR . "/articles/$filename");

                if ($image) {
                    $image->resize(120,90);
                    $image->save(DATA_DIR . "/articles/preview/$filename");
                }
            } catch (\Exception $e) {
                $this->flashMessage($e->getMessage(), 'error');
            }

            $values['mainImage'] = $filename;
        } else {
            unset($values['mainImage']);
        }
    }

    protected function _afterSave($id, $values){

        // ulozeni related vazeb do DB
        $this->_mnArticleRelated->saveRelations($id);
        $this->_mnArticleLabel->saveRelations($id);

    }

}
