<?php
namespace App\Control\Mainmenu;

use Control;

class MainmenuControl extends Control\AbstractControl
{
    private $_menuitemRepository;

    public function render()
    {
        // ziskani mutace do session
        $feLanguage     = $this->parent->getSession('feLanguage');

        $lang = 'cz';
        /*
        if (!is_null($feLanguage->id))
            $lang = $feLanguage->id;
        */

        $this->_menuitemRepository  = $this->parent->context->getService('cmsMenuItem');
        $this->template->url        = $this->parent->context->parameters['web']['url'];
        $this->template->activeItem = (!is_null($this->parent->getPage()) ? $this->parent->getPage()->id : null);
        $this->template->items      = $this->_menuitemRepository->findAll()->where("view = 1 AND core_language_id = ?", $lang)->order('_order');
        
        $this->template->setFile(__DIR__.'/default.latte');
        $this->template->render();
    }

} // end class