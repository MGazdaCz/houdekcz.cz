<?php

namespace App\EshopModule\Model;
use Nette\Database\Table\ActiveRow;

/**
 * Zakladni trida produktu
 */
class Product
{
    const PRICE_PRECISSION = 2;

    protected $_id;
    protected $_name;
    protected $_view;
    protected $_delete;
    protected $_url;
    protected $_price;
    protected $_pricePerItem;
    protected $_vat;
    protected $_priceWithVat;
    protected $_unit = 'ks';

    /**
     * Metoda zalozi produkt z predaneho active row.
     *
     * @param ActiveRow $row
     * @return \App\Model\EshopProduct
     */
    public static function getFromActiveRow(ActiveRow $row){
        $product = new self;
        $product->initFromActiveRow($row);

        return $product;
    }

    /**
     * Metoda inicializuje vlastnosti z active row.
     *
     * @param ActiveRow $row
     */
    public function initFromActiveRow(ActiveRow $row){
        $this->_id = $row->id;
        $this->_name = $row->name;
        $this->_view = $row->view;
        $this->_delete = $row->delete;
        $this->_url = $row->url;
        $this->_price = $row->price;
        $this->_pricePerItem = $row->pricePerItem;
        $this->_vat = $row->vat;
        $this->_priceWithVat = $row->priceWithVat;

        if ($row->eshop_cis_unit_id) {
            $this->_unit = $row->eshop_cis_unit->name;
        }
    }

    public function __get($attribute){
        $attrWithSuffix = '_'.$attribute;
        if (isset($this->$attrWithSuffix)) {
            return $this->$attrWithSuffix;
        }
        return null;
    }

    /**
     * Metoda vrati cenu produktu s DPH.
     *
     * @return float
     */
    public function getPriceWithVat() {
        if ($this->_priceWithVat) {
            return round($this->_price, self::PRICE_PRECISSION);
        }

        return round($this->_price * ((100 + $this->_vat) / 100), self::PRICE_PRECISSION);
    }

    /**
     * Metoda vrati centu produktu bez DPH.
     *
     * @return float
     */
    public function getPriceWithoutVat() {
        if ($this->_priceWithVat) {
            return round($this->_price / ((100 + $this->_vat) / 100), self::PRICE_PRECISSION);
        }

        return round($this->_price, self::PRICE_PRECISSION);
    }

    public function getPricePerItemWithVat() {
        if ($this->_pricePerItem > 0) {
            if ($this->_priceWithVat) {
                return round($this->_pricePerItem / ((100 + $this->_vat) / 100), self::PRICE_PRECISSION);
            }

            return round($this->_pricePerItem, self::PRICE_PRECISSION);
        }

        return $this->getPriceWithVat();
    }

    public function getPricePerItemWithoutVat(){
        if ($this->_pricePerItem > 0) {
            if ($this->_priceWithVat) {
                return round($this->_pricePerItem / ((100 + $this->_vat) / 100), self::PRICE_PRECISSION);
            }

            return round($this->_pricePerItem, self::PRICE_PRECISSION);
        }

        return $this->getPriceWithoutVat();
    }

    /**
     * Metoda vrati cenu s DPH.
     *
     * @return float
     */
    public function getVat() {
        return $this->_vat;
    }

    /**
     * Metoda vrati hodnotu DPH.
     *
     * @return float
     */
    public function getPriceVat() {
        return $this->getPriceWithVat() - $this->getPriceWithoutVat();
    }

    public static function getStockFromActiveRow(ActiveRow $productActiveRow){
        $stock    = $productActiveRow->stock;
        $variants = $productActiveRow->related('eshop_product_variant.eshop_product_id')->where(array('view' => 1));
        if ($variants->count() > 0) {
            $stock = 0;
            foreach ($variants as $variant) {
                $stock += $variant->stock;
            }
        }
        return $stock;
    }

    /**
     * Metoda vrati pocet desetinnych mist, na ktere se cena zaokrouhluje.
     *
     * @return int
     */
    public function getPricePrecission(){
        return self::PRICE_PRECISSION;
    }

    /**
     * Metoda vrati jednotku produktu.
     *
     * @return string
     */
    public function getUnit(){
        return $this->_unit;
    }

}