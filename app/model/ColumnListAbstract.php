<?php

namespace App\Model;

use Nette\DI\Container;

abstract class ColumnListAbstract {

    const WIDTH_MOBILE = 767;
    const WIDTH_TABLET = 1023;

    /**
     * @var \Nette\Security\User
     */
    protected $_user;


    /**
     * Metoda prerozdeli data do sloupcu.
     *
     * @param Container $context
     * @param int $pageWidth
     */
    public abstract function getColumnsData(Container $context, $pageWidth, $pageNumber = 1);

    /**
     * Metoda vrati pocet sloupcu stranky.
     *
     * @param int $pageWidth
     * @return int
     */
    public function getColumnCount($pageWidth){
        $columnCount = 3;
        if ($pageWidth <= self::WIDTH_MOBILE) {
            $columnCount = 1;
        } elseif ($pageWidth <= self::WIDTH_TABLET) {
            $columnCount = 2;
        }

        return $columnCount;
    }

    /**
     * Metoda vrati inicializovane navratove pole.
     *
     * @param int $columnCount
     * @return array
     */
    public function getReturnArray($columnCount){
        $returnData = array();
        for ($i = 0; $i < $columnCount; $i++) {
            $returnData[$i] = array();
        }
        return $returnData;
    }

    /**
     * @param \Nette\Security\User $user
     */
    public function setUser($user){
        $this->_user = $user;
    }

}