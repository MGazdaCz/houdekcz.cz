<?php

namespace App\Forms;

use App\Model\Repository\User;
use Nette,
	Nette\Application\UI\Form;


class RegisterNewsletterFactory extends Nette\Object
{
	public function __construct()
	{
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new PForm();
        $form
            ->addCheckbox('magazine', 'Magazín')
            ->setDefaultValue(true);
        $form
            ->addCheckbox('podcast', 'Podcasty')
            ->setDefaultValue(true);
        $form
            ->addCheckbox('actions', 'Akce')
            ->setDefaultValue(true);
        $form
            ->addText('email', null, 50)
            ->addRule(Form::EMAIL, 'Zadejte email ve správném tvaru.')
            ->setAttribute('placeholder', 'zadejte@email.cz');
        $form
            ->addSubmit('submit', 'OK');
            //->setAttribute('class', 'btn size-2 green');

		//$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}

}
