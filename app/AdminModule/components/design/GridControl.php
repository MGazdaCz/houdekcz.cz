<?php
namespace App\AdminModule\Control\Design;

use \Nette\Application\UI\Control;

class GridControl extends Control
{
    public function renderStart($width)
    {
        echo '<div class="col-sm-'.intval($width).'">'."\n";
    } // end function
    
    public function renderEnd()
    {
        echo '</div>'."\n";
    } // end function
} // end class