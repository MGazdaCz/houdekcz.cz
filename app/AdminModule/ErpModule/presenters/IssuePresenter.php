<?php

namespace App\AdminModule\ErpModule;

use App\AdminModule\ErpModule\Control\Issue\ListControl;
use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;

/**
 * Guestbook presenter.
 */
class IssuePresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    private $_issueStatusRepository;
    private $_userRepository;
    private $_projectRepository;

    protected function startup(){
        parent::startup();

        $this->_mainRepository        = $this->context->erpissueRepository;
        $this->_issueStatusRepository = $this->context->erpissuestatusRepository;
        $this->_userRepository        = $this->context->userRepository;
        $this->_projectRepository     = $this->context->projectRepository;

        $this->_listH1       = 'Úkoly';
        $this->_orderDefault = 'deadline';

        $this->template->issueRepository = $this->_mainRepository;
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare

        // nacteni projektu
        $projectsArray = $this->_projectRepository->getTable()->order('id')->fetchPairs('id', 'name');
        $projectsArray = array(null => ' - ') + $projectsArray;

        // nacteni stavu issue
        $issueStatusArray = $this->_issueStatusRepository->getTable()->order('id')->fetchPairs('id', 'name');
        $issueStatusArray = array(null => ' - ') + $issueStatusArray;

        // nacteni uzivatelu
        $usersArray = $this->_userRepository->getTable()->where("surname <> ''")->order('id')->fetchPairs('id', 'surname');
        $usersArray = array(null => ' - ') + $usersArray;

        $form->addGroup('Základní informace');
        $form
            ->addSelect('rel_project', 'Nadřízený projekt', $projectsArray)
            ->setAttribute('class', 'width-50')
            ->setRequired('Projekt musí být nastaven.');
        $form
            ->addText('name', 'Název úkolu')->setAttribute('class', 'width-50')
            ->addRule(Form::MIN_LENGTH, 'Minimální délka názvu úkolů je 5 znaků.', 5);
        //$form->addSelect('parent', 'Nadřízený úkol', $projectsArray)->setAttribute('class', 'width-50');
        $form
            ->addSelect('rel_reporter', 'Založil', $usersArray)
            ->setAttribute('class', 'width-50')
            ->setRequired('Reportér úkolu musí být nastaven.');
        $form
            ->addSelect('rel_assigned', 'Přiřazeno', $usersArray)
            ->setAttribute('class', 'width-50');
        $form
            ->addText('deadline', 'Termín dokončení')
            ->setAttribute('class', 'date-picker')
            ->setRequired('Termín dokončení úkolu musí být nastaven.');
        $form
            ->addText('time', 'Časový odhad')
            ->addRule(Form::NUMERIC, 'Časový odhad musí být číslo.');
        $form
            ->addSelect('rel_issuestatus', 'Stav úkolu', $issueStatusArray)
            ->setAttribute('class', 'width-50')
            ->setRequired('Stav úkolu musí být nastaven.');
        $form->addSubmit('submit', 'Uložit');

        $form->addGroup('Rozšířené informace');
        $form
            ->addTextArea('description', 'Popis', 75, 10)
            ->setAttribute('class', 'ckeditor')
            ->setRequired('Popis úkolu (zadání) musí být nastaven.');
        $form->addTextArea('analysis', 'Analýza', 75, 10)->setAttribute('class', 'ckeditor');

        $form->addSubmit('submit2', 'Uložit');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted())
        {
            if ($this->_itemId > 0)
            {
                $item = $this->_mainRepository->findByPk($this->_itemId);
                $item = $item->toArray();

                // osetreni formatu datumu
                $date = new \Nette\DateTime($item['deadline']);
                $item['deadline'] = $date->format('d.m.Y');
                // ---

                $form->setDefaults($item);
            }
            else
            {
                $form->setDefaults(array(
                    'time'            => 0.0,
                    'rel_reporter'    => $this->getUser()->id,
                    'rel_issuestatus' => 10,
                    'rel_project'     => $this->getParameter('project')
                ));
            } // end if
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni dat
        $values = (array)$form->values;

        if (empty($values['parent']))       $values['parent']       = null;
        if (empty($values['rel_reporter'])) $values['rel_reporter'] = null;
        if (empty($values['rel_assigned'])) $values['rel_assigned'] = null;

        $deadline = new \Nette\DateTime($values['deadline']);
        $values['deadline'] = $deadline->format('Y-m-d H:i:s');
        // ---

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['insertDate'] = date('Y-m-d H:i:s');

            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    public function renderDefault()
    {
        if (empty($this->_orderDefault))
            throw new \Exception('Chyba - zadejte v aktualnim presenteru  defaultni order v atributu "_orderDefault".');

        $this->template->h1 = (empty($this->_listH1) ? 'CHYBA - nastavte atribut "_listH1"' : $this->_listH1);
        $this->template->items = array();

        // podpora pro razeni + nacteni dat z repository
        $sortColumn = $this->getParameter('sort', $this->_orderDefault);

        // nacteni polozek
        $this->template->mainItemsActual   = $this->_mainRepository->getTable()->where('rel_assigned = '.$this->getUser()->id.' AND rel_issuestatus IN (10,20,50)')->order($sortColumn);
        $this->template->mainItemsSleeping = $this->_mainRepository->getTable()->where('rel_assigned = '.$this->getUser()->id.' AND rel_issuestatus IN (70)')->order($sortColumn);
        $this->template->itemsNotAssigned  = $this->_mainRepository->getTable()->where('rel_assigned IS NULL')->order($sortColumn);
        $this->template->itemsAll          = $this->_mainRepository->getTable()->order($sortColumn);
    } // end function

    public function renderPreview($id)
    {
        $this->_itemId = $id;
        if ($this->_itemId > 0)
        {
            $project = $this->_projectRepository->findByPk($this->_itemId);
            $this->template->item = $project;

            $this->template->h1 = 'Projekt: '.$project->name;
        } // end if
    } // end function

    public function renderDelete($id){
        $this->_projectRepository->delete(array('id' => $id));
        return $this->redirect('default');
    }

    /**
     * Registrace komponenty pro vypis
     *
     * @return \AdminModule\Control\Filter\FilterControl
     */
    protected function createComponentList()
    {
        $filter = new ListControl();

        return $filter;
    }

}
