$(document).ready(function () {

    // init tooltipu
    //$('.tool-tip').tooltip();

    // implementace datepickeru
    //$(".date-picker").datepicker();
    
    // scroll to effect
    $("#slider-button a, #footer-middle a").click(function() {
        $('html, body').animate({
            scrollTop: $("#content-right").offset().top
        }, 500);
        
        return false;
    });

    // implementace scrollTo
    $("a[href^='#']").click(function(){
        var url = $(this).attr("href");

        if (url == "#") {
            $.scrollTo(0, 700);
        } else {
            $.scrollTo($(this).attr("href"), 700);
        }

        // pokud jsem klikl na polozku v mobilnim menu, musim menicko po kliknuti zavrit
        if ($("#menu").hasClass("mobile-menu")) {
            setTimeout(function(){
                $("#menu").toggle();
            }, 200);
        }
        // vyjimka pro "tlaco" v pokladne
        if ($("#zobrazFakturacni a")) {
            return true;
        }

        return false;
    });
    
    // otevreni odkazu do noveho okna
    $("a.new_window").each(function(){
        $(this).attr("target", "_blank");
    });

    $("#header button.btn").click(function(){
        $("#menu").addClass("mobile-menu");
        $("#menu").toggle();
    });

    /*
    // init filtracniho formu v magazinu
    var filterForm = $("body.b-magazine form.ajax");
    if (filterForm.length) {
        var action = filterForm.attr("action") + "?width=" + $("body").width();
        filterForm.attr("action", action);
    }
    $("body.b-magazine form.ajax input.checkbox").on("click", function () {
        $(this).parents("form").ajaxSubmit();
    });
    $("body.b-magazine form.ajax select").on("change", function () {
        $(this).parents("form").ajaxSubmit();
    });
    */

    // implementace dropdown menu
    $("div.menu .dropdown").hide();
    $("div.menu .dropdown").removeClass("hidden");
    $("div.menu li").hover(function(){
        var dropdown = $(this).children(".dropdown");

        if (dropdown.length) {
            dropdown.show();
        }
    }, function(){
        var dropdown = $(this).children(".dropdown");

        if (dropdown.length) {
            dropdown.hide();
        }
    });
    // ---
});


// burger menu
$(function() {
    var pull        = $('.burger');
        menu        = $('nav#menu');
        menuHeight  = menu.height();

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 320 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});


// popup image
$(document).ready(function() {
	$('.photos').magnificPopup({
		delegate: 'a.a-photo',
		type: 'image',
		tLoading: 'Načítám obrázek #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">Obrázek #%curr%</a> se nepodařilo načíst.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});
});
