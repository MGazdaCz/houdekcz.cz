<?php

namespace App\EshopModule\Presenters;

use App\EshopModule\Control\Category\MenuControl;
use App\EshopModule\Control\Product\BreadCrumbControl;
use Nette;


/**
 * ESHOP BASE presenter.
 */
class BasePresenter extends \App\Presenters\BasePresenter
{
    /**
     * Atribut kosiku.
     *
     * @var \App\EshopModule\Model\Cart
     */
    protected $_cart;

    protected function startup(){
        parent::startup();

        // nastaveni sluzby cart
        $this->_cart = $this->context->getService('cart');

        $this->template->title       = 'Eshop | Houdekcz.cz';
        $this->template->description = '';
        $this->template->keywords    = '';
        $this->template->cart        = $this->_cart;

        $this->template->bodyClass = 'eshop';
    }

    /*********************************************
     *      Registrace komponent
     ********************************************/

    protected function createComponentProductBoxControl() {
        return new \App\EshopModule\Control\Product\BoxControl();
    }

    public function createComponentCategoryMenu(){
        return new MenuControl();
    }

    public function createComponentBreadCrumb(){
        return new BreadCrumbControl();
    }

}
