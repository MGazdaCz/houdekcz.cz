$(document).ready(function(){

    $(".sortable").sortable({
        /**
        * Metoda odesle seznam ID polozek po dokonceni razeni.
        * 
        * @param event
        * @param ui
        */
        stop: function( event, ui ) {
            var config = $(this).attr("data-config");
            var eIds   = $(this).find("input.id");
            var ids    = {};
            
            if (eIds.length) {
                $.each(eIds, function( key, $element ) {
                    ids[key] = $element.value;
                });
            } // end if
            
            var jConfig = $.parseJSON(config);
            
            $.ajax(jConfig.sortableUrl, {
                async: true,
                data: {
                    "table":jConfig.table,
                    "ids":ids
                },
                success: function(data) {}
            });
        }
    });
    $(".sortable").disableSelection();
    
});