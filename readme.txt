HoudekCz.cz
===============

Implementace eshopu houdekcz.cz

Installing
----------

1. git clone
git clone https://MGazdaCz@bitbucket.org/MGazdaCz/houdekcz.cz.git

2. download vendor directory:
http://home.mgazda.cz/_vendor/vendor-2.3.0.zip

3. extract vendor-2.3.0.zip to /houdekcz.cz/vendor

4. create directory
/houdekcz.cz/log
/houdekcz.cz/temp

5. create database
houdekcz.cz

6. import DB struct from /houdekcz.cz/_sql


License
-------
- all rights reserved
- copyright: WeboveReseni.cz