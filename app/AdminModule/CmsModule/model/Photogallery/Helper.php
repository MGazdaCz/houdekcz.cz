<?php
namespace App\AdminModule\CmsModule\Model\Photogallery;

use App\AdminModule\CoreModule\Model\Forms\ImageUpload;
use Nette\Utils\Strings;

class Helper {

    /**
     * Metoda generuje z nazvu alias fotogalerie.
     *
     * @param string $text
     * @return string
     */
    public static function getPhotogalleryAlias($text){
        $photogalleryAlias = Strings::webalize($text);
        $photogalleryAlias = Strings::substring($photogalleryAlias, 0, 60);

        return $photogalleryAlias;
    }

    /**
     * Metoda generuje z "polozek" (uploadovanych souboru) fotogalerii.
     *
     * @param Context $context
     * @param string $photogalleryName
     * @param array $items
     * @return ActiveRow | null
     */
    public static function createGalleryFromFiles($context, $photoGalleryName, $items){
        if (!empty($items)) {
            $photogalleryAlias = self::getPhotogalleryAlias($photoGalleryName);

            $photogallery = $context->getService('cmsPhotogallery')->insert(array(
                'name'  => $photoGalleryName,
                'alias' => $photogalleryAlias,
                'view'  => 1
            ));

            if (is_object($photogallery) && $photogallery->id > 0) {
                // overeni existence adresare galerie
                $dirPath = WWW_DIR.'/data/photogallery/'.$photogalleryAlias;
                if (!empty($photogalleryAlias) && !is_dir($dirPath))
                {
                    if (!mkdir($dirPath))
                    {
                        throw new \Exception('Chyba při vytváření adresáře "'.$photogalleryAlias.'" (path: '.$dirPath.').');
                    } // end if
                } // end if

                // pokud je zalozena, uploaduju soubory
                foreach ($items as $fileUpload) {
                    $imageUpload = new ImageUpload($fileUpload);

                    $state = false;
                    try {
                        $imageUpload->generatePreviews(array(
                            'dir' => $dirPath,
                            'previews' => $context->parameters['admin']['cms']['photogallery']['previews']
                        ));

                        $state = true;
                    } catch (\Exception $e) {
                        $context->flashMessage($e->getMessage());
                    } // end try/catch

                    if ($state)
                    {
                        $context->getService('cmsPhotogalleryItem')->insert(array(
                            'file' => $imageUpload->getSanitizedName(),
                            'cms_photogallery_id' => $photogallery->id
                        ));
                    } // end if

                    unset($imageUpload);
                }

                return $photogallery;
            } else {
                throw new \Exception('Nepovedlo se založit novou fotogalerii. Zkuste to prosím později.');
            }
        }

        return null;
    }

}