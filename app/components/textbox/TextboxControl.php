<?php
namespace App\Control\Textbox;

use Control;

class TextboxControl extends Control\AbstractControl
{
    private $_textboxRepository;

    public function render()
    {
        $this->_textboxRepository = $this->getParent()->context->getService('cmsText');
        $this->template->textbox  = $this->_textboxRepository->findBy(array('view' => 1, 'id = '.$this->_id))->fetch();

        $this->template->setFile(__DIR__.'/textbox.latte');
        $this->template->render();
    }

    public function renderPure($id)
    {
        $this->_textboxRepository = $this->getParent()->context->getService('cmsText');
        $this->template->textbox  = $this->_textboxRepository->findBy(array('view' => 1, 'id = '.$id))->fetch();

        $this->template->setFile(__DIR__.'/textbox-pure.latte');
        $this->template->render();
    }

    public function renderClean($id) {
        $this->renderPure($id);
    }

    public function setId($id){
        $this->_id = intval($id);
    }

}