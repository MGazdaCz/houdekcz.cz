<?php
namespace App\AdminModule\Control\Widget\Base;

use \Nette\Application\UI\Control;

class BaseControl extends Control
{
    private $_head;
    private $_content;
    
    function setHead($head) {
        $this->_head = $head;
        return $this;
    }
    
    function setContent($content) {
        $this->_content = $content;
        return $this;
    }
    
    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/base.latte');

        // vložíme do šablony parametry
        $template->head    = $this->_head;
        $template->content = $this->_content;

        // a vykreslíme ji
        $template->render();
    } // end function
} // end class