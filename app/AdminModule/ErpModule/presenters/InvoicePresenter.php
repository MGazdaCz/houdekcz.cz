<?php

namespace App\AdminModule\ErpModule;

use Model\Form\FormEditorDetail;
use \Nette\Application\UI\Form;
use App\AdminModule\ErpModule\ProjectModule;
use Nette\DateTime;

/**
 * Faktury presenter.
 */
class InvoicePresenter extends \App\AdminModule\CoreModule\Presenters\BasePresenter
{
    private $_companyRepository;
    private $_supplierRepository;

    protected function startup(){
        parent::startup();
        $this->_mainRepository     = $this->context->invoiceRepository;
        $this->_companyRepository  = $this->context->companyRepository;
        $this->_supplierRepository = $this->context->supplierRepository;

        $this->_listH1 = 'Faktury';

        $this->_orderDefault = 'num';
    }

    /**
     * Definujeme komponentu pro editacni formualar.
     *
     * @param string $name
     * @return Form
     */
    protected function createComponentDefaultForm($name)
    {
        $form = new FormEditorDetail($this, $name);

        // custom renderovani formulare
        //echo var_dump($form->getRenderer()); die;

        $companyArray  = $this->_companyRepository->getTable()->order('name')->fetchPairs('id', 'name');
        $supplierArray = $this->_supplierRepository->getTable()->order('name')->fetchPairs('id', 'name');

        $form->addGroup('Základní informace');
        $form->addText('num', 'Číslo faktury');
        $form->addSelect('supplier', 'Dodavatel', $supplierArray);
        $form->addSelect('rel_company', 'Odběratel', $companyArray);

        $form->addGroup('Identifikace platby');
        $form->addText('variableSymbol', 'Variabilní symbol');
        $form->addText('constantSymbol', 'Konstantní symbol');
        $form->addText('specificSymbol', 'Specifický symbol');

        $form->addGroup('Platební podmínky');
        $form->addText('issueDate', 'Datum vystavení')->setAttribute('class', 'date-picker');
        $form->addText('dueDate',   'Datum splatnosti')->setAttribute('class', 'date-picker');

        $form->addTextArea('note', 'Poznámky', 75, 10);

        $form->addSubmit('submit', 'Uložit')->setAttribute('class', 'btn btn-sm btn-success');

        // nastaveni defaultnich dat do formulare
        if (!$form->isSubmitted() && $this->_itemId > 0)
        {
            $item = $this->_mainRepository->findByPk($this->_itemId);
            $itemArray = $item->toArray();

            // osetreni formatu datumu
            if (!is_null($item->issueDate))
            {
                $date = new DateTime($item->issueDate);
                $itemArray['issueDate'] = $date->format('d.m.Y');
            } // end if

            if (!is_null($item->dueDate))
            {
                $date = new \Nette\DateTime($item->dueDate);
                $itemArray['dueDate'] = $date->format('d.m.Y');
            } // end if
            // ---

            $form->setDefaults($itemArray);
        } // end if

        $form->onSuccess[] = $this->defaultFormSubmitted;

        return $form;
    }

    /**
     * Metoda zpracuje odeslany formular.
     *
     * @param Form $form
     */
    public function defaultFormSubmitted(Form $form)
    {
        $requestParams = $this->request->getParameters();

        // osetreni formatu datumu
        $values = (array)$form->values;

        if (!empty($values['issueDate']))
        {
            $date = new \Nette\DateTime($values['issueDate']);
            $values['issueDate'] = $date->format('Y-m-d');
            unset($date);
        }
        else
        {
            $values['issueDate'] = null;
        }// end if

        if (!empty($values['dueDate']))
        {
            $date = new \Nette\DateTime($values['dueDate']);
            $values['dueDate'] = $date->format('Y-m-d');
            unset($date);
        }
        else
        {
            $values['dueDate'] = null;
        } // end if
        // ---

        if (empty($values['rel_company']))
            $values['rel_company'] = null;

        if (isset($requestParams['id']) && $requestParams['id'] > 0)
        {
            $this->_mainRepository->update($values, array('id' => $requestParams['id']));
        }
        else
        {
            $values['insertDate'] = date('Y-m-d H:i:s');

            $this->_mainRepository->insert($values);
        } // end if

        return $this->redirect('default');
    }

    public function renderAdd(){
        $this->template->h1 = 'Přidat fakturu';
        $this->template->invoice = $this->_mainRepository;

        $this->setView('form');
    }

    public function renderEdit($id){
        $this->template->h1 = 'Upravit položku';

        $this->setView('form');

        $this->_itemId = intval($id);
    }

}