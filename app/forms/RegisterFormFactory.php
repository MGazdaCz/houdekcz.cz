<?php

namespace App\Forms;

use App\Model\Repository\User;
use Nette,
	Nette\Application\UI\Form;


class RegisterFormFactory extends Nette\Object
{
	/** @var User */
	private $_userRepository;


	public function __construct(User $userRepository)
	{
		$this->_userRepository = $userRepository;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new PForm();
        $form
            ->addHidden('backUrl');
        $form
            ->addText('name', null, 50)
            ->setRequired('Vyplňte jméno.')
            ->setAttribute('placeholder', 'Jméno');
        $form
            ->addText('surname', null, 50)
            ->setRequired('Vyplňte příjmení.')
            ->setAttribute('placeholder', 'Příjmení');
        $form
            ->addText('email', null, 50)
            ->addRule(Form::EMAIL, 'Vyplňte email.')
            ->setAttribute('placeholder', 'E-mail');
        $form
            ->addPassword('password', null)
            ->setRequired('Vyplňte heslo.')
            ->setAttribute('placeholder', 'Heslo');
        $form
            ->addSubmit('submit', 'Registrovat')
            ->setAttribute('class', 'btn size-2 green');

		//$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded($form, $values)
	{

	}

}
