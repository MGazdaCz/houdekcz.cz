<?php

namespace App\Presenters;

use App\Control\Slider\BxSlider\BxSliderControl;
use App\EshopModule\Control\Product\BoxControl;
use Nette,
	App\Model\Homepage\Homepage;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

    /**
     * Metoda pro renderovani homepage.
     */
    public function renderDefault()
	{
        // nacteni detailu homepage
        $page = $this->context->cmsPage->getHomepage()->fetch();

        // osetreni zobrazeni chybovych stranek
        if (!$page) {
            return $this->forward('Error:default', array('exception' => new Nette\Application\BadRequestException('Homepage nenalezena.')));
        }

        // naplneni dat do sablony
        $this->template->homepage  = true;
        $this->template->bodyClass = 'home';

        $this->template->title       = $page->title;
        $this->template->description = $page->description;
        $this->template->keywords    = $page->keywords;

        $this->template->page = $page;

        //$this->template->products = $this->context->getService('eshopProduct')->findAll()->limit(12);
        $this->template->products = $this->context->getService('eshopActionProduct')->findBy(array('eshop_action_id = 1'))->limit(12);

	    //echo '<pre>'.print_r($_SERVER["REQUEST_URI"], true).'</pre>';
	    //echo '<pre>'.print_r($page, true).'</pre>';
        //die;
	}

    public function renderSort() {
        $colArticles = $this->getParameter('colArticles');

        $hpManager = new Homepage();
        $hpManager->sort($this->context, $colArticles);

        $this->sendJson(array('message' => 'OK'));
        return;
    }

    /*********************************************
     *      Registrace komponent
     ********************************************/
    public function createComponentEshopProductBox(){
        return new BoxControl();
    }

    public function createComponentBxSlider(){
        return new BxSliderControl();
    }

}
