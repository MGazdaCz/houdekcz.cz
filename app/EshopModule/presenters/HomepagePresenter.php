<?php

namespace App\EshopModule\Presenters;

use App\EshopModule\Control\Product\BoxControl;
use App\EshopModule\Model\Homepage;
use Nette;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Category
     */
    protected $_categoryRepo;


    protected function startup(){
        parent::startup();

        // init repositories
        $this->_categoryRepo = $this->context->getService('eshopCategory');
    }

    /**
     * Metoda pro renderovani homepage.
     */
    public function renderDefault()
	{
        // nacteni detailu homepage
        $page = $this->context->cmsPage->findBy(array('url' => '/eshop', 'view' => 1, 'delete' => 0))->fetch();

        // osetreni zobrazeni chybovych stranek
        if (!$page) {
            throw new Nette\Application\BadRequestException('Homepage eshopu nenalezena.', 404);
        }

        $this->template->page        = $page;
        $this->template->title       = $page->title;
        $this->template->description = $page->description;
        $this->template->keywords    = $page->keywords;
        $this->template->products    = $this->context->getService('eshopActionProduct')->findBy(array('eshop_action_id = 1'))->limit(12);

        $this->template->bodyClass = 'eshop-list';
	}

    /*********************************************
     *      Registrace komponent
     ********************************************/
    public function createComponentEshopProductBox(){
        return new BoxControl();
    }

}
