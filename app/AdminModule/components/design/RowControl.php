<?php
namespace App\AdminModule\Control\Design;

use \Nette\Application\UI\Control;

class RowControl extends Control
{
    public function renderStart()
    {
        echo '<div class="row">'."\n";
    } // end function
    
    public function renderEnd()
    {
        echo '</div>'."\n";
    } // end function
} // end class