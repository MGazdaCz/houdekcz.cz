<?php
namespace App\AdminModule\Control\Widget\Client;

use \Nette\Application\UI\Control;
use App\AdminModule\Control\Widget\Base\BaseControl;

class ClientControl extends Control
{
    public function render()
    {
        /*
        $template = $this->template;
        $template->setFile(__DIR__ . '/task.latte');
        */

        // vložíme do šablony parametry
        $content = '<p class="alert alert-info">Výčet aktuálních klientů.</p>';
        
        $baseControl = $this->getParent()->getComponent('widgetbase');
        $baseControl->setHead('Správa klientů');
        $baseControl->setContent($content);
        $baseControl->render();;
    } // end function
} // end class