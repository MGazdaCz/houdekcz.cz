<?php

namespace App\EshopModule\Model;

use App\AdminModule\EshopModule\Model\Repository\Delivery;
use App\AdminModule\EshopModule\Model\Repository\DeliveryPayment;
use Nette\Http\Session;
use Nette\Object;

/**
 * Checkout manager.
 */
class Checkout extends Object
{
    const SESSION_SECTION_NAME = 'eshop_checkout';

    protected $_deliveryId;
    protected $_delivery;
    protected $_paymentId;
    protected $_payment;

    /**
     * @var \App\EshopModule\Model\Cart
     */
    protected $_cart;
    /**
     * @var \Nette\Http\Session
     */
    protected $_checkout;

    protected $_deliveryRepository;
    protected $_deliveryPaymentRepository;
    protected $_paymentRepository;


    public function __construct(Session $session, Cart $cart, Delivery $deliveryRepository, DeliveryPayment $deliveryPaymentRepository) {
        $this->_cart     = $cart;
        $this->_checkout = $session->getSection(self::SESSION_SECTION_NAME);

        // nastaveni repositories
        $this->_deliveryRepository        = $deliveryRepository;
        $this->_deliveryPaymentRepository = $deliveryPaymentRepository;

        // inicializace kosiku
        $this->_initCheckout();
    }

    /**
     * Metoda provede inicializaci pokladny.
     *
     * @return $this
     */
    protected function _initCheckout(){
        if (!isset($this->_checkout->deliveryId)) {
            $this->_checkout->deliveryId = null;
        }
        $this->_deliveryId = &$this->_checkout->deliveryId;

        if (!isset($this->_checkout->delivery)) {
            $this->_checkout->delivery = null;
        }
        $this->_delivery = &$this->_checkout->delivery;

        if (!isset($this->_checkout->_paymentId)) {
            $this->_checkout->_paymentId = null;
        }
        $this->_paymentId = &$this->_checkout->paymentId;

        if (!isset($this->_checkout->_payment)) {
            $this->_checkout->_payment = null;
        }
        $this->_payment = &$this->_checkout->payment;

        // testovani - musi byt vybrana doprava
        if (empty($this->_deliveryId)) {
            // nastaveni defaultni dopravy
            $deliveries = $this->getPossibleDeliveries();
            if (!empty($deliveries)) {
                foreach ($deliveries as $delivery) {
                    $this->setDelivery($delivery->id);
                    break;
                }
            }
        }

        // testovani - musi byt vybrana platebni metoda
        if (empty($this->_paymentId)) {
            // nastaveni defaultni platebni metody
            $this->setDefaultPayment();
        }

        // dorucovaci udaje
        if (!isset($this->_checkout->shippingAddress)) {
            $this->_checkout->shippingAddress = array(
                'name'   => null,
                'street' => null,
                'city'   => null,
                'zip'    => null
            );
        }

        // fakturacni udaje
        if (!isset($this->_checkout->billingAddress)) {
            $this->_checkout->billingAddress = array(
                'company' => null,
                'person'  => null,
                'ico'     => null,
                'dic'     => null,
                'street'  => null,
                'city'    => null,
                'zip'     => null
            );
        }

        return $this;
    }

    public function setAddresses($data){
        // TODO - checkout - nastaveni adresy do session
    }

    /**
     * @return Cart
     */
    public function getCart(){
        return $this->_cart;
    }

    /**
     * Metoda vrati dosupne dopravy.
     *
     * @return array
     */
    public function getPossibleDeliveries(){
        $deliveries = $this->_deliveryRepository->findBy(array(
            'active' => 1
        ))->order('_order')->fetchAll();

        return $deliveries;
    }

    /**
     * Metoda nastavi zpusob doruceni.
     *
     * @param int $deliveryId
     * @return $this
     */
    public function setDelivery($deliveryId){
        $this->_deliveryId = intval($deliveryId);
        $this->_paymentId  = null;
        $this->_payment    = null;

        if (!empty($this->_deliveryId)) {
            $row = $this->_deliveryRepository->findByPk($this->_deliveryId);

            if (!is_null($row)) {
                $this->_delivery = $row->toArray();
            }

            // nastaveni defaultni platebni metody
            $this->setDefaultPayment();
        }

        return $this;
    }

    /**
     * Metoda vrati ID nastavene dopravy.
     *
     * @return int
     */
    public function getDeliveryId(){
        return $this->_deliveryId;
    }

    /**
     * Metoda vrati vybrany zpusob dopravy.
     *
     * @return array | null
     */
    public function getDelivery(){
        return $this->_delivery;
    }

    /**
     * Metoda vrati cenu dopravy.
     *
     * @return float
     */
    public function getDeliveryPrice(){
        if (!is_null($this->_delivery)) {
            return floatval($this->_delivery['price']);
        }
        return 0;
    }

    /**
     * Metoda vrati mozne platebni metody pro vybranou dopravu.
     *
     * @return array
     */
    public function getPossiblePayments() {
        $payments = $this->_deliveryPaymentRepository->findBy(array(
            'eshop_delivery_id' => $this->getDeliveryId()
        ))->fetchAll();

        return $payments;
    }

    /**
     * Metoda nastavi ID platebni metody.
     *
     * @param int $paymentId
     * @return $this
     */
    public function setPayment($paymentId){
        $this->_paymentId = intval($paymentId);

        if (!empty($this->_paymentId)) {
            $row = $this->_deliveryPaymentRepository->findByPk($this->_paymentId);

            if (!is_null($row)) {
                $this->_payment = $row->toArray();
            }
        }

        return $this;
    }

    /**
     * Metoda nastavi defaultni platebni metodu.
     *
     */
    public function setDefaultPayment(){
        $payments = $this->getPossiblePayments();
        if (!empty($payments)) {
            foreach ($payments as $payment) {
                $this->setPayment($payment->id);
                break;
            }
        }
    }

    /**
     * Metoda poskytne vybranou platebni metodu.
     *
     * @return array | null
     */
    public function getPayment(){
        return $this->_payment;
    }

    /**
     * Metoda vrati ID vybrane platby.
     *
     * @return int
     */
    public function getPaymentId($possiblePayments = array()){
        // moznost osetreni validace paymentId v zavislosti na mozny platebnich metodach
        if (!empty($possiblePayments)) {
            $paymentsIds = array_keys($possiblePayments);
            if (in_array($this->_paymentId, $paymentsIds)) {
                return $this->_paymentId;
            }
            return $paymentsIds[0];
        }
        // ---

        return $this->_paymentId;
    }

    /**
     * Metoda vrati cenu za platbu.
     *
     * @return float
     */
    public function getPaymentPrice(){

        if (!is_null($this->_payment)) {
            return floatval($this->_payment['price']);
        }
        return 0;
    }

    /**
     * Metoda vrati seznam produktu v kosiku.
     *
     * @return array
     */
    public function getProducts(){
        return $this->_cart->getProducts();
    }

    /**
     * Metoda vrati seznam polozek v kosiku.
     *
     * @return array
     */
    public function getItems(){
        return $this->getProducts();
    }

    /**
     * Metoda vrati pocet polozek v objednavce.
     *
     * @return int
     */
    public function getItemsCount(){
        return $this->_cart->getItemsCount();
    }

    /**
     * Metoda vrati celkovou cenu zbozi v kosiku s DPH.
     *
     * @return float
     */
    public function getPriceWithVat(){
        return $this->_cart->getPriceWithVat();
    }

    /**
     * Metoda vrati celkovou cenu zbozi v kosiku bez DPH.
     *
     * @return float
     */
    public function getPriceWithoutVat(){
        return $this->_cart->getPriceWithoutVat();
    }

    /**
     * Metoda vrati cenu dopravneho tj. doprava + platba.
     *
     * @return float
     */
    public function getShippingPriceWithVat(){
        if (!defined('DELIVERY_FREE_FROM') || (defined('DELIVERY_FREE_FROM') && $this->getPriceWithVat() < DELIVERY_FREE_FROM)) {
            return $this->getPaymentPrice() + $this->getDeliveryPrice();
        }
        return 0.00;
    }

    /**
     * Metoda vrati cenu dopravneho tj. doprava + platba bez DPH.
     *
     * @return float
     */
    public function getShippingPriceWithoutVat(){
        if (!defined('DELIVERY_FREE_FROM') || (defined('DELIVERY_FREE_FROM') && $this->getPriceWithVat() < DELIVERY_FREE_FROM)) {
            return $this->getPaymentPrice() + $this->getDeliveryPrice();
        }
        return 0.00;
    }

    /**
     * Metoda vrati "cenu", za kterou je treba objednat zbozi pro dopravu zdarma.
     *
     * @return float|int
     */
    public function getPriceToDeliveryFree(){
        $toFreeDelivery = DELIVERY_FREE_FROM - $this->getPriceWithVat();

        if ($toFreeDelivery > 0) {
            return $toFreeDelivery;
        }

        return 0.00;
    }

    /**
     * Metoda vrati celkovou cenu k uhrade.
     *
     * @return float
     */
    public function getTotalPrice(){
        return $this->getPriceWithVat() + $this->getShippingPriceWithVat();
    }

    /**
     * Metoda vrati celkovou cenu k uhrade.
     *
     * @return float
     */
    public function getTotalPriceWithVat(){
        return $this->getTotalPrice();
    }

    /**
     * Metoda vrati celkovou cenu k uhrade bez DPH.
     *
     * @return float
     */
    public function getTotalPriceWithoutVat(){
        return $this->getPriceWithoutVat() + $this->getShippingPriceWithoutVat();
    }

}
