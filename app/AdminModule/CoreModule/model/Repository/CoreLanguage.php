<?php
namespace App\AdminModule\CoreModule\Model\Repository;
use Nette;

class CoreLanguage extends Repository
{
    protected $_table = 'core_language';

    /**
     * Metoda nacte a vrati sparovane hodnoty pro select.
     *
     * @author MiG
     * @param string
     * @param string
     * @param bool
     * @return array
     */
    function fetchPairs($columnKey, $columnName, $nullValue = false) {
        return $this->getTable()->where("active = ?", 1)->fetchPairs($columnKey, $columnName);
    } // end function

} // end class