<?php
namespace App\AdminModule\ErpModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;
use Nette;

class TaskRepository extends MainRepository
{

    /**
     * Metoda vrati odpracovany cas v milisekundach pro zadany ukol.
     *
     * @param integer $taskId
     * @return integer
     */
    public function getTimeSpent($taskId)
    {
        return $this->getTable()->where('id = ?', $taskId)->sum('timeLength');
    } // end if

}