<?php

namespace App\AdminModule\EshopModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Category extends MainRepository
{
    protected $_table = 'eshop_category';

    /**
     * Nacte kategorii dle zadane url a pouzije rozsiri vhere o view a delete.
     *
     * @param $url
     * @return \Nette\Database\Table\ActiveRow
     */
    public function findVisibleByUrl($url){
        return $this->findBy(array(
            'url'    => $url,
            'view'   => 1,
            'delete' => 0
        ))->fetch();
    }

    public function findAllParents($categoryId, &$parents){
        $parent = $this->findBy(array(
            'id'     => $categoryId,
            'view'   => 1,
            'delete' => 0
        ))->fetch();

        if ($parent) {
            $parents[] = $parent;

            if ($parent->_parent) {
                return $this->findAllParents($parent->_parent, $parents);
            }
        }
        return $parents;
    }

    /**
     * Metoda nacte ID kategorii dle stromove struktury.
     *
     * @param int $categoryId
     * @return array
     */
    public function getAllChildrenCategories($categoryId){
        $categories = array();
        $this->_getChildrensCategories($categories, $categoryId);

        $categoriesArray = array();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categoriesArray[] = $category->id;
            }
        }

        return $categoriesArray;
    }

    protected function _getChildrensCategories(&$categories, $categoryId, $level = 0){
        $items = $this->findBy(array('_parent' => $categoryId, 'view' => 1, 'delete' => 0));

        if ($items->count()) {
            foreach ($items as $item) {
                $category = new \stdClass();
                $category->id    = $item->id;
                $category->name  = $item->name;
                $category->url   = $item->url;
                $category->level = $level;

                $categories[] = $category;

                $this->_getChildrensCategories($categories, $item->id, $level+1);
            }
        }
    }

    /**
     * Metoda nacte kategorie dle stromove struktury.
     *
     * @param int $categoryId
     * @return array
     */
    public function getCategoriesTreeArray($categoryId){
        $categories = array();
        $this->_getChildrensCategories($categories, $categoryId);
        return $categories;
    }

    /**
     * Metoda nacte kateogrie dle stromove struktury a nastavi odsazeni pro nazvy.
     *
     * @param $categoryId
     * @return array
     */
    public function getCategoriesArrayForMultiSelect($categoryId){
        $categories = array();
        $this->_getChildrensCategories($categories, $categoryId);

        $categoriesArray = array();
        if (!empty($categories)) {
            foreach ($categories as $key => $category) {
                $name = '';

                for ($i=0; $i < $category->level; $i++) {
                    $name .= '--';
                }
                $name .= $category->name;

                $category->name = $name;

                $categoriesArray[$category->id] = $category->name;
            }
        }

        return $categoriesArray;
    }

}