<?php

namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class ArticleColor extends MainRepository
{
    protected $_table = 'cms_article_color';
}