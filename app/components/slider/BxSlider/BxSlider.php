<?php
namespace App\Control\Slider\BxSlider;

use \App\Control\Slider\SliderControlAbstract;
use App\Model\News\News;

class BxSliderControl extends SliderControlAbstract
{
    public function render(array $options)
    {
        parent::_initOptions($options);

        // ziskani mutace do session
        $feLanguage = $this->parent->getSession('feLanguage');

        $lang = 'cz';
        /*
        if (!is_null($feLanguage->id))
            $lang = $feLanguage->id;
        */

        $this->template->language = $lang;
        $this->template->items    = $this->_items;
        $this->template->setFile(__DIR__.'/templates/default.latte');
        $this->template->render();
    }

    public function renderTextSlider(array $options)
    {
        parent::_initOptions($options);

        // ziskani mutace do session
        $feLanguage = $this->parent->getSession('feLanguage');

        $lang = 'cz';
        /*
        if (!is_null($feLanguage->id))
            $lang = $feLanguage->id;
        */

        $this->template->language = $lang;
        $this->template->items    = $this->_items;
        $this->template->setFile(__DIR__.'/templates/textSlider.latte');
        $this->template->render();
    }

    public function renderNewsVerticalSlider(){
        $news = News::getLastNews($this->parent->context, 5);

        $this->template->items = $news;
        $this->template->setFile(__DIR__.'/templates/newsTextSlider.latte');
        $this->template->render();
    }

    public function renderImage(array $options)
    {
        parent::_initOptions($options);

        $this->template->items = $this->_items;
        $this->template->setFile(__DIR__.'/templates/imageSlider.latte');
        $this->template->render();
    }

    public function renderById($sliderId){
        $sliderRepo = $this->getPresenter()->context->getService('cmsSlider');

        $slider = $sliderRepo->findByPk($sliderId);
        $items  = $slider->related('cms_slider_item.cms_slider_id');

        $itemsArray = array();
        if ($items->count()) {
            foreach ($items as $item) {
                $itemForArray = array(
                    'name' => $item->name,
                    'image' => DATA_WWW.'/slider/'.$item->file,
                );

                if (!empty($item->annotation)) {
                    $itemForArray['annotation'] = $item->annotation;
                }

                $itemsArray[] = $itemForArray;
            }
        }

        $this->template->items = $itemsArray;

        $this->template->setFile(__DIR__.'/templates/imageSlider.latte');
        $this->template->render();
    }

}