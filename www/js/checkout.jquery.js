$(document).ready(function(){


    // zobrazeni slevoveho voucheru
    $("body.b-checkout").on("click", "#displayVoucher a", function(){
        $("#displayVoucher").remove();
        $("#voucherForm").removeClass('hidden');

        return false;
    });

    // zobrazeni fakturani adresy
    $("body.b-checkout").on("click", "#zobrazFakturacni a", function(){
        $("#fakturacniUdaje").removeClass("hidden");
        $("#zobrazFakturacni").addClass("hidden");

        $('body.b-checkout input[name="showBillingAddress"]').attr("checked", true);

        return false;
    });

    // skryti fakturacni adresy
    $("body.b-checkout").on("click", "#skryjFakturacni a", function(){
        $("#fakturacniUdaje").addClass("hidden");
        $("#zobrazFakturacni").removeClass("hidden");

        $('body.b-checkout input[name="showBillingAddress"]').removeAttr("checked");

        return false;
    });

    // prebarvovani jednotlivych kroku
    $("body.b-checkout").on("click", "div.sekce", function(){
        $("div.sekce").removeClass('aktivni');
        $(this).addClass('aktivni');
    });

    // zmena doruceni
    $("body.b-checkout").on("change", "ul.doruceni input.radio", function () {
        // odeslani ajax pozadavku

        var url = window.location.href + "?" + "do=setdelivery";
        $.post(url, $("#frm-checkoutForm").serialize(), $.nette.success);

        return false;
    });

    // zmena platby
    $("body.b-checkout").on("change", "ul.platba input.radio", function () {
        // odeslani ajax pozadavku
        var url = window.location.href + "?" + "do=setpayment";
        $.post(url, $("#frm-checkoutForm").serialize(), $.nette.success);

        return false;
    });

});