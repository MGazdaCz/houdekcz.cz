/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ /*'find', 'selection',*/ 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert',    groups: [ 'Image', 'Table', 'Youtube' ] },
        //{ name: 'forms' },
        { name: 'tools' },
        { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
        //{ name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
        { name: 'styles' },
        { name: 'colors' }
        //{ name: 'wr' }
        //{ name: 'about' }
    ];

    config.language = 'cs';
    config.entities_latin = false;

    config.autoParagraph = false;

    config.allowedContent = true;
  
	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'link:advanced';
    
    // povoleni dodatecnych tagu
    config.protectedSource.push(/<i[^>]*><\/i>/g);
    config.protectedSource.push(/<iframe[^>]*><\/iframe>/g);

    //onfig.extraPlugins = 'imagebrowser';
    //config.imageBrowser_listUrl = "/admin/core/filebrowse/";

    // konfigurace file browseru
    config.filebrowserBrowseUrl = basePath + '/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = basePath + '/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = basePath + '/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = basePath + '/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = basePath + '/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = basePath + '/kcfinder/upload.php?opener=ckeditor&type=flash';
    // ...

    //config.extraPlugins = 'wrdixierelated,youtube,sourcedialog';
    config.extraPlugins = 'youtube';
};


// Konfigurace dialogu
CKEDITOR.on( 'dialogDefinition', function( ev )
{
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if ( dialogName == 'image' )
    {
        // odstraneni zalozek
        //dialogDefinition.removeContents( 'Link' );
        dialogDefinition.removeContents( 'Upload' );

        // advanced
        // konfigurace rozsirene zalozky
        var advancedTab = dialogDefinition.getContents( 'advanced' );

        var linkTab = dialogDefinition.getContents( 'Link' );

        // Zmena input na select
        var txtGenClass = advancedTab.get( 'txtGenClass' );
        items = [
            ['nevybráno'],
            ['Zarovnání vlevo', 'floatLeft' ],
            ['Zarovnání na střed', 'imageCenter' ],
            ['Zarovnání vpravo', 'floatRight' ]
            //['Zarovnání vlevo s rámečkem', 'floatLeft border' ],
            //['Zarovnání na střed s rámečkem', 'imageCenter border' ],
            //['Zarovnání vpravo s rámečkem', 'floatRight border' ],
            //['Zarovnání vlevo zakulacené rohy', 'floatLeft rounded-image' ],
            //['Zarovnání na střed zakulacené rohy', 'imageCenter rounded-image' ],
            //['Zarovnání vpravo zakulacené rohy', 'floatRight rounded-image' ],
            //['Zarovnání vlevo kruhový formát (doporučujeme čtvercový obrázek)', 'floatLeft round-image' ],
            //['Zarovnání na střed kruhový formát (doporučujeme čtvercový obrázek)', 'imageCenter round-image' ],
            //['Zarovnání vpravo kruhový formát (doporučujeme čtvercový obrázek)', 'floatRight round-image' ]
        ];
        txtGenClass.items = items;
        txtGenClass.type = "select";

        // odstraneni prvku v zalozce "advanced"
        advancedTab.remove( 'txtGenLongDescr');
        advancedTab.remove( 'cmbLangDir');
        advancedTab.remove( 'txtdlgGenStyle');
        advancedTab.remove( 'txtLangCode');

        // info
        // odstraneni prvku v zalozce info
        var infoTab = dialogDefinition.getContents( 'info' );
        infoTab.remove( 'txtBorder');
        infoTab.remove( 'txtHSpace');
        infoTab.remove( 'txtVSpace');
        infoTab.remove( 'cmbAlign' );

        linkTab.add({
            type: 'text',
            id: 'wrCustomLinkTitle',
            label: 'Titulek odkazu',
            'default': '',
            setup: function (type, element)
            {
                // LINK
                if ( type == 2 )
                {
                    this.setValue(element.getAttribute('title'));
                }
            },
            commit: function (type, element)
            {
                // LINK
                if (type == 2)
                {
                    if (this.getValue() || this.isChanged())
                    {
                        element.setAttribute('title', this.getValue());
                    }
                }
            }
        });

        linkTab.add({
            type: 'select',
            id: 'wrCustomLinkCSS',
            label: 'Třída stylu',
            items: [
                ['Nevybráno', null],
                ['Kotvička', 'no_underline FCK__AnchorC' ],
                ['Externí odkaz', 'external' ],
                ['Nové okno', 'new_window' ],
                ['Externí odkaz v novém okně', 'external_new_window' ],
                ['Lightbox', 'lightbox' ]
            ],
            setup: function (type, element)
            {
                // LINK
                if ( type == 2 )
                {
                    this.setValue(element.getAttribute('class'));
                }
            },
            commit: function (type, element)
            {
                // LINK
                if (type == 2)
                {
                    if (this.getValue() || this.isChanged())
                    {
                        element.setAttribute('class', this.getValue());
                    }
                }
            }
        });

        /*
        linkTab.add({
            type: 'select',
            id: 'wrCustomLinkRel',
            label: 'Fotogalerie (pouze pro \'Lightbox\')',
            items: [
                ['Nevybráno', null],
                ['Galerie 1', 'galerie_1' ],
                ['Galerie 2', 'galerie_2' ],
                ['Galerie 3', 'galerie_3' ],
                ['Galerie 4', 'galerie_4' ],
                ['Galerie 5', 'galerie_5' ]
            ],
            setup: function (type, element)
            {
                // LINK
                if ( type == 2 )
                {
                    this.setValue(element.getAttribute('rel'));
                }
            },
            commit: function (type, element)
            {
                // LINK
                if (type == 2)
                {
                    if (this.getValue() || this.isChanged())
                    {
                        element.setAttribute('rel', this.getValue());
                    }
                }
            }
        });
        */
    }

    if ( dialogName == 'cellProperties' )
    {
        var infoTab = dialogDefinition.getContents( 'info' );

        infoTab.remove( 'wordWrap');
        infoTab.remove( 'hAlign');
        infoTab.remove( 'vAlign');
        infoTab.remove( 'cellType' );
        infoTab.remove( 'bgColor' );
        infoTab.remove( 'bgColorChoose' );
        infoTab.remove( 'borderColor' );
        infoTab.remove( 'borderColorChoose' );
    }

});