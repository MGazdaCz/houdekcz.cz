<?php
namespace App\AdminModule\CoreModule\Model\Module;
use Nette;

/**
 * Prace s konfiguraci modulu.
 */
class Menu extends Nette\Object implements \ArrayAccess, \Iterator
{
    protected $_position;
    protected $_items;
    protected $_user;
    protected $_config;
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param array $config
    * @return void
    */
    function setConfig(array $config) {
        $this->_config = $config;
        return $this;
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param \Nette\Security\User $value
    * @return self
    */
    function setUser(\Nette\Security\User $user) {
        $this->_user = $user;
        return $this;
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function initItems() {
        foreach ($this->_config as $item) {
            if (isset($item['groups']))
            {
                foreach ($item['groups'] as $userGroup) {
                    if ($this->_user->isInRole($userGroup))
                    {
                        $this->_items[] = $item;
                        break 1;
                    } // end if
                } // end foreach
            } // end if
        } // end foreach
        
        return $this;
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function count() {
        return count($this->_items);
    } // end function
    
    
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_items[] = $value;
        } else {
            $this->_items[$offset] = $value;
        }
    }
    public function offsetExists($offset) {
        return isset($this->_items[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->_items[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->_items[$offset]) ? $this->_items[$offset] : null;
    }
    
    function rewind() {
        $this->_position = 0;
    }
    function current() {
        return $this->_items[$this->_position];
    }
    function key() {
        return $this->_position;
    }
    function next() {
        ++$this->_position;
    }
    function valid() {
        return isset($this->_items[$this->_position]);
    }
    
} // end class