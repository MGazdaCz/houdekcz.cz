<?php

namespace App\AdminModule\CoreModule\Presenters;

use App\ErpModule\Model\ProjectManager;
use MGazdaCz\MgSystem\AdminModule\Components\Paginator\PaginatorControl;
use Nette,
    App\Model;


/**
 * Base presenter for backend application presenters.
 */
abstract class BackendPresenter extends BasePresenter
{
    /**
     * Atribut pro evidenci vybraneho projektu.
     * @var int
     */
    protected $_erpProjectId = 2;

    protected $_listH1;
    protected $_addH1;
    protected $_editH1;
    protected $_itemId;
    protected $_projectManager;
    protected $_repository;

    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\Repository
     */
    protected $_mainRepository;
    /**
     * @var \App\AdminModule\CoreModule\Model\Repository\CoreLanguage
     */
    protected $_languageRepository;
    protected $_headMenu;
    protected $_listItems;
    protected $_orderDefault;
    protected $_quickFilter;
    protected $_formTemplate;


    protected function startup(){
        parent::startup();

        // pokus o zapamatovani odku jsem prisel
        // vyuzivam u tlacitka zpet - neni asi uplne dobre ale zatim to staci
        $this->template->urlBack = null;
        if (isset($_SERVER['HTTP_REFERER'])) {
            $this->template->urlBack = $_SERVER['HTTP_REFERER'];
        }

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/default/form.latte';

        // init project manageru - pro praci s vybranym projektem
        $this->_projectManager = $this->context->projectManager;

        $this->_languageRepository = $this->context->getService('coreLanguage');

        // TODO - finalizovat prepinani DB
        $this->template->cmsSectionEnable = false;
        /*
        try {
            $testConnection = $this->_projectManager->getDbConnection();

            if ($testConnection != null) {
                $this->template->cmsSectionEnable = true;
            }
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage());
        }
        */
    }

    /**
     * Metoda predpripravi data pro ulozeni.
     *
     * Prida napr. sloupce s informacemi o tom kdo a kdy zaznam vklada atd.
     *
     * @param array $values
     */
    protected function _prepareValuesForSave(&$values, $removeId = false, $appendInsertData = true){
        $id = $values['id'];

        if ($removeId) {
            unset($values['id']);
        }

        // udaje o vlozeni vkladam pouze pri vkladani noveho zaznamu
        if ($appendInsertData && empty($id)) {
            $values['_insertUser'] = $this->getUser()->getId();
            $values['_insertDate'] = date('Y-m-d H:i:s');
        }
    }

    /** ************************************************************
     *
     * 		Standardizovane akce pro ukladani dat do DB.
     *
     * ************************************************************
     */
    protected function _beforeSave($id, &$values){
        
    }

    protected function _afterSave($id, $values){
        
    }

    public function formSucceeded(Nette\Application\UI\Form $form, $values){
        // MiG - HOTFIX - nastaveni main repository
        $this->_repository = $this->_mainRepository;

        $id = $values['id'];

        // osetreni dat
        $this->_prepareValuesForSave($values, true);

        // moznost ovlivnit data pred ulozenim
        $this->_beforeSave($id, $values);

        if (empty($id)) {
            $result = $this->_repository->insert($values);

            if (isset($result->id)) {
                $id = $result->id;

                // moznost ovlivnit data po ulozeni
                $this->_afterSave($id, $values);

                $this->flashMessage('Záznam byl úspěšně založen.');

                $this->redirect('edit', array('id' => $id));
                return;
            }

            $this->redirect('add');
            return;
        } else {
            $result = $this->_repository->update($values, array(
                'id' => $id
            ));

            // moznost ovlivnit data po ulozeni
            $this->_afterSave($id, $values);

            $this->flashMessage('Záznam byl úspěšně uložen.');
        }

        $this->redirect('edit', array('id' => $id));

        /*
        echo '<pre>'.print_r($this->getParameters(), true).'</pre>';
        echo '<pre>'.print_r($result, true).'</pre>';
        echo '<pre>'.print_r($values, true).'</pre>';
        die;
        */
    }

    /** ************************************************************
     *
     * 		Akce pro renderovani zakladnich operaci.
     *
     * ************************************************************
     */

    protected function _beforeRenderDefault(&$querySelection){

    }

    /**
     * Metoda generuje zakladni vykresleni listu (seznamu polozek).
     *
     * @author MiG
     * @param void
     * @return void
     */
    function renderDefault() {
        $this->setView('default');

        if (empty($this->_orderDefault))
            throw new \Exception('Chyba - zadejte v aktualnim presenteru defaultni order v atributu "_orderDefault".');

        //$this->setView('../../../templates/default/listTable');

        $this->template->h1        = (empty($this->_listH1) ? 'CHYBA - nastavte atribut "_listH1"' : $this->_listH1);
        $this->template->items     = array();
        $this->template->headmenu  = $this->_headMenu;
        $this->template->listItems = $this->_listItems;

        // podpora pro razeni + nacteni dat z repository
        $sortColumn = $this->getParameter('sort', $this->_orderDefault);
        $querySelection = $this->_mainRepository->getTable();
        $this->_beforeRenderDefault($querySelection);
        $items = $querySelection->order($sortColumn);

        // nove pouziti filtru
        //$this->template->activeFilter = $this->getParameter('filter', $this->_quickFilter->getDefaultFilter());
        //$this->_quickFilter->setActualFilter($this->template->activeFilter);
        //$where = $this->_quickFilter->getWhere();

        // mozne rozsireni podminky where pred nactenim dat
        $where = '';
        //$this->_renderDefaultBeforeLoad($where);

        if (!empty($where)) $items->where($where);
        // ---

        $this->template->items = $items;
    }

    public function renderAdd(){
        $this->template->h1                = 'Přidat položku';
        $this->template->formComponentName = 'defaultForm';

        $this->template->setFile($this->_formTemplate);
    }

    public function renderEdit($id){
        $this->template->h1                = 'Upravit položku';
        $this->template->formComponentName = 'defaultForm';

        $this->template->setFile($this->_formTemplate);

        $this->_itemId = $id;
    }

    public function renderDelete($id){
        $result = $this->_mainRepository->delete(array(
            'id' => $id
        ));

        if ($result) {
            $this->flashMessage('Záznam byl odstraněn.');
        } else {
            $this->flashMessage('Záznam se nepovedlo odstranit, zkuste to prosím později.');
        }

        $this->redirect('default');
    }


    /** ************************************************************
     *
     * 		Registrovani komponent.
     *
     * ************************************************************
     */
    protected function createComponentBackButton() {
        return new \App\AdminModule\CoreModule\Component\Buttons\BackButton();
    }

    protected function createComponentListAddButton() {
        return new \App\AdminModule\CoreModule\Component\AddButton();
    }

    protected function createComponentListActionButtons() {
        return new \App\AdminModule\CoreModule\Component\ActionButtons();
    }

    protected function createComponentListYesNo() {
        return new \App\AdminModule\CoreModule\Component\YesNo();
    }

    protected function createComponentListPaginator() {
        return new PaginatorControl();
    }

}
