<?php
namespace App\AdminModule\CoreModule\Model\Forms\Controls;

use \Nette\Utils\Html;
use App\AdminModule\CoreModule\Model\Forms\ImageUpload;

class ImageInput extends \Nette\Forms\Controls\BaseControl
{
    private $_name;
    private $_file;
    
    private $_pathDir;
    private $_pathWww;
    private $_deleteUrl;
    

    public function __construct($name, $label = NULL)
    {
        parent::__construct($label);
        $this->_name = $name;
    }


    public function setValue($value)
    {
        if ($value) {
            $this->_file = $value;
        } else {
            $this->_file = NULL;
        }
    }
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function setPathDir($path) {
        $this->_pathDir = $path;
        return $this;
    } // end function
    
    /**
    * put your comment there...
    * 
    * @author MiG
    * @param void
    * @return void
    */
    function setPathWww($path) {
        $this->_pathWww = $path;
        return $this;
    } // end function
    
    /**
    * Metoda nastavi url pro odkaz na smazani obrazku.
    * 
    * @author MiG
    * @param string $url
    * @return self
    */
    function setDeleteUrl($url) {
        $this->_deleteUrl = $url;
        return $this;
    } // end function

    /**
     * @return DateTime|NULL
     */
     /*
    public function getValue()
    {
        return self::validateDate($this)
            ? date_create()->setDate($this->year, $this->month, $this->day)
            : NULL;
    }
    */
    
    public function loadHttpData()
    {
        $value = $this->getHttpData(\Nette\Forms\Form::DATA_FILE);
        $this->value = new ImageUpload($value);
        
        if ($this->value === NULL) {
            $this->value = new \Nette\Http\FileUpload(null);
        }
    }


    /**
     * Generates control's HTML element.
     */
    public function getControl()
    {
        $name = $this->getHtmlName();
        
        $htmlEl = Html::el();
        
        $htmlEl->add(Html::el('input')->addAttributes(array('type' => 'file', 'class' => 'inline'))->name($name));
        
        if (!empty($this->_pathWww))
        {
            if (file_exists($this->_pathDir.'/'.$this->_file)) {
                $htmlEl->add(Html::el('img')->addAttributes(array(
                    'src'   => $this->_pathWww.'/'.$this->_file
                )));
                
                if (!empty($this->_deleteUrl)) {
                    $htmlEl->add(Html::el('a', array(
                        'href' => $this->_deleteUrl
                    ))->setHtml('smazat'));
                } // end if
            }
            else {
                $htmlEl->add(Html::el('span')->setHtml('Náhled není k dispozici'));
            } // end if
        }
        else
        {
            //$htmlEl->add(Html::el('span')->setHtml('Náhled není k dispozici'));
        } // end if
        
        return $htmlEl;
    }

}