<?php
namespace App\AdminModule\ErpModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;
use Nette;

class ProjectRepository extends MainRepository
{

    public function getTimeSpent($projectId){
        return $this->_dbContext->table('ErpIssue')->where('rel_project = ?',$projectId)->sum(':task.timeLength');
    }

}