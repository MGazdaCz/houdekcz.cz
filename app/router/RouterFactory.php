<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
    $router = new RouteList();

    // routovani API administrace
    $router[] = $apiRouters = new RouteList('Api');
    $apiRouters[] = new Route('admin/api[/<presenter>][/<action>][/<id>]', 'Default:default');

    // routovani administrace
    $router[] = $adminRouters = new RouteList('Admin');
    $adminRouters[] = new Route('admin/<module>[/<presenter>][/<action>][/<id>]', 'Default:default');
    $adminRouters[] = new Route('admin/', 'Core:Homepage:default');
    $adminRouters[] = new Route('admin/core/<presenter>[/<action>][/<id>]', 'Default:default');

    // routovani eshopu
    $router[] = $eshopRouters = new RouteList('Eshop');
    $eshopRouters[] = new Route('eshop', 'Homepage:default');
    $eshopRouters[] = new Route('kosik/<action>', 'Cart:default');
    $eshopRouters[] = new Route('pokladna', 'Checkout:default');
    $eshopRouters[] = new Route('pokladna/debug', 'Checkout:debug');                  // debugovani kosiku
    $eshopRouters[] = new Route('kategorie<url [a-z0-9-\/]+>', 'Category:default');   // debugovani kosiku
    $eshopRouters[] = new Route('produkt<url [a-z0-9-\/]+>', 'Product:default');      // routovani detailu produktu
    $eshopRouters[] = new Route('wr-feed/<presenter>[/<action>][/<id>].xml', 'Export:default'); // esxporty z eshopu pomoci XML

    // routovani webu
    $router[] = new Route('chyba/<action>', 'Error:default');
    $router[] = new Route('search', 'Search:default');
    $router[] = new Route('<url [a-z0-9-\/]+>', 'Page:default');                        // routovani vsech ostatnich stranek
    $router[] = new SimpleRouter('Homepage:default');                                   // routovani homepage

		return $router;
	}

}
