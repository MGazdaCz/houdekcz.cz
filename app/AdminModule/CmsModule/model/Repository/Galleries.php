<?php
namespace App\AdminModule\CmsModule\Model\Repository;

use App\AdminModule\CoreModule\Model\Repository\Repository as MainRepository;

/**
 * @author Martin Hromádko <hromadkom@gmail.com>
 */
class Galleries extends MainRepository {
	
	protected $_table = 'cms_gallery';
        
        public function getGalleryDirectory($galleryId){
            return $this->getDbContext()->table('cms_gallery_directory')->where(array('cms_gallery_id'=>$galleryId))->fetch();
        }
        
        public function addGalleryDirectory($galleryId, $directoryId){
            $sql = "INSERT INTO cms_gallery_directory (cms_gallery_id,cms_directory_id) VALUES (?,?)";
            return $this->getDbContext()->query($sql, $galleryId, $directoryId);
        }
	
}
