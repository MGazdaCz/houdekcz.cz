<?php

namespace App\AdminModule\CoreModule\Presenters;

use App\AdminModule\CmsModule\Model\Permission;
use App\AdminModule\Control\Design\GridControl;
use App\AdminModule\Control\Design\RowControl;
use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    protected function startup(){
        parent::startup();

        $authentication = true;
        if (isset($this->context->parameters['authentication'])) {
            $authentication = $this->context->parameters['authentication'];
        }

        // TODO - moznost predani stavu autentifikace
        $this->template->authentication = $authentication;

        if ($authentication && !$this->user->isLoggedIn())
        {
            $this->redirect(':Admin:Core:Sign:in');
        } // end if

        $this->template->cmsName = $this->context->parameters['web']['cmsName'];
        $this->template->sidebar = $this->context->parameters['sidebar'];
        $this->template->dataWww = DATA_WWW;

        // inicializace opravneni
        $this->template->cmsPermission = new Permission();
    }

    /**
     * Metoda vrati cisty nazev presenteru.
     *
     * @return string
     */
    public function getPureName()
    {
        $pos = strrpos($this->name, ':');
        if (is_int($pos)) {
            return substr($this->name, $pos + 1);
        }

        return $this->name;
    }

    /** ************************************************************
     *
     * 		Registrovani komponent.
     *
     * ************************************************************
     */
    protected function createComponentGrid()
    {
        $component = new GridControl();

        return $component;
    }
    protected function createComponentRow()
    {
        $component = new RowControl();

        return $component;
    }
}
