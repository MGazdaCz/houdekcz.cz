<?php
namespace App\AdminModule\CmsModule\Model;
use Nette\Database\Table\Selection;

/**
 * Created by PhpStorm.
 * User: Milan
 * Date: 28.8.15
 * Time: 15:16
 */
class Permission extends \Nette\Security\Permission {

    public function __construct(){
        // inicializace roli
        $this->addRole('guest');
        $this->addRole('employee', 'guest');
        $this->addRole('supervisor', 'employee');
        $this->addRole('administrator', 'supervisor');

        // definice zdroju (modulu)
        $this->addResource('action');
        $this->addResource('article');
        $this->addResource('articlecolor', 'article');
        $this->addResource('articlelabel', 'article');
        $this->addResource('banner');
        $this->addResource('bannertype', 'banner');
        $this->addResource('menu');
        $this->addResource('menuitem', 'menu');
        $this->addResource('newsletter');
        $this->addResource('page');
        $this->addResource('photogallery');
        $this->addResource('photogalleryitem', 'photogallery');
        $this->addResource('podcast');
        $this->addResource('podcastcategory', 'podcast');
        $this->addResource('slider');
        $this->addResource('slideritem', 'slider');
        $this->addResource('text');

        // definice zdroju eshopu
        $this->addResource('eshop');
        $this->addResource('eshopproduct', 'eshop');

        // TODO - zdroje z core
        $this->addResource('settings');

        // pravidla pro praci
        // zamestnanec muze jen pracovat s clanky
        $this->allow('employee', 'article', array('list', 'add', 'edit'));
        $this->allow('employee', 'photogallery', array('list', 'add', 'edit'));

        // spravce muze nasledujici
        $this->allow('supervisor', 'action', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'article', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'articlecolor', array('list'));
        $this->allow('supervisor', 'articlelabel', array('list'));
        $this->allow('supervisor', 'banner', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'bannertype', array('list'));
        $this->allow('supervisor', 'page', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'podcast', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'podcastcategory', array('list', 'edit'));
        $this->allow('supervisor', 'text', array('list', 'edit'));
        $this->allow('supervisor', 'eshop', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'newsletter', array('list', 'add', 'edit', 'delete'));
        $this->allow('supervisor', 'slider', array('list', 'add', 'edit', 'delete'));

        // admin muze vse
        $this->allow('administrator', \Nette\Security\Permission::ALL, array('list', 'add', 'edit', 'delete'));
    }

}