SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+02:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

ALTER TABLE `eshop_product` ADD `pricePerItem` decimal(10,2) NOT NULL DEFAULT '0.00' AFTER `price`, COMMENT='';

