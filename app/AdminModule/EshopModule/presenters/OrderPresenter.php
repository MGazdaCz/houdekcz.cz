<?php

namespace App\AdminModule\EshopModule\Presenters;

use App\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Order presenter.
 */
class OrderPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Order
     */
    protected $_repository;
    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\OrderStatus
     */
    protected $_orderStatusRepo;

    /**
     * @var array
     */
    protected $_statusArray;

    protected function startup(){
        parent::startup();

        $this->_repository      = $this->context->getService('eshopOrder');
        $this->_orderStatusRepo = $this->context->getService('eshopOrderStatus');

        $this->_statusArray = $this->_orderStatusRepo->fetchPairs('id', 'name');

        $this->template->bodyClass   = 'module-order';
        $this->template->statusArray = $this->_statusArray;

        // TODO - bude chtit asi nejaky zakladni presenter pro eshop!
        // predani konfigurace modulu
        $this->template->eshopConfig = $this->context->getParameters()['eshop'];
    }

    public function renderDefault()
    {
        $filter = $this->getParameter('filter');

        $this->template->action = 'default';

        $whereArray = $this->_getFilterWhereArray();

        if (!empty($whereArray)) {
            $this->template->items  = $this->_repository->findBy($whereArray)->order('id DESC');
        } else {
            $this->template->items = $this->_repository->findAll()->order('id DESC');
        }

        $this->template->h1 = 'Objednávky';
    }

    public function renderProducts()
    {
        $filter = $this->getParameter('filter');

        $this->template->action = 'products';

        $whereArray = $this->_getFilterWhereArray();

        if (!empty($whereArray)) {
            $this->template->items  = $this->_repository->findBy($whereArray)->order('id DESC');
        } else {
            $this->template->items = $this->_repository->findAll()->order('id DESC');
        }

        $this->template->h1 = 'Výpis objednávek s produkty';
    }

    protected function _getFilterWhereArray(){
        $whereArray = array();

        $filter = $this->getParameter('filter');

        if ($filter == 1) {
            $this->template->filter = 'all';

            // filtr pomoci filtracniho formulare
            if ($this->getParameter('number')) {
                $whereArray["id LIKE ?"] = '%'.$this->getParameter('number').'%';
            }

            if ($this->getParameter('dateFrom') != null) {
                $date = Nette\Utils\DateTime::from($this->getParameter('dateFrom'));
                $whereArray["_insertDate >= ?"] = $date->format('Y-m-d');
            }

            if ($this->getParameter('dateTo')) {
                $date = Nette\Utils\DateTime::from($this->getParameter('dateTo'));
                $whereArray["_insertDate <= ?"] = $date->format('Y-m-d 23:59:59');
            }

            if ($this->getParameter('status')) {
                $whereArray["eshop_order_status_id = ?"] = $this->getParameter('status');
            }
        } else {
            // filtrovani dle rychlych filtru
            if (is_null($filter) || $filter == 'today') {
                $whereArray[] = 'DATE(_insertDate) = CURDATE()';

                $this->template->filter = 'today';
            } elseif ($filter == 'yesterday') {
                $whereArray[] = 'DATE(_insertDate) = (CURDATE() - INTERVAL 1 DAY)';

                $this->template->filter = 'yesterday';
            } else {
                // all
                $this->template->filter = 'all';
            }
        }

        return $whereArray;
    }

    public function renderAdd()
    {
        $this->redirect('default');
    }

    protected function createComponentFilterForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->setMethod(Nette\Application\UI\Form::GET);

        $form->addHidden('filter', 1);
        $form
            ->addText('number', 'Číslo obj.', 10)
            ->setAttribute('placeholder', 'Č. objednávky');
        $form
            ->addText('dateFrom', 'Datum od', 10)
            ->setAttribute('placeholder', 'Datum od')
            ->setAttribute('class', 'date-picker');
        $form
            ->addText('dateTo', 'Datum od', 10)
            ->setAttribute('placeholder', 'datum do')
            ->setAttribute('class', 'date-picker');
        $form
            ->addSelect('status', 'Stav', $this->_orderStatusRepo->fetchPairs('id', 'name', true));
        $form->addSubmit('submit', 'Filtruj');

        //$form->onSuccess[] = array($this, 'filterFormSucceeded');

        return $form;
    }

    public function renderDetail($id) {
        $this->template->h1 = 'Detail objednávky: '.$id;

        if (!empty($id)) {
            $order = $this->_repository->findBy(array('id' => $id));

            if (!is_null($order) && $order->count() > 0) {
                $order = $order->fetch();

                $this->template->order   = $order;
                $this->template->content = $order->content;
            } else {
                $this->flashMessage('Objednávka s číslem <strong>'.$id.'</strong> nenalezena.');
                $this->redirect('default');
            }
        } else {
            $this->flashMessage('Nebylo zadáno číslo objednávky.');
            $this->redirect('default');
        }
    }

    public function renderExportgeis()
    {
        $whereArray = array(
            'eshop_delivery_id IN ?' => array(3, 6)
        );

        $this->_exportForCsv('geis', $whereArray);
    }

    public function renderExportpost()
    {
        $whereArray = array(
            'eshop_delivery_id IN ?' => array(1, 5)
        );

        $this->_exportForCsv('posta', $whereArray);
    }

    /**
     * Exportovani vybranych dat do CSV souboru.
     *
     * @param $name
     * @param array $whereArray
     */
    protected function _exportForCsv($name, array $whereArray){
        header('Content-Type: application/csv, utf-8');
        header('Content-Disposition: attachment;filename="'.$name.'.csv"');
        header('Cache-Control: max-age=0');

        $whereArray = $this->_getFilterWhereArray();

        // exportuji jen objednavky ve stavu "exportováno"
        $whereArray['eshop_order_status_id = ?'] = 3;

        $this->template->items = $this->_repository->findBy($whereArray)->order('id');
    }

    public function renderShipped() {
        $this->setView('null');

        $id      = $this->getParameter('id');
        $shipped = $this->getParameter('shipped');

        if ($id) {
            $result = $this->_repository->update(array('shipped' => $shipped), array('id' => $id));
        }
    }

    public function handleChangestatus() {
        $id       = $this->getParameter('orderId');
        $statusId = $this->getParameter('eshop_order_status_id');

        if ($id && $statusId) {
            $result = $this->_repository->update(array('eshop_order_status_id' => $statusId), array('id' => $id));

            if ($result) {
                $orderDetail = $this->_repository->findByPk($id);

                // odeslani info mailu
                $this->_sendEmail($orderDetail);

                $this->flashMessage('Stav objednávky změněn.', 'success');
            }
        } else {
            $this->flashMessage('Něco se u nás pokazilo. Zkuste prosím stav objednávky uložit později.', 'danger');
        }

        $this->redrawControl('flashMessages');
    }

    /**
     * Odeslani informacnich emailu o zmene stavu objednavky.
     *
     * @param Nette\Database\Table\ActiveRow $order
     */
    protected function _sendEmail(Nette\Database\Table\ActiveRow $order){
        $templateName = null;

        // potvrzená objednávka
        if ($order->eshop_order_status_id == 2) {
            $templateName = 'confirmed';
        }

        // expedováno / uzavřená objednávka
        if ($order->eshop_order_status_id == 3) {
            $templateName = 'shipped';
        }

        if (!is_null($templateName)) {
            // nastaveni sablony emailu
            $template = $this->createTemplate()->setFile(__DIR__.'/../templates/Order/email/'.$templateName.'.latte');
            $template->order = $order;

            // ziskani parametru shopu
            $eshopParams = $this->context->getParameters()['eshop'];

            if (isset($eshopParams[$templateName])) {
                $emailParams = $eshopParams[$templateName];

                // odeslani emailu
                $mail = new \Nette\Mail\Message();
                $mail
                    ->setFrom($emailParams['from'])
                    ->addTo($order->email)
                    ->setSubject($emailParams['subject'].$order->id)
                    ->setHtmlBody($template);

                if (!empty($eshopParams['bcc'])) {
                    foreach ($eshopParams['bcc'] as $email) {
                        $mail->addBcc($email);
                    }
                }

                $mailer = new \Nette\Mail\SendmailMailer();
                $mailer->send($mail);
            }

        }
    }

    public function renderPdf($id) {
        $orderDetail = $this->_repository->findByPk($id);

        require_once VENDOR_DIR.'/mpdf60/mpdf.php';

        $mpdf = new \mPDF();
        $mpdf->WriteHTML($orderDetail->content);

        $mpdf->Output();
        exit;
    }

}
