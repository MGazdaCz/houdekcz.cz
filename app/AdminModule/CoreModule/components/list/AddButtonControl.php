<?php
namespace App\AdminModule\CoreModule\Component;

use \Nette\Application\UI\Control;

class AddButton extends Control
{
    public function render($name = null, $title = null, $url = null)
    {
        $this->template->url   = $url;
        $this->template->name  = $name;
        $this->template->title = $title;

        if (is_null($url)) {
            $this->template->url = $this->presenter->link('add');
        } else {
            $this->template->url = $this->presenter->link($url);
        }

        $this->template->setFile(__DIR__.'/templates/addButton.latte');
        $this->template->presenter = $this->getParent();
        $this->template->render();
    }

}