-- mapuju produkty na kategorie
SELECT c.id, c.name, p.id, p.name
FROM eshop_category c, eshop_product p
WHERE c.externKey LIKE CONCAT('%', REPLACE(p.externCategory, '-', '%'));

-- ulozeni vazeb mezi kategoriemi a produkty
INSERT INTO eshop_category_product (eshop_category_id, eshop_product_id)
  SELECT c.id, p.id
    FROM eshop_category c, eshop_product p
    WHERE c.externKey LIKE CONCAT('%', REPLACE(p.externCategory, '-', '%'));