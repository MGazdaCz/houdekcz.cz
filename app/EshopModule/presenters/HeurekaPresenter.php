<?php

namespace App\EshopModule\Presenters;

use App\EshopModule\Model\Heureka\Product;
use Nette,
    App\Model;


/**
 * Heureka presenter.
 */
class HeurekaPresenter extends BasePresenter
{

    /**
     * Renderovani zakladniho XML feedu.
     */
    public function renderDefault(){
        $productRepo = $this->context->getService('eshopProduct');

        $productsRows = $productRepo->findBy(array('view' => 1));

        $productsArray = array();
        foreach ($productsRows as $row) {
            $productsArray[] = Product::getFromActiveRow($row);
        }

        header("Content-type: text/xml");
        $this->template->wwwRoot      = WWW_ROOT;
        $this->template->deliveryDate = 2;
        $this->template->products     = $productsArray;
    }

}
