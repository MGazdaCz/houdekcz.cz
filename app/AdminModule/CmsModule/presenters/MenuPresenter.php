<?php

namespace App\AdminModule\CmsModule\Presenters;

use App\AdminModule\CmsModule\Model\Form\PageBase;
use Nette,
    App\Model;


/**
 * Menu presenter.
 */
class MenuPresenter extends \App\AdminModule\CoreModule\Presenters\BackendPresenter
{
    /**
     * @var \App\AdminModule\CmsModule\Model\Repository\MenuItem
     */
    protected $_repositoryItems;

    protected function startup(){
        parent::startup();

        $this->_mainRepository  = $this->context->getService('cmsMenu');
        $this->_repositoryItems = $this->context->getService('cmsMenuItem');

        $this->template->bodyClass = 'module-menu';

        $this->_listH1 = 'Navigační menu';
        $this->_orderDefault = 'name';

        // nastaveni defaultni sablony formulare
        $this->_formTemplate = __DIR__.'/../templates/Menu/form.latte';
    }

    /*
    public function renderDefault()
    {
        $this->template->items = $this->_repository->findAll()->order('name');
    }
    */

    public function renderAdd()
    {
        $this->setView('form');

        $this->template->h1 = 'Nové menu';
    }

    public function renderEdit($id){
        parent::renderEdit($id);

        $this->template->h1    = 'Editace menu: '.$this->_mainRepository->findByPk($id)->name;
        $this->template->id    = $id;
        $this->template->items = $this->_repositoryItems->findBy(array('cms_menu_id' => $id))->order('id');
    }

    protected function createComponentForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addHidden('id');
        $form
            ->addText('name', '', 50)
            ->setAttribute('placeholder', 'Název menu')
            ->setRequired('Zadejte název menu.');
        $form
            ->addTextArea('description', '', 50, 5)
            ->setAttribute('placeholder', 'Popisek');
        $form->addSubmit('submit', PageBase::SUBMIT_DEFAULT_LABEL);
        $form->onSuccess[] = array($this, 'formSucceeded');

        $id = $this->getParameter('id');
        if (!is_null($id)) {
            $item = $this->_mainRepository->findByPk($id);

            if (!is_null($item)) {
                $form->setDefaults($item->toArray());
            } else {
                $this->flashMessage('Chyba při načítání záznamu. Zkuste to prosím později.');
                $this->redirect('default');
            }
        }

        return $form;
    }

    /*
    public function formSucceeded(Nette\Application\UI\Form $form, $values){
        $id = $values['id'];

        unset($values['id']);

        if (empty($id)) {
            $result = $this->_mainRepository->insert($values);

            if (isset($result->id)) {
                $id = $result->id;

                $this->flashMessage('Záznam byl úspěšně založen.');

                $this->redirect('edit', array('id' => $id));
                return;
            }

            $this->redirect('add');
            return;
        } else {
            $result = $this->_mainRepository->update($values, array(
                'id' => $id
            ));

            $this->flashMessage('Záznam byl úspěšně uložen.');
        }

        $this->redirect('edit', array('id' => $id));
    }
    */

}