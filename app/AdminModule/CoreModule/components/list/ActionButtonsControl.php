<?php
namespace App\AdminModule\CoreModule\Component;

use \Nette\Application\UI\Control;

class ActionButtons extends Control
{
    public function render($pk)
    {
        $this->template->setFile(__DIR__.'/templates/actionButtons.latte');
        $this->template->pk        = $pk;
        $this->template->presenter = $this->getParent();
        $this->template->render();
    }

} // end class