<?php
namespace App\EshopModule\Control\Cart;

use App\EshopModule\Model\Product;
use App\Model\Helper\PriceHelper;
use Nette\Application\UI\Control;

class MiniControl extends Control
{
    /**
     * @var \App\EshopModule\Model\Cart
     */
    protected $_cart;

    /**
     * @var \App\AdminModule\EshopModule\Model\Repository\Product
     */
    protected $_productRepository;

    public function render()
    {
        //$this->_productRepository = $this->getParent()->context->getService('eshopProduct');

        $this->_cart          = $this->getParent()->context->getService('cart');
        $this->template->cart = $this->_cart;
        //$this->template->product = null;

        // pokud je kosik prazdny, nactu info o produktu pro zobrazeni ceny
        if ($this->_cart->isEmpty()) {
            /*
            $productActiveRow        = $this->_productRepository->findByPk(1);
            $product                 = Product::getFromActiveRow($productActiveRow);
            $this->template->product = $product;
            */
        }

        $this->template->registerHelper('wrPrice', function($s){
            return PriceHelper::format($s);
        });

        $this->template->setFile(__DIR__.'/mini.latte');
        $this->template->render();
    }

}